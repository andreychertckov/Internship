# Train Alogrithm
1. Collect a dataset
    - Intercept system function calls throw them to the log(C/C++ part)
    - Build a sequenses of 32 elements. Each element is a system function(Java part)
2.  Analysing and processing dataset
    - Bulid ngram tf-idf
    ```python
        for pair_words in all_words: # all_words represnt a corpus with 225 pair of system functions
            temp = 0
            for sentence in dataset:
                for ind in range(len(sentence)):
                    temp += (dataset[sentence][ind],dataset[sentence][ind +1]) == pair_words ? 1 : 0
            tf_idf[pair_words] = temp/len(all_words) # len(all_words) = 15*15 = 225
        save_tf_idf(".csv") # save to file in csv format
    ```
    - Build vectors with demensiong equal 225
    ```python
        for sentence in dataset:
            vector = [0] * 225
            for word_index in range(len(sentence)-1):
                pair_words = (sentence[word_index],sentence[word_index+1])
                vector_index = indexes[pair_words]
                vector[vector_index] += tf_idf[pair_words]
            procesed_dataset.append(vector)
    ```
3. Build LSTM neural network
    - Architecture of NN
        - Dense input layer wich consist of 225 neurons
        - LSTM layes wich consist of 113 neurons
        - output Dense relu layer - 1 neuron  
4. Train built LSTM neural network on processed dataset and save weight to file.

# Main Application algorithm
1. Load and build Neural network and load tf-idf weights.
2. Intercept system functions, throw to logs
3. In appliacation catch information from logs
4. Build vector using tf_idf weights
5. Put vector into Neural network prediction function 
6. Based on result of prediction alarm user.
