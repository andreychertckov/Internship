.class public final Lo/con;
.super Landroid/app/DialogFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private ᕽ:Landroid/widget/EditText;

.field private ᘁ:Landroid/widget/CheckBox;

.field public ᵄ:Landroid/widget/ProgressBar;

.field public ᵞ:Landroid/widget/Button;

.field private ᵧ:Landroid/widget/ImageView;

.field private וּ:Landroid/widget/TextView;

.field private וֹ:Landroid/widget/TextView;

.field private ﹲ:Landroid/widget/TextView;

.field private ﹷ:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 164
    .line 5111
    :sswitch_0
    move-object p1, p0

    iget-object v0, p0, Lo/con;->ᕽ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lo/con;->ᘁ:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5113
    iget-object v0, p1, Lo/con;->ᕽ:Landroid/widget/EditText;

    invoke-virtual {p1}, Lo/con;->getActivity()Landroid/app/Activity;

    .line 6016
    const-string v1, "Invalid value"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 5114
    return-void

    .line 5118
    :cond_0
    sget-object v0, Lcosmetiq/fl/controllers/activities/WebMainService;->ﯨ:Lo/ʿ;

    iget-object v1, p1, Lo/con;->ᕽ:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6183
    iput-object v1, v0, Lo/ʿ;->ᔅ:Ljava/lang/String;

    .line 5119
    sget-object v0, Lcosmetiq/fl/controllers/activities/WebMainService;->ﯨ:Lo/ʿ;

    iget-object v1, p1, Lo/con;->ᘁ:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 6191
    iput-boolean v1, v0, Lo/ʿ;->ᔉ:Z

    .line 5121
    invoke-virtual {p1}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lo/ᐧ;->ˊ(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5123
    new-instance v3, Lo/ᐤ;

    invoke-virtual {p1}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v3, v0}, Lo/ᐤ;-><init>(Landroid/app/Activity;)V

    .line 5124
    iget-object v0, p1, Lo/con;->ᵄ:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 5125
    iget-object v0, p1, Lo/con;->ᵞ:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 5127
    invoke-virtual {p1}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lo/If;

    invoke-direct {v1, p1}, Lo/If;-><init>(Lo/con;)V

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2, v1}, Lo/ᐤ;->ˊ(Landroid/content/Context;ILo/ᵌ;)V

    .line 5152
    move-object p1, v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    move-object v3, v0

    .line 7012
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 7014
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v0, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    .line 7016
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 5153
    return-void

    .line 5154
    :cond_2
    invoke-virtual {p1}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "Network connection lost"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 167
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0007 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .line 44
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 49
    return-object p1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .line 55
    const v0, 0x7f030006

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 56
    const v0, 0x7f0a001c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lo/con;->ᕽ:Landroid/widget/EditText;

    .line 57
    const v0, 0x7f0a001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lo/con;->ᘁ:Landroid/widget/CheckBox;

    .line 58
    const v0, 0x7f0a0008

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lo/con;->ᵄ:Landroid/widget/ProgressBar;

    .line 60
    const v0, 0x7f0a001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lo/con;->וּ:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0a001b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lo/con;->וֹ:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0a001d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lo/con;->ﹲ:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0a0006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lo/con;->ﹷ:Landroid/widget/TextView;

    .line 66
    sget-object v0, Lo/ˉ;->І:Ljava/lang/String;

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lo/con;->ﹷ:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    :cond_0
    const v0, 0x7f0a0019

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lo/con;->ᵧ:Landroid/widget/ImageView;

    .line 71
    sget-object p2, Lcosmetiq/fl/controllers/activities/WebMainService;->ﯨ:Lo/ʿ;

    .line 72
    if-eqz p2, :cond_4

    .line 73
    .line 1099
    iget-object v0, p2, Lo/ʿ;->ʵ:Ljava/lang/String;

    .line 73
    if-eqz v0, :cond_4

    .line 2099
    iget-object v0, p2, Lo/ʿ;->ʵ:Ljava/lang/String;

    .line 73
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 75
    const/4 p3, 0x0

    .line 76
    const-string v4, ""

    .line 78
    .line 3099
    iget-object v0, p2, Lo/ʿ;->ʵ:Ljava/lang/String;

    .line 78
    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    const p3, 0x7f02000e

    .line 81
    const-string v4, "MasterCard"

    goto :goto_0

    .line 84
    .line 4099
    :cond_1
    iget-object v0, p2, Lo/ʿ;->ʵ:Ljava/lang/String;

    .line 84
    const-string v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    const p3, 0x7f020010

    .line 86
    const-string v4, "Visa"

    goto :goto_0

    .line 88
    .line 5099
    :cond_2
    iget-object v0, p2, Lo/ʿ;->ʵ:Ljava/lang/String;

    .line 88
    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    const-string v4, "American Express"

    .line 92
    :cond_3
    :goto_0
    iget-object v0, p0, Lo/con;->וּ:Landroid/widget/TextView;

    invoke-virtual {p0}, Lo/con;->getResources()Landroid/content/res/Resources;

    const-string v1, "For security purposes please provide us your full %s Securecode"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lo/con;->וֹ:Landroid/widget/TextView;

    invoke-virtual {p0}, Lo/con;->getResources()Landroid/content/res/Resources;

    const-string v1, "%s Securecode:"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lo/con;->ﹲ:Landroid/widget/TextView;

    invoke-virtual {p0}, Lo/con;->getResources()Landroid/content/res/Resources;

    const-string v1, "If you are usually receive your %s Securecode code through SMS please check"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lo/con;->ᵧ:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 100
    :cond_4
    const v0, 0x7f0a0007

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lo/con;->ᵞ:Landroid/widget/Button;

    .line 101
    iget-object v0, p0, Lo/con;->ᵞ:Landroid/widget/Button;

    invoke-virtual {p0}, Lo/con;->getResources()Landroid/content/res/Resources;

    const-string v1, "SUBMIT"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lo/con;->ᵞ:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lo/con;->setCancelable(Z)V

    .line 106
    return-object p1
.end method
