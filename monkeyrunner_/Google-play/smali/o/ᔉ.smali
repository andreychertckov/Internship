.class public Lo/ᔉ;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field protected ڌ:Lo/ﹾ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .line 22
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ʹ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pdus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object p2, v0

    check-cast p2, [Ljava/lang/Object;

    .line 25
    new-instance v7, Ljava/util/ArrayList;

    array-length v0, p2

    new-array v0, v0, [Landroid/telephony/SmsMessage;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 27
    const/4 v8, 0x0

    :goto_0
    array-length v0, p2

    if-ge v8, v0, :cond_0

    .line 29
    aget-object v0, p2, v8

    check-cast v0, [B

    invoke-static {v0}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ՙ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 35
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ٴ()J

    move-result-wide v10

    .line 36
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->י()J

    move-result-wide v0

    sub-long v0, v8, v0

    cmp-long v0, v0, v10

    if-lez v0, :cond_1

    .line 38
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ﾞ()V

    .line 39
    iget-object v0, p0, Lo/ᔉ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ﹳ()V

    .line 43
    :cond_1
    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :goto_1
    const-string v0, "o.AuX"

    :try_start_0
    invoke-static {v0}, Lo/AuX$ˑ;->ˊ(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 44
    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :goto_2
    const/4 v0, 0x2

    :try_start_1
    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x1

    aput-object v7, v2, v0

    const/4 v0, 0x0

    aput-object p1, v2, v0

    const-string v0, "o.AuX"

    invoke-static {v0}, Lo/AuX$ˑ;->ˊ(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v3, "\u02ca"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Landroid/content/Context;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const-class v5, Ljava/util/List;

    const/4 v6, 0x1

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ᔋ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 45
    invoke-interface {v0, p0, p1, v7}, Lo/ᔋ;->ˊ(Lo/ᔉ;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 47
    :cond_2
    return-void
.end method
