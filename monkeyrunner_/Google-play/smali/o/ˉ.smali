.class public Lo/ˉ;
.super Landroid/app/Activity;
.source ""


# static fields
.field public static І:Ljava/lang/String;

.field public static Ӏ:Ljava/lang/String;


# instance fields
.field private і:Z

.field private ї:Lo/ᵛ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lo/ˉ;->і:Z

    return-void
.end method


# virtual methods
.method public getResources()Landroid/content/res/Resources;
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lif;->ˊ(Landroid/content/res/Resources;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .line 46
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    const-string v0, "android.intent.category.HOME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 49
    invoke-virtual {p0, v1}, Lo/ˉ;->startActivity(Landroid/content/Intent;)V

    .line 51
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .line 55
    const/4 v0, 0x1

    sput-boolean v0, Lo/ι;->Ȋ:Z

    .line 56
    invoke-virtual {p0}, Lo/ˉ;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᒡ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lo/ˉ;->І:Ljava/lang/String;

    .line 59
    .line 1222
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ǃ:Ljava/lang/String;

    sget-object v2, Lo/ͺ;->ː:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    sput-object v0, Lo/ˉ;->Ӏ:Ljava/lang/String;

    .line 63
    sget-object v0, Lo/ι;->ǐ:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    sget-object v0, Lo/ι;->ǐ:Ljava/util/HashMap;

    sget-object v1, Lo/ˉ;->І:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    sget-object v0, Lo/ι;->ǐ:Ljava/util/HashMap;

    sget-object v1, Lo/ˉ;->І:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lo/ٴ;

    .line 66
    .line 2020
    iget-boolean v0, v3, Lo/ٴ;->Ϊ:Z

    .line 66
    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lo/ˉ;->і:Z

    .line 67
    sget-object v3, Lo/ˉ;->І:Ljava/lang/String;

    .line 2105
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2106
    const-string v0, "com.skype.raider"

    const-string v1, "#E4EEF2"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2107
    const-string v0, "com.facebook.orca"

    const-string v1, "#627aad"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2108
    const-string v0, "com.facebook.katana"

    const-string v1, "#627aad"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2109
    const-string v0, "com.instagram.android"

    const-string v1, "#ECF0F1"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2110
    const-string v0, "com.viber.voip"

    const-string v1, "#59267c"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2111
    const-string v0, "com.whatsapp"

    const-string v1, "#455a64"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2113
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 68
    goto :goto_1

    .line 71
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/ˉ;->і:Z

    .line 75
    :goto_1
    iget-boolean v0, p0, Lo/ˉ;->і:Z

    if-eqz v0, :cond_2

    const v0, 0x7f070008

    goto :goto_2

    :cond_2
    const v0, 0x7f070009

    :goto_2
    invoke-virtual {p0, v0}, Lo/ˉ;->setTheme(I)V

    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lo/ˉ;->setContentView(I)V

    .line 78
    const v0, 0x7f0a0009

    invoke-virtual {p0, v0}, Lo/ˉ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lo/ᵛ;

    iput-object v0, p0, Lo/ˉ;->ї:Lo/ᵛ;

    .line 86
    new-instance v0, Lo/cOn;

    invoke-direct {v0}, Lo/cOn;-><init>()V

    .line 87
    invoke-virtual {p0}, Lo/ˉ;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "first"

    invoke-virtual {v0, v1, v2}, Lo/cOn;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 94
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 96
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x1

    return v0

    .line 100
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .line 119
    invoke-virtual {p0}, Lo/ˉ;->finish()V

    .line 121
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 123
    return-void
.end method
