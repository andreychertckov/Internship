.class public abstract Lo/เ;
.super Landroid/os/AsyncTask;
.source ""


# instance fields
.field private status:I

.field private ᵥ:Landroid/content/Context;

.field private ị:Lo/ᵌ;

.field private ゝ:Lorg/apache/http/client/methods/HttpRequestBase;

.field public ー:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private ˑ()Z
    .locals 5

    .line 44
    iget-object v0, p0, Lo/เ;->ᵥ:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 45
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 49
    :goto_0
    :try_start_0
    iget-object v3, p0, Lo/เ;->ᵥ:Landroid/content/Context;

    .line 1062
    const-string v0, "connectivity"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 1064
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1065
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1067
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-nez v0, :cond_1

    .line 1068
    const-string v0, "connectivity"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_1
    nop

    .line 51
    .line 56
    :catch_0
    return v2
.end method

.method private varargs ـ()Lo/Ꭵ;
    .locals 8

    .line 77
    invoke-direct {p0}, Lo/เ;->ˑ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    new-instance v5, Lo/Ꭵ;

    invoke-direct {v5}, Lo/Ꭵ;-><init>()V

    .line 82
    move-object v4, v5

    iget v6, p0, Lo/เ;->status:I

    const-string v0, "{response: \"%s\"}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "No connection"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2012
    iput v6, v5, Lo/Ꭵ;->一:I

    .line 2013
    const/16 v0, 0x3e6

    iput v0, v5, Lo/Ꭵ;->status:I

    .line 2014
    iput-object v7, v5, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 84
    return-object v4

    .line 90
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lo/เ;->ᐧ()Lo/Ꭵ;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 91
    return-object v0

    .line 94
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 95
    const-string v5, "Connection error"

    .line 98
    new-instance v0, Lo/Ꭵ;

    invoke-direct {v0}, Lo/Ꭵ;-><init>()V

    .line 99
    move-object v4, v0

    iget v6, p0, Lo/เ;->status:I

    const-string v1, "{response: \"%s\"}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 3012
    move-object v5, v0

    iput v6, v0, Lo/Ꭵ;->一:I

    .line 3013
    const/16 v0, 0x3e7

    iput v0, v5, Lo/Ꭵ;->status:I

    .line 3014
    iput-object v7, v5, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 101
    return-object v4
.end method

.method private ᐧ()Lo/Ꭵ;
    .locals 9

    .line 134
    :try_start_0
    invoke-virtual {p0}, Lo/เ;->ˍ()Lorg/apache/http/client/methods/HttpRequestBase;

    move-result-object v0

    iput-object v0, p0, Lo/เ;->ゝ:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 136
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 137
    const/16 v0, 0x3a98

    invoke-static {v4, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 138
    const/16 v0, 0x3a98

    invoke-static {v4, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 139
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iget-object v1, p0, Lo/เ;->ゝ:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 141
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 143
    const-string v0, "glory"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request done "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    new-instance v0, Lo/Ꭵ;

    invoke-direct {v0}, Lo/Ꭵ;-><init>()V

    .line 146
    move-object v4, v0

    iget v1, p0, Lo/เ;->status:I

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    move-object v8, v6

    move v6, v1

    .line 4012
    move-object v5, v0

    iput v6, v0, Lo/Ꭵ;->一:I

    .line 4013
    iput v7, v5, Lo/Ꭵ;->status:I

    .line 4014
    iput-object v8, v5, Lo/Ꭵ;->ヽ:Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4

    .line 147
    .line 149
    return-object v4

    .line 151
    .line 152
    :catch_0
    const-string v5, "No connection"

    .line 165
    goto :goto_0

    .line 153
    .line 154
    :catch_1
    const-string v5, "No connection"

    .line 165
    goto :goto_0

    .line 155
    .line 157
    :catch_2
    new-instance v5, Lo/Ꭵ;

    invoke-direct {v5}, Lo/Ꭵ;-><init>()V

    .line 158
    move-object v4, v5

    iget v6, p0, Lo/เ;->status:I

    const-string v0, "{response: \"%s\"}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "No connection"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 5012
    iput v6, v5, Lo/Ꭵ;->一:I

    .line 5013
    const/16 v0, 0x194

    iput v0, v5, Lo/Ꭵ;->status:I

    .line 5014
    iput-object v8, v5, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 160
    return-object v4

    .line 161
    .line 162
    :catch_3
    const-string v5, "No connection"

    .line 165
    goto :goto_0

    .line 163
    .line 164
    :catch_4
    const-string v5, "No connection"

    .line 167
    :goto_0
    new-instance v4, Lo/Ꭵ;

    invoke-direct {v4}, Lo/Ꭵ;-><init>()V

    .line 168
    const-string v0, "glory"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Response done: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget v6, p0, Lo/เ;->status:I

    const-string v0, "{response: \"%s\"}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 6012
    move-object v5, v4

    iput v6, v4, Lo/Ꭵ;->一:I

    .line 6013
    const/16 v0, 0x3e7

    iput v0, v5, Lo/Ꭵ;->status:I

    .line 6014
    iput-object v8, v5, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 171
    return-object v4
.end method


# virtual methods
.method protected doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 179
    invoke-direct {p0}, Lo/เ;->ـ()Lo/Ꭵ;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getUrl()Ljava/lang/String;
.end method

.method protected onCancelled()V
    .locals 0

    .line 183
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 184
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .line 188
    move-object v1, p1

    check-cast v1, Lo/Ꭵ;

    move-object p1, p0

    .line 6109
    .line 7020
    iget v0, v1, Lo/Ꭵ;->status:I

    .line 6109
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 6113
    :sswitch_0
    iget-object v0, p1, Lo/เ;->ị:Lo/ᵌ;

    invoke-interface {v0, v1}, Lo/ᵌ;->ˊ(Lo/Ꭵ;)V

    .line 6114
    return-void

    .line 6116
    :goto_0
    iget-object v0, p1, Lo/เ;->ị:Lo/ᵌ;

    invoke-interface {v0, v1}, Lo/ᵌ;->ˋ(Lo/Ꭵ;)V

    .line 189
    return-void

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xd0 -> :sswitch_0
        0x130 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 194
    return-void
.end method

.method public ˊ(Landroid/content/Context;ILo/ᵌ;)V
    .locals 3

    .line 34
    iput-object p1, p0, Lo/เ;->ᵥ:Landroid/content/Context;

    .line 35
    iput p2, p0, Lo/เ;->status:I

    .line 36
    iput-object p3, p0, Lo/เ;->ị:Lo/ᵌ;

    .line 37
    .line 1013
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᒽ:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    iput-object v0, p0, Lo/เ;->ー:Ljava/lang/String;

    .line 40
    return-void
.end method

.method protected abstract ˍ()Lorg/apache/http/client/methods/HttpRequestBase;
.end method
