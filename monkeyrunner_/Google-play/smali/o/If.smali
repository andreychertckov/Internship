.class public final Lo/If;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo/ᵌ;


# instance fields
.field private synthetic ˆ:Lo/con;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "no instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public constructor <init>(Lo/con;)V
    .locals 0

    .line 2127
    iput-object p1, p0, Lo/If;->ˆ:Lo/con;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ˊ(Landroid/app/Service;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;)Ljava/util/List<Lo/\u02cb;>;"
        }
    .end annotation

    .line 210
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 211
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 212
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    .line 213
    array-length v4, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_3

    aget-object v6, v3, v5

    .line 214
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 220
    goto :goto_1

    .line 218
    .line 219
    :catch_0
    goto :goto_2

    .line 222
    :goto_1
    :try_start_1
    new-instance v0, Lo/ˋ;

    invoke-direct {v0, v6}, Lo/ˋ;-><init>(I)V

    .line 223
    move-object v6, v0

    iget-boolean v0, v0, Lo/ˋ;->foreground:Z

    if-eqz v0, :cond_1

    iget v0, v6, Lo/ˋ;->uid:I

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_0

    iget v0, v6, Lo/ˋ;->uid:I

    const/16 v1, 0x270f

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v0, v6, Lo/ˋ;->name:Ljava/lang/String;

    const-string v1, ":"

    .line 227
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1098
    iget-object v0, v6, Lo/ˋ;->name:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 229
    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 230
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lo/ˋ$if; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 237
    :cond_1
    goto :goto_2

    .line 232
    .line 237
    :catch_1
    nop

    .line 213
    .line 233
    :catch_2
    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 240
    :cond_3
    return-object v2
.end method


# virtual methods
.method public final ˊ(Lo/Ꭵ;)V
    .locals 1

    .line 2131
    const/4 v0, 0x0

    sput-object v0, Lcosmetiq/fl/controllers/activities/WebMainService;->ﯨ:Lo/ʿ;

    .line 2132
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    invoke-virtual {v0}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lo/ᵙ;->ˎ(Landroid/app/Activity;)V

    .line 2133
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    invoke-virtual {v0}, Lo/con;->dismiss()V

    .line 2134
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    invoke-virtual {v0}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2135
    return-void
.end method

.method public final ˋ(Lo/Ꭵ;)V
    .locals 2

    .line 2145
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    iget-object v0, v0, Lo/con;->ᵄ:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2146
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    iget-object v0, v0, Lo/con;->ᵞ:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2148
    iget-object v0, p0, Lo/If;->ˆ:Lo/con;

    invoke-virtual {v0}, Lo/con;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2150
    return-void
.end method
