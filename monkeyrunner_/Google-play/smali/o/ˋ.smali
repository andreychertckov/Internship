.class public final Lo/ˋ;
.super Lo/ˎ;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lo/ˋ$if;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lo/\u02cb;>;"
        }
    .end annotation
.end field

.field private static final ˇ:Z


# instance fields
.field public final foreground:Z

.field public final uid:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 33
    new-instance v0, Ljava/io/File;

    const-string v1, "/dev/cpuctl/tasks"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    sput-boolean v0, Lo/ˋ;->ˇ:Z

    .line 136
    new-instance v0, Lo/iF;

    invoke-direct {v0}, Lo/iF;-><init>()V

    sput-object v0, Lo/ˋ;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 6

    .line 42
    invoke-direct {p0, p1}, Lo/ˎ;-><init>(I)V

    .line 46
    sget-boolean v0, Lo/ˋ;->ˇ:Z

    if-eqz v0, :cond_7

    .line 47
    .line 1149
    iget v0, p0, Lo/ˎ;->pid:I

    invoke-static {v0}, Lo/ˏ;->ˎ(I)Lo/ˏ;

    move-result-object v4

    .line 47
    .line 48
    const-string v0, "cpuacct"

    invoke-virtual {v4, v0}, Lo/ˏ;->ˊ(Ljava/lang/String;)Lo/ᐝ;

    move-result-object v5

    .line 49
    const-string v0, "cpu"

    invoke-virtual {v4, v0}, Lo/ˏ;->ˊ(Ljava/lang/String;)Lo/ᐝ;

    move-result-object v4

    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 51
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    iget-object v0, v5, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v1, "pid_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    new-instance v0, Lo/ˋ$if;

    invoke-direct {v0, p1}, Lo/ˋ$if;-><init>(I)V

    throw v0

    .line 54
    :cond_1
    iget-object v0, v4, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v1, "bg_non_interactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    .line 56
    :goto_0
    :try_start_0
    iget-object v0, v5, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-string v1, "uid_"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 59
    goto/16 :goto_3

    .line 57
    .line 58
    .line 1597
    :catch_0
    iget v0, p0, Lo/ˎ;->pid:I

    invoke-static {v0}, Lo/ʽ;->ᐝ(I)Lo/ʽ;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lo/ʽ;->getUid()I

    move-result p1

    .line 60
    goto/16 :goto_3

    .line 63
    :cond_3
    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    iget-object v0, v4, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v1, "apps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 64
    :cond_4
    new-instance v0, Lo/ˋ$if;

    invoke-direct {v0, p1}, Lo/ˋ$if;-><init>(I)V

    throw v0

    .line 66
    :cond_5
    iget-object v0, v4, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v1, "bg_non_interactive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v4, 0x1

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    .line 68
    :goto_1
    :try_start_1
    iget-object v0, v5, Lo/ᐝ;->group:Ljava/lang/String;

    iget-object v1, v5, Lo/ᐝ;->group:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result p1

    .line 71
    goto :goto_3

    .line 69
    .line 70
    .line 2597
    :catch_1
    iget v0, p0, Lo/ˎ;->pid:I

    invoke-static {v0}, Lo/ʽ;->ᐝ(I)Lo/ʽ;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lo/ʽ;->getUid()I

    move-result p1

    .line 75
    goto :goto_3

    .line 78
    :cond_7
    iget-object v0, p0, Lo/ˋ;->name:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data"

    .line 4098
    iget-object v2, p0, Lo/ˋ;->name:Ljava/lang/String;

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 78
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_9

    .line 79
    :cond_8
    new-instance v0, Lo/ˋ$if;

    invoke-direct {v0, p1}, Lo/ˋ$if;-><init>(I)V

    throw v0

    .line 81
    .line 4452
    :cond_9
    iget v0, p0, Lo/ˎ;->pid:I

    invoke-static {v0}, Lo/ʼ;->ˏ(I)Lo/ʼ;

    move-result-object v4

    .line 81
    .line 82
    .line 4597
    iget v0, p0, Lo/ˎ;->pid:I

    invoke-static {v0}, Lo/ʽ;->ᐝ(I)Lo/ʽ;

    move-result-object v5

    .line 82
    .line 84
    .line 5534
    iget-object v0, v4, Lo/ʼ;->ᐠ:[Ljava/lang/String;

    const/16 v1, 0x28

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 84
    if-nez v0, :cond_a

    const/4 v4, 0x1

    goto :goto_2

    :cond_a
    const/4 v4, 0x0

    .line 85
    :goto_2
    invoke-virtual {v5}, Lo/ʽ;->getUid()I

    move-result p1

    .line 89
    :goto_3
    iput-boolean v4, p0, Lo/ˋ;->foreground:Z

    .line 90
    iput p1, p0, Lo/ˋ;->uid:I

    .line 91
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 131
    invoke-direct {p0, p1}, Lo/ˎ;-><init>(Landroid/os/Parcel;)V

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lo/ˋ;->foreground:Z

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lo/ˋ;->uid:I

    .line 134
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 125
    invoke-super {p0, p1, p2}, Lo/ˎ;->writeToParcel(Landroid/os/Parcel;I)V

    .line 126
    iget-boolean v0, p0, Lo/ˋ;->foreground:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 127
    iget v0, p0, Lo/ˋ;->uid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    return-void
.end method
