.class public final Lo/ˊ;
.super Ljava/io/FilterInputStream;
.source ""


# static fields
.field private static final ʹ:[I

.field private static final ՙ:[I

.field private static final י:[I

.field private static final ᵎ:[I

.field private static final ﹳ:[B


# instance fields
.field private ʳ:I

.field private ʴ:I

.field private final ᴵ:[I

.field private final ᵔ:I

.field private final ᵢ:[[B

.field private final ⁱ:[I

.field private final ﹶ:[B

.field private final ﹺ:[B

.field private ｰ:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    sget-object v0, Lo/if;->ﾞ:[B

    sput-object v0, Lo/ˊ;->ﹳ:[B

    .line 24
    sget-object v0, Lo/if;->ʹ:[I

    sput-object v0, Lo/ˊ;->ᵎ:[I

    .line 25
    sget-object v0, Lo/if;->ՙ:[I

    sput-object v0, Lo/ˊ;->ʹ:[I

    .line 26
    sget-object v0, Lo/if;->י:[I

    sput-object v0, Lo/ˊ;->ՙ:[I

    .line 27
    sget-object v0, Lo/if;->ٴ:[I

    sput-object v0, Lo/ˊ;->י:[I

    return-void
.end method

.method public constructor <init>(Ljava/io/ByteArrayInputStream;I[B[[B)V
    .locals 1

    .line 60
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lo/ˊ;->ⁱ:[I

    .line 38
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lo/ˊ;->ﹶ:[B

    .line 39
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lo/ˊ;->ﹺ:[B

    .line 41
    const v0, 0x7fffffff

    iput v0, p0, Lo/ˊ;->ｰ:I

    .line 42
    const/16 v0, 0x10

    iput v0, p0, Lo/ˊ;->ʳ:I

    .line 43
    const/16 v0, 0x10

    iput v0, p0, Lo/ˊ;->ʴ:I

    .line 62
    iput p2, p0, Lo/ˊ;->ᵔ:I

    .line 63
    invoke-static {p3, p2}, Lo/if;->ˋ([BI)[I

    move-result-object v0

    iput-object v0, p0, Lo/ˊ;->ᴵ:[I

    .line 64
    invoke-static {p4}, Lo/ˊ;->ˊ([[B)[[B

    move-result-object v0

    iput-object v0, p0, Lo/ˊ;->ᵢ:[[B

    .line 65
    return-void
.end method

.method private ˊ()I
    .locals 4

    .line 189
    iget v0, p0, Lo/ˊ;->ｰ:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lo/ˊ;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lo/ˊ;->ｰ:I

    .line 195
    :cond_0
    iget v0, p0, Lo/ˊ;->ʳ:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 198
    iget-object v0, p0, Lo/ˊ;->ﹶ:[B

    iget v1, p0, Lo/ˊ;->ｰ:I

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 201
    const/4 v3, 0x1

    .line 204
    :cond_1
    iget-object v0, p0, Lo/ˊ;->in:Ljava/io/InputStream;

    iget-object v1, p0, Lo/ˊ;->ﹶ:[B

    rsub-int/lit8 v2, v3, 0x10

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    add-int/2addr v0, v3

    move v3, v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 207
    iget-object v0, p0, Lo/ˊ;->ﹶ:[B

    iget-object v1, p0, Lo/ˊ;->ﹺ:[B

    invoke-direct {p0, v0, v1}, Lo/ˊ;->ˊ([B[B)V

    .line 210
    iget-object v0, p0, Lo/ˊ;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lo/ˊ;->ｰ:I

    .line 213
    const/4 v0, 0x0

    iput v0, p0, Lo/ˊ;->ʳ:I

    .line 217
    iget v0, p0, Lo/ˊ;->ｰ:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lo/ˊ;->ﹺ:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    rsub-int/lit8 v0, v0, 0x10

    goto :goto_0

    :cond_2
    const/16 v0, 0x10

    :goto_0
    iput v0, p0, Lo/ˊ;->ʴ:I

    .line 222
    :cond_3
    iget v0, p0, Lo/ˊ;->ʴ:I

    return v0
.end method

.method private ˊ([B[B)V
    .locals 10

    .line 242
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x0

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x18

    const/4 v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/4 v2, 0x2

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/4 v2, 0x3

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    iget-object v2, p0, Lo/ˊ;->ᴵ:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    xor-int/2addr v1, v2

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 246
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x4

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x18

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    iget-object v2, p0, Lo/ˊ;->ᴵ:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    xor-int/2addr v1, v2

    const/4 v2, 0x1

    aput v1, v0, v2

    .line 250
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/16 v1, 0x8

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x18

    const/16 v2, 0x9

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/16 v2, 0xb

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    iget-object v2, p0, Lo/ˊ;->ᴵ:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    xor-int/2addr v1, v2

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 254
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/16 v1, 0xc

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x18

    const/16 v2, 0xd

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/16 v2, 0xe

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/16 v2, 0xf

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    iget-object v2, p0, Lo/ˊ;->ᴵ:[I

    const/4 v3, 0x3

    aget v2, v2, v3

    xor-int/2addr v1, v2

    const/4 v2, 0x3

    aput v1, v0, v2

    .line 262
    const/4 p1, 0x4

    .line 263
    const/4 v5, 0x1

    :goto_0
    iget v0, p0, Lo/ˊ;->ᵔ:I

    if-ge v5, v0, :cond_0

    .line 265
    sget-object v0, Lo/ˊ;->ᵎ:[I

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget v0, v0, v1

    sget-object v1, Lo/ˊ;->ʹ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x10

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->ՙ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->י:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    iget-object v1, p0, Lo/ˊ;->ᴵ:[I

    aget v1, v1, p1

    xor-int v6, v0, v1

    .line 269
    sget-object v0, Lo/ˊ;->ᵎ:[I

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget v0, v0, v1

    sget-object v1, Lo/ˊ;->ʹ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x10

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->ՙ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->י:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-byte v3, v3, v4

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    iget-object v1, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v2, p1, 0x1

    aget v1, v1, v2

    xor-int v7, v0, v1

    .line 273
    sget-object v0, Lo/ˊ;->ᵎ:[I

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget v0, v0, v1

    sget-object v1, Lo/ˊ;->ʹ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x10

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->ՙ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->י:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    iget-object v1, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v2, p1, 0x2

    aget v1, v1, v2

    xor-int v8, v0, v1

    .line 277
    sget-object v0, Lo/ˊ;->ᵎ:[I

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget v0, v0, v1

    sget-object v1, Lo/ˊ;->ʹ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x10

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->ՙ:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x2

    aget-object v3, v3, v4

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    aget v2, v2, v3

    ushr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    sget-object v1, Lo/ˊ;->י:[I

    iget-object v2, p0, Lo/ˊ;->ⁱ:[I

    iget-object v3, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v4, 0x3

    aget-object v3, v3, v4

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    iget-object v1, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v2, p1, 0x3

    aget v1, v1, v2

    xor-int v9, v0, v1

    .line 281
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x0

    aput v6, v0, v1

    .line 282
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x1

    aput v7, v0, v1

    .line 283
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x2

    aput v8, v0, v1

    .line 284
    iget-object v0, p0, Lo/ˊ;->ⁱ:[I

    const/4 v1, 0x3

    aput v9, v0, v1

    .line 263
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 p1, p1, 0x4

    goto/16 :goto_0

    .line 292
    :cond_0
    iget-object v0, p0, Lo/ˊ;->ᴵ:[I

    aget v5, v0, p1

    .line 293
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x18

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x0

    aput-byte v0, p2, v1

    .line 294
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x10

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x1

    aput-byte v0, p2, v1

    .line 295
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x8

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x2

    aput-byte v0, p2, v1

    .line 296
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    aget v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    xor-int/2addr v0, v5

    int-to-byte v0, v0

    const/4 v1, 0x3

    aput-byte v0, p2, v1

    .line 298
    iget-object v0, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v1, p1, 0x1

    aget v5, v0, v1

    .line 299
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x18

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x4

    aput-byte v0, p2, v1

    .line 300
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x10

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x5

    aput-byte v0, p2, v1

    .line 301
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x8

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/4 v1, 0x6

    aput-byte v0, p2, v1

    .line 302
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    aget v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    xor-int/2addr v0, v5

    int-to-byte v0, v0

    const/4 v1, 0x7

    aput-byte v0, p2, v1

    .line 304
    iget-object v0, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v1, p1, 0x2

    aget v5, v0, v1

    .line 305
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x18

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x8

    aput-byte v0, p2, v1

    .line 306
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x10

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0x9

    aput-byte v0, p2, v1

    .line 307
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x8

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xa

    aput-byte v0, p2, v1

    .line 308
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    aget v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    xor-int/2addr v0, v5

    int-to-byte v0, v0

    const/16 v1, 0xb

    aput-byte v0, p2, v1

    .line 310
    iget-object v0, p0, Lo/ˊ;->ᴵ:[I

    add-int/lit8 v1, p1, 0x3

    aget v5, v0, v1

    .line 311
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x18

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x18

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xc

    aput-byte v0, p2, v1

    .line 312
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x10

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xd

    aput-byte v0, p2, v1

    .line 313
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aget v1, v1, v2

    ushr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    ushr-int/lit8 v1, v5, 0x8

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    const/16 v1, 0xe

    aput-byte v0, p2, v1

    .line 314
    sget-object v0, Lo/ˊ;->ﹳ:[B

    iget-object v1, p0, Lo/ˊ;->ⁱ:[I

    iget-object v2, p0, Lo/ˊ;->ᵢ:[[B

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    aget v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    aget-byte v0, v0, v1

    xor-int/2addr v0, v5

    int-to-byte v0, v0

    const/16 v1, 0xf

    aput-byte v0, p2, v1

    .line 315
    return-void
.end method

.method private static ˊ([[B)[[B
    .locals 6

    .line 169
    array-length v0, p0

    new-array v3, v0, [[B

    .line 170
    const/4 v4, 0x0

    :goto_0
    array-length v0, p0

    if-ge v4, v0, :cond_1

    .line 172
    aget-object v0, p0, v4

    array-length v0, v0

    new-array v0, v0, [B

    aput-object v0, v3, v4

    .line 173
    const/4 v5, 0x0

    :goto_1
    aget-object v0, p0, v4

    array-length v0, v0

    if-ge v5, v0, :cond_0

    .line 175
    aget-object v0, v3, v4

    aget-object v1, p0, v4

    aget-byte v1, v1, v5

    int-to-byte v2, v5

    aput-byte v2, v0, v1

    .line 173
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 170
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 179
    :cond_1
    return-object v3
.end method


# virtual methods
.method public final available()I
    .locals 2

    .line 130
    iget v0, p0, Lo/ˊ;->ʴ:I

    iget v1, p0, Lo/ˊ;->ʳ:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final close()V
    .locals 0

    .line 137
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 138
    return-void
.end method

.method public final declared-synchronized mark(I)V
    .locals 0

    monitor-enter p0

    .line 150
    monitor-exit p0

    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 3

    .line 72
    invoke-direct {p0}, Lo/ˊ;->ˊ()I

    .line 75
    iget v0, p0, Lo/ˊ;->ʳ:I

    iget v1, p0, Lo/ˊ;->ʴ:I

    if-lt v0, v1, :cond_0

    .line 77
    const/4 v0, -0x1

    return v0

    .line 80
    :cond_0
    iget-object v0, p0, Lo/ˊ;->ﹺ:[B

    iget v1, p0, Lo/ˊ;->ʳ:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lo/ˊ;->ʳ:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final read([B)I
    .locals 2

    .line 86
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lo/ˊ;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 6

    .line 92
    move v4, p2

    .line 93
    add-int v5, p2, p3

    .line 97
    :goto_0
    if-ge v4, v5, :cond_2

    .line 99
    invoke-direct {p0}, Lo/ˊ;->ˊ()I

    .line 102
    iget v0, p0, Lo/ˊ;->ʳ:I

    iget v1, p0, Lo/ˊ;->ʴ:I

    if-lt v0, v1, :cond_1

    .line 105
    if-ne v4, p2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    sub-int v0, v5, v4

    sub-int v0, p3, v0

    return v0

    .line 108
    :cond_1
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    iget-object v1, p0, Lo/ˊ;->ﹺ:[B

    iget v2, p0, Lo/ˊ;->ʳ:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lo/ˊ;->ʳ:I

    aget-byte v1, v1, v2

    aput-byte v1, p1, v0

    goto :goto_0

    .line 111
    :cond_2
    return p3
.end method

.method public final declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    .line 156
    monitor-exit p0

    return-void
.end method

.method public final skip(J)J
    .locals 4

    .line 118
    const-wide/16 v2, 0x0

    .line 119
    :goto_0
    cmp-long v0, v2, p1

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lo/ˊ;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 121
    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    goto :goto_0

    .line 124
    :cond_0
    return-wide v2
.end method
