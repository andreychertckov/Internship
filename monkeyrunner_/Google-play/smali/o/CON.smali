.class public Lo/CON;
.super Landroid/app/Activity;
.source ""


# instance fields
.field private ᕪ:Z

.field private ᙆ:Landroid/webkit/WebView;

.field ᴊ:Ljava/lang/String;

.field ᴷ:Z

.field private ᵏ:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/CON;->ᴷ:Z

    return-void
.end method


# virtual methods
.method public getResources()Landroid/content/res/Resources;
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lif;->ˊ(Landroid/content/res/Resources;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lo/CON;->setContentView(I)V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/CON;->ᕪ:Z

    .line 89
    sget-object v0, Lo/ι;->Ɨ:Ljava/lang/String;

    iput-object v0, p0, Lo/CON;->ᵏ:Ljava/lang/String;

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/CON;->ᕪ:Z

    .line 92
    sget-object v0, Lo/ι;->ᴊ:Ljava/lang/String;

    iput-object v0, p0, Lo/CON;->ᴊ:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lo/CON;->ᵏ:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 97
    const v0, 0x7f0a002c

    invoke-virtual {p0, v0}, Lo/CON;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lo/CON;->ᙆ:Landroid/webkit/WebView;

    .line 100
    iget-object v0, p0, Lo/CON;->ᙆ:Landroid/webkit/WebView;

    new-instance v1, Lo/ᐨ;

    invoke-direct {v1, p0}, Lo/ᐨ;-><init>(Lo/CON;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 134
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 139
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 141
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x1

    return v0

    .line 145
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .line 150
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    const/4 v0, 0x0

    sput-boolean v0, Lo/ι;->ſ:Z

    .line 153
    invoke-virtual {p0}, Lo/CON;->finish()V

    .line 154
    return-void
.end method

.method protected onResume()V
    .locals 4

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 70
    sget-object v0, Lo/ι;->Ɨ:Ljava/lang/String;

    iput-object v0, p0, Lo/CON;->ᵏ:Ljava/lang/String;

    .line 71
    .line 1222
    sget-object v0, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ǃ:Ljava/lang/String;

    sget-object v2, Lo/ͺ;->ː:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lo/CON;->ᵏ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lo/ｰ;->ˋ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-static {}, Lo/ᵔ;->ͺ()V

    .line 78
    iget-object v0, p0, Lo/CON;->ᙆ:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 79
    iget-object v0, p0, Lo/CON;->ᙆ:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 81
    return-void
.end method
