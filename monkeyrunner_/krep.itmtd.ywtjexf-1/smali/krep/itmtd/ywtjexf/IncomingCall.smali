.class public Lkrep/itmtd/ywtjexf/IncomingCall;
.super Landroid/content/BroadcastReceiver;
.source "IncomingCall.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;
    }
.end annotation


# instance fields
.field public ctx:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    iput-object p1, p0, Lkrep/itmtd/ywtjexf/IncomingCall;->ctx:Landroid/content/Context;

    .line 22
    :try_start_0
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 21
    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 25
    .local v2, "tmgr":Landroid/telephony/TelephonyManager;
    new-instance v0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;

    invoke-direct {v0, p0}, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;-><init>(Lkrep/itmtd/ywtjexf/IncomingCall;)V

    .line 28
    .local v0, "PhoneListener":Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;
    const/16 v3, 0x20

    invoke-virtual {v2, v0, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .end local v0    # "PhoneListener":Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;
    .end local v2    # "tmgr":Landroid/telephony/TelephonyManager;
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v1

    .line 31
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Phone Receive Error"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
