.class public Lkrep/itmtd/ywtjexf/MeAction;
.super Ljava/lang/Object;
.source "MeAction.java"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lkrep/itmtd/ywtjexf/MeAction;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method


# virtual methods
.method public Del(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "ff"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v2, p0, Lkrep/itmtd/ywtjexf/MeAction;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 119
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 121
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 122
    return-void
.end method

.method public Get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "string"    # Ljava/lang/String;
    .param p3, "ff"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v2, p0, Lkrep/itmtd/ywtjexf/MeAction;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, p3, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 113
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "out":Ljava/lang/String;
    return-object v0
.end method

.method public List(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "ff"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v6, p0, Lkrep/itmtd/ywtjexf/MeAction;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 126
    .local v5, "settings":Landroid/content/SharedPreferences;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 127
    .local v2, "jsonObj":Lorg/json/JSONObject;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 128
    .local v1, "jsonArr":Lorg/json/JSONArray;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 129
    .local v0, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 130
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 136
    :cond_0
    const-string v6, "result"

    invoke-virtual {v2, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 137
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 130
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 131
    .local v4, "s":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 132
    .local v3, "pnObj":Lorg/json/JSONObject;
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 133
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0
.end method

.method public ListBoot()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 46
    const-string v0, "Boot_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListCall()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "Call_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListCmd()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 95
    const-string v0, "Cmd_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListNetwork()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 53
    const-string v0, "Network_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListPower()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 60
    const-string v0, "Power_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListSMS()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    const-string v0, "SMS_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListStart()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 81
    const-string v0, "Start_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListTimeout()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "Timeout_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ListTimers()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 101
    const-string v0, "Timers_conf"

    invoke-virtual {p0, v0}, Lkrep/itmtd/ywtjexf/MeAction;->List(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "string"    # Ljava/lang/String;
    .param p3, "ff"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v2, p0, Lkrep/itmtd/ywtjexf/MeAction;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, p3, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 109
    return-void
.end method

.method public SetBoot(Ljava/lang/String;)V
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 43
    const-string v0, "1"

    const-string v1, "Boot_conf"

    invoke-virtual {p0, p1, v0, v1}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public SetCall(Ljava/lang/String;)V
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 64
    const-string v0, "1"

    const-string v1, "Call_conf"

    invoke-virtual {p0, p1, v0, v1}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public SetCmd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "string"    # Ljava/lang/String;

    .prologue
    .line 92
    const-string v0, "Cmd_conf"

    invoke-virtual {p0, p1, p2, v0}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public SetNetwork(Ljava/lang/String;)V
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v0, "1"

    const-string v1, "Network_conf"

    invoke-virtual {p0, p1, v0, v1}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public SetPower(Ljava/lang/String;)V
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v0, "1"

    const-string v1, "Power_conf"

    invoke-virtual {p0, p1, v0, v1}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public SetSMS(Ljava/lang/String;)V
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string v0, "1"

    const-string v1, "SMS_conf"

    invoke-virtual {p0, p1, v0, v1}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public SetStart(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "packagename"    # Ljava/lang/String;

    .prologue
    .line 78
    const-string v0, "Start_conf"

    invoke-virtual {p0, p1, p2, v0}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public SetTimeout(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string v0, "Timeout_conf"

    invoke-virtual {p0, p1, p2, v0}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public SetTimer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v0, "Timers_conf"

    invoke-virtual {p0, p1, p2, v0}, Lkrep/itmtd/ywtjexf/MeAction;->Set(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method
