.class Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
.super Ljava/lang/Object;
.source "BeNewsArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/benews/BeNewsArrayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolderItem"
.end annotation


# instance fields
.field date:Landroid/widget/TextView;

.field imageView:Landroid/widget/ImageView;

.field secondLine:Landroid/widget/TextView;

.field final synthetic this$0:Lorg/benews/BeNewsArrayAdapter;

.field title:Landroid/widget/TextView;

.field view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lorg/benews/BeNewsArrayAdapter;Landroid/view/View;)V
    .locals 2
    .param p2, "inflated"    # Landroid/view/View;

    .prologue
    .line 141
    iput-object p1, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->this$0:Lorg/benews/BeNewsArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p2, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    .line 143
    iget-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    const v1, 0x7f07002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->title:Landroid/widget/TextView;

    .line 144
    iget-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    const v1, 0x7f07004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->secondLine:Landroid/widget/TextView;

    .line 145
    iget-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->imageView:Landroid/widget/ImageView;

    .line 146
    iget-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->date:Landroid/widget/TextView;

    .line 148
    return-void
.end method
