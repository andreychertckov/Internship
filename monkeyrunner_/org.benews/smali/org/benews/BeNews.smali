.class public Lorg/benews/BeNews;
.super Landroid/support/v4/app/FragmentActivity;
.source "BeNews.java"

# interfaces
.implements Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;
.implements Landroid/view/View$OnClickListener;
.implements Lorg/benews/BackgroundSocket$NewsUpdateListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "BeNews"

.field private static context:Landroid/content/Context;

.field private static pb:Landroid/widget/ProgressBar;


# instance fields
.field listAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMessageReceiver:Landroid/content/BroadcastReceiver;

.field toUpdate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/benews/BeNews;->toUpdate:Z

    .line 154
    new-instance v0, Lorg/benews/BeNews$2;

    invoke-direct {v0, p0}, Lorg/benews/BeNews$2;-><init>(Lorg/benews/BeNews;)V

    iput-object v0, p0, Lorg/benews/BeNews;->mMessageReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lorg/benews/BeNews;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public finishOnStart()V
    .locals 6

    .prologue
    .line 121
    invoke-static {}, Lorg/benews/BackgroundSocket;->self()Lorg/benews/BackgroundSocket;

    move-result-object v3

    .line 122
    .local v3, "sucker":Lorg/benews/BackgroundSocket;
    new-instance v1, Lorg/benews/BeNewsFragList;

    invoke-direct {v1}, Lorg/benews/BeNewsFragList;-><init>()V

    .line 123
    .local v1, "bfl":Lorg/benews/BeNewsFragList;
    invoke-virtual {p0}, Lorg/benews/BeNews;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 124
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v4, 0x7f070041

    invoke-virtual {v2, v4, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 125
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 126
    new-instance v4, Lorg/benews/BeNewsArrayAdapter;

    invoke-virtual {v3}, Lorg/benews/BackgroundSocket;->getList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lorg/benews/BeNewsArrayAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v4, p0, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    .line 127
    iget-object v4, p0, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v4}, Lorg/benews/BeNewsFragList;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    const v4, 0x7f070042

    invoke-virtual {p0, v4}, Lorg/benews/BeNews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 129
    .local v0, "b":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v4, 0x7f07003f

    invoke-virtual {p0, v4}, Lorg/benews/BeNews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    sput-object v4, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    .line 131
    sget-object v4, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 132
    sget-object v4, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 133
    invoke-virtual {v3, p0}, Lorg/benews/BackgroundSocket;->setOnNewsUpdateListener(Lorg/benews/BackgroundSocket$NewsUpdateListener;)V

    .line 134
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/benews/BeNews;->setToUpdate(Z)V

    .line 136
    return-void
.end method

.method public declared-synchronized isToUpdate()Z
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/benews/BeNews;->toUpdate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 209
    invoke-virtual {p0}, Lorg/benews/BeNews;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070044

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lorg/benews/BeNews;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "DETAILS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 225
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 226
    .local v0, "button":Landroid/widget/Button;
    invoke-static {}, Lorg/benews/BackgroundSocket;->self()Lorg/benews/BackgroundSocket;

    move-result-object v2

    .line 227
    .local v2, "sucker":Lorg/benews/BackgroundSocket;
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 228
    invoke-virtual {v2, v3}, Lorg/benews/BackgroundSocket;->setRun(Z)V

    .line 229
    const/4 v1, 0x0

    .line 230
    .local v1, "i":I
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lorg/benews/BeNews$3;

    invoke-direct {v4, p0, v2}, Lorg/benews/BeNews$3;-><init>(Lorg/benews/BeNews;Lorg/benews/BackgroundSocket;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 245
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 106
    iget-object v0, p0, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 110
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v1, 0x7f030018

    invoke-virtual {p0, v1}, Lorg/benews/BeNews;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lorg/benews/BeNews;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v1}, Lorg/benews/BitmapHelper;->init(F)V

    .line 46
    invoke-virtual {p0}, Lorg/benews/BeNews;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lorg/benews/BeNews;->context:Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lorg/benews/BeNews;->context:Landroid/content/Context;

    const-class v2, Lorg/benews/PullIntentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    .local v0, "serviceIntent":Landroid/content/Intent;
    sget-object v1, Lorg/benews/BeNews;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 49
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 166
    invoke-virtual {p0}, Lorg/benews/BeNews;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public onItemPress(I)V
    .locals 9
    .param p1, "position"    # I

    .prologue
    .line 188
    :try_start_0
    iget-object v6, p0, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    .line 189
    .local v4, "o":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 190
    .local v3, "keyword":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "You selected: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 191
    invoke-static {}, Lorg/benews/BackgroundSocket;->self()Lorg/benews/BackgroundSocket;

    move-result-object v5

    .line 192
    .local v5, "sucker":Lorg/benews/BackgroundSocket;
    if-eqz v5, :cond_0

    .line 193
    check-cast v4, Ljava/util/HashMap;

    .end local v4    # "o":Ljava/lang/Object;
    invoke-static {v4}, Lorg/benews/DetailFragView;->newInstance(Ljava/util/HashMap;)Lorg/benews/DetailFragView;

    move-result-object v0

    .line 194
    .local v0, "details":Lorg/benews/DetailFragView;
    invoke-virtual {p0}, Lorg/benews/BeNews;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 195
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v6, 0x7f070041

    invoke-virtual {v2, v6, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 197
    const-string v6, "DETAILS"

    invoke-virtual {v2, v6}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 198
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    .end local v0    # "details":Lorg/benews/DetailFragView;
    .end local v2    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v3    # "keyword":Ljava/lang/String;
    .end local v5    # "sucker":Lorg/benews/BackgroundSocket;
    :cond_0
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v1

    .line 201
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "BeNews"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized onNewsUpdate()V
    .locals 2

    .prologue
    .line 69
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lorg/benews/BeNews;->setToUpdate(Z)V

    .line 70
    const v1, 0x7f070042

    invoke-virtual {p0, v1}, Lorg/benews/BeNews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 71
    .local v0, "b":Landroid/widget/Button;
    new-instance v1, Lorg/benews/BeNews$1;

    invoke-direct {v1, p0, v0}, Lorg/benews/BeNews$1;-><init>(Lorg/benews/BeNews;Landroid/widget/Button;)V

    invoke-virtual {p0, v1}, Lorg/benews/BeNews;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 69
    .end local v0    # "b":Landroid/widget/Button;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 175
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 178
    .local v0, "id":I
    const v1, 0x7f07004d

    if-ne v0, v1, :cond_0

    .line 179
    const/4 v1, 0x1

    .line 182
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 149
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lorg/benews/BeNews;->mMessageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 150
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 151
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 140
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 142
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lorg/benews/BeNews;->mMessageReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "upAndRunning"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 143
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 114
    invoke-static {}, Lorg/benews/BackgroundSocket;->self()Lorg/benews/BackgroundSocket;

    move-result-object v0

    invoke-virtual {v0}, Lorg/benews/BackgroundSocket;->isThreadStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lorg/benews/BeNews;->finishOnStart()V

    .line 118
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 101
    return-void
.end method

.method public setProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 52
    sget-object v0, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lorg/benews/BeNews;->pb:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 56
    :cond_0
    return-void
.end method

.method public declared-synchronized setToUpdate(Z)V
    .locals 1
    .param p1, "toUpdate"    # Z

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/benews/BeNews;->toUpdate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    monitor-exit p0

    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
