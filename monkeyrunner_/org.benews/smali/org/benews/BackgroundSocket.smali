.class public Lorg/benews/BackgroundSocket;
.super Landroid/app/Activity;
.source "BackgroundSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/benews/BackgroundSocket$SocketAsyncTask;,
        Lorg/benews/BackgroundSocket$NewsUpdateListener;
    }
.end annotation


# static fields
.field public static final READY:Ljava/lang/String; = "upAndRunning"

.field private static final TAG:Ljava/lang/String; = "BackgroundSocket"

.field static news_n:I = 0x0

.field private static runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask; = null

.field private static final serialFile:Ljava/lang/String; = ".news"

.field private static final serialFileTs:Ljava/lang/String; = ".ts"

.field private static serviceRunning:Z

.field static singleton:Lorg/benews/BackgroundSocket;


# instance fields
.field args_for_bkg:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field assets:Landroid/content/res/AssetManager;

.field private coreThread:Ljava/lang/Thread;

.field private dumpFolder:Ljava/lang/String;

.field private imei:Ljava/lang/String;

.field private last_timestamp:J

.field private list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/benews/BackgroundSocket$NewsUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private main:Lorg/benews/BeNews;

.field private noData:Z

.field private run:Z

.field private serializeFolder:Ljava/io/File;

.field private serviceMain:Lorg/benews/PullIntentService;

.field private sf:Ljavax/net/SocketFactory;

.field private socket:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    sput-boolean v0, Lorg/benews/BackgroundSocket;->serviceRunning:Z

    .line 60
    sput v0, Lorg/benews/BackgroundSocket;->news_n:I

    .line 64
    const/4 v0, 0x0

    sput-object v0, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    iput-boolean v3, p0, Lorg/benews/BackgroundSocket;->run:Z

    .line 63
    iput-object v2, p0, Lorg/benews/BackgroundSocket;->main:Lorg/benews/BeNews;

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    .line 67
    iput-object v2, p0, Lorg/benews/BackgroundSocket;->dumpFolder:Ljava/lang/String;

    .line 68
    iput-object v2, p0, Lorg/benews/BackgroundSocket;->imei:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/benews/BackgroundSocket;->args_for_bkg:Ljava/util/HashMap;

    .line 72
    iput-boolean v3, p0, Lorg/benews/BackgroundSocket;->noData:Z

    .line 73
    iput-object v2, p0, Lorg/benews/BackgroundSocket;->sf:Ljavax/net/SocketFactory;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/benews/BackgroundSocket;->listeners:Ljava/util/ArrayList;

    .line 323
    return-void
.end method

.method private Core()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public static Sleep(I)V
    .locals 3
    .param p0, "i"    # I

    .prologue
    .line 223
    mul-int/lit16 v1, p0, 0x3e8

    int-to-long v1, v1

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    return-void

    .line 224
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lorg/benews/BackgroundSocket;)Ljava/net/Socket;
    .locals 1
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->socket:Ljava/net/Socket;

    return-object v0
.end method

.method static synthetic access$002(Lorg/benews/BackgroundSocket;Ljava/net/Socket;)Ljava/net/Socket;
    .locals 0
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;
    .param p1, "x1"    # Ljava/net/Socket;

    .prologue
    .line 52
    iput-object p1, p0, Lorg/benews/BackgroundSocket;->socket:Ljava/net/Socket;

    return-object p1
.end method

.method static synthetic access$200(Lorg/benews/BackgroundSocket;)Z
    .locals 1
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-boolean v0, p0, Lorg/benews/BackgroundSocket;->noData:Z

    return v0
.end method

.method static synthetic access$202(Lorg/benews/BackgroundSocket;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lorg/benews/BackgroundSocket;->noData:Z

    return p1
.end method

.method static synthetic access$300(Lorg/benews/BackgroundSocket;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->imei:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lorg/benews/BackgroundSocket;)J
    .locals 2
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-wide v0, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    return-wide v0
.end method

.method static synthetic access$402(Lorg/benews/BackgroundSocket;J)J
    .locals 0
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;
    .param p1, "x1"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    return-wide p1
.end method

.method static synthetic access$500(Lorg/benews/BackgroundSocket;)Ljavax/net/SocketFactory;
    .locals 1
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->sf:Ljavax/net/SocketFactory;

    return-object v0
.end method

.method static synthetic access$502(Lorg/benews/BackgroundSocket;Ljavax/net/SocketFactory;)Ljavax/net/SocketFactory;
    .locals 0
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;
    .param p1, "x1"    # Ljavax/net/SocketFactory;

    .prologue
    .line 52
    iput-object p1, p0, Lorg/benews/BackgroundSocket;->sf:Ljavax/net/SocketFactory;

    return-object p1
.end method

.method static synthetic access$600(Lorg/benews/BackgroundSocket;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/benews/BackgroundSocket;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static newCore(Lorg/benews/PullIntentService;)Lorg/benews/BackgroundSocket;
    .locals 1
    .param p0, "serviceMain"    # Lorg/benews/PullIntentService;

    .prologue
    .line 230
    sget-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Lorg/benews/BackgroundSocket;

    invoke-direct {v0}, Lorg/benews/BackgroundSocket;-><init>()V

    sput-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    .line 233
    :cond_0
    sget-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    iput-object p0, v0, Lorg/benews/BackgroundSocket;->serviceMain:Lorg/benews/PullIntentService;

    .line 236
    sget-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    return-object v0
.end method

.method private runUntilStop(Ljava/util/HashMap;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 187
    :goto_0
    iget-boolean v2, p0, Lorg/benews/BackgroundSocket;->run:Z

    if-eqz v2, :cond_6

    .line 190
    const-wide/16 v0, 0x0

    .line 192
    .local v0, "old_ts":J
    :try_start_0
    const-string v2, "date"

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    const-string v2, "date"

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_1
    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    if-eqz v2, :cond_1

    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->isRunning()Z

    move-result v2

    if-nez v2, :cond_2

    .line 199
    :cond_1
    new-instance v2, Lorg/benews/BackgroundSocket$SocketAsyncTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lorg/benews/BackgroundSocket$SocketAsyncTask;-><init>(Lorg/benews/BackgroundSocket;Ljava/util/HashMap;Lorg/benews/BackgroundSocket$1;)V

    sput-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    .line 200
    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    new-array v3, v5, [Ljava/util/HashMap;

    aput-object p1, v3, v4

    invoke-virtual {v2, v3}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 202
    :cond_2
    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    if-eqz v2, :cond_5

    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 203
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->getLast_timestamp()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->isConnectionError()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    sget-object v2, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->noData()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 204
    :cond_4
    const-string v2, "BackgroundSocket"

    const-string v3, " (runUntilStop): No new news waiting ..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/16 v2, 0x3c

    invoke-static {v2}, Lorg/benews/BackgroundSocket;->Sleep(I)V

    .line 208
    :cond_5
    invoke-static {v5}, Lorg/benews/BackgroundSocket;->Sleep(I)V

    goto :goto_0

    .line 211
    .end local v0    # "old_ts":J
    :cond_6
    return v4

    .line 195
    .restart local v0    # "old_ts":J
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public static declared-synchronized self()Lorg/benews/BackgroundSocket;
    .locals 2

    .prologue
    .line 166
    const-class v1, Lorg/benews/BackgroundSocket;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lorg/benews/BackgroundSocket;

    invoke-direct {v0}, Lorg/benews/BackgroundSocket;-><init>()V

    sput-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;

    .line 170
    :cond_0
    sget-object v0, Lorg/benews/BackgroundSocket;->singleton:Lorg/benews/BackgroundSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public Start()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240
    sget-boolean v1, Lorg/benews/BackgroundSocket;->serviceRunning:Z

    if-ne v1, v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 253
    :goto_0
    return v0

    .line 244
    :cond_0
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lorg/benews/BackgroundSocket;->coreThread:Ljava/lang/Thread;

    .line 246
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lorg/benews/BackgroundSocket;->setRun(Z)V

    .line 247
    iget-object v1, p0, Lorg/benews/BackgroundSocket;->coreThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :goto_1
    sput-boolean v0, Lorg/benews/BackgroundSocket;->serviceRunning:Z

    goto :goto_0

    .line 248
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public getDumpFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->dumpFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getImei()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->imei:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFile()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 276
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFile()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 277
    .local v1, "fis":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 278
    .local v2, "is":Ljava/io/ObjectInputStream;
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    iput-object v3, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    .line 279
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "is":Ljava/io/ObjectInputStream;
    :cond_0
    :goto_0
    :try_start_2
    iget-object v3, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 286
    const-string v3, "BackgroundSocket"

    const-string v4, " (getList) initializing list"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    .line 289
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFileTs()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 291
    :try_start_3
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFileTs()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 292
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 293
    .restart local v2    # "is":Ljava/io/ObjectInputStream;
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    .line 294
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "is":Ljava/io/ObjectInputStream;
    :cond_2
    :goto_1
    :try_start_4
    iget-object v3, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v3

    .line 280
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v3, "BackgroundSocket"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " (getList):"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 274
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 295
    :catch_1
    move-exception v0

    .line 296
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    const-string v3, "BackgroundSocket"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " (getList Ts):"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1
.end method

.method public getSerialFile()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/benews/BackgroundSocket;->serializeFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".news"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSerialFileTs()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/benews/BackgroundSocket;->serializeFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    if-nez v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 309
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lorg/benews/BackgroundSocket;->runningTask:Lorg/benews/BackgroundSocket$SocketAsyncTask;

    invoke-virtual {v0}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->isRunning()Z

    move-result v0

    goto :goto_0
.end method

.method public isThreadStarted()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->coreThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/benews/BackgroundSocket;->coreThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized reset_news()V
    .locals 4

    .prologue
    .line 110
    monitor-enter p0

    const-wide/16 v1, 0x0

    :try_start_0
    iput-wide v1, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    .line 111
    iget-object v1, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :try_start_1
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->serialise()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :goto_0
    :try_start_2
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->updateListeners()V

    .line 118
    const/4 v1, 0x1

    invoke-static {v1}, Lorg/benews/BackgroundSocket;->Sleep(I)V

    .line 119
    const-string v1, "BackgroundSocket"

    const-string v2, " (reset_news):Done"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/benews/BackgroundSocket;->noData:Z

    .line 121
    iget-object v1, p0, Lorg/benews/BackgroundSocket;->args_for_bkg:Ljava/util/HashMap;

    const-string v2, "date"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " (setStop):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 110
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->args_for_bkg:Ljava/util/HashMap;

    const-string v1, "date"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->args_for_bkg:Ljava/util/HashMap;

    const-string v1, "checksum"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getList()Ljava/util/ArrayList;

    .line 179
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->updateListeners()V

    .line 181
    :goto_0
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->args_for_bkg:Ljava/util/HashMap;

    invoke-direct {p0, v0}, Lorg/benews/BackgroundSocket;->runUntilStop(Ljava/util/HashMap;)Z

    .line 182
    const/4 v0, 0x2

    invoke-static {v0}, Lorg/benews/BackgroundSocket;->Sleep(I)V

    goto :goto_0
.end method

.method public declared-synchronized saveStauts()V
    .locals 4

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->serialise_list()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :goto_0
    monitor-exit p0

    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " (saveStauts):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 216
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized serialise()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->serialise_list()V

    .line 143
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->serialise_ts()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized serialise_list()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFile()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 125
    .local v0, "fos":Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 126
    .local v1, "os":Ljava/io/ObjectOutputStream;
    iget-object v2, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    new-instance v0, Ljava/io/FileOutputStream;

    .end local v0    # "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFile()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 128
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    .end local v1    # "os":Ljava/io/ObjectOutputStream;
    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 129
    .restart local v1    # "os":Ljava/io/ObjectOutputStream;
    iget-object v2, p0, Lorg/benews/BackgroundSocket;->list:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 130
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .end local v1    # "os":Ljava/io/ObjectOutputStream;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized serialise_ts()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p0}, Lorg/benews/BackgroundSocket;->getSerialFileTs()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 137
    .local v0, "fos":Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 138
    .local v1, "os":Ljava/io/ObjectOutputStream;
    new-instance v2, Ljava/lang/Long;

    iget-wide v3, p0, Lorg/benews/BackgroundSocket;->last_timestamp:J

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 139
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    monitor-exit p0

    return-void

    .line 136
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    .end local v1    # "os":Ljava/io/ObjectOutputStream;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setAssets(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "assets"    # Landroid/content/res/AssetManager;

    .prologue
    .line 78
    iput-object p1, p0, Lorg/benews/BackgroundSocket;->assets:Landroid/content/res/AssetManager;

    .line 79
    return-void
.end method

.method public setDumpFolder(Ljava/lang/String;)V
    .locals 1
    .param p1, "dumpFolder"    # Ljava/lang/String;

    .prologue
    .line 257
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/benews/BackgroundSocket;->dumpFolder:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public setImei(Ljava/lang/String;)V
    .locals 1
    .param p1, "imei"    # Ljava/lang/String;

    .prologue
    .line 264
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/benews/BackgroundSocket;->imei:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setOnNewsUpdateListener(Lorg/benews/BackgroundSocket$NewsUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/benews/BackgroundSocket$NewsUpdateListener;

    .prologue
    .line 91
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public setRun(Z)V
    .locals 2
    .param p1, "run"    # Z

    .prologue
    .line 147
    if-nez p1, :cond_0

    .line 148
    iget-object v0, p0, Lorg/benews/BackgroundSocket;->socket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lorg/benews/BackgroundSocket$1;

    invoke-direct {v1, p0}, Lorg/benews/BackgroundSocket$1;-><init>(Lorg/benews/BackgroundSocket;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 162
    :cond_0
    iput-boolean p1, p0, Lorg/benews/BackgroundSocket;->run:Z

    .line 163
    return-void
.end method

.method public setSerializeFolder(Ljava/io/File;)V
    .locals 0
    .param p1, "filesDir"    # Ljava/io/File;

    .prologue
    .line 313
    iput-object p1, p0, Lorg/benews/BackgroundSocket;->serializeFolder:Ljava/io/File;

    .line 314
    return-void
.end method

.method public updateListeners()V
    .locals 3

    .prologue
    .line 317
    iget-object v2, p0, Lorg/benews/BackgroundSocket;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/benews/BackgroundSocket$NewsUpdateListener;

    .line 319
    .local v1, "listener":Lorg/benews/BackgroundSocket$NewsUpdateListener;
    invoke-interface {v1}, Lorg/benews/BackgroundSocket$NewsUpdateListener;->onNewsUpdate()V

    goto :goto_0

    .line 321
    .end local v1    # "listener":Lorg/benews/BackgroundSocket$NewsUpdateListener;
    :cond_0
    return-void
.end method
