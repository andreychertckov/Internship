.class public Lorg/benews/BeNewsFragList;
.super Landroid/support/v4/app/Fragment;
.source "BeNewsFragList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mListView:Landroid/widget/AbsListView;

.field private mListener:Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 79
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 85
    :try_start_0
    move-object v0, p1

    check-cast v0, Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;

    move-object v2, v0

    iput-object v2, p0, Lorg/benews/BeNewsFragList;->mListener:Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    return-void

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must implement OnFragmentInteractionListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    const v1, 0x7f03001b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 65
    .local v0, "view":Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView;

    iput-object v1, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    .line 66
    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    iget-object v2, p0, Lorg/benews/BeNewsFragList;->mAdapter:Landroid/widget/ListAdapter;

    if-eq v1, v2, :cond_0

    .line 68
    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    iget-object v2, p0, Lorg/benews/BeNewsFragList;->mAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 69
    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v1, p0}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 72
    :cond_0
    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/benews/BeNewsFragList;->mListener:Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;

    .line 97
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lorg/benews/BeNewsFragList;->mListener:Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lorg/benews/BeNewsFragList;->mListener:Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;

    invoke-interface {v0, p3}, Lorg/benews/BeNewsFragList$OnFragmentInteractionListener;->onItemPress(I)V

    .line 106
    :cond_0
    return-void
.end method

.method public setEmptyText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "emptyText"    # Ljava/lang/CharSequence;

    .prologue
    .line 115
    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    .line 117
    .local v0, "emptyView":Landroid/view/View;
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 118
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "emptyView":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_0
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "mAdapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 41
    iput-object p1, p0, Lorg/benews/BeNewsFragList;->mAdapter:Landroid/widget/ListAdapter;

    .line 43
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lorg/benews/BeNewsFragList;->mAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 45
    iget-object v0, p0, Lorg/benews/BeNewsFragList;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 47
    :cond_0
    return-void
.end method
