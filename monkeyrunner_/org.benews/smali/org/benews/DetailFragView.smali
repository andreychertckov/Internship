.class public Lorg/benews/DetailFragView;
.super Landroid/support/v4/app/Fragment;
.source "DetailFragView.java"


# static fields
.field public static final str_layout:Ljava/lang/String; = "layoutId"


# instance fields
.field protected content:Landroid/view/View;

.field protected date:Landroid/view/View;

.field protected headline:Landroid/view/View;

.field protected item_content:Ljava/lang/String;

.field protected item_date:Ljava/lang/String;

.field protected item_headline:Ljava/lang/String;

.field protected item_path:Ljava/lang/String;

.field protected item_title:Ljava/lang/String;

.field protected item_type:Ljava/lang/String;

.field protected layoutId:I

.field protected media:Landroid/view/View;

.field protected title:Landroid/view/View;

.field protected view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lorg/benews/DetailFragView;->layoutId:I

    .line 37
    return-void
.end method

.method public static newInstance(Ljava/util/HashMap;)Lorg/benews/DetailFragView;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/benews/DetailFragView;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "news":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 48
    .local v3, "k":Ljava/lang/String;
    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putCharArray(Ljava/lang/String;[C)V

    goto :goto_0

    .line 51
    .end local v3    # "k":Ljava/lang/String;
    :cond_0
    const-string v5, "type"

    invoke-virtual {p0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 52
    .local v4, "type":Ljava/lang/String;
    const/4 v1, 0x0

    .line 53
    .local v1, "f":Lorg/benews/DetailFragView;
    if-eqz v4, :cond_1

    .line 54
    const-string v5, "img"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 55
    new-instance v1, Lorg/benews/DetailFragViewImage;

    .end local v1    # "f":Lorg/benews/DetailFragView;
    invoke-direct {v1}, Lorg/benews/DetailFragViewImage;-><init>()V

    .line 56
    .restart local v1    # "f":Lorg/benews/DetailFragView;
    const-string v5, "layoutId"

    const v6, 0x7f030019

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 62
    invoke-virtual {v1, v0}, Lorg/benews/DetailFragView;->setArguments(Landroid/os/Bundle;)V

    .line 64
    :cond_2
    return-object v1

    .line 58
    :cond_3
    new-instance v1, Lorg/benews/DetailFragView;

    .end local v1    # "f":Lorg/benews/DetailFragView;
    invoke-direct {v1}, Lorg/benews/DetailFragView;-><init>()V

    .restart local v1    # "f":Lorg/benews/DetailFragView;
    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 105
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-super {p0, p3}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 73
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_path:Ljava/lang/String;

    .line 75
    :cond_0
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_type:Ljava/lang/String;

    .line 77
    :cond_1
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_date:Ljava/lang/String;

    .line 79
    :cond_2
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_3

    .line 80
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_title:Ljava/lang/String;

    .line 81
    :cond_3
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "headline"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_4

    .line 82
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "headline"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_headline:Ljava/lang/String;

    .line 83
    :cond_4
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    if-eqz v0, :cond_5

    .line 84
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->item_content:Ljava/lang/String;

    .line 85
    :cond_5
    invoke-virtual {p0}, Lorg/benews/DetailFragView;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "layoutId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/benews/DetailFragView;->layoutId:I

    .line 88
    :cond_6
    iget v0, p0, Lorg/benews/DetailFragView;->layoutId:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    .line 89
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    const v1, 0x7f070049

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->media:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    const v1, 0x7f07002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->title:Landroid/view/View;

    .line 91
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    const v1, 0x7f070047

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->headline:Landroid/view/View;

    .line 92
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    const v1, 0x7f07004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->content:Landroid/view/View;

    .line 93
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/benews/DetailFragView;->date:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lorg/benews/DetailFragView;->view:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 110
    return-void
.end method
