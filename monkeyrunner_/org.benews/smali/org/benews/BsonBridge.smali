.class public Lorg/benews/BsonBridge;
.super Ljava/lang/Object;
.source "BsonBridge.java"


# static fields
.field public static final BSON_TYPE_TEXT:I = 0x0

.field public static final TAG:Ljava/lang/String; = "BsonBridge"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "bson"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getToken(Ljava/lang/String;JLjava/lang/String;)[B
.end method

.method public static getTokenBson(Ljava/lang/String;JLjava/lang/String;)[B
    .locals 2
    .param p0, "imei"    # Ljava/lang/String;
    .param p1, "key"    # J
    .param p3, "cks"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string v0, "BsonBridge"

    const-string v1, "getToken called\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {p0, p1, p2, p3}, Lorg/benews/BsonBridge;->getToken(Ljava/lang/String;JLjava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static native serialize(Ljava/lang/String;Ljava/nio/ByteBuffer;)Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public static serializeBson(Ljava/lang/String;Ljava/nio/ByteBuffer;)Ljava/util/HashMap;
    .locals 2
    .param p0, "baseDir"    # Ljava/lang/String;
    .param p1, "payload"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    const-string v0, "BsonBridge"

    const-string v1, "serialize called\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-static {p0, p1}, Lorg/benews/BsonBridge;->serialize(Ljava/lang/String;Ljava/nio/ByteBuffer;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method
