.class Lorg/benews/BackgroundSocket$SocketAsyncTask;
.super Landroid/os/AsyncTask;
.source "BackgroundSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/benews/BackgroundSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/nio/ByteBuffer;",
        ">;"
    }
.end annotation


# instance fields
.field private final args:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private connectionError:Z

.field private running:Z

.field final synthetic this$0:Lorg/benews/BackgroundSocket;


# direct methods
.method private constructor <init>(Lorg/benews/BackgroundSocket;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "args":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 339
    iput-object p1, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    .line 340
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 327
    iput-boolean v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->running:Z

    .line 329
    iput-boolean v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->connectionError:Z

    .line 341
    iput-object p2, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->args:Ljava/util/HashMap;

    .line 343
    return-void
.end method

.method synthetic constructor <init>(Lorg/benews/BackgroundSocket;Ljava/util/HashMap;Lorg/benews/BackgroundSocket$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/benews/BackgroundSocket;
    .param p2, "x1"    # Ljava/util/HashMap;
    .param p3, "x2"    # Lorg/benews/BackgroundSocket$1;

    .prologue
    .line 323
    invoke-direct {p0, p1, p2}, Lorg/benews/BackgroundSocket$SocketAsyncTask;-><init>(Lorg/benews/BackgroundSocket;Ljava/util/HashMap;)V

    return-void
.end method

.method private createSSLSocket(Ljavax/net/SocketFactory;)Ljava/net/Socket;
    .locals 4
    .param p1, "sf"    # Ljavax/net/SocketFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;,
            Ljava/io/IOException;,
            Ljava/security/KeyStoreException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyManagementException;
        }
    .end annotation

    .prologue
    .line 457
    const-string v2, "46.38.48.178"

    const/16 v3, 0x1bb

    invoke-virtual {p1, v2, v3}, Ljavax/net/SocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/SSLSocket;

    .line 459
    .local v1, "socket":Ljavax/net/ssl/SSLSocket;
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    .line 463
    .local v0, "hv":Ljavax/net/ssl/HostnameVerifier;
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 464
    invoke-direct {p0, v1}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->printServerCertificate(Ljavax/net/ssl/SSLSocket;)V

    .line 465
    invoke-direct {p0, v1}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->printSocketInfo(Ljavax/net/ssl/SSLSocket;)V

    .line 467
    return-object v1
.end method

.method private getSocketFactory()Ljavax/net/SocketFactory;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;,
            Ljava/io/IOException;,
            Ljava/security/KeyStoreException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyManagementException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 424
    const-string v9, "X.509"

    invoke-static {v9}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 425
    .local v3, "cf":Ljava/security/cert/CertificateFactory;
    iget-object v9, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    iget-object v9, v9, Lorg/benews/BackgroundSocket;->assets:Landroid/content/res/AssetManager;

    const-string v10, "server.crt"

    invoke-virtual {v9, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 429
    .local v2, "caInput":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v1

    .line 430
    .local v1, "ca":Ljava/security/cert/Certificate;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ca="

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object v0, v1

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v9, v0

    invoke-virtual {v9}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 436
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v6

    .line 437
    .local v6, "keyStoreType":Ljava/lang/String;
    invoke-static {v6}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v5

    .line 438
    .local v5, "keyStore":Ljava/security/KeyStore;
    invoke-virtual {v5, v12, v12}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 439
    const-string v9, "ca"

    invoke-virtual {v5, v9, v1}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    .line 442
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v8

    .line 443
    .local v8, "tmfAlgorithm":Ljava/lang/String;
    invoke-static {v8}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v7

    .line 444
    .local v7, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v7, v5}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 447
    const-string v9, "TLS"

    invoke-static {v9}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 448
    .local v4, "context":Ljavax/net/ssl/SSLContext;
    invoke-virtual {v7}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v9

    invoke-virtual {v4, v12, v9, v12}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 452
    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v9

    return-object v9

    .line 432
    .end local v1    # "ca":Ljava/security/cert/Certificate;
    .end local v4    # "context":Ljavax/net/ssl/SSLContext;
    .end local v5    # "keyStore":Ljava/security/KeyStore;
    .end local v6    # "keyStoreType":Ljava/lang/String;
    .end local v7    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    .end local v8    # "tmfAlgorithm":Ljava/lang/String;
    :catchall_0
    move-exception v9

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v9
.end method

.method private printServerCertificate(Ljavax/net/ssl/SSLSocket;)V
    .locals 7
    .param p1, "socket"    # Ljavax/net/ssl/SSLSocket;

    .prologue
    .line 473
    :try_start_0
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v4

    invoke-interface {v4}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v3

    .line 475
    .local v3, "serverCerts":[Ljava/security/cert/Certificate;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 476
    aget-object v2, v3, v1

    .line 477
    .local v2, "myCert":Ljava/security/cert/Certificate;
    const-string v4, "BackgroundSocket"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "====Certificate:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "===="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const-string v4, "BackgroundSocket"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-Public Key-\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    const-string v4, "BackgroundSocket"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-Certificate Type-\n "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->println()V
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 483
    .end local v1    # "i":I
    .end local v2    # "myCert":Ljava/security/cert/Certificate;
    .end local v3    # "serverCerts":[Ljava/security/cert/Certificate;
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Ljavax/net/ssl/SSLPeerUnverifiedException;
    const-string v4, "BackgroundSocket"

    const-string v5, "Could not verify peer"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    invoke-virtual {v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;->printStackTrace()V

    .line 486
    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    .line 488
    .end local v0    # "e":Ljavax/net/ssl/SSLPeerUnverifiedException;
    :cond_0
    return-void
.end method

.method private printSocketInfo(Ljavax/net/ssl/SSLSocket;)V
    .locals 4
    .param p1, "s"    # Ljavax/net/ssl/SSLSocket;

    .prologue
    .line 490
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Socket class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Remote address = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Remote port = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Local socket address = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Local address = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Local port = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getLocalPort()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Need client authentication = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getNeedClientAuth()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    .line 502
    .local v0, "ss":Ljavax/net/ssl/SSLSession;
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Cipher suite = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getCipherSuite()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const-string v1, "BackgroundSocket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   Protocol = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    return-void
.end method

.method private publishProgress(I)V
    .locals 0
    .param p1, "read"    # I

    .prologue
    .line 516
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 323
    check-cast p1, [Ljava/util/HashMap;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->doInBackground([Ljava/util/HashMap;)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/util/HashMap;)Ljava/nio/ByteBuffer;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/nio/ByteBuffer;"
        }
    .end annotation

    .prologue
    .line 353
    .local p1, "args":[Ljava/util/HashMap;, "[Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 356
    .local v10, "wrapped":Ljava/nio/ByteBuffer;
    const/4 v11, 0x0

    :try_start_0
    iput-boolean v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->connectionError:Z

    .line 357
    const-string v1, "0"

    .line 358
    .local v1, "cks":Ljava/lang/String;
    array-length v11, p1

    if-lez v11, :cond_0

    .line 359
    const/4 v11, 0x0

    aget-object v11, p1, v11

    const-string v12, "checksum"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 360
    const/4 v11, 0x0

    aget-object v11, p1, v11

    const-string v12, "checksum"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "cks":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 365
    .restart local v1    # "cks":Ljava/lang/String;
    :cond_0
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v11}, Lorg/benews/BackgroundSocket;->access$300(Lorg/benews/BackgroundSocket;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v12}, Lorg/benews/BackgroundSocket;->access$400(Lorg/benews/BackgroundSocket;)J

    move-result-wide v12

    invoke-static {v11, v12, v13, v1}, Lorg/benews/BsonBridge;->getTokenBson(Ljava/lang/String;JLjava/lang/String;)[B

    move-result-object v4

    .line 370
    .local v4, "obj":[B
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v11}, Lorg/benews/BackgroundSocket;->access$500(Lorg/benews/BackgroundSocket;)Ljavax/net/SocketFactory;

    move-result-object v11

    if-nez v11, :cond_1

    .line 371
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-direct {p0}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->getSocketFactory()Ljavax/net/SocketFactory;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/benews/BackgroundSocket;->access$502(Lorg/benews/BackgroundSocket;Ljavax/net/SocketFactory;)Ljavax/net/SocketFactory;

    .line 373
    :cond_1
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    iget-object v12, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v12}, Lorg/benews/BackgroundSocket;->access$500(Lorg/benews/BackgroundSocket;)Ljavax/net/SocketFactory;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->createSSLSocket(Ljavax/net/SocketFactory;)Ljava/net/Socket;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/benews/BackgroundSocket;->access$002(Lorg/benews/BackgroundSocket;Ljava/net/Socket;)Ljava/net/Socket;

    .line 379
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v11}, Lorg/benews/BackgroundSocket;->access$000(Lorg/benews/BackgroundSocket;)Ljava/net/Socket;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 380
    .local v3, "is":Ljava/io/InputStream;
    new-instance v5, Ljava/io/BufferedOutputStream;

    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v11}, Lorg/benews/BackgroundSocket;->access$000(Lorg/benews/BackgroundSocket;)Ljava/net/Socket;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 382
    .local v5, "out":Ljava/io/BufferedOutputStream;
    invoke-virtual {v5, v4}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 383
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V

    .line 384
    const/4 v4, 0x0

    .line 385
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 387
    const/4 v11, 0x4

    new-array v9, v11, [B

    .line 388
    .local v9, "size":[B
    invoke-virtual {v3, v9}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 389
    .local v6, "read":I
    if-lez v6, :cond_2

    .line 390
    invoke-static {v9}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 391
    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 392
    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    .line 393
    .local v8, "s":I
    add-int/lit8 v11, v8, -0x4

    new-array v0, v11, [B

    .line 394
    .local v0, "buffer":[B
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 395
    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 396
    const/4 v11, 0x0

    array-length v12, v9

    invoke-virtual {v10, v9, v11, v12}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 397
    :goto_0
    sub-int v11, v8, v6

    if-lez v11, :cond_2

    .line 398
    invoke-direct {p0, v6}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->publishProgress(I)V

    .line 399
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .line 400
    .local v7, "res":I
    if-lez v7, :cond_2

    .line 401
    const/4 v11, 0x0

    invoke-virtual {v10, v0, v11, v7}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 402
    add-int/2addr v6, v7

    .line 406
    goto :goto_0

    .line 408
    .end local v0    # "buffer":[B
    .end local v7    # "res":I
    .end local v8    # "s":I
    :cond_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 409
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    .line 410
    iget-object v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v11}, Lorg/benews/BackgroundSocket;->access$000(Lorg/benews/BackgroundSocket;)Ljava/net/Socket;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    const/4 v4, 0x0

    .line 417
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 419
    .end local v1    # "cks":Ljava/lang/String;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .end local v6    # "read":I
    .end local v9    # "size":[B
    :goto_1
    return-object v10

    .line 411
    .end local v4    # "obj":[B
    :catch_0
    move-exception v2

    .line 412
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v11, "BackgroundSocket"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const/4 v11, 0x1

    iput-boolean v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->connectionError:Z

    .line 414
    const/4 v11, 0x0

    iput-boolean v11, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->running:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416
    const/4 v4, 0x0

    .line 417
    .restart local v4    # "obj":[B
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_1

    .line 416
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "obj":[B
    :catchall_0
    move-exception v11

    const/4 v4, 0x0

    .line 417
    .restart local v4    # "obj":[B
    invoke-static {}, Ljava/lang/System;->gc()V

    throw v11
.end method

.method public getLast_timestamp()J
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v0}, Lorg/benews/BackgroundSocket;->access$400(Lorg/benews/BackgroundSocket;)J

    move-result-wide v0

    return-wide v0
.end method

.method public isConnectionError()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->connectionError:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 507
    iget-boolean v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->running:Z

    return v0
.end method

.method public noData()Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v0}, Lorg/benews/BackgroundSocket;->access$200(Lorg/benews/BackgroundSocket;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 323
    check-cast p1, Ljava/nio/ByteBuffer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/benews/BackgroundSocket$SocketAsyncTask;->onPostExecute(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method protected onPostExecute(Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1, "result"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 521
    monitor-enter p0

    .line 522
    if-eqz p1, :cond_3

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    if-lez v4, :cond_3

    .line 523
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-virtual {v4}, Lorg/benews/BackgroundSocket;->getDumpFolder()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lorg/benews/BsonBridge;->serializeBson(Ljava/lang/String;Ljava/nio/ByteBuffer;)Ljava/util/HashMap;

    move-result-object v2

    .line 525
    .local v2, "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 526
    const-string v4, "date"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 527
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->args:Ljava/util/HashMap;

    const-string v5, "date"

    const-string v6, "date"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v5, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    const-string v4, "date"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lorg/benews/BackgroundSocket;->access$402(Lorg/benews/BackgroundSocket;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    :try_start_1
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-virtual {v4}, Lorg/benews/BackgroundSocket;->serialise_ts()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535
    :cond_0
    :goto_0
    :try_start_2
    const-string v4, "checksum"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 536
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->args:Ljava/util/HashMap;

    const-string v5, "checksum"

    const-string v6, "checksum"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    const-string v4, "checksum"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 538
    .local v0, "cks":Ljava/lang/String;
    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "path"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 539
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v4}, Lorg/benews/BackgroundSocket;->access$600(Lorg/benews/BackgroundSocket;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-virtual {v4}, Lorg/benews/BackgroundSocket;->saveStauts()V

    .line 541
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-virtual {v4}, Lorg/benews/BackgroundSocket;->updateListeners()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 543
    :try_start_3
    const-string v4, "date"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 544
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->args:Ljava/util/HashMap;

    const-string v5, "date"

    const-string v6, "date"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->args:Ljava/util/HashMap;

    const-string v5, "ok"

    const-string v6, "0"

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 551
    :cond_1
    :goto_1
    :try_start_4
    sget v4, Lorg/benews/BackgroundSocket;->news_n:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lorg/benews/BackgroundSocket;->news_n:I

    .line 555
    .end local v0    # "cks":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lorg/benews/BackgroundSocket;->access$202(Lorg/benews/BackgroundSocket;Z)Z

    .line 556
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 560
    .end local v2    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->running:Z

    .line 561
    monitor-exit p0

    .line 563
    return-void

    .line 531
    .restart local v2    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 532
    .local v3, "x":Ljava/lang/Exception;
    const-string v4, "BackgroundSocket"

    const-string v5, " (onPostExecute): failed to serialize ts "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 561
    .end local v2    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "x":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 548
    .restart local v0    # "cks":Ljava/lang/String;
    .restart local v2    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_1
    move-exception v1

    .line 549
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "BackgroundSocket"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " (onPostExecute): failed to parse "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    invoke-static {v6}, Lorg/benews/BackgroundSocket;->access$400(Lorg/benews/BackgroundSocket;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 558
    .end local v0    # "cks":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ret":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    iget-object v4, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->this$0:Lorg/benews/BackgroundSocket;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lorg/benews/BackgroundSocket;->access$202(Lorg/benews/BackgroundSocket;Z)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/benews/BackgroundSocket$SocketAsyncTask;->running:Z

    .line 348
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 349
    return-void
.end method
