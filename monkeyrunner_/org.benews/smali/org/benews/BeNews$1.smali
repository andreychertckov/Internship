.class Lorg/benews/BeNews$1;
.super Ljava/lang/Object;
.source "BeNews.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/benews/BeNews;->onNewsUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/benews/BeNews;

.field final synthetic val$b:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lorg/benews/BeNews;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    iput-object p2, p0, Lorg/benews/BeNews$1;->val$b:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized run()V
    .locals 4

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    invoke-virtual {v2}, Lorg/benews/BeNews;->isToUpdate()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    iget-object v2, v2, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 75
    invoke-static {}, Lorg/benews/BackgroundSocket;->self()Lorg/benews/BackgroundSocket;

    move-result-object v1

    .line 76
    .local v1, "sucker":Lorg/benews/BackgroundSocket;
    iget-object v2, p0, Lorg/benews/BeNews$1;->val$b:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 77
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/benews/BackgroundSocket;->setRun(Z)V

    .line 78
    const/16 v0, 0x64

    .line 79
    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/benews/BackgroundSocket;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    .line 80
    add-int/lit8 v0, v0, -0x14

    .line 81
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    invoke-virtual {v2, v0}, Lorg/benews/BeNews;->setProgressBar(I)V

    .line 82
    const/4 v2, 0x1

    invoke-static {v2}, Lorg/benews/BackgroundSocket;->Sleep(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 73
    .end local v0    # "i":I
    .end local v1    # "sucker":Lorg/benews/BackgroundSocket;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 84
    .restart local v0    # "i":I
    .restart local v1    # "sucker":Lorg/benews/BackgroundSocket;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/benews/BeNews;->setProgressBar(I)V

    .line 85
    iget-object v2, p0, Lorg/benews/BeNews$1;->val$b:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 87
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    iget-object v2, v2, Lorg/benews/BeNews;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 88
    iget-object v2, p0, Lorg/benews/BeNews$1;->this$0:Lorg/benews/BeNews;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/benews/BeNews;->setToUpdate(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    .end local v1    # "sucker":Lorg/benews/BackgroundSocket;
    :cond_2
    monitor-exit p0

    return-void
.end method
