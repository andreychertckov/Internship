.class public Lorg/benews/DetailFragViewImage;
.super Lorg/benews/DetailFragView;
.source "DetailFragViewImage.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/benews/DetailFragView;-><init>()V

    .line 23
    const-string v0, "DetailFragViewImage"

    iput-object v0, p0, Lorg/benews/DetailFragViewImage;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v10, 0x43480000    # 200.0f

    .line 27
    invoke-super {p0, p1}, Lorg/benews/DetailFragView;->onActivityCreated(Landroid/os/Bundle;)V

    .line 28
    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_path:Ljava/lang/String;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_type:Ljava/lang/String;

    if-eqz v11, :cond_6

    .line 29
    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_type:Ljava/lang/String;

    const-string v12, "img"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 30
    new-instance v6, Ljava/io/File;

    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_path:Ljava/lang/String;

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 31
    .local v6, "imgFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 33
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 34
    .local v9, "myBitmap":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_2

    .line 35
    iget-object v5, p0, Lorg/benews/DetailFragViewImage;->media:Landroid/view/View;

    check-cast v5, Landroid/widget/ImageView;

    .line 36
    .local v5, "imageView":Landroid/widget/ImageView;
    sget v11, Lorg/benews/BitmapHelper;->img_view_limit_high:I

    if-nez v11, :cond_7

    const/high16 v7, 0x44160000    # 600.0f

    .line 37
    .local v7, "it_M":F
    :goto_0
    const/high16 v11, 0x42700000    # 60.0f

    sget v12, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    mul-float v8, v11, v12

    .line 38
    .local v8, "it_m":F
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 39
    .local v4, "h":I
    int-to-float v11, v4

    cmpl-float v11, v11, v7

    if-gtz v11, :cond_0

    int-to-float v11, v4

    cmpg-float v11, v11, v8

    if-gez v11, :cond_1

    .line 40
    :cond_0
    sget v11, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-nez v11, :cond_8

    :goto_1
    float-to-int v10, v10

    invoke-static {v9, v10}, Lorg/benews/BitmapHelper;->scaleToFitHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 41
    :cond_1
    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v4    # "h":I
    .end local v5    # "imageView":Landroid/widget/ImageView;
    .end local v6    # "imgFile":Ljava/io/File;
    .end local v7    # "it_M":F
    .end local v8    # "it_m":F
    .end local v9    # "myBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_2
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->item_title:Ljava/lang/String;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->title:Landroid/view/View;

    if-eqz v10, :cond_3

    .line 49
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->title:Landroid/view/View;

    check-cast v10, Landroid/widget/TextView;

    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_title:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_3
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->item_headline:Ljava/lang/String;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->headline:Landroid/view/View;

    if-eqz v10, :cond_4

    .line 52
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->headline:Landroid/view/View;

    check-cast v10, Landroid/widget/TextView;

    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_headline:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    :cond_4
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->item_content:Ljava/lang/String;

    if-eqz v10, :cond_5

    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->content:Landroid/view/View;

    if-eqz v10, :cond_5

    .line 55
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->content:Landroid/view/View;

    check-cast v10, Landroid/widget/TextView;

    iget-object v11, p0, Lorg/benews/DetailFragViewImage;->item_content:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    :cond_5
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->item_date:Ljava/lang/String;

    if-eqz v10, :cond_6

    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->date:Landroid/view/View;

    if-eqz v10, :cond_6

    .line 59
    :try_start_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 60
    .local v0, "date_f":Ljava/util/Date;
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->item_date:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 61
    .local v2, "epoch":J
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v2

    invoke-virtual {v0, v10, v11}, Ljava/util/Date;->setTime(J)V

    .line 63
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->date:Landroid/view/View;

    check-cast v10, Landroid/widget/TextView;

    sget-object v11, Lorg/benews/BeNewsArrayAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 72
    .end local v0    # "date_f":Ljava/util/Date;
    .end local v2    # "epoch":J
    :cond_6
    :goto_3
    return-void

    .line 36
    .restart local v5    # "imageView":Landroid/widget/ImageView;
    .restart local v6    # "imgFile":Ljava/io/File;
    .restart local v9    # "myBitmap":Landroid/graphics/Bitmap;
    :cond_7
    :try_start_2
    sget v11, Lorg/benews/BitmapHelper;->img_view_limit_high:I

    int-to-float v7, v11

    goto :goto_0

    .line 40
    .restart local v4    # "h":I
    .restart local v7    # "it_M":F
    .restart local v8    # "it_m":F
    :cond_8
    sget v11, Lorg/benews/BitmapHelper;->dp2dpi_factor:F
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    mul-float/2addr v10, v11

    goto :goto_1

    .line 43
    .end local v4    # "h":I
    .end local v5    # "imageView":Landroid/widget/ImageView;
    .end local v7    # "it_M":F
    .end local v8    # "it_m":F
    .end local v9    # "myBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/lang/Exception;
    const-string v10, "DetailFragViewImage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " (onActivityCreated):"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 64
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "imgFile":Ljava/io/File;
    :catch_1
    move-exception v1

    .line 65
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v10, "DetailFragViewImage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid date "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lorg/benews/DetailFragViewImage;->item_date:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v10, p0, Lorg/benews/DetailFragViewImage;->date:Landroid/view/View;

    check-cast v10, Landroid/widget/TextView;

    const-string v11, "--/--/----"

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
