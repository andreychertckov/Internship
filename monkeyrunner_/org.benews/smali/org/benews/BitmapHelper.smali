.class public Lorg/benews/BitmapHelper;
.super Ljava/lang/Object;
.source "BitmapHelper.java"


# static fields
.field public static final IMG_PREVIEW_LIMIT_HIGHT:F = 50.0f

.field public static final IMG_VIEW_LIMIT_HIGHT:F = 150.0f

.field public static density:F

.field public static dp2dpi_factor:F

.field public static img_preview_limit_high:I

.field public static img_view_limit_high:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 10
    sput v1, Lorg/benews/BitmapHelper;->img_preview_limit_high:I

    .line 12
    sput v1, Lorg/benews/BitmapHelper;->img_view_limit_high:I

    .line 13
    sput v0, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    .line 14
    sput v0, Lorg/benews/BitmapHelper;->density:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(F)V
    .locals 2
    .param p0, "density"    # F

    .prologue
    .line 19
    sput p0, Lorg/benews/BitmapHelper;->density:F

    .line 20
    sget v0, Lorg/benews/BitmapHelper;->img_preview_limit_high:I

    if-nez v0, :cond_0

    .line 22
    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v0, p0

    sput v0, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    .line 23
    const/high16 v0, 0x42480000    # 50.0f

    sget v1, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lorg/benews/BitmapHelper;->img_preview_limit_high:I

    .line 24
    const/high16 v0, 0x43160000    # 150.0f

    sget v1, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lorg/benews/BitmapHelper;->img_view_limit_high:I

    .line 26
    :cond_0
    return-void
.end method

.method public static scaleToFill(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 41
    int-to-float v3, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 42
    .local v0, "factorH":F
    int-to-float v3, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v2, v3, v4

    .line 43
    .local v2, "factorW":F
    cmpl-float v3, v0, v2

    if-lez v3, :cond_0

    move v1, v2

    .line 44
    .local v1, "factorToUse":F
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v1

    float-to-int v4, v4

    const/4 v5, 0x0

    invoke-static {p0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3

    .end local v1    # "factorToUse":F
    :cond_0
    move v1, v0

    .line 43
    goto :goto_0
.end method

.method public static scaleToFitHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "height"    # I

    .prologue
    .line 35
    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 36
    .local v0, "factor":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-static {p0, v1, p1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static scaleToFitWidth(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I

    .prologue
    .line 29
    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 30
    .local v0, "factor":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static strechToFill(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 49
    int-to-float v2, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 50
    .local v0, "factorH":F
    int-to-float v2, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 51
    .local v1, "factorW":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method
