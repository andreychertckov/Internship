.class public Lorg/benews/BeNewsArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BeNewsArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final HASH_FIELD_CHECKSUM:Ljava/lang/String; = "checksum"

.field public static final HASH_FIELD_CONTENT:Ljava/lang/String; = "content"

.field public static final HASH_FIELD_DATE:Ljava/lang/String; = "date"

.field public static final HASH_FIELD_HEADLINE:Ljava/lang/String; = "headline"

.field public static final HASH_FIELD_PATH:Ljava/lang/String; = "path"

.field public static final HASH_FIELD_TITLE:Ljava/lang/String; = "title"

.field public static final HASH_FIELD_TYPE:Ljava/lang/String; = "type"

.field private static final LEFT_ALIGNED_VIEW:I = 0x0

.field private static final RIGHT_ALIGNED_VIEW:I = 0x1

.field public static final TAG:Ljava/lang/String; = "BeNewsArrayAdapter"

.field public static final TYPE_AUDIO_DIR:Ljava/lang/String; = "audio"

.field public static final TYPE_HTML_DIR:Ljava/lang/String; = "html"

.field public static final TYPE_IMG_DIR:Ljava/lang/String; = "img"

.field public static final TYPE_TEXT_DIR:Ljava/lang/String; = "text"

.field public static final TYPE_VIDEO_DIR:Ljava/lang/String; = "video"

.field public static final dateFormatter:Ljava/text/SimpleDateFormat;


# instance fields
.field private final context:Landroid/content/Context;

.field private final list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy hh:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/benews/BeNewsArrayAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const v0, 0x7f03001d

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 47
    iput-object p2, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    .line 48
    iput-object p1, p0, Lorg/benews/BeNewsArrayAdapter;->context:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private getCachedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "viewTipe"    # I

    .prologue
    const/4 v2, 0x0

    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    packed-switch p3, :pswitch_data_0

    .line 125
    new-instance v0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;

    .end local v0    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    const v1, 0x7f03001d

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;-><init>(Lorg/benews/BeNewsArrayAdapter;Landroid/view/View;)V

    .line 131
    .restart local v0    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    :goto_0
    return-object v0

    .line 128
    :pswitch_0
    new-instance v0, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;

    .end local v0    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    const v1, 0x7f03001c

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;-><init>(Lorg/benews/BeNewsArrayAdapter;Landroid/view/View;)V

    .restart local v0    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 55
    rem-int/lit8 v12, p1, 0x2

    if-nez v12, :cond_5

    .line 56
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->context:Landroid/content/Context;

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v12, v0, v13}, Lorg/benews/BeNewsArrayAdapter;->getCachedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;

    move-result-object v11

    .line 60
    .local v11, "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    :goto_0
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    if-eqz v12, :cond_4

    .line 61
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    .line 62
    .local v7, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v12, "path"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 63
    .local v9, "path":Ljava/lang/String;
    const-string v12, "type"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 64
    .local v10, "type":Ljava/lang/String;
    if-eqz v9, :cond_4

    if-eqz v10, :cond_4

    .line 65
    const-string v12, "img"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 67
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .local v5, "imgFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 69
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 70
    .local v8, "myBitmap":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_1

    .line 71
    sget v12, Lorg/benews/BitmapHelper;->img_preview_limit_high:I

    if-nez v12, :cond_6

    const/16 v6, 0x64

    .line 72
    .local v6, "it":I
    :goto_1
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    if-le v12, v6, :cond_0

    .line 73
    sget v12, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-nez v12, :cond_7

    const/high16 v12, 0x42400000    # 48.0f

    :goto_2
    float-to-int v12, v12

    invoke-static {v8, v12}, Lorg/benews/BitmapHelper;->scaleToFitHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 75
    :cond_0
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v12, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v5    # "imgFile":Ljava/io/File;
    .end local v6    # "it":I
    .end local v8    # "myBitmap":Landroid/graphics/Bitmap;
    :cond_1
    const-string v12, "title"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 96
    iget-object v13, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->title:Landroid/widget/TextView;

    const-string v12, "title"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    :cond_2
    const-string v12, "headline"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 99
    iget-object v13, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->secondLine:Landroid/widget/TextView;

    const-string v12, "headline"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :cond_3
    const-string v12, "date"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 104
    :try_start_1
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 105
    .local v1, "date":Ljava/util/Date;
    const-string v12, "date"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 106
    .local v3, "epoch":J
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v3

    invoke-virtual {v1, v12, v13}, Ljava/util/Date;->setTime(J)V

    .line 108
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->date:Landroid/widget/TextView;

    sget-object v13, Lorg/benews/BeNewsArrayAdapter;->dateFormatter:Ljava/text/SimpleDateFormat;

    invoke-virtual {v13, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 117
    .end local v1    # "date":Ljava/util/Date;
    .end local v3    # "epoch":J
    .end local v7    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "path":Ljava/lang/String;
    .end local v10    # "type":Ljava/lang/String;
    :cond_4
    :goto_3
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    :goto_4
    return-object v12

    .line 58
    .end local v11    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    :cond_5
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->context:Landroid/content/Context;

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-direct {p0, v12, v0, v13}, Lorg/benews/BeNewsArrayAdapter;->getCachedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;

    move-result-object v11

    .restart local v11    # "viewElements":Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;
    goto/16 :goto_0

    .line 71
    .restart local v5    # "imgFile":Ljava/io/File;
    .restart local v7    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v8    # "myBitmap":Landroid/graphics/Bitmap;
    .restart local v9    # "path":Ljava/lang/String;
    .restart local v10    # "type":Ljava/lang/String;
    :cond_6
    :try_start_2
    sget v6, Lorg/benews/BitmapHelper;->img_preview_limit_high:I

    goto/16 :goto_1

    .line 73
    .restart local v6    # "it":I
    :cond_7
    const/high16 v12, 0x42400000    # 48.0f

    sget v13, Lorg/benews/BitmapHelper;->dp2dpi_factor:F

    mul-float/2addr v12, v13

    goto :goto_2

    .line 79
    .end local v6    # "it":I
    .end local v8    # "myBitmap":Landroid/graphics/Bitmap;
    :cond_8
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 80
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {p0}, Lorg/benews/BeNewsArrayAdapter;->notifyDataSetChanged()V

    .line 83
    :cond_9
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4

    .line 85
    .end local v5    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 87
    .local v2, "e":Ljava/lang/Exception;
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 88
    iget-object v12, p0, Lorg/benews/BeNewsArrayAdapter;->list:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 89
    invoke-virtual {p0}, Lorg/benews/BeNewsArrayAdapter;->notifyDataSetChanged()V

    .line 91
    :cond_a
    const-string v12, "BeNewsArrayAdapter"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " (getView):"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->view:Landroid/view/View;

    goto :goto_4

    .line 109
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 110
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v13, "BeNewsArrayAdapter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Invalid date "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v12, "date"

    invoke-virtual {v7, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v13, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v12, v11, Lorg/benews/BeNewsArrayAdapter$ViewHolderItem;->date:Landroid/widget/TextView;

    const-string v13, "--/--/----"

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method
