
function DeviceInformation() {}

DeviceInformation.prototype.get = function(successFunc, failFunc) {
    cordova.exec(successFunc, failFunc, "DeviceInformation","get",[]);
};

cordova.addConstructor(function()  {    
    if(!window.plugins)
    {
    window.plugins = {};
    }

    // shim to work in 1.5 and 1.6
    if (!window.Cordova) {
    window.Cordova = cordova;
    };

    window.plugins.DeviceInformation = new DeviceInformation();
    });
