.class public Lin/edelworks/sharedpreferences/Sharedpreferences;
.super Lorg/apache/cordova/api/CordovaPlugin;
.source "Sharedpreferences.java"


# static fields
.field public static final CLEAR:Ljava/lang/String; = "clear"

.field public static final GET_BOOLEAN:Ljava/lang/String; = "getBoolean"

.field public static final GET_FLOAT:Ljava/lang/String; = "getFloat"

.field public static final GET_INT:Ljava/lang/String; = "getInt"

.field public static final GET_LONG:Ljava/lang/String; = "getLong"

.field public static final GET_SHARED_PREFERENCES:Ljava/lang/String; = "getSharedPreferences"

.field public static final GET_STRING:Ljava/lang/String; = "getString"

.field public static final MODE_ARRAY:[Ljava/lang/String;

.field public static PREF_FILE:Ljava/lang/String; = null

.field public static final PUT_BOOLEAN:Ljava/lang/String; = "putBoolean"

.field public static final PUT_FLOAT:Ljava/lang/String; = "putFloat"

.field public static final PUT_INT:Ljava/lang/String; = "putInt"

.field public static final PUT_LONG:Ljava/lang/String; = "putLong"

.field public static final PUT_STRING:Ljava/lang/String; = "putString"

.field public static final REMOVE:Ljava/lang/String; = "remove"

.field public static final SHARED_PREFERENCES:Ljava/lang/String; = "SharedPreferences"


# instance fields
.field SharedPref:Landroid/content/SharedPreferences;

.field editor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-string v0, ""

    sput-object v0, Lin/edelworks/sharedpreferences/Sharedpreferences;->PREF_FILE:Ljava/lang/String;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MODE_APPEND"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MODE_PRIVATE"

    aput-object v2, v0, v1

    sput-object v0, Lin/edelworks/sharedpreferences/Sharedpreferences;->MODE_ARRAY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/apache/cordova/api/CordovaPlugin;-><init>()V

    return-void
.end method

.method public static in_array([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "haystack"    # [Ljava/lang/String;
    .param p1, "needle"    # Ljava/lang/String;

    .prologue
    .line 202
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 207
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 203
    :cond_0
    aget-object v1, p0, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    const/4 v1, 0x1

    goto :goto_1

    .line 202
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/String;Lorg/json/JSONArray;Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 10
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "args"    # Lorg/json/JSONArray;
    .param p3, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    const-string v6, "getSharedPreferences"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 37
    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lin/edelworks/sharedpreferences/Sharedpreferences;->PREF_FILE:Ljava/lang/String;

    .line 38
    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 39
    .local v3, "modeType":Ljava/lang/String;
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->cordova:Lorg/apache/cordova/api/CordovaInterface;

    invoke-interface {v6}, Lorg/apache/cordova/api/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 40
    .local v1, "context":Landroid/content/Context;
    sget-object v6, Lin/edelworks/sharedpreferences/Sharedpreferences;->MODE_ARRAY:[Ljava/lang/String;

    invoke-static {v6, v3}, Lin/edelworks/sharedpreferences/Sharedpreferences;->in_array([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 41
    const-string v6, "MODE_APPEND"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 43
    :try_start_0
    sget-object v6, Lin/edelworks/sharedpreferences/Sharedpreferences;->PREF_FILE:Ljava/lang/String;

    const v7, 0x8000

    invoke-virtual {v1, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :cond_0
    :goto_0
    const-string v4, "Shared Preferences Created"

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 197
    .end local v1    # "context":Landroid/content/Context;
    .end local v3    # "modeType":Ljava/lang/String;
    :goto_1
    return v4

    .line 44
    .restart local v1    # "context":Landroid/content/Context;
    .restart local v3    # "modeType":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 45
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error creating Shared Preferences"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 48
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v6, "MODE_PRIVATE"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 50
    :try_start_1
    sget-object v6, Lin/edelworks/sharedpreferences/Sharedpreferences;->PREF_FILE:Ljava/lang/String;

    const v7, 0x8000

    invoke-virtual {v1, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 51
    :catch_1
    move-exception v2

    .line 52
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error creating Shared Preferences"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 59
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v5, "Invalid Mode provided"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 64
    .end local v1    # "context":Landroid/content/Context;
    .end local v3    # "modeType":Ljava/lang/String;
    :cond_3
    const-string v6, "putString"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 65
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 67
    :try_start_2
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 68
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 73
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Value "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to Preferences key "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 74
    goto/16 :goto_1

    .line 69
    :catch_2
    move-exception v2

    .line 70
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error editing Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 75
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v6, "getString"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 78
    :try_start_3
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 79
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "KeyVal":Ljava/lang/String;
    invoke-virtual {p3, v0}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 81
    goto/16 :goto_1

    .line 83
    .end local v0    # "KeyVal":Ljava/lang/String;
    :cond_5
    const-string v5, "No data"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    .line 86
    :catch_3
    move-exception v2

    .line 87
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could Not Retreive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 90
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_6
    const-string v6, "putBoolean"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 91
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 93
    :try_start_4
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Lorg/json/JSONArray;->getBoolean(I)Z

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 94
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Value "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getBoolean(I)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to Preferences key "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 100
    goto/16 :goto_1

    .line 95
    :catch_4
    move-exception v2

    .line 96
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error editing Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getBoolean(I)Z

    move-result v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 101
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_7
    const-string v6, "getBoolean"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 104
    :try_start_5
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 105
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 106
    .local v0, "KeyVal":Ljava/lang/Boolean;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 107
    const-string v6, "1"

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    :goto_2
    move v4, v5

    .line 111
    goto/16 :goto_1

    .line 109
    :cond_8
    const-string v6, "0"

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    .line 116
    .end local v0    # "KeyVal":Ljava/lang/Boolean;
    :catch_5
    move-exception v2

    .line 117
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could Not Retreive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 113
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    :try_start_6
    const-string v5, "No data"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    .line 120
    :cond_a
    const-string v6, "putInt"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 121
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 123
    :try_start_7
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 124
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 129
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Value "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to Preferences key "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 130
    goto/16 :goto_1

    .line 125
    :catch_6
    move-exception v2

    .line 126
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error editing Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getInt(I)I

    move-result v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 131
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_b
    const-string v6, "getInt"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 134
    :try_start_8
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 135
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 136
    .local v0, "KeyVal":Ljava/lang/Integer;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 137
    goto/16 :goto_1

    .line 139
    .end local v0    # "KeyVal":Ljava/lang/Integer;
    :cond_c
    const-string v5, "No data"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_1

    .line 142
    :catch_7
    move-exception v2

    .line 143
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could Not Retreive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 146
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_d
    const-string v6, "putLong"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 147
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 149
    :try_start_9
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 150
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 155
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Added Value "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to Preferences key "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 156
    goto/16 :goto_1

    .line 151
    :catch_8
    move-exception v2

    .line 152
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error editing Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 157
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_e
    const-string v6, "getLong"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 160
    :try_start_a
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 161
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 162
    .local v0, "KeyVal":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 163
    goto/16 :goto_1

    .line 165
    .end local v0    # "KeyVal":Ljava/lang/Long;
    :cond_f
    const-string v5, "No data"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_9

    goto/16 :goto_1

    .line 168
    :catch_9
    move-exception v2

    .line 169
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could Not Retreive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 172
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_10
    const-string v6, "remove"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 173
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 175
    :try_start_b
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 176
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    .line 181
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Removed Value from Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 182
    goto/16 :goto_1

    .line 177
    :catch_a
    move-exception v2

    .line 178
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error editing Key "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 183
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_11
    const-string v6, "clear"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 184
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->SharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iput-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 186
    :try_start_c
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 187
    iget-object v6, p0, Lin/edelworks/sharedpreferences/Sharedpreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b

    .line 192
    const-string v4, "Cleared preference File "

    invoke-virtual {p3, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v4, v5

    .line 194
    goto/16 :goto_1

    .line 188
    :catch_b
    move-exception v2

    .line 189
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could Not Clear Shared preference File "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 196
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_12
    const-string v5, "Invalid Action"

    invoke-virtual {p3, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
