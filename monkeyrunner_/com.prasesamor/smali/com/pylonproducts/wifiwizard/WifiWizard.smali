.class public Lcom/pylonproducts/wifiwizard/WifiWizard;
.super Lorg/apache/cordova/api/CordovaPlugin;
.source "WifiWizard.java"


# static fields
.field private static final ADD_NETWORK:Ljava/lang/String; = "addNetwork"

.field private static final CONNECT_NETWORK:Ljava/lang/String; = "connectNetwork"

.field private static final DISCONNECT:Ljava/lang/String; = "disconnect"

.field private static final DISCONNECT_NETWORK:Ljava/lang/String; = "disconnectNetwork"

.field private static final GET_CONNECTED_SSID:Ljava/lang/String; = "getConnectedSSID"

.field private static final GET_SCAN_RESULTS:Ljava/lang/String; = "getScanResults"

.field private static final IS_WIFI_ENABLED:Ljava/lang/String; = "isWifiEnabled"

.field private static final LIST_NETWORKS:Ljava/lang/String; = "listNetworks"

.field private static final REMOVE_NETWORK:Ljava/lang/String; = "removeNetwork"

.field private static final SET_WIFI_ENABLED:Ljava/lang/String; = "setWifiEnabled"

.field private static final START_SCAN:Ljava/lang/String; = "startScan"

.field private static final TAG:Ljava/lang/String; = "WifiWizard"


# instance fields
.field private callbackContext:Lorg/apache/cordova/api/CallbackContext;

.field private wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/apache/cordova/api/CordovaPlugin;-><init>()V

    return-void
.end method

.method private addNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z
    .locals 10
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .param p2, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v9, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 92
    new-instance v4, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v4}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 94
    .local v4, "wifi":Landroid/net/wifi/WifiConfiguration;
    const-string v7, "WifiWizard"

    const-string v8, "WifiWizard: addNetwork entered."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const/4 v7, 0x1

    :try_start_0
    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "authType":Ljava/lang/String;
    const-string v7, "WPA"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 107
    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "newSSID":Ljava/lang/String;
    iput-object v3, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 109
    const/4 v7, 0x2

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "newPass":Ljava/lang/String;
    iput-object v2, v4, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 112
    const/4 v7, 0x2

    iput v7, v4, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 113
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 114
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 115
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 116
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 117
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 118
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 120
    invoke-direct {p0, v3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->ssidToNetworkId(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 122
    iget v7, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne v7, v9, :cond_0

    .line 123
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7, v4}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    .line 124
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " successfully added."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 131
    :goto_0
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 169
    .end local v0    # "authType":Ljava/lang/String;
    .end local v2    # "newPass":Ljava/lang/String;
    .end local v3    # "newSSID":Ljava/lang/String;
    :goto_1
    return v5

    .line 127
    .restart local v0    # "authType":Ljava/lang/String;
    .restart local v2    # "newPass":Ljava/lang/String;
    .restart local v3    # "newSSID":Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7, v4}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    .line 128
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " successfully updated."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 166
    .end local v0    # "authType":Ljava/lang/String;
    .end local v2    # "newPass":Ljava/lang/String;
    .end local v3    # "newSSID":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 167
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 168
    const-string v5, "WifiWizard"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 169
    goto :goto_1

    .line 134
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "authType":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v7, "WEP"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 136
    const-string v5, "WifiWizard"

    const-string v7, "WEP unsupported."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const-string v5, "WEP unsupported"

    invoke-virtual {p1, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    move v5, v6

    .line 138
    goto :goto_1

    .line 140
    :cond_2
    const-string v7, "NONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 141
    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 142
    .restart local v3    # "newSSID":Ljava/lang/String;
    iput-object v3, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 143
    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 145
    invoke-direct {p0, v3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->ssidToNetworkId(Ljava/lang/String;)I

    move-result v7

    iput v7, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 147
    iget v7, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne v7, v9, :cond_3

    .line 148
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7, v4}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    .line 149
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " successfully added."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 156
    :goto_2
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    goto/16 :goto_1

    .line 152
    :cond_3
    iget-object v7, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7, v4}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    .line 153
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " successfully updated."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    goto :goto_2

    .line 161
    .end local v3    # "newSSID":Ljava/lang/String;
    :cond_4
    const-string v5, "WifiWizard"

    const-string v7, "Wifi Authentication Type Not Supported."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Wifi Authentication Type Not Supported: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v5, v6

    .line 163
    goto/16 :goto_1
.end method

.method private connectNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z
    .locals 7
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .param p2, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 222
    const-string v5, "WifiWizard"

    const-string v6, "WifiWizard: connectNetwork entered."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-direct {p0, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->validateData(Lorg/json/JSONArray;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 224
    const-string v4, "WifiWizard: connectNetwork invalid data"

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 225
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: connectNetwork invalid data."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :goto_0
    return v3

    .line 228
    :cond_0
    const-string v2, ""

    .line 231
    .local v2, "ssidToConnect":Ljava/lang/String;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p2, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 239
    invoke-direct {p0, v2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->ssidToNetworkId(Ljava/lang/String;)I

    move-result v1

    .line 241
    .local v1, "networkIdToConnect":I
    if-lez v1, :cond_1

    .line 244
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v1}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    .line 245
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v1, v4}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    .line 246
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Network "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " connected!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    move v3, v4

    .line 247
    goto :goto_0

    .line 233
    .end local v1    # "networkIdToConnect":I
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 235
    const-string v4, "WifiWizard"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 250
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "networkIdToConnect":I
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Network "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 251
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: Network not found to connect."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private disconnect(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 2
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 302
    const-string v0, "WifiWizard"

    const-string v1, "WifiWizard: disconnect entered."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->disconnect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "Disconnected from current network"

    invoke-virtual {p1, v0}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 305
    const/4 v0, 0x1

    .line 308
    :goto_0
    return v0

    .line 307
    :cond_0
    const-string v0, "Unable to disconnect from the current network"

    invoke-virtual {p1, v0}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 308
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private disconnectNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z
    .locals 6
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .param p2, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v3, 0x0

    .line 264
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: disconnectNetwork entered."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-direct {p0, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->validateData(Lorg/json/JSONArray;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 266
    const-string v4, "WifiWizard: disconnectNetwork invalid data"

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 267
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: disconnectNetwork invalid data"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :goto_0
    return v3

    .line 270
    :cond_0
    const-string v2, ""

    .line 273
    .local v2, "ssidToDisconnect":Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 281
    invoke-direct {p0, v2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->ssidToNetworkId(Ljava/lang/String;)I

    move-result v1

    .line 283
    .local v1, "networkIdToDisconnect":I
    if-lez v1, :cond_1

    .line 284
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v1}, Landroid/net/wifi/WifiManager;->disableNetwork(I)Z

    .line 285
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " disconnected!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 286
    const/4 v3, 0x1

    goto :goto_0

    .line 275
    .end local v1    # "networkIdToDisconnect":I
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 277
    const-string v4, "WifiWizard"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "networkIdToDisconnect":I
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Network "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 290
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: Network not found to disconnect."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getConnectedSSID(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 4
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 385
    const-string v3, "Wifi is disabled"

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 405
    :goto_0
    return v2

    .line 389
    :cond_0
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 391
    .local v0, "info":Landroid/net/wifi/WifiInfo;
    if-nez v0, :cond_1

    .line 392
    const-string v3, "Unable to read wifi info"

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 396
    :cond_1
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "ssid":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 398
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    .line 399
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 400
    const-string v3, "SSID is empty"

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 404
    :cond_3
    invoke-virtual {p1, v1}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 405
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getScanResults(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 9
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 342
    iget-object v5, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    .line 344
    .local v4, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 346
    .local v2, "returnList":Lorg/json/JSONArray;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 357
    invoke-virtual {p1, v2}, Lorg/apache/cordova/api/CallbackContext;->success(Lorg/json/JSONArray;)V

    .line 358
    const/4 v5, 0x1

    return v5

    .line 346
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/ScanResult;

    .line 347
    .local v3, "scan":Landroid/net/wifi/ScanResult;
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 349
    .local v1, "lvl":Lorg/json/JSONObject;
    :try_start_0
    const-string v6, "level"

    iget v7, v3, Landroid/net/wifi/ScanResult;->level:I

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v7

    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 350
    const-string v6, "SSID"

    iget-object v7, v3, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 351
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private isWifiEnabled(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 3
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 416
    const-string v1, "WifiWizard"

    const-string v2, "WifiWizard: isWifiEnabled entered."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v1, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    .line 418
    .local v0, "isEnabled":Z
    if-eqz v0, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {p1, v1}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 419
    return v0

    .line 418
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method private listNetworks(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 5
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 321
    const-string v3, "WifiWizard"

    const-string v4, "WifiWizard: listNetworks entered."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v2

    .line 324
    .local v2, "wifiList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 326
    .local v0, "returnList":Lorg/json/JSONArray;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 330
    invoke-virtual {p1, v0}, Lorg/apache/cordova/api/CallbackContext;->success(Lorg/json/JSONArray;)V

    .line 331
    const/4 v3, 0x1

    return v3

    .line 326
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    .line 327
    .local v1, "wifi":Landroid/net/wifi/WifiConfiguration;
    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0
.end method

.method private removeNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z
    .locals 6
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .param p2, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v3, 0x0

    .line 181
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: removeNetwork entered."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-direct {p0, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->validateData(Lorg/json/JSONArray;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 184
    const-string v4, "WifiWizard: removeNetwork data invalid"

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 185
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: removeNetwork data invalid"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_0
    return v3

    .line 191
    :cond_0
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, "ssidToDisconnect":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->ssidToNetworkId(Ljava/lang/String;)I

    move-result v1

    .line 195
    .local v1, "networkIdToRemove":I
    if-ltz v1, :cond_1

    .line 196
    iget-object v4, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, v1}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 197
    iget-object v4, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 198
    const-string v4, "Network removed."

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->success(Ljava/lang/String;)V

    .line 199
    const/4 v3, 0x1

    goto :goto_0

    .line 202
    :cond_1
    const-string v4, "Network not found."

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 203
    const-string v4, "WifiWizard"

    const-string v5, "WifiWizard: Network not found, can\'t remove."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    .end local v1    # "networkIdToRemove":I
    .end local v2    # "ssidToDisconnect":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 209
    const-string v4, "WifiWizard"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setWifiEnabled(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z
    .locals 5
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .param p2, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v2, 0x0

    .line 445
    invoke-direct {p0, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->validateData(Lorg/json/JSONArray;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 446
    const-string v3, "WifiWizard: disconnectNetwork invalid data"

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 447
    const-string v3, "WifiWizard"

    const-string v4, "WifiWizard: disconnectNetwork invalid data"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :goto_0
    return v2

    .line 450
    :cond_0
    const-string v1, ""

    .line 452
    .local v1, "status":Ljava/lang/String;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 460
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    const-string v4, "true"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 461
    invoke-virtual {p1}, Lorg/apache/cordova/api/CallbackContext;->success()V

    .line 462
    const/4 v2, 0x1

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 456
    const-string v3, "WifiWizard"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v3, "Cannot enable wifi"

    invoke-virtual {p1, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private ssidToNetworkId(Ljava/lang/String;)I
    .locals 5
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 428
    iget-object v3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    .line 429
    .local v0, "currentNetworks":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    const/4 v1, -0x1

    .line 432
    .local v1, "networkId":I
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 438
    return v1

    .line 432
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    .line 433
    .local v2, "test":Landroid/net/wifi/WifiConfiguration;
    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 434
    iget v1, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    goto :goto_0
.end method

.method private startScan(Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 1
    .param p1, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 368
    iget-object v0, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p1}, Lorg/apache/cordova/api/CallbackContext;->success()V

    .line 370
    const/4 v0, 0x1

    .line 373
    :goto_0
    return v0

    .line 372
    :cond_0
    const-string v0, "Scan failed"

    invoke-virtual {p1, v0}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    .line 373
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private validateData(Lorg/json/JSONArray;)Z
    .locals 4
    .param p1, "data"    # Lorg/json/JSONArray;

    .prologue
    const/4 v1, 0x0

    .line 471
    if-eqz p1, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 472
    :cond_0
    iget-object v2, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->callbackContext:Lorg/apache/cordova/api/CallbackContext;

    const-string v3, "Data is null."

    invoke-virtual {v2, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :goto_0
    return v1

    .line 475
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->callbackContext:Lorg/apache/cordova/api/CallbackContext;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/String;Lorg/json/JSONArray;Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONArray;
    .param p3, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object p3, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->callbackContext:Lorg/apache/cordova/api/CallbackContext;

    .line 50
    const-string v1, "isWifiEnabled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->isWifiEnabled(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    .line 79
    :goto_0
    return v0

    .line 52
    :cond_0
    const-string v1, "setWifiEnabled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    invoke-direct {p0, p3, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->setWifiEnabled(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z

    move-result v0

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 55
    const-string v1, "Wifi is not enabled."

    invoke-virtual {p3, v1}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_2
    const-string v1, "addNetwork"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    invoke-direct {p0, p3, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->addNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_3
    const-string v1, "removeNetwork"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 60
    invoke-direct {p0, p3, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->removeNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z

    move-result v0

    goto :goto_0

    .line 61
    :cond_4
    const-string v1, "connectNetwork"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 62
    invoke-direct {p0, p3, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->connectNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z

    move-result v0

    goto :goto_0

    .line 63
    :cond_5
    const-string v1, "disconnectNetwork"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 64
    invoke-direct {p0, p3, p2}, Lcom/pylonproducts/wifiwizard/WifiWizard;->disconnectNetwork(Lorg/apache/cordova/api/CallbackContext;Lorg/json/JSONArray;)Z

    move-result v0

    goto :goto_0

    .line 65
    :cond_6
    const-string v1, "listNetworks"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 66
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->listNetworks(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    goto :goto_0

    .line 67
    :cond_7
    const-string v1, "startScan"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 68
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->startScan(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    goto :goto_0

    .line 69
    :cond_8
    const-string v1, "getScanResults"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 70
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->getScanResults(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    goto :goto_0

    .line 71
    :cond_9
    const-string v1, "disconnect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 72
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->disconnect(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    goto/16 :goto_0

    .line 73
    :cond_a
    const-string v1, "getConnectedSSID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 74
    invoke-direct {p0, p3}, Lcom/pylonproducts/wifiwizard/WifiWizard;->getConnectedSSID(Lorg/apache/cordova/api/CallbackContext;)Z

    move-result v0

    goto/16 :goto_0

    .line 76
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incorrect action parameter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lorg/apache/cordova/api/CallbackContext;->error(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public initialize(Lorg/apache/cordova/api/CordovaInterface;Lorg/apache/cordova/CordovaWebView;)V
    .locals 2
    .param p1, "cordova"    # Lorg/apache/cordova/api/CordovaInterface;
    .param p2, "webView"    # Lorg/apache/cordova/CordovaWebView;

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lorg/apache/cordova/api/CordovaPlugin;->initialize(Lorg/apache/cordova/api/CordovaInterface;Lorg/apache/cordova/CordovaWebView;)V

    .line 42
    invoke-interface {p1}, Lorg/apache/cordova/api/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/pylonproducts/wifiwizard/WifiWizard;->wifiManager:Landroid/net/wifi/WifiManager;

    .line 43
    return-void
.end method
