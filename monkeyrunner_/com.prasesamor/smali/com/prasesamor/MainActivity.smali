.class public Lcom/prasesamor/MainActivity;
.super Lorg/apache/cordova/DroidGap;
.source "MainActivity.java"


# static fields
.field public static act:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/apache/cordova/DroidGap;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lorg/apache/cordova/DroidGap;->onCreate(Landroid/os/Bundle;)V

    .line 17
    sput-object p0, Lcom/prasesamor/MainActivity;->act:Landroid/app/Activity;

    .line 18
    invoke-virtual {p0}, Lcom/prasesamor/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 19
    invoke-virtual {p0}, Lcom/prasesamor/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 20
    const-string v0, "file:///android_asset/www/index.html"

    invoke-super {p0, v0}, Lorg/apache/cordova/DroidGap;->loadUrl(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 25
    invoke-super {p0}, Lorg/apache/cordova/DroidGap;->onPause()V

    .line 26
    new-instance v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin;

    invoke-direct {v1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin;-><init>()V

    .line 28
    .local v1, "sms":Linfo/asankan/phonegap/smsplugin/SmsPlugin;
    :try_start_0
    const-string v2, "STOP_RECEIVE_SMS"

    const-string v3, "[]"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->execute(Ljava/lang/String;Ljava/lang/String;Lorg/apache/cordova/api/CallbackContext;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
