.class public Linfo/asankan/phonegap/smsplugin/SmsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsReceiver.java"


# static fields
.field public static final SMS_EXTRA_NAME:Ljava/lang/String; = "pdus"


# instance fields
.field private broadcast:Z

.field private callback_receive:Lorg/apache/cordova/api/CallbackContext;

.field private isReceiving:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->isReceiving:Z

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->broadcast:Z

    .line 17
    return-void
.end method


# virtual methods
.method public broadcast(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->broadcast:Z

    .line 66
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 29
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 31
    const-string v7, "pdus"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    .line 33
    .local v5, "smsExtra":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, v5

    if-lt v2, v7, :cond_1

    .line 60
    .end local v2    # "i":I
    .end local v5    # "smsExtra":[Ljava/lang/Object;
    :cond_0
    return-void

    .line 35
    .restart local v2    # "i":I
    .restart local v5    # "smsExtra":[Ljava/lang/Object;
    :cond_1
    aget-object v7, v5, v2

    check-cast v7, [B

    invoke-static {v7}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v4

    .line 37
    .local v4, "sms":Landroid/telephony/SmsMessage;
    iget-boolean v7, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->isReceiving:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    if-eqz v7, :cond_2

    .line 39
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "formattedMsg":Ljava/lang/String;
    new-instance v3, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->OK:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v3, v7, v1}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;Ljava/lang/String;)V

    .line 41
    .local v3, "result":Lorg/apache/cordova/api/PluginResult;
    invoke-virtual {v3, v9}, Lorg/apache/cordova/api/PluginResult;->setKeepCallback(Z)V

    .line 42
    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    invoke-virtual {v7, v3}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 46
    .end local v1    # "formattedMsg":Ljava/lang/String;
    .end local v3    # "result":Lorg/apache/cordova/api/PluginResult;
    :cond_2
    iget-boolean v7, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->isReceiving:Z

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->broadcast:Z

    if-nez v7, :cond_3

    .line 49
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 50
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "address"

    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v7, "read"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 52
    const-string v7, "body"

    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "content://sms/inbox"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 54
    invoke-virtual {p0}, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->abortBroadcast()V

    .line 33
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startReceiving(Lorg/apache/cordova/api/CallbackContext;)V
    .locals 1
    .param p1, "ctx"    # Lorg/apache/cordova/api/CallbackContext;

    .prologue
    .line 70
    iput-object p1, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->isReceiving:Z

    .line 72
    return-void
.end method

.method public stopReceiving()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->isReceiving:Z

    .line 78
    return-void
.end method
