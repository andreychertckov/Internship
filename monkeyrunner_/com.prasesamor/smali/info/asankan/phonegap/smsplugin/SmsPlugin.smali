.class public Linfo/asankan/phonegap/smsplugin/SmsPlugin;
.super Lorg/apache/cordova/api/CordovaPlugin;
.source "SmsPlugin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$info$asankan$phonegap$smsplugin$SmsPlugin$ActionType:[I


# instance fields
.field private callback_receive:Lorg/apache/cordova/api/CallbackContext;

.field private isReceiving:Z

.field private pluginResult:Lorg/apache/cordova/api/PluginResult;

.field private result:Z

.field private smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

.field private smsSender:Linfo/asankan/phonegap/smsplugin/SmsSender;


# direct methods
.method static synthetic $SWITCH_TABLE$info$asankan$phonegap$smsplugin$SmsPlugin$ActionType()[I
    .locals 3

    .prologue
    .line 13
    sget-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->$SWITCH_TABLE$info$asankan$phonegap$smsplugin$SmsPlugin$ActionType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->values()[Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->HAS_SMS_POSSIBILITY:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-virtual {v1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-virtual {v1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->SEND_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-virtual {v1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->STOP_RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-virtual {v1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->$SWITCH_TABLE$info$asankan$phonegap$smsplugin$SmsPlugin$ActionType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Lorg/apache/cordova/api/CordovaPlugin;-><init>()V

    .line 22
    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->isReceiving:Z

    .line 23
    iput-boolean v0, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    .line 13
    return-void
.end method


# virtual methods
.method public execute(Ljava/lang/String;Lorg/json/JSONArray;Lorg/apache/cordova/api/CallbackContext;)Z
    .locals 10
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "args"    # Lorg/json/JSONArray;
    .param p3, "callbackContext"    # Lorg/apache/cordova/api/CallbackContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 29
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 31
    invoke-static {}, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->$SWITCH_TABLE$info$asankan$phonegap$smsplugin$SmsPlugin$ActionType()[I

    move-result-object v6

    invoke-static {p1}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->valueOf(Ljava/lang/String;)Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    move-result-object v7

    invoke-virtual {v7}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 116
    iput-boolean v9, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    .line 118
    :goto_0
    iget-boolean v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    return v6

    .line 34
    :pswitch_0
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p2, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 35
    .local v5, "phoneNumber":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-virtual {p2, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "message":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-virtual {p2, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 37
    .local v4, "method":Ljava/lang/String;
    new-instance v6, Linfo/asankan/phonegap/smsplugin/SmsSender;

    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->cordova:Lorg/apache/cordova/api/CordovaInterface;

    invoke-interface {v7}, Lorg/apache/cordova/api/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, v7}, Linfo/asankan/phonegap/smsplugin/SmsSender;-><init>(Landroid/app/Activity;)V

    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsSender:Linfo/asankan/phonegap/smsplugin/SmsSender;

    .line 38
    const-string v6, "INTENT"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 39
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsSender:Linfo/asankan/phonegap/smsplugin/SmsSender;

    invoke-virtual {v6, v5, v3}, Linfo/asankan/phonegap/smsplugin/SmsSender;->invokeSMSIntent(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->NO_RESULT:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 45
    :goto_1
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->OK:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 46
    const/4 v6, 0x1

    iput-boolean v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 48
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "method":Ljava/lang/String;
    .end local v5    # "phoneNumber":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 49
    .local v1, "ex":Lorg/json/JSONException;
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->JSON_EXCEPTION:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    goto :goto_0

    .line 42
    .end local v1    # "ex":Lorg/json/JSONException;
    .restart local v3    # "message":Ljava/lang/String;
    .restart local v4    # "method":Ljava/lang/String;
    .restart local v5    # "phoneNumber":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsSender:Linfo/asankan/phonegap/smsplugin/SmsSender;

    invoke-virtual {v6, v5, v3}, Linfo/asankan/phonegap/smsplugin/SmsSender;->sendSMS(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 53
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "method":Ljava/lang/String;
    .end local v5    # "phoneNumber":Ljava/lang/String;
    :pswitch_1
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->cordova:Lorg/apache/cordova/api/CordovaInterface;

    invoke-interface {v6}, Lorg/apache/cordova/api/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 54
    .local v0, "ctx":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "android.hardware.telephony"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 55
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->OK:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7, v8}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;Z)V

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 59
    :goto_2
    iput-boolean v8, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    goto :goto_0

    .line 57
    :cond_1
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->OK:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7, v9}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;Z)V

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    goto :goto_2

    .line 64
    .end local v0    # "ctx":Landroid/app/Activity;
    :pswitch_2
    iget-boolean v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->isReceiving:Z

    if-eqz v6, :cond_2

    .line 66
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->NO_RESULT:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    .line 67
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {v6, v9}, Lorg/apache/cordova/api/PluginResult;->setKeepCallback(Z)V

    .line 68
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {v6, v7}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 72
    :cond_2
    iput-boolean v8, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->isReceiving:Z

    .line 74
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    if-nez v6, :cond_3

    .line 75
    new-instance v6, Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    invoke-direct {v6}, Linfo/asankan/phonegap/smsplugin/SmsReceiver;-><init>()V

    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    .line 76
    new-instance v2, Landroid/content/IntentFilter;

    const-string v6, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v2, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 77
    .local v2, "fp":Landroid/content/IntentFilter;
    const/16 v6, 0x3e8

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 79
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->cordova:Lorg/apache/cordova/api/CordovaInterface;

    invoke-interface {v6}, Lorg/apache/cordova/api/CordovaInterface;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    invoke-virtual {v6, v7, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 82
    .end local v2    # "fp":Landroid/content/IntentFilter;
    :cond_3
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    invoke-virtual {v6, p3}, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->startReceiving(Lorg/apache/cordova/api/CallbackContext;)V

    .line 84
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    .line 85
    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->NO_RESULT:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    .line 84
    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    .line 86
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {v6, v8}, Lorg/apache/cordova/api/PluginResult;->setKeepCallback(Z)V

    .line 87
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 88
    iput-object p3, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    .line 90
    iput-boolean v8, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    goto/16 :goto_0

    .line 94
    :pswitch_3
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    if-eqz v6, :cond_4

    .line 95
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->smsReceiver:Linfo/asankan/phonegap/smsplugin/SmsReceiver;

    invoke-virtual {v6}, Linfo/asankan/phonegap/smsplugin/SmsReceiver;->stopReceiving()V

    .line 98
    :cond_4
    iput-boolean v9, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->isReceiving:Z

    .line 101
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    .line 102
    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->NO_RESULT:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    .line 101
    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    .line 103
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {v6, v9}, Lorg/apache/cordova/api/PluginResult;->setKeepCallback(Z)V

    .line 104
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    if-eqz v6, :cond_5

    .line 105
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    iget-object v7, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {v6, v7}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 108
    :cond_5
    new-instance v6, Lorg/apache/cordova/api/PluginResult;

    .line 109
    sget-object v7, Lorg/apache/cordova/api/PluginResult$Status;->OK:Lorg/apache/cordova/api/PluginResult$Status;

    invoke-direct {v6, v7}, Lorg/apache/cordova/api/PluginResult;-><init>(Lorg/apache/cordova/api/PluginResult$Status;)V

    .line 108
    iput-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    .line 110
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->callback_receive:Lorg/apache/cordova/api/CallbackContext;

    if-eqz v6, :cond_6

    .line 111
    iget-object v6, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->pluginResult:Lorg/apache/cordova/api/PluginResult;

    invoke-virtual {p3, v6}, Lorg/apache/cordova/api/CallbackContext;->sendPluginResult(Lorg/apache/cordova/api/PluginResult;)V

    .line 113
    :cond_6
    iput-boolean v8, p0, Linfo/asankan/phonegap/smsplugin/SmsPlugin;->result:Z

    goto/16 :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
