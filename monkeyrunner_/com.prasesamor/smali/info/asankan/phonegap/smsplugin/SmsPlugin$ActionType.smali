.class public final enum Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;
.super Ljava/lang/Enum;
.source "SmsPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Linfo/asankan/phonegap/smsplugin/SmsPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

.field public static final enum HAS_SMS_POSSIBILITY:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

.field public static final enum RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

.field public static final enum SEND_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

.field public static final enum STOP_RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    const-string v1, "SEND_SMS"

    invoke-direct {v0, v1, v2}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->SEND_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    new-instance v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    const-string v1, "HAS_SMS_POSSIBILITY"

    invoke-direct {v0, v1, v3}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->HAS_SMS_POSSIBILITY:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    new-instance v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    const-string v1, "RECEIVE_SMS"

    invoke-direct {v0, v1, v4}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    new-instance v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    const-string v1, "STOP_RECEIVE_SMS"

    invoke-direct {v0, v1, v5}, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->STOP_RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    .line 14
    const/4 v0, 0x4

    new-array v0, v0, [Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->SEND_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    aput-object v1, v0, v2

    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->HAS_SMS_POSSIBILITY:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    aput-object v1, v0, v3

    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    aput-object v1, v0, v4

    sget-object v1, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->STOP_RECEIVE_SMS:Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    aput-object v1, v0, v5

    sput-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ENUM$VALUES:[Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    return-object v0
.end method

.method public static values()[Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;->ENUM$VALUES:[Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    array-length v1, v0

    new-array v2, v1, [Linfo/asankan/phonegap/smsplugin/SmsPlugin$ActionType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
