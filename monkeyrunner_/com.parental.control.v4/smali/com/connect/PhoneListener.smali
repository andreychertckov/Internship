.class public Lcom/connect/PhoneListener;
.super Landroid/telephony/PhoneStateListener;
.source "PhoneListener.java"


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 21
    packed-switch p1, :pswitch_data_0

    .line 39
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 23
    :pswitch_1
    iget-object v2, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    const-class v5, Lcom/connect/RecordService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 25
    .local v1, "stopped":Ljava/lang/Boolean;
    goto :goto_0

    .line 29
    .end local v1    # "stopped":Ljava/lang/Boolean;
    :pswitch_2
    iget-object v2, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "RecordCalls"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    const-class v3, Lcom/connect/RecordService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "callIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/connect/PhoneListener;->context:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
