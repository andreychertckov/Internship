.class Lcom/connect/CameraView$1;
.super Ljava/lang/Object;
.source "CameraView.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/CameraView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/connect/CameraView;


# direct methods
.method constructor <init>(Lcom/connect/CameraView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 61
    if-eqz p1, :cond_0

    .line 63
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    iget-object v2, v2, Lcom/connect/CameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->stopPreview()V

    .line 64
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/connect/CameraView;->mPreviewRunning:Z

    .line 65
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    iget-object v2, v2, Lcom/connect/CameraView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    .line 68
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 69
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    invoke-static {v2, p1}, Lcom/connect/CameraView;->access$0(Lcom/connect/CameraView;[B)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 70
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 73
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    invoke-virtual {v2, v0}, Lcom/connect/CameraView;->saveBitmap(Landroid/graphics/Bitmap;)V

    .line 74
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 76
    const-string v2, "com.connect"

    const-string v3, "BITMAP SAVED"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v2, p0, Lcom/connect/CameraView$1;->this$0:Lcom/connect/CameraView;

    invoke-virtual {v2}, Lcom/connect/CameraView;->finish()V

    .line 83
    :cond_0
    return-void

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
