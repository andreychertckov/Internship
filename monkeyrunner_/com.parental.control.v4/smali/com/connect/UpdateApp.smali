.class public Lcom/connect/UpdateApp;
.super Landroid/os/AsyncTask;
.source "UpdateApp.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/UpdateApp;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 14
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    .line 24
    :try_start_0
    new-instance v10, Ljava/net/URL;

    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-direct {v10, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 25
    .local v10, "url":Ljava/net/URL;
    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 26
    .local v2, "c":Ljava/net/HttpURLConnection;
    const-string v11, "GET"

    invoke-virtual {v2, v11}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 27
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 28
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 30
    const-string v0, "/mnt/sdcard/Download/"

    .line 31
    .local v0, "PATH":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 32
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 33
    new-instance v9, Ljava/io/File;

    const-string v11, "update.apk"

    invoke-direct {v9, v4, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 34
    .local v9, "outputFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 35
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 37
    :cond_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 39
    .local v5, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 41
    .local v7, "is":Ljava/io/InputStream;
    const/16 v11, 0x400

    new-array v1, v11, [B

    .line 42
    .local v1, "buffer":[B
    const/4 v8, 0x0

    .line 43
    .local v8, "len1":I
    :goto_0
    invoke-virtual {v7, v1}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v11, -0x1

    if-ne v8, v11, :cond_1

    .line 46
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 47
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 49
    new-instance v6, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v6, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v6, "intent":Landroid/content/Intent;
    new-instance v11, Ljava/io/File;

    const-string v12, "/mnt/sdcard/Download/update.apk"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    const-string v12, "application/vnd.android.package-archive"

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const/high16 v11, 0x10000000

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 52
    iget-object v11, p0, Lcom/connect/UpdateApp;->context:Landroid/content/Context;

    invoke-virtual {v11, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 58
    .end local v0    # "PATH":Ljava/lang/String;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v8    # "len1":I
    .end local v9    # "outputFile":Ljava/io/File;
    .end local v10    # "url":Ljava/net/URL;
    :goto_1
    const/4 v11, 0x0

    return-object v11

    .line 44
    .restart local v0    # "PATH":Ljava/lang/String;
    .restart local v1    # "buffer":[B
    .restart local v2    # "c":Ljava/net/HttpURLConnection;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v8    # "len1":I
    .restart local v9    # "outputFile":Ljava/io/File;
    .restart local v10    # "url":Ljava/net/URL;
    :cond_1
    const/4 v11, 0x0

    invoke-virtual {v5, v1, v11, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    .end local v0    # "PATH":Ljava/lang/String;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v8    # "len1":I
    .end local v9    # "outputFile":Ljava/io/File;
    .end local v10    # "url":Ljava/net/URL;
    :catch_0
    move-exception v3

    .line 56
    .local v3, "e":Ljava/lang/Exception;
    const-string v11, "UpdateAPP"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Update error! "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "contextf"    # Landroid/content/Context;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/connect/UpdateApp;->context:Landroid/content/Context;

    .line 19
    return-void
.end method
