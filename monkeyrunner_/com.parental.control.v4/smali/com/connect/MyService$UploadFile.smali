.class Lcom/connect/MyService$UploadFile;
.super Landroid/os/AsyncTask;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method public constructor <init>(Lcom/connect/MyService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "j"    # Ljava/lang/String;
    .param p3, "i"    # Ljava/lang/String;

    .prologue
    .line 2290
    iput-object p1, p0, Lcom/connect/MyService$UploadFile;->this$0:Lcom/connect/MyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2288
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$UploadFile;->j:Ljava/lang/String;

    .line 2289
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$UploadFile;->i:Ljava/lang/String;

    .line 2291
    iput-object p2, p0, Lcom/connect/MyService$UploadFile;->j:Ljava/lang/String;

    .line 2292
    iput-object p3, p0, Lcom/connect/MyService$UploadFile;->i:Ljava/lang/String;

    .line 2293
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$UploadFile;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 25
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 2297
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/connect/MyService$UploadFile;->j:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2298
    .local v16, "sd":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 2300
    const/4 v7, 0x0

    .line 2301
    .local v7, "connection":Ljava/net/HttpURLConnection;
    const/4 v13, 0x0

    .line 2302
    .local v13, "outputStream":Ljava/io/DataOutputStream;
    const/4 v10, 0x0

    .line 2304
    .local v10, "inputStream":Ljava/io/DataInputStream;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/connect/MyService$UploadFile;->j:Ljava/lang/String;

    .line 2305
    .local v15, "pathToOurFile":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/connect/MyService$UploadFile;->this$0:Lcom/connect/MyService;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/connect/MyService;->access$2(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/connect/MyService$UploadFile;->i:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 2307
    .local v21, "urlServer":Ljava/lang/String;
    const-string v22, "com.connect"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "pathToOurFile : "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2308
    const-string v22, "com.connect"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "urlServer : "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2311
    const-string v11, "\r\n"

    .line 2312
    .local v11, "lineEnd":Ljava/lang/String;
    const-string v19, "--"

    .line 2313
    .local v19, "twoHyphens":Ljava/lang/String;
    const-string v2, "*****"

    .line 2316
    .local v2, "boundary":Ljava/lang/String;
    const/high16 v12, 0x71000000

    .line 2320
    .local v12, "maxBufferSize":I
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2322
    .local v9, "fileInputStream":Ljava/io/FileInputStream;
    new-instance v20, Ljava/net/URL;

    invoke-direct/range {v20 .. v21}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2323
    .local v20, "url":Ljava/net/URL;
    invoke-virtual/range {v20 .. v20}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v22

    move-object/from16 v0, v22

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v7, v0

    .line 2326
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 2327
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 2328
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 2331
    const-string v22, "POST"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 2333
    const-string v22, "Connection"

    const-string v23, "Keep-Alive"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334
    const-string v22, "Content-Type"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "multipart/form-data;boundary="

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2336
    new-instance v14, Ljava/io/DataOutputStream;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2337
    .end local v13    # "outputStream":Ljava/io/DataOutputStream;
    .local v14, "outputStream":Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 2338
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Content-Disposition: form-data; name=\"file\";filename=\""

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 2339
    invoke-virtual {v14, v11}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 2341
    invoke-virtual {v9}, Ljava/io/FileInputStream;->available()I

    move-result v5

    .line 2342
    .local v5, "bytesAvailable":I
    invoke-static {v5, v12}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2343
    .local v4, "bufferSize":I
    new-array v3, v4, [B

    .line 2346
    .local v3, "buffer":[B
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v9, v3, v0, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v6

    .line 2348
    .local v6, "bytesRead":I
    :goto_0
    if-gtz v6, :cond_1

    .line 2356
    invoke-virtual {v14, v11}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 2357
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 2360
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v17

    .line 2361
    .local v17, "serverResponseCode":I
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v18

    .line 2362
    .local v18, "serverResponseMessage":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 2363
    invoke-virtual {v14}, Ljava/io/DataOutputStream;->flush()V

    .line 2364
    invoke-virtual {v14}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2370
    .end local v2    # "boundary":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "bufferSize":I
    .end local v5    # "bytesAvailable":I
    .end local v6    # "bytesRead":I
    .end local v7    # "connection":Ljava/net/HttpURLConnection;
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v10    # "inputStream":Ljava/io/DataInputStream;
    .end local v11    # "lineEnd":Ljava/lang/String;
    .end local v12    # "maxBufferSize":I
    .end local v14    # "outputStream":Ljava/io/DataOutputStream;
    .end local v15    # "pathToOurFile":Ljava/lang/String;
    .end local v17    # "serverResponseCode":I
    .end local v18    # "serverResponseMessage":Ljava/lang/String;
    .end local v19    # "twoHyphens":Ljava/lang/String;
    .end local v20    # "url":Ljava/net/URL;
    .end local v21    # "urlServer":Ljava/lang/String;
    :cond_0
    :goto_1
    const-string v22, "Executed"

    return-object v22

    .line 2350
    .restart local v2    # "boundary":Ljava/lang/String;
    .restart local v3    # "buffer":[B
    .restart local v4    # "bufferSize":I
    .restart local v5    # "bytesAvailable":I
    .restart local v6    # "bytesRead":I
    .restart local v7    # "connection":Ljava/net/HttpURLConnection;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/DataInputStream;
    .restart local v11    # "lineEnd":Ljava/lang/String;
    .restart local v12    # "maxBufferSize":I
    .restart local v14    # "outputStream":Ljava/io/DataOutputStream;
    .restart local v15    # "pathToOurFile":Ljava/lang/String;
    .restart local v19    # "twoHyphens":Ljava/lang/String;
    .restart local v20    # "url":Ljava/net/URL;
    .restart local v21    # "urlServer":Ljava/lang/String;
    :cond_1
    const/16 v22, 0x0

    :try_start_2
    move/from16 v0, v22

    invoke-virtual {v14, v3, v0, v4}, Ljava/io/DataOutputStream;->write([BII)V

    .line 2351
    invoke-virtual {v9}, Ljava/io/FileInputStream;->available()I

    move-result v5

    .line 2352
    invoke-static {v5, v12}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2353
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v9, v3, v0, v4}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v6

    goto :goto_0

    .line 2367
    .end local v3    # "buffer":[B
    .end local v4    # "bufferSize":I
    .end local v5    # "bytesAvailable":I
    .end local v6    # "bytesRead":I
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v14    # "outputStream":Ljava/io/DataOutputStream;
    .end local v20    # "url":Ljava/net/URL;
    .restart local v13    # "outputStream":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v8

    .line 2368
    .local v8, "ex":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2367
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v13    # "outputStream":Ljava/io/DataOutputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v14    # "outputStream":Ljava/io/DataOutputStream;
    .restart local v20    # "url":Ljava/net/URL;
    :catch_1
    move-exception v8

    move-object v13, v14

    .end local v14    # "outputStream":Ljava/io/DataOutputStream;
    .restart local v13    # "outputStream":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$UploadFile;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 2373
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 2375
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$UploadFile;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 2378
    return-void
.end method
