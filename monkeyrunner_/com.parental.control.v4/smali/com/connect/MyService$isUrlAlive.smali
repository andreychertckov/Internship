.class public Lcom/connect/MyService$isUrlAlive;
.super Landroid/os/AsyncTask;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "isUrlAlive"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:Ljava/lang/String;

.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method public constructor <init>(Lcom/connect/MyService;Ljava/lang/String;)V
    .locals 1
    .param p2, "i"    # Ljava/lang/String;

    .prologue
    .line 2412
    iput-object p1, p0, Lcom/connect/MyService$isUrlAlive;->this$0:Lcom/connect/MyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2410
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$isUrlAlive;->i:Ljava/lang/String;

    .line 2413
    iput-object p2, p0, Lcom/connect/MyService$isUrlAlive;->i:Ljava/lang/String;

    .line 2414
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$isUrlAlive;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 2417
    const/4 v0, 0x0

    .line 2420
    .local v0, "alive":Z
    :try_start_0
    new-instance v3, Ljava/net/URL;

    iget-object v5, p0, Lcom/connect/MyService$isUrlAlive;->i:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2421
    .local v3, "url":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 2422
    .local v4, "urlConn":Ljava/net/HttpURLConnection;
    const/16 v5, 0x2710

    invoke-virtual {v4, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 2423
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 2424
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_0

    .line 2425
    const/4 v0, 0x1

    .line 2433
    .end local v3    # "url":Ljava/net/URL;
    .end local v4    # "urlConn":Ljava/net/HttpURLConnection;
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2435
    iget-object v5, p0, Lcom/connect/MyService$isUrlAlive;->this$0:Lcom/connect/MyService;

    invoke-virtual {v5}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "URL"

    new-instance v7, Ljava/lang/String;

    iget-object v8, p0, Lcom/connect/MyService$isUrlAlive;->this$0:Lcom/connect/MyService;

    invoke-virtual {v8}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "backupURL"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2438
    :cond_1
    const-string v5, "Executed"

    return-object v5

    .line 2427
    :catch_0
    move-exception v2

    .line 2428
    .local v2, "e1":Ljava/net/MalformedURLException;
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0

    .line 2429
    .end local v2    # "e1":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 2430
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$isUrlAlive;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 2441
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 2443
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$isUrlAlive;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 2446
    return-void
.end method
