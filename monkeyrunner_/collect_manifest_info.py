import os, re

PATH = os.getenv('MALWARE','~/internships/android-malware')
packages = {}
activities_ = {}
for f in os.listdir(PATH):
    if os.path.isdir(PATH+'/'+f):
        apk = ''
        for ff in os.listdir(PATH+'/'+f):
            if ff.find('.apk') != -1:
                apk = str(ff)
                break
        else:
            continue
        print(PATH+'/'+f)
        if not os.path.exists(apk[:-4]):
            os.system('apktool -f d ' + '/'.join([PATH,f,apk]))
        if os.path.isfile('/'.join([apk[:-4],'AndroidManifest.xml'])):
            with open('/'.join([apk[:-4],'AndroidManifest.xml'])) as a_man:
                xml_ = a_man.read()
            package = ''
            ind = xml_.find('package="')
            for i in range(ind + len('package="'),ind + len('package="')+100):
                if xml_[i] == '"':
                    break
                package += xml_[i]
            print(package)
            packages[apk] = package
            activities = []
            result = re.findall(r'<activity (\w*:\w*="@\w*\/\w*" android:name="(\w*|\.)*"|android:name="(\w*|\.)*")',xml_)
            for r in result:
                print(r[0])
                ind = r[0].find('name="')
                active = ''
                for i in range(ind + len('name="'),ind + len('name="')+400):
                    if r[0][i] == '"':
                        break
                    active += r[0][i]
                activities.append(active)
            print(activities)
            activities_[apk] = activities
        else:
            print('/'.join([apk[:-4],'AndroidManifest.xml']), 'Not found')
print(packages)
print(activities_)
with open('packages','w') as f:
    f.write(str(packages))
with open('activities','w') as f:
    f.write(str(activities_))