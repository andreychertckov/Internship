.class public Lorg/apache/commons/net/tftp/TFTPClient;
.super Lorg/apache/commons/net/tftp/TFTP;
.source "TFTPClient.java"


# static fields
.field public static final DEFAULT_MAX_TIMEOUTS:I = 0x5


# instance fields
.field private __maxTimeouts:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lorg/apache/commons/net/tftp/TFTP;-><init>()V

    .line 76
    const/4 v0, 0x5

    iput v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    .line 77
    return-void
.end method


# virtual methods
.method public getMaxTimeouts()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/lang/String;)I
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "output"    # Ljava/io/OutputStream;
    .param p4, "hostname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/lang/String;I)I
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "output"    # Ljava/io/OutputStream;
    .param p4, "hostname"    # Ljava/lang/String;
    .param p5, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;)I
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "output"    # Ljava/io/OutputStream;
    .param p4, "host"    # Ljava/net/InetAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 329
    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I
    .locals 22
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "output"    # Ljava/io/OutputStream;
    .param p4, "host"    # Ljava/net/InetAddress;
    .param p5, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v15, 0x0

    .line 132
    .local v15, "received":Lorg/apache/commons/net/tftp/TFTPPacket;
    new-instance v5, Lorg/apache/commons/net/tftp/TFTPAckPacket;

    const/16 v18, 0x0

    move-object v0, v5

    move-object/from16 v1, p4

    move/from16 v2, p5

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/net/tftp/TFTPAckPacket;-><init>(Ljava/net/InetAddress;II)V

    .line 134
    .local v5, "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->beginBufferedOps()V

    .line 136
    const/4 v7, 0x0

    .local v7, "bytesRead":I
    move v12, v7

    .local v12, "hostPort":I
    move v13, v7

    .local v13, "lastBlock":I
    move v9, v7

    .line 137
    .local v9, "dataLength":I
    const/4 v6, 0x1

    .line 139
    .local v6, "block":I
    if-nez p2, :cond_0

    .line 140
    new-instance v14, Lorg/apache/commons/net/io/FromNetASCIIOutputStream;

    move-object v0, v14

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lorg/apache/commons/net/io/FromNetASCIIOutputStream;-><init>(Ljava/io/OutputStream;)V

    .end local p3    # "output":Ljava/io/OutputStream;
    .local v14, "output":Ljava/io/OutputStream;
    move-object/from16 p3, v14

    .line 142
    .end local v14    # "output":Ljava/io/OutputStream;
    .restart local p3    # "output":Ljava/io/OutputStream;
    :cond_0
    new-instance v16, Lorg/apache/commons/net/tftp/TFTPReadRequestPacket;

    move-object/from16 v0, v16

    move-object/from16 v1, p4

    move/from16 v2, p5

    move-object/from16 v3, p1

    move/from16 v4, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/net/tftp/TFTPReadRequestPacket;-><init>(Ljava/net/InetAddress;ILjava/lang/String;I)V

    .line 148
    .local v16, "sent":Lorg/apache/commons/net/tftp/TFTPPacket;
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 153
    :cond_2
    const/16 v17, 0x0

    .line 154
    .local v17, "timeouts":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    .line 158
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedReceive()Lorg/apache/commons/net/tftp/TFTPPacket;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/commons/net/tftp/TFTPPacketException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v15

    .line 188
    :cond_4
    if-nez v13, :cond_5

    .line 190
    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v12

    .line 191
    invoke-virtual {v5, v12}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setPort(I)V

    .line 192
    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v18

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 194
    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object p4

    .line 195
    move-object v0, v5

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 196
    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 202
    :cond_5
    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v18

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v18

    move/from16 v0, v18

    move v1, v12

    if-ne v0, v1, :cond_9

    .line 206
    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getType()I

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 256
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 257
    new-instance v18, Ljava/io/IOException;

    const-string v19, "Received unexpected packet type."

    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 161
    :catch_0
    move-exception v10

    .line 163
    .local v10, "e":Ljava/net/SocketException;
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    .line 165
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 166
    new-instance v18, Ljava/io/IOException;

    const-string v19, "Connection timed out."

    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 170
    .end local v10    # "e":Ljava/net/SocketException;
    :catch_1
    move-exception v10

    .line 172
    .local v10, "e":Ljava/io/InterruptedIOException;
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    .line 174
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 175
    new-instance v18, Ljava/io/IOException;

    const-string v19, "Connection timed out."

    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 179
    .end local v10    # "e":Ljava/io/InterruptedIOException;
    :catch_2
    move-exception v10

    .line 181
    .local v10, "e":Lorg/apache/commons/net/tftp/TFTPPacketException;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 182
    new-instance v18, Ljava/io/IOException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Bad packet: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v10}, Lorg/apache/commons/net/tftp/TFTPPacketException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 209
    .end local v10    # "e":Lorg/apache/commons/net/tftp/TFTPPacketException;
    :pswitch_1
    move-object v0, v15

    check-cast v0, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    move-object v11, v0

    .line 210
    .local v11, "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 211
    new-instance v18, Ljava/io/IOException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Error code "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getError()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " received: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 214
    .end local v11    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    :pswitch_2
    move-object v0, v15

    check-cast v0, Lorg/apache/commons/net/tftp/TFTPDataPacket;

    move-object v8, v0

    .line 215
    .local v8, "data":Lorg/apache/commons/net/tftp/TFTPDataPacket;
    invoke-virtual {v8}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getDataLength()I

    move-result v9

    .line 217
    invoke-virtual {v8}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getBlockNumber()I

    move-result v13

    .line 219
    if-ne v13, v6, :cond_7

    .line 223
    :try_start_1
    invoke-virtual {v8}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getData()[B

    move-result-object v18

    invoke-virtual {v8}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getDataOffset()I

    move-result v19

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    move/from16 v2, v19

    move v3, v9

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 235
    add-int/lit8 v6, v6, 0x1

    .line 236
    const v18, 0xffff

    move v0, v6

    move/from16 v1, v18

    if-le v0, v1, :cond_6

    .line 239
    const/4 v6, 0x0

    .line 275
    :cond_6
    invoke-virtual {v5, v13}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setBlockNumber(I)V

    .line 276
    move-object/from16 v16, v5

    .line 277
    add-int/2addr v7, v9

    .line 280
    .end local v8    # "data":Lorg/apache/commons/net/tftp/TFTPDataPacket;
    :goto_0
    const/16 v18, 0x200

    move v0, v9

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 282
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 283
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 285
    return v7

    .line 226
    .restart local v8    # "data":Lorg/apache/commons/net/tftp/TFTPDataPacket;
    :catch_3
    move-exception v18

    move-object/from16 v10, v18

    .line 228
    .local v10, "e":Ljava/io/IOException;
    new-instance v11, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    const/16 v18, 0x3

    const-string v19, "File write failed."

    move-object v0, v11

    move-object/from16 v1, p4

    move v2, v12

    move/from16 v3, v18

    move-object/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 231
    .restart local v11    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    move-object/from16 v0, p0

    move-object v1, v11

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 232
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 233
    throw v10

    .line 246
    .end local v10    # "e":Ljava/io/IOException;
    .end local v11    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->discardPackets()V

    .line 248
    if-nez v6, :cond_8

    const v18, 0xffff

    :goto_1
    move v0, v13

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    goto :goto_0

    :cond_8
    const/16 v18, 0x1

    sub-int v18, v6, v18

    goto :goto_1

    .line 262
    .end local v8    # "data":Lorg/apache/commons/net/tftp/TFTPDataPacket;
    :cond_9
    new-instance v11, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v18

    invoke-virtual {v15}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v19

    const/16 v20, 0x5

    const-string v21, "Unexpected host or port."

    move-object v0, v11

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move-object/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 266
    .restart local v11    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    move-object/from16 v0, p0

    move-object v1, v11

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "input"    # Ljava/io/InputStream;
    .param p4, "hostname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 607
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 609
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/lang/String;I)V
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "input"    # Ljava/io/InputStream;
    .param p4, "hostname"    # Ljava/lang/String;
    .param p5, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 570
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 571
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;)V
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "input"    # Ljava/io/InputStream;
    .param p4, "host"    # Ljava/net/InetAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 589
    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 590
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V
    .locals 28
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "input"    # Ljava/io/InputStream;
    .param p4, "host"    # Ljava/net/InetAddress;
    .param p5, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 372
    const/16 v24, 0x0

    .line 374
    .local v24, "received":Lorg/apache/commons/net/tftp/TFTPPacket;
    new-instance v5, Lorg/apache/commons/net/tftp/TFTPDataPacket;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    move-object v9, v0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object/from16 v6, p4

    move/from16 v7, p5

    invoke-direct/range {v5 .. v11}, Lorg/apache/commons/net/tftp/TFTPDataPacket;-><init>(Ljava/net/InetAddress;II[BII)V

    .line 378
    .local v5, "data":Lorg/apache/commons/net/tftp/TFTPDataPacket;
    const/16 v20, 0x1

    .line 380
    .local v20, "justStarted":Z
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->beginBufferedOps()V

    .line 382
    const/16 v27, 0x0

    .local v27, "totalThisPacket":I
    move/from16 v14, v27

    .local v14, "bytesRead":I
    move/from16 v18, v27

    .local v18, "hostPort":I
    move/from16 v22, v27

    .local v22, "lastBlock":I
    move/from16 v15, v27

    .line 383
    .local v15, "dataLength":I
    const/4 v13, 0x0

    .line 384
    .local v13, "block":I
    const/16 v21, 0x0

    .line 386
    .local v21, "lastAckWait":Z
    if-nez p2, :cond_0

    .line 387
    new-instance v19, Lorg/apache/commons/net/io/ToNetASCIIInputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lorg/apache/commons/net/io/ToNetASCIIInputStream;-><init>(Ljava/io/InputStream;)V

    .end local p3    # "input":Ljava/io/InputStream;
    .local v19, "input":Ljava/io/InputStream;
    move-object/from16 p3, v19

    .line 389
    .end local v19    # "input":Ljava/io/InputStream;
    .restart local p3    # "input":Ljava/io/InputStream;
    :cond_0
    new-instance v25, Lorg/apache/commons/net/tftp/TFTPWriteRequestPacket;

    move-object/from16 v0, v25

    move-object/from16 v1, p4

    move/from16 v2, p5

    move-object/from16 v3, p1

    move/from16 v4, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/net/tftp/TFTPWriteRequestPacket;-><init>(Ljava/net/InetAddress;ILjava/lang/String;I)V

    .line 397
    .local v25, "sent":Lorg/apache/commons/net/tftp/TFTPPacket;
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 405
    :cond_2
    const/16 v26, 0x0

    .line 406
    .local v26, "timeouts":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move v6, v0

    move/from16 v0, v26

    move v1, v6

    if-ge v0, v1, :cond_4

    .line 410
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedReceive()Lorg/apache/commons/net/tftp/TFTPPacket;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/commons/net/tftp/TFTPPacketException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v24

    .line 440
    :cond_4
    if-eqz v20, :cond_5

    .line 442
    const/16 v20, 0x0

    .line 443
    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v18

    .line 444
    move-object v0, v5

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setPort(I)V

    .line 445
    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p4

    move-object v1, v6

    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 447
    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object p4

    .line 448
    move-object v0, v5

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 449
    move-object/from16 v0, v25

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 455
    :cond_5
    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p4

    move-object v1, v6

    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v6

    move v0, v6

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 459
    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 499
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 500
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Received unexpected packet type."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 413
    :catch_0
    move-exception v16

    .line 415
    .local v16, "e":Ljava/net/SocketException;
    add-int/lit8 v26, v26, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move v6, v0

    move/from16 v0, v26

    move v1, v6

    if-lt v0, v1, :cond_3

    .line 417
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 418
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Connection timed out."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 422
    .end local v16    # "e":Ljava/net/SocketException;
    :catch_1
    move-exception v16

    .line 424
    .local v16, "e":Ljava/io/InterruptedIOException;
    add-int/lit8 v26, v26, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    move v6, v0

    move/from16 v0, v26

    move v1, v6

    if-lt v0, v1, :cond_3

    .line 426
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 427
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Connection timed out."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 431
    .end local v16    # "e":Ljava/io/InterruptedIOException;
    :catch_2
    move-exception v16

    .line 433
    .local v16, "e":Lorg/apache/commons/net/tftp/TFTPPacketException;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 434
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad packet: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/tftp/TFTPPacketException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 462
    .end local v16    # "e":Lorg/apache/commons/net/tftp/TFTPPacketException;
    :pswitch_0
    move-object/from16 v0, v24

    check-cast v0, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    move-object/from16 v17, v0

    .line 463
    .local v17, "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 464
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error code "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getError()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " received: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 467
    .end local v17    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    :pswitch_1
    move-object/from16 v0, v24

    check-cast v0, Lorg/apache/commons/net/tftp/TFTPAckPacket;

    move-object v12, v0

    .line 469
    .local v12, "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    invoke-virtual {v12}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->getBlockNumber()I

    move-result v22

    .line 471
    move/from16 v0, v22

    move v1, v13

    if-ne v0, v1, :cond_7

    .line 473
    add-int/lit8 v13, v13, 0x1

    .line 474
    const v6, 0xffff

    if-le v13, v6, :cond_6

    .line 477
    const/4 v13, 0x0

    .line 479
    :cond_6
    if-eqz v21, :cond_a

    .line 545
    .end local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 546
    return-void

    .line 489
    .restart local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->discardPackets()V

    .line 491
    if-nez v13, :cond_8

    const v6, 0xffff

    :goto_1
    move/from16 v0, v22

    move v1, v6

    if-ne v0, v1, :cond_2

    .line 540
    .end local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :goto_2
    if-gtz v27, :cond_1

    if-nez v21, :cond_1

    goto :goto_0

    .line 491
    .restart local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :cond_8
    const/4 v6, 0x1

    sub-int v6, v13, v6

    goto :goto_1

    .line 505
    .end local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :cond_9
    new-instance v17, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual/range {v24 .. v24}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v7

    const/4 v8, 0x5

    const-string v9, "Unexpected host or port."

    move-object/from16 v0, v17

    move-object v1, v6

    move v2, v7

    move v3, v8

    move-object v4, v9

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 509
    .restart local v17    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    goto :goto_2

    .line 521
    .end local v17    # "error":Lorg/apache/commons/net/tftp/TFTPErrorPacket;
    .restart local v12    # "ack":Lorg/apache/commons/net/tftp/TFTPAckPacket;
    :cond_a
    const/16 v15, 0x200

    .line 522
    const/16 v23, 0x4

    .line 523
    .local v23, "offset":I
    const/16 v27, 0x0

    .line 525
    :goto_3
    if-lez v15, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    move-object v6, v0

    move-object/from16 v0, p3

    move-object v1, v6

    move/from16 v2, v23

    move v3, v15

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v14

    if-lez v14, :cond_b

    .line 527
    add-int v23, v23, v14

    .line 528
    sub-int/2addr v15, v14

    .line 529
    add-int v27, v27, v14

    goto :goto_3

    .line 532
    :cond_b
    const/16 v6, 0x200

    move/from16 v0, v27

    move v1, v6

    if-ge v0, v1, :cond_c

    .line 534
    const/16 v21, 0x1

    .line 536
    :cond_c
    invoke-virtual {v5, v13}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setBlockNumber(I)V

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    move-object v6, v0

    const/4 v7, 0x4

    move-object v0, v5

    move-object v1, v6

    move v2, v7

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setData([BII)V

    .line 538
    move-object/from16 v25, v5

    goto :goto_2

    .line 459
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setMaxTimeouts(I)V
    .locals 1
    .param p1, "numTimeouts"    # I

    .prologue
    const/4 v0, 0x1

    .line 91
    if-ge p1, v0, :cond_0

    .line 92
    iput v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    iput p1, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    goto :goto_0
.end method
