.class public final Lorg/apache/commons/net/pop3/POP3Command;
.super Ljava/lang/Object;
.source "POP3Command.java"


# static fields
.field public static final APOP:I = 0x9

.field public static final DELE:I = 0x6

.field public static final LIST:I = 0x4

.field public static final NOOP:I = 0x7

.field public static final PASS:I = 0x1

.field public static final QUIT:I = 0x2

.field public static final RETR:I = 0x5

.field public static final RSET:I = 0x8

.field public static final STAT:I = 0x3

.field public static final TOP:I = 0xa

.field public static final UIDL:I = 0xb

.field public static final USER:I

.field static final _commands:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "USER"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PASS"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "QUIT"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "STAT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "LIST"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RETR"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DELE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NOOP"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "RSET"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "APOP"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TOP"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "UIDL"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/net/pop3/POP3Command;->_commands:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getCommand(I)Ljava/lang/String;
    .locals 1
    .param p0, "command"    # I

    .prologue
    .line 70
    sget-object v0, Lorg/apache/commons/net/pop3/POP3Command;->_commands:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method
