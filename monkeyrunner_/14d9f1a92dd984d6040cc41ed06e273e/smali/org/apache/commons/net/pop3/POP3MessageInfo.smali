.class public final Lorg/apache/commons/net/pop3/POP3MessageInfo;
.super Ljava/lang/Object;
.source "POP3MessageInfo.java"


# instance fields
.field public identifier:Ljava/lang/String;

.field public number:I

.field public size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->size:I

    iput v0, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->number:I

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->identifier:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "num"    # I
    .param p2, "octets"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->number:I

    .line 68
    iput p2, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->size:I

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->identifier:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "num"    # I
    .param p2, "uid"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput p1, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->number:I

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->size:I

    .line 81
    iput-object p2, p0, Lorg/apache/commons/net/pop3/POP3MessageInfo;->identifier:Ljava/lang/String;

    .line 82
    return-void
.end method
