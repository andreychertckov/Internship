.class public Lorg/apache/commons/net/ntp/TimeStamp;
.super Ljava/lang/Object;
.source "TimeStamp.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/commons/net/ntp/TimeStamp;",
        ">;"
    }
.end annotation


# static fields
.field public static final NTP_DATE_FORMAT:Ljava/lang/String; = "EEE, MMM dd yyyy HH:mm:ss.SSS"

.field protected static final msb0baseTime:J = 0x1e5ae01dc00L

.field protected static final msb1baseTime:J = -0x20251fe2400L

.field private static final serialVersionUID:J = 0x70f667418312e431L

.field private static simpleFormatter:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private static utcFormatter:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ntpTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    sput-object v0, Lorg/apache/commons/net/ntp/TimeStamp;->simpleFormatter:Ljava/lang/ref/SoftReference;

    .line 69
    sput-object v0, Lorg/apache/commons/net/ntp/TimeStamp;->utcFormatter:Ljava/lang/ref/SoftReference;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .param p1, "ntpTime"    # J

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-wide p1, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-static {p1}, Lorg/apache/commons/net/ntp/TimeStamp;->decodeNtpHexString(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    .line 113
    return-void
.end method

.method public constructor <init>(Ljava/util/Date;)V
    .locals 2
    .param p1, "d"    # Ljava/util/Date;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    .line 124
    return-void

    .line 123
    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/commons/net/ntp/TimeStamp;->toNtpTime(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static appendHexString(Ljava/lang/StringBuilder;J)V
    .locals 3
    .param p0, "buf"    # Ljava/lang/StringBuilder;
    .param p1, "l"    # J

    .prologue
    .line 368
    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    .line 369
    .local v1, "s":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x8

    if-ge v0, v2, :cond_0

    .line 370
    const/16 v2, 0x30

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    return-void
.end method

.method protected static decodeNtpHexString(Ljava/lang/String;)J
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x20

    const/16 v4, 0x10

    .line 254
    if-nez p0, :cond_0

    .line 255
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "null"

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 257
    :cond_0
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 258
    .local v0, "ind":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 259
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    const-wide/16 v1, 0x0

    .line 263
    :goto_0
    return-wide v1

    .line 260
    :cond_1
    invoke-static {p0, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v1

    shl-long/2addr v1, v3

    goto :goto_0

    .line 263
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v1

    shl-long/2addr v1, v3

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v3

    or-long/2addr v1, v3

    goto :goto_0
.end method

.method public static getCurrentTime()Lorg/apache/commons/net/ntp/TimeStamp;
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/commons/net/ntp/TimeStamp;->getNtpTime(J)Lorg/apache/commons/net/ntp/TimeStamp;

    move-result-object v0

    return-object v0
.end method

.method public static getNtpTime(J)Lorg/apache/commons/net/ntp/TimeStamp;
    .locals 3
    .param p0, "date"    # J

    .prologue
    .line 229
    new-instance v0, Lorg/apache/commons/net/ntp/TimeStamp;

    invoke-static {p0, p1}, Lorg/apache/commons/net/ntp/TimeStamp;->toNtpTime(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/net/ntp/TimeStamp;-><init>(J)V

    return-object v0
.end method

.method public static getTime(J)J
    .locals 12
    .param p0, "ntpTimeValue"    # J

    .prologue
    const-wide v8, 0xffffffffL

    const-wide/16 v10, 0x3e8

    .line 192
    const/16 v6, 0x20

    ushr-long v6, p0, v6

    and-long v4, v6, v8

    .line 193
    .local v4, "seconds":J
    and-long v0, p0, v8

    .line 196
    .local v0, "fraction":J
    const-wide v6, 0x408f400000000000L    # 1000.0

    long-to-double v8, v0

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x41f0000000000000L    # 4.294967296E9

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    .line 207
    const-wide v6, 0x80000000L

    and-long v2, v4, v6

    .line 208
    .local v2, "msb":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_0

    .line 210
    const-wide v6, 0x1e5ae01dc00L

    mul-long v8, v4, v10

    add-long/2addr v6, v8

    add-long/2addr v6, v0

    .line 213
    :goto_0
    return-wide v6

    :cond_0
    const-wide v6, -0x20251fe2400L

    mul-long v8, v4, v10

    add-long/2addr v6, v8

    add-long/2addr v6, v0

    goto :goto_0
.end method

.method public static parseNtpString(Ljava/lang/String;)Lorg/apache/commons/net/ntp/TimeStamp;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 278
    new-instance v0, Lorg/apache/commons/net/ntp/TimeStamp;

    invoke-static {p0}, Lorg/apache/commons/net/ntp/TimeStamp;->decodeNtpHexString(Ljava/lang/String;)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/net/ntp/TimeStamp;-><init>(J)V

    return-object v0
.end method

.method protected static toNtpTime(J)J
    .locals 5
    .param p0, "t"    # J

    .prologue
    .line 289
    const-wide v0, 0x1e5ae01dc00L

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 291
    .local v2, "useBase1":Z
    :goto_0
    if-eqz v2, :cond_2

    .line 292
    const-wide v0, -0x20251fe2400L

    sub-long/2addr p0, v0

    .line 298
    .local p0, "baseTime":J
    :goto_1
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 299
    .local v0, "seconds":J
    const-wide/16 v3, 0x3e8

    rem-long/2addr p0, v3

    .end local p0    # "baseTime":J
    const-wide v3, 0x100000000L

    mul-long/2addr p0, v3

    const-wide/16 v3, 0x3e8

    div-long/2addr p0, v3

    .line 301
    .local p0, "fraction":J
    if-eqz v2, :cond_0

    .line 302
    const-wide v2, 0x80000000L

    or-long/2addr v0, v2

    .line 305
    .end local v2    # "useBase1":Z
    :cond_0
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    or-long/2addr p0, v0

    .line 306
    .end local v0    # "seconds":J
    .local p0, "time":J
    return-wide p0

    .line 289
    .local p0, "t":J
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 295
    .restart local v2    # "useBase1":Z
    :cond_2
    const-wide v0, 0x1e5ae01dc00L

    sub-long/2addr p0, v0

    .local p0, "baseTime":J
    goto :goto_1
.end method

.method public static toString(J)Ljava/lang/String;
    .locals 5
    .param p0, "ntpTime"    # J

    .prologue
    const-wide v3, 0xffffffffL

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 387
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/16 v1, 0x20

    ushr-long v1, p0, v1

    and-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lorg/apache/commons/net/ntp/TimeStamp;->appendHexString(Ljava/lang/StringBuilder;J)V

    .line 390
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 391
    and-long v1, p0, v3

    invoke-static {v0, v1, v2}, Lorg/apache/commons/net/ntp/TimeStamp;->appendHexString(Ljava/lang/StringBuilder;J)V

    .line 393
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, Lorg/apache/commons/net/ntp/TimeStamp;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ntp/TimeStamp;->compareTo(Lorg/apache/commons/net/ntp/TimeStamp;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/commons/net/ntp/TimeStamp;)I
    .locals 5
    .param p1, "anotherTimeStamp"    # Lorg/apache/commons/net/ntp/TimeStamp;

    .prologue
    .line 463
    iget-wide v2, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    .line 464
    .local v2, "thisVal":J
    iget-wide v0, p1, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    .line 465
    .local v0, "anotherVal":J
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    const/4 v4, -0x1

    :goto_0
    return v4

    :cond_0
    cmp-long v4, v2, v0

    if-nez v4, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 339
    instance-of v0, p1, Lorg/apache/commons/net/ntp/TimeStamp;

    if-eqz v0, :cond_1

    .line 340
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    check-cast p1, Lorg/apache/commons/net/ntp/TimeStamp;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/commons/net/ntp/TimeStamp;->ntpValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 342
    :goto_0
    return v0

    :cond_0
    move v0, v4

    .line 340
    goto :goto_0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    move v0, v4

    .line 342
    goto :goto_0
.end method

.method public getDate()Ljava/util/Date;
    .locals 4

    .prologue
    .line 173
    iget-wide v2, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    invoke-static {v2, v3}, Lorg/apache/commons/net/ntp/TimeStamp;->getTime(J)J

    move-result-wide v0

    .line 174
    .local v0, "time":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    return-object v2
.end method

.method public getFraction()J
    .locals 4

    .prologue
    .line 153
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getSeconds()J
    .locals 4

    .prologue
    .line 143
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 163
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    invoke-static {v0, v1}, Lorg/apache/commons/net/ntp/TimeStamp;->getTime(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 323
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    iget-wide v2, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public ntpValue()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    return-wide v0
.end method

.method public toDateString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "formatter":Ljava/text/DateFormat;
    sget-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->simpleFormatter:Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_0

    .line 409
    sget-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->simpleFormatter:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "formatter":Ljava/text/DateFormat;
    check-cast v0, Ljava/text/DateFormat;

    .line 411
    .restart local v0    # "formatter":Ljava/text/DateFormat;
    :cond_0
    if-nez v0, :cond_1

    .line 413
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "formatter":Ljava/text/DateFormat;
    const-string v2, "EEE, MMM dd yyyy HH:mm:ss.SSS"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 414
    .restart local v0    # "formatter":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 415
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->simpleFormatter:Ljava/lang/ref/SoftReference;

    .line 417
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/net/ntp/TimeStamp;->getDate()Ljava/util/Date;

    move-result-object v1

    .line 418
    .local v1, "ntpDate":Ljava/util/Date;
    monitor-enter v0

    .line 419
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    monitor-exit v0

    return-object v2

    .line 420
    :catchall_0
    move-exception v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 357
    iget-wide v0, p0, Lorg/apache/commons/net/ntp/TimeStamp;->ntpTime:J

    invoke-static {v0, v1}, Lorg/apache/commons/net/ntp/TimeStamp;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUTCString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 434
    const/4 v0, 0x0

    .line 435
    .local v0, "formatter":Ljava/text/DateFormat;
    sget-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->utcFormatter:Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_0

    .line 436
    sget-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->utcFormatter:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "formatter":Ljava/text/DateFormat;
    check-cast v0, Ljava/text/DateFormat;

    .line 437
    .restart local v0    # "formatter":Ljava/text/DateFormat;
    :cond_0
    if-nez v0, :cond_1

    .line 439
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "formatter":Ljava/text/DateFormat;
    const-string v2, "EEE, MMM dd yyyy HH:mm:ss.SSS \'UTC\'"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 441
    .restart local v0    # "formatter":Ljava/text/DateFormat;
    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 442
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lorg/apache/commons/net/ntp/TimeStamp;->utcFormatter:Ljava/lang/ref/SoftReference;

    .line 444
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/net/ntp/TimeStamp;->getDate()Ljava/util/Date;

    move-result-object v1

    .line 445
    .local v1, "ntpDate":Ljava/util/Date;
    monitor-enter v0

    .line 446
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    monitor-exit v0

    return-object v2

    .line 447
    :catchall_0
    move-exception v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
