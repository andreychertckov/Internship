.class public final Lorg/apache/commons/net/io/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field public static final DEFAULT_COPY_BUFFER_SIZE:I = 0x400


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final copyReader(Ljava/io/Reader;Ljava/io/Writer;)J
    .locals 2
    .param p0, "source"    # Ljava/io/Reader;
    .param p1, "dest"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 331
    const/16 v0, 0x400

    invoke-static {p0, p1, v0}, Lorg/apache/commons/net/io/Util;->copyReader(Ljava/io/Reader;Ljava/io/Writer;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final copyReader(Ljava/io/Reader;Ljava/io/Writer;I)J
    .locals 6
    .param p0, "source"    # Ljava/io/Reader;
    .param p1, "dest"    # Ljava/io/Writer;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 320
    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/commons/net/io/Util;->copyReader(Ljava/io/Reader;Ljava/io/Writer;IJLorg/apache/commons/net/io/CopyStreamListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final copyReader(Ljava/io/Reader;Ljava/io/Writer;IJLorg/apache/commons/net/io/CopyStreamListener;)J
    .locals 8
    .param p0, "source"    # Ljava/io/Reader;
    .param p1, "dest"    # Ljava/io/Writer;
    .param p2, "bufferSize"    # I
    .param p3, "streamSize"    # J
    .param p5, "listener"    # Lorg/apache/commons/net/io/CopyStreamListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 257
    new-array v6, p2, [C

    .line 258
    .local v6, "buffer":[C
    const-wide/16 v1, 0x0

    .line 262
    .local v1, "total":J
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0, v6}, Ljava/io/Reader;->read([C)I

    move-result v3

    .local v3, "chars":I
    const/4 v0, -0x1

    if-eq v3, v0, :cond_1

    .line 266
    if-nez v3, :cond_3

    .line 268
    invoke-virtual {p0}, Ljava/io/Reader;->read()I

    move-result v3

    .line 269
    if-gez v3, :cond_2

    .line 292
    :cond_1
    return-wide v1

    .line 271
    :cond_2
    invoke-virtual {p1, v3}, Ljava/io/Writer;->write(I)V

    .line 272
    invoke-virtual {p1}, Ljava/io/Writer;->flush()V

    .line 273
    const-wide/16 v4, 0x1

    add-long/2addr v1, v4

    .line 274
    if-eqz p5, :cond_0

    move-object v0, p5

    move-wide v4, p3

    .line 275
    invoke-interface/range {v0 .. v5}, Lorg/apache/commons/net/io/CopyStreamListener;->bytesTransferred(JIJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 286
    .end local v3    # "chars":I
    :catch_0
    move-exception v0

    move-object v7, v0

    .line 288
    .local v7, "e":Ljava/io/IOException;
    new-instance v0, Lorg/apache/commons/net/io/CopyStreamException;

    const-string v4, "IOException caught while copying."

    invoke-direct {v0, v4, v1, v2, v7}, Lorg/apache/commons/net/io/CopyStreamException;-><init>(Ljava/lang/String;JLjava/io/IOException;)V

    throw v0

    .line 279
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v3    # "chars":I
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p1, v6, v0, v3}, Ljava/io/Writer;->write([CII)V

    .line 280
    invoke-virtual {p1}, Ljava/io/Writer;->flush()V

    .line 281
    int-to-long v4, v3

    add-long/2addr v1, v4

    .line 282
    if-eqz p5, :cond_0

    move-object v0, p5

    move-wide v4, p3

    .line 283
    invoke-interface/range {v0 .. v5}, Lorg/apache/commons/net/io/CopyStreamListener;->bytesTransferred(JIJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static final copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 2
    .param p0, "source"    # Ljava/io/InputStream;
    .param p1, "dest"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 213
    const/16 v0, 0x400

    invoke-static {p0, p1, v0}, Lorg/apache/commons/net/io/Util;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;I)J
    .locals 6
    .param p0, "source"    # Ljava/io/InputStream;
    .param p1, "dest"    # Ljava/io/OutputStream;
    .param p2, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 202
    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/commons/net/io/Util;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/CopyStreamListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/CopyStreamListener;)J
    .locals 7
    .param p0, "source"    # Ljava/io/InputStream;
    .param p1, "dest"    # Ljava/io/OutputStream;
    .param p2, "bufferSize"    # I
    .param p3, "streamSize"    # J
    .param p5, "listener"    # Lorg/apache/commons/net/io/CopyStreamListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 173
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lorg/apache/commons/net/io/Util;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/CopyStreamListener;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/CopyStreamListener;Z)J
    .locals 8
    .param p0, "source"    # Ljava/io/InputStream;
    .param p1, "dest"    # Ljava/io/OutputStream;
    .param p2, "bufferSize"    # I
    .param p3, "streamSize"    # J
    .param p5, "listener"    # Lorg/apache/commons/net/io/CopyStreamListener;
    .param p6, "flush"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/io/CopyStreamException;
        }
    .end annotation

    .prologue
    .line 95
    new-array v6, p2, [B

    .line 96
    .local v6, "buffer":[B
    const-wide/16 v1, 0x0

    .line 100
    .local v1, "total":J
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0, v6}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "bytes":I
    const/4 v0, -0x1

    if-eq v3, v0, :cond_1

    .line 105
    if-nez v3, :cond_4

    .line 107
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 108
    if-gez v3, :cond_2

    .line 133
    :cond_1
    return-wide v1

    .line 110
    :cond_2
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 111
    if-eqz p6, :cond_3

    .line 112
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 113
    :cond_3
    const-wide/16 v4, 0x1

    add-long/2addr v1, v4

    .line 114
    if-eqz p5, :cond_0

    .line 115
    const/4 v3, 0x1

    move-object v0, p5

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lorg/apache/commons/net/io/CopyStreamListener;->bytesTransferred(JIJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    .end local v3    # "bytes":I
    :catch_0
    move-exception v0

    move-object v7, v0

    .line 129
    .local v7, "e":Ljava/io/IOException;
    new-instance v0, Lorg/apache/commons/net/io/CopyStreamException;

    const-string v4, "IOException caught while copying."

    invoke-direct {v0, v4, v1, v2, v7}, Lorg/apache/commons/net/io/CopyStreamException;-><init>(Ljava/lang/String;JLjava/io/IOException;)V

    throw v0

    .line 119
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v3    # "bytes":I
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p1, v6, v0, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 120
    if-eqz p6, :cond_5

    .line 121
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 122
    :cond_5
    int-to-long v4, v3

    add-long/2addr v1, v4

    .line 123
    if-eqz p5, :cond_0

    move-object v0, p5

    move-wide v4, p3

    .line 124
    invoke-interface/range {v0 .. v5}, Lorg/apache/commons/net/io/CopyStreamListener;->bytesTransferred(JIJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
