.class public final Lorg/apache/commons/net/io/DotTerminatedMessageReader;
.super Ljava/io/Reader;
.source "DotTerminatedMessageReader.java"


# static fields
.field private static final LS:Ljava/lang/String;


# instance fields
.field LS_CHARS:[C

.field private atBeginning:Z

.field private eof:Z

.field private internalBuffer:[C

.field private internalReader:Ljava/io/PushbackReader;

.field private pos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Ljava/io/Reader;-><init>(Ljava/lang/Object;)V

    .line 58
    sget-object v0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS_CHARS:[C

    .line 59
    iget-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS_CHARS:[C

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    .line 60
    iget-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    array-length v0, v0

    iput v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->atBeginning:Z

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    .line 64
    new-instance v0, Ljava/io/PushbackReader;

    invoke-direct {v0, p1}, Ljava/io/PushbackReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    .line 65
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 260
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    if-nez v1, :cond_0

    .line 262
    monitor-exit v0

    .line 277
    :goto_0
    return-void

    .line 265
    :cond_0
    iget-boolean v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    if-nez v1, :cond_2

    .line 267
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->read()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 272
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    .line 273
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->atBeginning:Z

    .line 274
    iget-object v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    array-length v1, v1

    iput v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    .line 275
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    .line 276
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xd

    const/4 v6, -0x1

    const/16 v5, 0x2e

    const/4 v4, 0x1

    .line 84
    iget-object v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    iget-object v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 88
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    iget v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    aget-char v2, v2, v3

    monitor-exit v1

    move v1, v2

    .line 166
    :goto_0
    return v1

    .line 91
    :cond_0
    iget-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    if-eqz v2, :cond_1

    .line 93
    monitor-exit v1

    move v1, v6

    goto :goto_0

    .line 96
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    move-result v0

    .local v0, "ch":I
    if-ne v0, v6, :cond_2

    .line 98
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    .line 99
    monitor-exit v1

    move v1, v6

    goto :goto_0

    .line 102
    :cond_2
    iget-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->atBeginning:Z

    if-eqz v2, :cond_4

    .line 104
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->atBeginning:Z

    .line 105
    if-ne v0, v5, :cond_4

    .line 107
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    move-result v0

    .line 109
    if-eq v0, v5, :cond_3

    .line 112
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    .line 113
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    .line 114
    monitor-exit v1

    move v1, v6

    goto :goto_0

    .line 118
    :cond_3
    monitor-exit v1

    move v1, v5

    goto :goto_0

    .line 123
    :cond_4
    if-ne v0, v7, :cond_5

    .line 125
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    move-result v0

    .line 127
    const/16 v2, 0xa

    if-ne v0, v2, :cond_8

    .line 129
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    move-result v0

    .line 131
    if-ne v0, v5, :cond_7

    .line 133
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    move-result v0

    .line 135
    if-eq v0, v5, :cond_6

    .line 138
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2}, Ljava/io/PushbackReader;->read()I

    .line 139
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->eof:Z

    .line 151
    :goto_1
    iget v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    iget-object v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS_CHARS:[C

    array-length v3, v3

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    .line 152
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS_CHARS:[C

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    iget v5, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    iget-object v6, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->LS_CHARS:[C

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    iget v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    aget-char v0, v2, v3

    .line 166
    :cond_5
    :goto_2
    monitor-exit v1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_6
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    iget v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    sub-int/2addr v3, v4

    iput v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    int-to-char v4, v0

    aput-char v4, v2, v3

    goto :goto_1

    .line 167
    .end local v0    # "ch":I
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 148
    .restart local v0    # "ch":I
    :cond_7
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2, v0}, Ljava/io/PushbackReader;->unread(I)V

    goto :goto_1

    .line 156
    :cond_8
    if-ne v0, v7, :cond_9

    .line 157
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v2, v0}, Ljava/io/PushbackReader;->unread(I)V

    goto :goto_2

    .line 161
    :cond_9
    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    iget v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    sub-int/2addr v3, v4

    iput v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    int-to-char v4, v0

    aput-char v4, v2, v3

    .line 162
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v7

    goto/16 :goto_0
.end method

.method public read([C)I
    .locals 2
    .param p1, "buffer"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->read([CII)I

    move-result v0

    return v0
.end method

.method public read([CII)I
    .locals 6
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 204
    iget-object v3, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 206
    const/4 v4, 0x1

    if-ge p3, v4, :cond_0

    .line 208
    const/4 v4, 0x0

    :try_start_0
    monitor-exit v3

    move v3, v4

    .line 222
    :goto_0
    return v3

    .line 210
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->read()I

    move-result v0

    .local v0, "ch":I
    if-ne v0, v5, :cond_1

    .line 212
    monitor-exit v3

    move v3, v5

    goto :goto_0

    .line 214
    :cond_1
    move v1, p2

    .local v1, "off":I
    move v2, p2

    .line 218
    .end local p2    # "offset":I
    .local v2, "offset":I
    :goto_1
    add-int/lit8 p2, v2, 0x1

    .end local v2    # "offset":I
    .restart local p2    # "offset":I
    int-to-char v4, v0

    aput-char v4, p1, v2

    .line 220
    add-int/lit8 p3, p3, -0x1

    if-lez p3, :cond_2

    invoke-virtual {p0}, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->read()I

    move-result v0

    if-ne v0, v5, :cond_3

    .line 222
    :cond_2
    sub-int v4, p2, v1

    monitor-exit v3

    move v3, v4

    goto :goto_0

    .line 223
    .end local v0    # "ch":I
    .end local v1    # "off":I
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "ch":I
    .restart local v1    # "off":I
    :cond_3
    move v2, p2

    .end local p2    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_1
.end method

.method public ready()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 237
    :try_start_0
    iget v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->pos:I

    iget-object v2, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalBuffer:[C

    array-length v2, v2

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lorg/apache/commons/net/io/DotTerminatedMessageReader;->internalReader:Ljava/io/PushbackReader;

    invoke-virtual {v1}, Ljava/io/PushbackReader;->ready()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    monitor-exit v0

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
