.class public Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;
.super Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;
.source "UnixFTPEntryParser.java"


# static fields
.field static final DEFAULT_DATE_FORMAT:Ljava/lang/String; = "MMM d yyyy"

.field static final DEFAULT_RECENT_DATE_FORMAT:Ljava/lang/String; = "MMM d HH:mm"

.field public static final NUMERIC_DATE_CONFIG:Lorg/apache/commons/net/ftp/FTPClientConfig;

.field static final NUMERIC_DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd HH:mm"

.field private static final REGEX:Ljava/lang/String; = "([bcdelfmpSs-])(((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-])))\\+?\\s*(\\d+)\\s+(?:(\\S+(?:\\s\\S+)*?)\\s+)?(?:(\\S+(?:\\s\\S+)*)\\s+)?(\\d+(?:,\\s*\\d+)?)\\s+((?:\\d+[-/]\\d+[-/]\\d+)|(?:[a-zA-Z]{3}\\s+\\d{1,2})|(?:\\d{1,2}\\s+[a-zA-Z]{3}))\\s+(\\d+(?::\\d+)?)\\s+(\\S*)(\\s*.*)"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 58
    new-instance v0, Lorg/apache/commons/net/ftp/FTPClientConfig;

    const-string v1, "UNIX"

    const-string v2, "yyyy-MM-dd HH:mm"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/FTPClientConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->NUMERIC_DATE_CONFIG:Lorg/apache/commons/net/ftp/FTPClientConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;-><init>(Lorg/apache/commons/net/ftp/FTPClientConfig;)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/FTPClientConfig;)V
    .locals 1
    .param p1, "config"    # Lorg/apache/commons/net/ftp/FTPClientConfig;

    .prologue
    .line 134
    const-string v0, "([bcdelfmpSs-])(((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-])))\\+?\\s*(\\d+)\\s+(?:(\\S+(?:\\s\\S+)*?)\\s+)?(?:(\\S+(?:\\s\\S+)*)\\s+)?(\\d+(?:,\\s*\\d+)?)\\s+((?:\\d+[-/]\\d+[-/]\\d+)|(?:[a-zA-Z]{3}\\s+\\d{1,2})|(?:\\d{1,2}\\s+[a-zA-Z]{3}))\\s+(\\d+(?::\\d+)?)\\s+(\\S*)(\\s*.*)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;-><init>(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->configure(Lorg/apache/commons/net/ftp/FTPClientConfig;)V

    .line 136
    return-void
.end method


# virtual methods
.method protected getDefaultConfiguration()Lorg/apache/commons/net/ftp/FTPClientConfig;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 290
    new-instance v0, Lorg/apache/commons/net/ftp/FTPClientConfig;

    const-string v1, "UNIX"

    const-string v2, "MMM d yyyy"

    const-string v3, "MMM d HH:mm"

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/FTPClientConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public parseFTPEntry(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 22
    .param p1, "entry"    # Ljava/lang/String;

    .prologue
    .line 150
    new-instance v9, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v9}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 151
    .local v9, "file":Lorg/apache/commons/net/ftp/FTPFile;
    move-object v0, v9

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setRawListing(Ljava/lang/String;)V

    .line 153
    const/4 v14, 0x0

    .line 155
    .local v14, "isDevice":Z
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 157
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v17

    .line 158
    .local v17, "typeStr":Ljava/lang/String;
    const/16 v19, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 159
    .local v13, "hardLinkCount":Ljava/lang/String;
    const/16 v19, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v18

    .line 160
    .local v18, "usr":Ljava/lang/String;
    const/16 v19, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 161
    .local v12, "grp":Ljava/lang/String;
    const/16 v19, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 162
    .local v10, "filesize":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v20, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 163
    .local v5, "datestr":Ljava/lang/String;
    const/16 v19, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v15

    .line 164
    .local v15, "name":Ljava/lang/String;
    const/16 v19, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 168
    .local v7, "endtoken":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    move-object v1, v5

    invoke-super {v0, v1}, Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;->parseTimestamp(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v19

    move-object v0, v9

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setTimestamp(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_2

    .line 177
    :goto_0
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v19

    sparse-switch v19, :sswitch_data_0

    .line 198
    const/16 v16, 0x3

    .line 201
    .local v16, "type":I
    :goto_1
    move-object v0, v9

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 203
    const/4 v11, 0x4

    .line 204
    .local v11, "g":I
    const/4 v4, 0x0

    .local v4, "access":I
    :goto_2
    const/16 v19, 0x3

    move v0, v4

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 207
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move v1, v11

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_0

    const/16 v20, 0x1

    :goto_3
    move-object v0, v9

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 209
    const/16 v19, 0x1

    add-int/lit8 v20, v11, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    const/16 v20, 0x1

    :goto_4
    move-object v0, v9

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 212
    add-int/lit8 v19, v11, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 213
    .local v8, "execPerm":Ljava/lang/String;
    const-string v19, "-"

    move-object v0, v8

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const/16 v19, 0x0

    move-object v0, v8

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v19

    if-nez v19, :cond_2

    .line 215
    const/16 v19, 0x2

    const/16 v20, 0x1

    move-object v0, v9

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 204
    :goto_5
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v11, v11, 0x4

    goto :goto_2

    .line 180
    .end local v4    # "access":I
    .end local v8    # "execPerm":Ljava/lang/String;
    .end local v11    # "g":I
    .end local v16    # "type":I
    :sswitch_0
    const/16 v16, 0x1

    .line 181
    .restart local v16    # "type":I
    goto/16 :goto_1

    .line 183
    .end local v16    # "type":I
    :sswitch_1
    const/16 v16, 0x2

    .line 184
    .restart local v16    # "type":I
    goto/16 :goto_1

    .line 186
    .end local v16    # "type":I
    :sswitch_2
    const/16 v16, 0x2

    .line 187
    .restart local v16    # "type":I
    goto/16 :goto_1

    .line 190
    .end local v16    # "type":I
    :sswitch_3
    const/4 v14, 0x1

    .line 195
    :sswitch_4
    const/16 v16, 0x0

    .line 196
    .restart local v16    # "type":I
    goto/16 :goto_1

    .line 207
    .restart local v4    # "access":I
    .restart local v11    # "g":I
    :cond_0
    const/16 v20, 0x0

    goto :goto_3

    .line 209
    :cond_1
    const/16 v20, 0x0

    goto :goto_4

    .line 219
    .restart local v8    # "execPerm":Ljava/lang/String;
    :cond_2
    const/16 v19, 0x2

    const/16 v20, 0x0

    move-object v0, v9

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    goto :goto_5

    .line 223
    .end local v8    # "execPerm":Ljava/lang/String;
    :cond_3
    if-nez v14, :cond_4

    .line 227
    :try_start_1
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move-object v0, v9

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setHardLinkCount(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 235
    :cond_4
    :goto_6
    move-object v0, v9

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    .line 236
    invoke-virtual {v9, v12}, Lorg/apache/commons/net/ftp/FTPFile;->setGroup(Ljava/lang/String;)V

    .line 240
    :try_start_2
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v19

    move-object v0, v9

    move-wide/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    .line 247
    :goto_7
    if-nez v7, :cond_5

    .line 249
    invoke-virtual {v9, v15}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    :goto_8
    move-object/from16 v19, v9

    .line 279
    .end local v4    # "access":I
    .end local v5    # "datestr":Ljava/lang/String;
    .end local v7    # "endtoken":Ljava/lang/String;
    .end local v10    # "filesize":Ljava/lang/String;
    .end local v11    # "g":I
    .end local v12    # "grp":Ljava/lang/String;
    .end local v13    # "hardLinkCount":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "type":I
    .end local v17    # "typeStr":Ljava/lang/String;
    .end local v18    # "usr":Ljava/lang/String;
    :goto_9
    return-object v19

    .line 255
    .restart local v4    # "access":I
    .restart local v5    # "datestr":Ljava/lang/String;
    .restart local v7    # "endtoken":Ljava/lang/String;
    .restart local v10    # "filesize":Ljava/lang/String;
    .restart local v11    # "g":I
    .restart local v12    # "grp":Ljava/lang/String;
    .restart local v13    # "hardLinkCount":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v16    # "type":I
    .restart local v17    # "typeStr":Ljava/lang/String;
    .restart local v18    # "usr":Ljava/lang/String;
    :cond_5
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object v1, v15

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object v1, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 256
    const/16 v19, 0x2

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 259
    const-string v19, " -> "

    move-object v0, v15

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 261
    .local v6, "end":I
    const/16 v19, -0x1

    move v0, v6

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 263
    invoke-virtual {v9, v15}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto :goto_8

    .line 267
    :cond_6
    const/16 v19, 0x0

    move-object v0, v15

    move/from16 v1, v19

    move v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    move-object v0, v9

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 268
    add-int/lit8 v19, v6, 0x4

    move-object v0, v15

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    move-object v0, v9

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setLink(Ljava/lang/String;)V

    goto :goto_8

    .line 274
    .end local v6    # "end":I
    :cond_7
    invoke-virtual {v9, v15}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto :goto_8

    .line 279
    .end local v4    # "access":I
    .end local v5    # "datestr":Ljava/lang/String;
    .end local v7    # "endtoken":Ljava/lang/String;
    .end local v10    # "filesize":Ljava/lang/String;
    .end local v11    # "g":I
    .end local v12    # "grp":Ljava/lang/String;
    .end local v13    # "hardLinkCount":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "type":I
    .end local v17    # "typeStr":Ljava/lang/String;
    .end local v18    # "usr":Ljava/lang/String;
    :cond_8
    const/16 v19, 0x0

    goto :goto_9

    .line 242
    .restart local v4    # "access":I
    .restart local v5    # "datestr":Ljava/lang/String;
    .restart local v7    # "endtoken":Ljava/lang/String;
    .restart local v10    # "filesize":Ljava/lang/String;
    .restart local v11    # "g":I
    .restart local v12    # "grp":Ljava/lang/String;
    .restart local v13    # "hardLinkCount":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v16    # "type":I
    .restart local v17    # "typeStr":Ljava/lang/String;
    .restart local v18    # "usr":Ljava/lang/String;
    :catch_0
    move-exception v19

    goto :goto_7

    .line 229
    :catch_1
    move-exception v19

    goto :goto_6

    .line 170
    .end local v4    # "access":I
    .end local v11    # "g":I
    .end local v16    # "type":I
    :catch_2
    move-exception v19

    goto/16 :goto_0

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_4
        0x62 -> :sswitch_3
        0x63 -> :sswitch_3
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_4
        0x6c -> :sswitch_2
    .end sparse-switch
.end method
