.class public Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;
.super Ljava/lang/Object;
.source "DefaultFTPFileEntryParserFactory.java"

# interfaces
.implements Lorg/apache/commons/net/ftp/parser/FTPFileEntryParserFactory;


# instance fields
.field private config:Lorg/apache/commons/net/ftp/FTPClientConfig;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    return-void
.end method


# virtual methods
.method public createFileEntryParser(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 9
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 81
    if-nez p1, :cond_0

    .line 82
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v7, "Parser key cannot be null"

    invoke-direct {v6, v7}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 84
    :cond_0
    const/4 v4, 0x0

    .line 85
    .local v4, "parserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x0

    .line 88
    .local v3, "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/commons/net/ftp/parser/ParserInitializationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v4

    .line 90
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    check-cast v3, Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/apache/commons/net/ftp/parser/ParserInitializationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5

    .line 161
    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :goto_0
    instance-of v6, v3, Lorg/apache/commons/net/ftp/Configurable;

    if-eqz v6, :cond_1

    .line 162
    move-object v0, v3

    check-cast v0, Lorg/apache/commons/net/ftp/Configurable;

    move-object v6, v0

    iget-object v7, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    invoke-interface {v6, v7}, Lorg/apache/commons/net/ftp/Configurable;->configure(Lorg/apache/commons/net/ftp/FTPClientConfig;)V

    .line 164
    :cond_1
    return-object v3

    .line 91
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 92
    .local v1, "e":Ljava/lang/ClassCastException;
    :try_start_2
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " does not implement the interface "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "org.apache.commons.net.ftp.FTPFileEntryParser."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lorg/apache/commons/net/ftp/parser/ParserInitializationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5

    .line 97
    .end local v1    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v6

    move-object v1, v6

    .line 101
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    :try_start_3
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 102
    .local v5, "ukey":Ljava/lang/String;
    const-string v6, "UNIX"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_2

    .line 104
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createUnixFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto :goto_0

    .line 106
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_2
    const-string v6, "VMS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_3

    .line 108
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createVMSVersioningFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto :goto_0

    .line 110
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_3
    const-string v6, "WINDOWS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_4

    .line 112
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createNTFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto :goto_0

    .line 114
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_4
    const-string v6, "OS/2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_5

    .line 116
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createOS2FTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto :goto_0

    .line 118
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_5
    const-string v6, "OS/400"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_6

    const-string v6, "AS/400"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_7

    .line 121
    :cond_6
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createOS400FTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto/16 :goto_0

    .line 123
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_7
    const-string v6, "MVS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_8

    .line 125
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createMVSEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto/16 :goto_0

    .line 127
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_8
    const-string v6, "NETWARE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_9

    .line 129
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createNetwareFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto/16 :goto_0

    .line 131
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_9
    const-string v6, "TYPE: L8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_a

    .line 135
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createUnixFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v3

    .restart local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    goto/16 :goto_0

    .line 139
    .end local v3    # "parser":Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    :cond_a
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown parser type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_3
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_3 .. :try_end_3} :catch_2

    .line 143
    .end local v5    # "ukey":Ljava/lang/String;
    :catch_2
    move-exception v6

    move-object v2, v6

    .line 144
    .local v2, "nf":Ljava/lang/NoClassDefFoundError;
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v7, "Error initializing parser"

    invoke-direct {v6, v7, v2}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 148
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    .end local v2    # "nf":Ljava/lang/NoClassDefFoundError;
    :catch_3
    move-exception v6

    move-object v1, v6

    .line 150
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v7, "Error initializing parser"

    invoke-direct {v6, v7, v1}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 152
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_4
    move-exception v6

    move-object v1, v6

    .line 154
    .local v1, "e":Lorg/apache/commons/net/ftp/parser/ParserInitializationException;
    throw v1

    .line 156
    .end local v1    # "e":Lorg/apache/commons/net/ftp/parser/ParserInitializationException;
    :catch_5
    move-exception v6

    move-object v1, v6

    .line 158
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v6, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v7, "Error initializing parser"

    invoke-direct {v6, v7, v1}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method

.method public createFileEntryParser(Lorg/apache/commons/net/ftp/FTPClientConfig;)Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 2
    .param p1, "config"    # Lorg/apache/commons/net/ftp/FTPClientConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/commons/net/ftp/parser/ParserInitializationException;
        }
    .end annotation

    .prologue
    .line 190
    iput-object p1, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    .line 191
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClientConfig;->getServerSystemKey()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->createFileEntryParser(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    move-result-object v1

    return-object v1
.end method

.method public createMVSEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lorg/apache/commons/net/ftp/parser/MVSFTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/MVSFTPEntryParser;-><init>()V

    return-object v0
.end method

.method public createNTFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    if-eqz v0, :cond_0

    const-string v0, "WINDOWS"

    iget-object v1, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPClientConfig;->getServerSystemKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    new-instance v0, Lorg/apache/commons/net/ftp/parser/NTFTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/NTFTPEntryParser;-><init>()V

    .line 217
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/commons/net/ftp/parser/CompositeFileEntryParser;

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    const/4 v2, 0x0

    new-instance v3, Lorg/apache/commons/net/ftp/parser/NTFTPEntryParser;

    invoke-direct {v3}, Lorg/apache/commons/net/ftp/parser/NTFTPEntryParser;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;

    invoke-direct {v3}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;-><init>()V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/CompositeFileEntryParser;-><init>([Lorg/apache/commons/net/ftp/FTPFileEntryParser;)V

    goto :goto_0
.end method

.method public createNetwareFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lorg/apache/commons/net/ftp/parser/NetwareFTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/NetwareFTPEntryParser;-><init>()V

    return-object v0
.end method

.method public createOS2FTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 1

    .prologue
    .line 227
    new-instance v0, Lorg/apache/commons/net/ftp/parser/OS2FTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/OS2FTPEntryParser;-><init>()V

    return-object v0
.end method

.method public createOS400FTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    if-eqz v0, :cond_0

    const-string v0, "OS/400"

    iget-object v1, p0, Lorg/apache/commons/net/ftp/parser/DefaultFTPFileEntryParserFactory;->config:Lorg/apache/commons/net/ftp/FTPClientConfig;

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPClientConfig;->getServerSystemKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Lorg/apache/commons/net/ftp/parser/OS400FTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/OS400FTPEntryParser;-><init>()V

    .line 237
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/commons/net/ftp/parser/CompositeFileEntryParser;

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/commons/net/ftp/FTPFileEntryParser;

    const/4 v2, 0x0

    new-instance v3, Lorg/apache/commons/net/ftp/parser/OS400FTPEntryParser;

    invoke-direct {v3}, Lorg/apache/commons/net/ftp/parser/OS400FTPEntryParser;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;

    invoke-direct {v3}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;-><init>()V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/CompositeFileEntryParser;-><init>([Lorg/apache/commons/net/ftp/FTPFileEntryParser;)V

    goto :goto_0
.end method

.method public createUnixFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/UnixFTPEntryParser;-><init>()V

    return-object v0
.end method

.method public createVMSVersioningFTPEntryParser()Lorg/apache/commons/net/ftp/FTPFileEntryParser;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lorg/apache/commons/net/ftp/parser/VMSVersioningFTPEntryParser;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/VMSVersioningFTPEntryParser;-><init>()V

    return-object v0
.end method
