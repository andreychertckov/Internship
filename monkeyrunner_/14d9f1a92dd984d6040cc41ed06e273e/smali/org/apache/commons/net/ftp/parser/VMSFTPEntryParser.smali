.class public Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;
.super Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;
.source "VMSFTPEntryParser.java"


# static fields
.field private static final DEFAULT_DATE_FORMAT:Ljava/lang/String; = "d-MMM-yyyy HH:mm:ss"

.field private static final REGEX:Ljava/lang/String; = "(.*;[0-9]+)\\s*(\\d+)/\\d+\\s*(\\S+)\\s+(\\S+)\\s+\\[(([0-9$A-Za-z_]+)|([0-9$A-Za-z_]+),([0-9$a-zA-Z_]+))\\]?\\s*\\([a-zA-Z]*,([a-zA-Z]*),([a-zA-Z]*),([a-zA-Z]*)\\)"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;-><init>(Lorg/apache/commons/net/ftp/FTPClientConfig;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/FTPClientConfig;)V
    .locals 1
    .param p1, "config"    # Lorg/apache/commons/net/ftp/FTPClientConfig;

    .prologue
    .line 97
    const-string v0, "(.*;[0-9]+)\\s*(\\d+)/\\d+\\s*(\\S+)\\s+(\\S+)\\s+\\[(([0-9$A-Za-z_]+)|([0-9$A-Za-z_]+),([0-9$a-zA-Z_]+))\\]?\\s*\\([a-zA-Z]*,([a-zA-Z]*),([a-zA-Z]*),([a-zA-Z]*)\\)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;-><init>(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->configure(Lorg/apache/commons/net/ftp/FTPClientConfig;)V

    .line 99
    return-void
.end method


# virtual methods
.method protected getDefaultConfiguration()Lorg/apache/commons/net/ftp/FTPClientConfig;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 276
    new-instance v0, Lorg/apache/commons/net/ftp/FTPClientConfig;

    const-string v1, "VMS"

    const-string v2, "d-MMM-yyyy HH:mm:ss"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/FTPClientConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected isVersioning()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public parseFTPEntry(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 21
    .param p1, "entry"    # Ljava/lang/String;

    .prologue
    .line 141
    const-wide/16 v8, 0x200

    .line 143
    .local v8, "longBlock":J
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 145
    new-instance v6, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v6}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 146
    .local v6, "f":Lorg/apache/commons/net/ftp/FTPFile;
    move-object v0, v6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setRawListing(Ljava/lang/String;)V

    .line 147
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 148
    .local v10, "name":Ljava/lang/String;
    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v14

    .line 149
    .local v14, "size":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "datestr":Ljava/lang/String;
    const/16 v19, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v11

    .line 151
    .local v11, "owner":Ljava/lang/String;
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object v13, v0

    .line 152
    .local v13, "permissions":[Ljava/lang/String;
    const/16 v19, 0x0

    const/16 v20, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 153
    const/16 v19, 0x1

    const/16 v20, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 154
    const/16 v19, 0x2

    const/16 v20, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v13, v19

    .line 157
    :try_start_0
    move-object/from16 v0, p0

    move-object v1, v5

    invoke-super {v0, v1}, Lorg/apache/commons/net/ftp/parser/ConfigurableFTPFileEntryParserImpl;->parseTimestamp(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v19

    move-object v0, v6

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setTimestamp(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    new-instance v17, Ljava/util/StringTokenizer;

    const-string v19, ","

    move-object/from16 v0, v17

    move-object v1, v11

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .local v17, "t":Ljava/util/StringTokenizer;
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v19

    packed-switch v19, :pswitch_data_0

    .line 178
    const/4 v7, 0x0

    .line 179
    .local v7, "grp":Ljava/lang/String;
    const/16 v18, 0x0

    .line 182
    .local v18, "user":Ljava/lang/String;
    :goto_1
    const-string v19, ".DIR"

    move-object v0, v10

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 184
    const/16 v19, 0x1

    move-object v0, v6

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 192
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/ftp/parser/VMSFTPEntryParser;->isVersioning()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 194
    invoke-virtual {v6, v10}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 203
    :goto_3
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v19

    mul-long v15, v19, v8

    .line 204
    .local v15, "sizeInBytes":J
    move-object v0, v6

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V

    .line 206
    invoke-virtual {v6, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setGroup(Ljava/lang/String;)V

    .line 207
    move-object v0, v6

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    .line 215
    const/4 v4, 0x0

    .local v4, "access":I
    :goto_4
    const/16 v19, 0x3

    move v0, v4

    move/from16 v1, v19

    if-ge v0, v1, :cond_5

    .line 217
    aget-object v12, v13, v4

    .line 219
    .local v12, "permission":Ljava/lang/String;
    const/16 v19, 0x0

    const/16 v20, 0x52

    move-object v0, v12

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v20

    if-ltz v20, :cond_2

    const/16 v20, 0x1

    :goto_5
    move-object v0, v6

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 220
    const/16 v19, 0x1

    const/16 v20, 0x57

    move-object v0, v12

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v20

    if-ltz v20, :cond_3

    const/16 v20, 0x1

    :goto_6
    move-object v0, v6

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 221
    const/16 v19, 0x2

    const/16 v20, 0x45

    move-object v0, v12

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v20

    if-ltz v20, :cond_4

    const/16 v20, 0x1

    :goto_7
    move-object v0, v6

    move v1, v4

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setPermission(IIZ)V

    .line 215
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 170
    .end local v4    # "access":I
    .end local v7    # "grp":Ljava/lang/String;
    .end local v12    # "permission":Ljava/lang/String;
    .end local v15    # "sizeInBytes":J
    .end local v18    # "user":Ljava/lang/String;
    :pswitch_0
    const/4 v7, 0x0

    .line 171
    .restart local v7    # "grp":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    .line 172
    .restart local v18    # "user":Ljava/lang/String;
    goto/16 :goto_1

    .line 174
    .end local v7    # "grp":Ljava/lang/String;
    .end local v18    # "user":Ljava/lang/String;
    :pswitch_1
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 175
    .restart local v7    # "grp":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    .line 176
    .restart local v18    # "user":Ljava/lang/String;
    goto/16 :goto_1

    .line 188
    :cond_0
    const/16 v19, 0x0

    move-object v0, v6

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    goto/16 :goto_2

    .line 198
    :cond_1
    const/16 v19, 0x0

    const-string v20, ";"

    move-object v0, v10

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v20

    move-object v0, v10

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 199
    invoke-virtual {v6, v10}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 219
    .restart local v4    # "access":I
    .restart local v12    # "permission":Ljava/lang/String;
    .restart local v15    # "sizeInBytes":J
    :cond_2
    const/16 v20, 0x0

    goto :goto_5

    .line 220
    :cond_3
    const/16 v20, 0x0

    goto :goto_6

    .line 221
    :cond_4
    const/16 v20, 0x0

    goto :goto_7

    .end local v12    # "permission":Ljava/lang/String;
    :cond_5
    move-object/from16 v19, v6

    .line 226
    .end local v4    # "access":I
    .end local v5    # "datestr":Ljava/lang/String;
    .end local v6    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .end local v7    # "grp":Ljava/lang/String;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "owner":Ljava/lang/String;
    .end local v13    # "permissions":[Ljava/lang/String;
    .end local v14    # "size":Ljava/lang/String;
    .end local v15    # "sizeInBytes":J
    .end local v17    # "t":Ljava/util/StringTokenizer;
    .end local v18    # "user":Ljava/lang/String;
    :goto_8
    return-object v19

    :cond_6
    const/16 v19, 0x0

    goto :goto_8

    .line 159
    .restart local v5    # "datestr":Ljava/lang/String;
    .restart local v6    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .restart local v10    # "name":Ljava/lang/String;
    .restart local v11    # "owner":Ljava/lang/String;
    .restart local v13    # "permissions":[Ljava/lang/String;
    .restart local v14    # "size":Ljava/lang/String;
    :catch_0
    move-exception v19

    goto/16 :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public parseFileList(Ljava/io/InputStream;)[Lorg/apache/commons/net/ftp/FTPFile;
    .locals 2
    .param p1, "listStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 121
    new-instance v0, Lorg/apache/commons/net/ftp/FTPListParseEngine;

    invoke-direct {v0, p0}, Lorg/apache/commons/net/ftp/FTPListParseEngine;-><init>(Lorg/apache/commons/net/ftp/FTPFileEntryParser;)V

    .line 122
    .local v0, "engine":Lorg/apache/commons/net/ftp/FTPListParseEngine;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/commons/net/ftp/FTPListParseEngine;->readServerList(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPListParseEngine;->getFiles()[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v1

    return-object v1
.end method

.method public readNextEntry(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 4
    .param p1, "reader"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "line":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v0, "entry":Ljava/lang/StringBuilder;
    :goto_0
    if-eqz v1, :cond_2

    .line 249
    const-string v2, "Directory"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Total"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 250
    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 251
    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 261
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 259
    :cond_3
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 261
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
