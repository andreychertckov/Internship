.class public Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;
.super Lorg/apache/commons/net/ftp/parser/RegexFTPFileEntryParserImpl;
.source "EnterpriseUnixFTPEntryParser.java"


# static fields
.field private static final MONTHS:Ljava/lang/String; = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"

.field private static final REGEX:Ljava/lang/String; = "(([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z]))(\\S*)\\s*(\\S+)\\s*(\\S*)\\s*(\\d*)\\s*(\\d*)\\s*(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s*((?:[012]\\d*)|(?:3[01]))\\s*((\\d\\d\\d\\d)|((?:[01]\\d)|(?:2[0123])):([012345]\\d))\\s(\\S*)(\\s*.*)"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "(([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z])([\\-]|[A-Z]))(\\S*)\\s*(\\S+)\\s*(\\S*)\\s*(\\d*)\\s*(\\d*)\\s*(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s*((?:[012]\\d*)|(?:3[01]))\\s*((\\d\\d\\d\\d)|((?:[01]\\d)|(?:2[0123])):([012345]\\d))\\s(\\S*)(\\s*.*)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/RegexFTPFileEntryParserImpl;-><init>(Ljava/lang/String;)V

    .line 71
    return-void
.end method


# virtual methods
.method public parseFTPEntry(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 19
    .param p1, "entry"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v5, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v5}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 87
    .local v5, "file":Lorg/apache/commons/net/ftp/FTPFile;
    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setRawListing(Ljava/lang/String;)V

    .line 89
    invoke-virtual/range {p0 .. p1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->matches(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 91
    const/16 v17, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v14

    .line 92
    .local v14, "usr":Ljava/lang/String;
    const/16 v17, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 93
    .local v7, "grp":Ljava/lang/String;
    const/16 v17, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 94
    .local v6, "filesize":Ljava/lang/String;
    const/16 v17, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 95
    .local v10, "mo":Ljava/lang/String;
    const/16 v17, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 96
    .local v4, "da":Ljava/lang/String;
    const/16 v17, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v16

    .line 97
    .local v16, "yr":Ljava/lang/String;
    const/16 v17, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 98
    .local v8, "hr":Ljava/lang/String;
    const/16 v17, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 99
    .local v9, "min":Ljava/lang/String;
    const/16 v17, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/parser/EnterpriseUnixFTPEntryParser;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 101
    .local v12, "name":Ljava/lang/String;
    const/16 v17, 0x0

    move-object v0, v5

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 102
    invoke-virtual {v5, v14}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v5, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setGroup(Ljava/lang/String;)V

    .line 106
    :try_start_0
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    move-object v0, v5

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 113
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 114
    .local v3, "cal":Ljava/util/Calendar;
    const/16 v17, 0xe

    const/16 v18, 0x0

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 115
    const/16 v17, 0xd

    const/16 v18, 0x0

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 117
    const/16 v17, 0xc

    const/16 v18, 0x0

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 119
    const/16 v17, 0xb

    const/16 v18, 0x0

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 124
    :try_start_1
    const-string v17, "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"

    move-object/from16 v0, v17

    move-object v1, v10

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    .line 125
    .local v13, "pos":I
    div-int/lit8 v11, v13, 0x4

    .line 126
    .local v11, "month":I
    if-eqz v16, :cond_0

    .line 129
    const/16 v17, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 150
    :goto_1
    const/16 v17, 0x2

    move-object v0, v3

    move/from16 v1, v17

    move v2, v11

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 152
    const/16 v17, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 154
    invoke-virtual {v5, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setTimestamp(Ljava/util/Calendar;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 160
    .end local v11    # "month":I
    .end local v13    # "pos":I
    :goto_2
    invoke-virtual {v5, v12}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    move-object/from16 v17, v5

    .line 164
    .end local v3    # "cal":Ljava/util/Calendar;
    .end local v4    # "da":Ljava/lang/String;
    .end local v6    # "filesize":Ljava/lang/String;
    .end local v7    # "grp":Ljava/lang/String;
    .end local v8    # "hr":Ljava/lang/String;
    .end local v9    # "min":Ljava/lang/String;
    .end local v10    # "mo":Ljava/lang/String;
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "usr":Ljava/lang/String;
    .end local v16    # "yr":Ljava/lang/String;
    :goto_3
    return-object v17

    .line 135
    .restart local v3    # "cal":Ljava/util/Calendar;
    .restart local v4    # "da":Ljava/lang/String;
    .restart local v6    # "filesize":Ljava/lang/String;
    .restart local v7    # "grp":Ljava/lang/String;
    .restart local v8    # "hr":Ljava/lang/String;
    .restart local v9    # "min":Ljava/lang/String;
    .restart local v10    # "mo":Ljava/lang/String;
    .restart local v11    # "month":I
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v13    # "pos":I
    .restart local v14    # "usr":Ljava/lang/String;
    .restart local v16    # "yr":Ljava/lang/String;
    :cond_0
    const/16 v17, 0x1

    :try_start_2
    move-object v0, v3

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 139
    .local v15, "year":I
    const/16 v17, 0x2

    move-object v0, v3

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v17

    move/from16 v0, v17

    move v1, v11

    if-ge v0, v1, :cond_1

    .line 141
    add-int/lit8 v15, v15, -0x1

    .line 143
    :cond_1
    const/16 v17, 0x1

    move-object v0, v3

    move/from16 v1, v17

    move v2, v15

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 145
    const/16 v17, 0xb

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 147
    const/16 v17, 0xc

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object v0, v3

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 156
    .end local v11    # "month":I
    .end local v13    # "pos":I
    .end local v15    # "year":I
    :catch_0
    move-exception v17

    goto :goto_2

    .line 164
    .end local v3    # "cal":Ljava/util/Calendar;
    .end local v4    # "da":Ljava/lang/String;
    .end local v6    # "filesize":Ljava/lang/String;
    .end local v7    # "grp":Ljava/lang/String;
    .end local v8    # "hr":Ljava/lang/String;
    .end local v9    # "min":Ljava/lang/String;
    .end local v10    # "mo":Ljava/lang/String;
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "usr":Ljava/lang/String;
    .end local v16    # "yr":Ljava/lang/String;
    :cond_2
    const/16 v17, 0x0

    goto :goto_3

    .line 108
    .restart local v4    # "da":Ljava/lang/String;
    .restart local v6    # "filesize":Ljava/lang/String;
    .restart local v7    # "grp":Ljava/lang/String;
    .restart local v8    # "hr":Ljava/lang/String;
    .restart local v9    # "min":Ljava/lang/String;
    .restart local v10    # "mo":Ljava/lang/String;
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v14    # "usr":Ljava/lang/String;
    .restart local v16    # "yr":Ljava/lang/String;
    :catch_1
    move-exception v17

    goto/16 :goto_0
.end method
