.class public Lcom/google/progress/Locate;
.super Ljava/lang/Object;
.source "Locate.java"


# instance fields
.field private context:Landroid/content/Context;

.field gsm:Landroid/telephony/gsm/GsmCellLocation;

.field telManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/progress/Locate;->context:Landroid/content/Context;

    .line 43
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/progress/Locate;->telManager:Landroid/telephony/TelephonyManager;

    .line 44
    return-void
.end method


# virtual methods
.method public getLocation()Ljava/lang/String;
    .locals 29

    .prologue
    .line 49
    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    .line 51
    .local v22, "sbLocation":Ljava/lang/StringBuffer;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/Locate;->telManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v3

    check-cast v3, Landroid/telephony/gsm/GsmCellLocation;

    move-object v0, v3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/progress/Locate;->gsm:Landroid/telephony/gsm/GsmCellLocation;

    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/Locate;->gsm:Landroid/telephony/gsm/GsmCellLocation;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v5

    .line 53
    .local v5, "cid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/Locate;->gsm:Landroid/telephony/gsm/GsmCellLocation;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v13

    .line 54
    .local v13, "lac":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/Locate;->telManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v25

    .line 55
    .local v25, "strNetOper":Ljava/lang/String;
    const/16 v26, 0x0

    const/16 v27, 0x3

    invoke-virtual/range {v25 .. v27}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 56
    .local v16, "mcc":I
    const/16 v26, 0x3

    const/16 v27, 0x5

    invoke-virtual/range {v25 .. v27}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 58
    .local v17, "mnc":I
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 59
    .local v10, "holder":Lorg/json/JSONObject;
    const-string v26, "version"

    const-string v27, "1.1.0"

    move-object v0, v10

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    const-string v26, "host"

    const-string v27, "maps.google.com"

    move-object v0, v10

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    const-string v26, "request_address"

    const/16 v27, 0x1

    move-object v0, v10

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 62
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 63
    .local v3, "array":Lorg/json/JSONArray;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 64
    .local v7, "data":Lorg/json/JSONObject;
    const-string v26, "cell_id"

    move-object v0, v7

    move-object/from16 v1, v26

    move v2, v5

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 65
    const-string v26, "location_area_code"

    move-object v0, v7

    move-object/from16 v1, v26

    move v2, v13

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 66
    const-string v26, "mobile_country_code"

    move-object v0, v7

    move-object/from16 v1, v26

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 67
    const-string v26, "mobile_network_code"

    move-object v0, v7

    move-object/from16 v1, v26

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 68
    invoke-virtual {v3, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 69
    const-string v26, "cell_towers"

    move-object v0, v10

    move-object/from16 v1, v26

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    new-instance v6, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 71
    .local v6, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v18, Lorg/apache/http/client/methods/HttpPost;

    const-string v26, "http://www.google.com/loc/json"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 72
    .local v18, "post":Lorg/apache/http/client/methods/HttpPost;
    new-instance v23, Lorg/apache/http/entity/StringEntity;

    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 73
    .local v23, "se":Lorg/apache/http/entity/StringEntity;
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 74
    move-object v0, v6

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v19

    .line 75
    .local v19, "resp":Lorg/apache/http/HttpResponse;
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v27, "GPS\u83b7\u53d6\u7ecf\u7eac\u5ea6\u5f97\u5230\u54cd\u5e94"

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 76
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    .line 77
    .local v8, "entity":Lorg/apache/http/HttpEntity;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v26, Ljava/io/InputStreamReader;

    .line 78
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 77
    move-object v0, v4

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 79
    .local v4, "br":Ljava/io/BufferedReader;
    new-instance v21, Ljava/lang/StringBuffer;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuffer;-><init>()V

    .line 80
    .local v21, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    .line 81
    .local v20, "result":Ljava/lang/String;
    :goto_0
    if-nez v20, :cond_0

    .line 85
    new-instance v11, Lorg/json/JSONObject;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object v0, v11

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 86
    .local v11, "jsonObject":Lorg/json/JSONObject;
    new-instance v12, Lorg/json/JSONObject;

    .line 87
    const-string v26, "location"

    move-object v0, v11

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 86
    move-object v0, v12

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 88
    .local v12, "jsonObject1":Lorg/json/JSONObject;
    const-string v26, "latitude"

    move-object v0, v12

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 89
    .local v14, "latitude":Ljava/lang/String;
    const-string v26, "longitude"

    move-object v0, v12

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 90
    .local v15, "longitude":Ljava/lang/String;
    const-string v24, "(\u57fa\u7ad9)\u6253\u5f00\u5730\u56fe\u67e5\u770b"

    .line 91
    .local v24, "strAddress":Ljava/lang/String;
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "\u7eac\u5ea6:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object v1, v14

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, " \u7ecf\u5ea6:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object v1, v15

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, " \u4f4d\u7f6e:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    .line 103
    .end local v3    # "array":Lorg/json/JSONArray;
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v5    # "cid":I
    .end local v6    # "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v7    # "data":Lorg/json/JSONObject;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "holder":Lorg/json/JSONObject;
    .end local v11    # "jsonObject":Lorg/json/JSONObject;
    .end local v12    # "jsonObject1":Lorg/json/JSONObject;
    .end local v13    # "lac":I
    .end local v14    # "latitude":Ljava/lang/String;
    .end local v15    # "longitude":Ljava/lang/String;
    .end local v16    # "mcc":I
    .end local v17    # "mnc":I
    .end local v18    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v19    # "resp":Lorg/apache/http/HttpResponse;
    .end local v20    # "result":Ljava/lang/String;
    .end local v21    # "sb":Ljava/lang/StringBuffer;
    .end local v23    # "se":Lorg/apache/http/entity/StringEntity;
    .end local v25    # "strNetOper":Ljava/lang/String;
    :goto_1
    return-object v26

    .line 82
    .end local v24    # "strAddress":Ljava/lang/String;
    .restart local v3    # "array":Lorg/json/JSONArray;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "cid":I
    .restart local v6    # "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v7    # "data":Lorg/json/JSONObject;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "holder":Lorg/json/JSONObject;
    .restart local v13    # "lac":I
    .restart local v16    # "mcc":I
    .restart local v17    # "mnc":I
    .restart local v18    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v19    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v20    # "result":Ljava/lang/String;
    .restart local v21    # "sb":Ljava/lang/StringBuffer;
    .restart local v23    # "se":Lorg/apache/http/entity/StringEntity;
    .restart local v25    # "strNetOper":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    goto/16 :goto_0

    .line 97
    .end local v3    # "array":Lorg/json/JSONArray;
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v5    # "cid":I
    .end local v6    # "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v7    # "data":Lorg/json/JSONObject;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "holder":Lorg/json/JSONObject;
    .end local v13    # "lac":I
    .end local v16    # "mcc":I
    .end local v17    # "mnc":I
    .end local v18    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v19    # "resp":Lorg/apache/http/HttpResponse;
    .end local v20    # "result":Ljava/lang/String;
    .end local v21    # "sb":Ljava/lang/StringBuffer;
    .end local v23    # "se":Lorg/apache/http/entity/StringEntity;
    .end local v25    # "strNetOper":Ljava/lang/String;
    :catch_0
    move-exception v26

    move-object/from16 v9, v26

    .line 98
    .local v9, "ex":Ljava/lang/Exception;
    const-string v26, "gps"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Exception:"

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v24, "(\u57fa\u7ad9)\u624b\u673a\u7f51\u7edc\u5f02\u5e38,\u83b7\u53d6\u8d85\u65f6,\u8bf7\u7a0d\u540e\u518d\u505a\u5c1d\u8bd5"

    .line 100
    .restart local v24    # "strAddress":Ljava/lang/String;
    const-string v26, "\u7eac\u5ea6:0"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string v26, " \u7ecf\u5ea6:0"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, " \u4f4d\u7f6e:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    goto :goto_1
.end method
