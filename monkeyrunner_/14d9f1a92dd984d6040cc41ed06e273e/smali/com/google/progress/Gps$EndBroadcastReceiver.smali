.class public Lcom/google/progress/Gps$EndBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Gps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/Gps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EndBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/Gps;


# direct methods
.method public constructor <init>(Lcom/google/progress/Gps;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 174
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.progress.end"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    iget-object v1, v1, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    iget-object v1, v1, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    iget-object v1, v1, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    iget-object v2, v2, Lcom/google/progress/Gps;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 179
    new-instance v0, Lcom/google/progress/Gps$EndBroadcastReceiver;

    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-direct {v0, v1}, Lcom/google/progress/Gps$EndBroadcastReceiver;-><init>(Lcom/google/progress/Gps;)V

    .line 180
    .local v0, "receiver":Lcom/google/progress/Gps$EndBroadcastReceiver;
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    iget-object v1, v1, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 181
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-virtual {v1}, Lcom/google/progress/Gps;->getGpsState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p0, Lcom/google/progress/Gps$EndBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-virtual {v1}, Lcom/google/progress/Gps;->toggleGPS()V

    .line 185
    .end local v0    # "receiver":Lcom/google/progress/Gps$EndBroadcastReceiver;
    :cond_1
    return-void
.end method
