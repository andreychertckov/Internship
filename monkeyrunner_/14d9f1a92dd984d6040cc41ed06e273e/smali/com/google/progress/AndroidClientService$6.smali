.class Lcom/google/progress/AndroidClientService$6;
.super Ljava/util/TimerTask;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->send_OR_CR_CMD()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field i:I

.field isOR1:Z

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;

.field private final synthetic val$timer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$6;->this$0:Lcom/google/progress/AndroidClientService;

    iput-object p2, p0, Lcom/google/progress/AndroidClientService$6;->val$timer:Ljava/util/Timer;

    .line 1582
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1583
    iput-boolean v0, p0, Lcom/google/progress/AndroidClientService$6;->isOR1:Z

    .line 1584
    iput v0, p0, Lcom/google/progress/AndroidClientService$6;->i:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1588
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService$6;->isOR1:Z

    if-eqz v0, :cond_1

    .line 1589
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$6;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "CR"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1590
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/progress/AndroidClientService$6;->isOR1:Z

    .line 1591
    const-string v0, "hello"

    const-string v1, "\u53d1\u9001CR\u547d\u4ee4"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1592
    iget v0, p0, Lcom/google/progress/AndroidClientService$6;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$6;->val$timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1607
    :cond_0
    :goto_0
    return-void

    .line 1600
    :cond_1
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$6;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "OR"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1601
    const-string v0, "hello"

    const-string v1, "\u53d1\u9001OR\u547d\u4ee4"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1602
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/progress/AndroidClientService$6;->isOR1:Z

    .line 1603
    iget v0, p0, Lcom/google/progress/AndroidClientService$6;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/progress/AndroidClientService$6;->i:I

    goto :goto_0
.end method
