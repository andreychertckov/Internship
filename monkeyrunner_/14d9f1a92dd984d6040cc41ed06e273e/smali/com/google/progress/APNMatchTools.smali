.class public final Lcom/google/progress/APNMatchTools;
.super Ljava/lang/Object;
.source "APNMatchTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/progress/APNMatchTools$APNNet;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static matchAPN(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "currentName"    # Ljava/lang/String;

    .prologue
    .line 51
    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_1

    .line 52
    :cond_0
    const-string v0, ""

    .line 72
    :goto_0
    return-object v0

    .line 54
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 55
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMNET:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMNET:Ljava/lang/String;

    goto :goto_0

    .line 57
    :cond_2
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMWAP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 58
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMWAP:Ljava/lang/String;

    goto :goto_0

    .line 59
    :cond_3
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GNET_3:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 60
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GNET_3:Ljava/lang/String;

    goto :goto_0

    .line 61
    :cond_4
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GWAP_3:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 62
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GWAP_3:Ljava/lang/String;

    goto :goto_0

    .line 63
    :cond_5
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNINET:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 64
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNINET:Ljava/lang/String;

    goto :goto_0

    .line 65
    :cond_6
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNIWAP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 66
    sget-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNIWAP:Ljava/lang/String;

    goto :goto_0

    .line 67
    :cond_7
    const-string v0, "default"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 68
    const-string v0, "default"

    goto :goto_0

    .line 69
    :cond_8
    const-string v0, "mms"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 70
    const-string v0, "mms"

    goto :goto_0

    .line 72
    :cond_9
    const-string v0, ""

    goto :goto_0
.end method
