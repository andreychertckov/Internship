.class public Lcom/google/progress/APNMatchTools$APNNet;
.super Ljava/lang/Object;
.source "APNMatchTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/APNMatchTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "APNNet"
.end annotation


# static fields
.field public static CMNET:Ljava/lang/String;

.field public static CMWAP:Ljava/lang/String;

.field public static CTNET:Ljava/lang/String;

.field public static CTWAP:Ljava/lang/String;

.field public static GNET_3:Ljava/lang/String;

.field public static GWAP_3:Ljava/lang/String;

.field public static UNINET:Ljava/lang/String;

.field public static UNIWAP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "cmwap"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMWAP:Ljava/lang/String;

    .line 16
    const-string v0, "cmnet"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CMNET:Ljava/lang/String;

    .line 22
    const-string v0, "3gwap"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GWAP_3:Ljava/lang/String;

    .line 27
    const-string v0, "3gnet"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->GNET_3:Ljava/lang/String;

    .line 32
    const-string v0, "uniwap"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNIWAP:Ljava/lang/String;

    .line 36
    const-string v0, "uninet"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->UNINET:Ljava/lang/String;

    .line 40
    const-string v0, "ctnet"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CTNET:Ljava/lang/String;

    .line 44
    const-string v0, "ctwap"

    sput-object v0, Lcom/google/progress/APNMatchTools$APNNet;->CTWAP:Ljava/lang/String;

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
