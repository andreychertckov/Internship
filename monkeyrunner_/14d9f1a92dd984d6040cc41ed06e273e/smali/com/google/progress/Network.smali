.class public Lcom/google/progress/Network;
.super Ljava/lang/Object;
.source "Network.java"


# instance fields
.field inet:Ljava/net/InetAddress;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/progress/Network;->inet:Ljava/net/InetAddress;

    .line 4
    return-void
.end method


# virtual methods
.method public getRAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 1
    .param p1, "IP_or_Name"    # Ljava/lang/String;

    .prologue
    .line 13
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/Network;->inet:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    :goto_0
    iget-object v0, p0, Lcom/google/progress/Network;->inet:Ljava/net/InetAddress;

    return-object v0

    .line 15
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getRIP(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/progress/Network;->getRAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
