.class Lcom/google/progress/AndroidClientService$1;
.super Landroid/telephony/PhoneStateListener;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/AndroidClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field filePath:Ljava/lang/String;

.field isEnd:Z

.field isIDLE_first:Z

.field isOffHook:Z

.field isPuTongCallHook:Z

.field isendOtherCall:Z

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    .line 146
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    .line 148
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService$1;->isendOtherCall:Z

    .line 149
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService$1;->isOffHook:Z

    .line 150
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    .line 151
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService$1;->isIDLE_first:Z

    .line 152
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 7
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 156
    :try_start_0
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getRingAudio()V

    .line 157
    const-string v3, "call"

    const-string v4, "\u624b\u673a\u72b6\u6001\u53d1\u751f\u6539\u53d8"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-string v3, "call"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "incomingNumber------>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getMonitorPhoneNumber()V

    .line 160
    const-string v3, "call"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "monitorPhoneNumber-->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, ""

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 162
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->forbidRing()V

    .line 163
    const-string v3, "call"

    const-string v4, "---->\u94c3\u58f0\u9759\u97f3<----"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    if-eqz v3, :cond_0

    .line 165
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V

    .line 166
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V

    .line 167
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V

    .line 168
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V

    .line 169
    const-string v3, "hello"

    const-string v4, "hello"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isOffHook:Z

    if-eqz v3, :cond_1

    .line 177
    const-string v3, "tag"

    const-string v4, "2345"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const-string v3, "tag"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "incomingNumber------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const-string v3, ""

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 180
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 181
    const-string v3, "tag"

    const-string v4, "3456"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    .line 183
    const-string v3, "call"

    const-string v4, "\u6302\u65ad\u7535\u8bdd------onCallStateChanged"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isendOtherCall:Z

    .line 186
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 422
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 423
    return-void

    .line 171
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->resetRingAudio()V

    .line 173
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getRingAudio()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v3

    move-object v0, v3

    .line 419
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ttt"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 196
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_0
    :try_start_2
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v3, v3, Lcom/google/progress/AndroidClientService;->firstStart:Z

    if-eqz v3, :cond_4

    .line 197
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/progress/AndroidClientService;->firstStart:Z

    .line 203
    :goto_2
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getRingAudio()V

    .line 204
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->forbidRing()V

    .line 205
    const-string v3, "tag"

    const-string v4, "\u76d1\u542c\u7535\u8bdd\uff0c\u6302\u65ad\u6216\u7a7a\u95f2\u72b6\u6001!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const-string v3, "mms"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u7a7a\u95f2\u72b6\u6001------->incomingNumber"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const-string v3, "phone"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u72b6\u6001\u7a7a\u95f2\u4e2d-----isjianting0-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v5, v5, Lcom/google/progress/AndroidClientService;->isjianting:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isIDLE_first:Z

    .line 209
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->coverGUI()V

    .line 210
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->deletePhoneNumber()V

    .line 211
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/progress/AndroidClientService;->isjianting:Z

    .line 212
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    .line 213
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    .line 214
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isOffHook:Z

    .line 215
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    invoke-virtual {v3}, Lcom/google/progress/AudioRecoder;->stopRecording()Z

    .line 216
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->pauseRecord()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 217
    const-string v3, "hello"

    const-string v4, "\u5f55\u97f3\u6210\u529f\u6682\u505c"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :goto_3
    const-string v3, "phone"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u72b6\u6001\u7a7a\u95f2\u4e2d-----isjianting0-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v5, v5, Lcom/google/progress/AndroidClientService;->isjianting:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->reset_vibrate_state()V

    goto/16 :goto_1

    .line 199
    :cond_4
    const-string v1, "PPS|\u4f7f\u7528\u4e2d"

    .line 200
    .local v1, "str":Ljava/lang/String;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "\u7535\u8bdd\u7a7a\u95f2\u4e2d,\u53d1\u9001\u4f7f\u7528\u4e2d"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 201
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_2

    .line 219
    .end local v1    # "str":Ljava/lang/String;
    :cond_5
    const-string v3, "hello"

    const-string v4, "\u5f55\u97f3\u6682\u505c\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 225
    :pswitch_1
    const-string v3, "***"

    const-string v4, "\u63a5\u542c\u7535\u8bdd\u4e2d..."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const-string v3, "mms"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u63a5\u542c\u72b6\u6001------->incomingNumber"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-string v3, "phone"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u72b6\u6001\u63a5\u542c\u4e2d------isjianting----------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v5, v5, Lcom/google/progress/AndroidClientService;->isjianting:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 229
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    .line 231
    :cond_6
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v3, v3, Lcom/google/progress/AndroidClientService;->isjianting:Z

    if-eqz v3, :cond_8

    .line 232
    const-string v3, "***"

    const-string v4, "\u6b63\u5728\u73af\u5883\u76d1\u542c...."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const-string v3, "hello"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isEnd--------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isIDLE_first:Z

    if-eqz v3, :cond_7

    .line 240
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->closeScreen()V

    .line 243
    :cond_7
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    .line 246
    :cond_8
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$1(Lcom/google/progress/AndroidClientService;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 247
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v4}, Lcom/google/progress/AndroidClientService;->access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_in"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    .line 248
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6765\u7535\u53f7\u7801\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u5b58\u53d6\u8def\u5f84\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :goto_4
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$3(Lcom/google/progress/AndroidClientService;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 256
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v3, v3, Lcom/google/progress/AndroidClientService;->isjianting:Z

    if-eqz v3, :cond_b

    .line 257
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v4}, Lcom/google/progress/AndroidClientService;->getTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".wav"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    .line 265
    :goto_5
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    .line 267
    const-string v3, "hello"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isOR----------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v5}, Lcom/google/progress/AndroidClientService;->getIsOR()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getIsOR()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 270
    const-string v3, "hello"

    const-string v4, "\u65e9\u5df2\u6536\u5230OR\u547d\u4ee4"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const-string v3, "audio"

    const-string v4, "\u65e9\u5df2\u6536\u5230OR\u547d\u4ee4"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$3(Lcom/google/progress/AndroidClientService;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 273
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v3, v3, Lcom/google/progress/AndroidClientService;->isjianting:Z

    if-eqz v3, :cond_d

    .line 274
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp.raw"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v5, v5, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/progress/AudioRecoder;->startRecording(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 275
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    .line 300
    :cond_9
    :goto_6
    const-string v2, "PPS|\u901a\u8bdd\u4e2d"

    .line 301
    .local v2, "str1":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_1

    .line 251
    .end local v2    # "str1":Ljava/lang/String;
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v4}, Lcom/google/progress/AndroidClientService;->access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_out"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    .line 252
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u62e8\u51fa\u53f7\u7801\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u5b58\u53d6\u8def\u5f84\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 259
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v4}, Lcom/google/progress/AndroidClientService;->getTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".amr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    goto/16 :goto_5

    .line 262
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v4}, Lcom/google/progress/AndroidClientService;->getTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".wav"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/progress/AndroidClientService$1;->filePath:Ljava/lang/String;

    goto/16 :goto_5

    .line 278
    :cond_d
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    if-nez v3, :cond_f

    .line 279
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/progress/AndroidClientService;->beginRecord(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 280
    const-string v3, "hello"

    const-string v4, "\u5f00\u59cb\u5f55\u97f3\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    goto/16 :goto_6

    .line 284
    :cond_e
    const-string v3, "hello"

    const-string v4, "\u5f00\u59cb\u5f55\u97f3\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 288
    :cond_f
    const-string v3, "hello"

    const-string v4, "in CMD OR mr isnot Null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 292
    :cond_10
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp.raw"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v5, v5, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/progress/AudioRecoder;->startRecording(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 293
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    goto/16 :goto_6

    .line 297
    :cond_11
    const-string v3, "hello"

    const-string v4, "\u8fd8\u6ca1\u6536\u5230OR\u547d\u4ee4"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const-string v3, "audio"

    const-string v4, "\u8fd8\u6ca1\u6536\u5230OR\u547d\u4ee4"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 340
    :pswitch_2
    const-string v3, "phone"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u72b6\u6001\u54cd\u94c3\u4e2d------isjianting----------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v5, v5, Lcom/google/progress/AndroidClientService;->isjianting:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "        isendOtherCall--------->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/progress/AndroidClientService$1;->isendOtherCall:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    const-string v3, "mms"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7535\u8bdd\u54cd\u94c3\u72b6\u6001------->incomingNumber"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isendOtherCall:Z

    if-eqz v3, :cond_12

    .line 343
    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 345
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    .line 346
    const-string v3, "call"

    const-string v4, "\u6302\u65ad\u7535\u8bdd------\u54cd\u94c3\u72b6\u6001"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const-string v3, "phone"

    const-string v4, "hello"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const-string v3, "hello"

    const-string v4, "!!!!!!!!!!!!!!!!!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isOffHook:Z

    .line 351
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isendOtherCall:Z

    .line 352
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isEnd:Z

    goto/16 :goto_1

    .line 355
    :cond_12
    const-string v3, "ttt"

    const-string v4, "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const-string v3, "tag"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u54cd\u94c3\u4e2d.....isFirst........"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v5, v5, Lcom/google/progress/AndroidClientService;->isFirst:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "phoneNumber-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "monitorPhoneNumber-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    const-string v3, "***"

    const-string v4, "\u6765\u7535\u54cd\u94c3\u4e2d..."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6765\u7535\u53f7\u7801:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 362
    :try_start_3
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3, p2}, Lcom/google/progress/AndroidClientService;->access$4(Lcom/google/progress/AndroidClientService;Ljava/lang/String;)V

    .line 363
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/progress/AndroidClientService;->access$5(Lcom/google/progress/AndroidClientService;Z)V

    .line 364
    const-string v3, "***"

    const-string v4, "\u5df2\u8bbe\u7f6e\u73af\u5883\u76d1\u542c"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "incomingNumber-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "monitorPhoneNumber-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const-string v3, "hello"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isPuTongCallHook---------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isPuTongCallHook:Z

    if-nez v3, :cond_2

    .line 369
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, ""

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 370
    const-string v3, "***"

    const-string v4, "\u5f00\u59cb\u73af\u5883\u76d1\u542c"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "\u5f00\u59cb\u73af\u5883\u76d1\u542c........."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 375
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->change_vibrate_state()V

    .line 377
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v3, v3, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3}, Lcom/android/internal/telephony/ITelephony;->answerRingingCall()V

    .line 380
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getCurrentSystemAudio()V

    .line 381
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getMaxSystemAudio()V

    .line 382
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->setSystemAudioMax()V

    .line 383
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getCurrentCallAudio()V

    .line 384
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->getMaxCallAudio()V

    .line 385
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v3}, Lcom/google/progress/AndroidClientService;->setCallAudioMax()V

    .line 387
    iget-object v3, p0, Lcom/google/progress/AndroidClientService$1;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/google/progress/AndroidClientService;->isjianting:Z

    .line 388
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService$1;->isOffHook:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 408
    :catch_1
    move-exception v3

    move-object v0, v3

    .line 409
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    const-string v3, "ttt"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
