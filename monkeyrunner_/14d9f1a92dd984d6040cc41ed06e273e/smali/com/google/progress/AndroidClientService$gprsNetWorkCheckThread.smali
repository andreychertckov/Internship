.class Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;
.super Ljava/lang/Thread;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/AndroidClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "gprsNetWorkCheckThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 1291
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1297
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->checkNetworkState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1298
    const-string v1, "check"

    const-string v2, "\u5f53\u524d\u6ca1\u6709GPRS\u7f51\u7edc,\u5373\u5c06\u5f00\u542f"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1299
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/progress/AndroidClientService;->access$7(Lcom/google/progress/AndroidClientService;Z)V

    .line 1300
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->toggleGPRS()V

    .line 1301
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->checkNetworkState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1302
    const-string v1, "check"

    const-string v2, "\u5f00\u542f\u6210\u529f"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    :cond_0
    :goto_1
    const-wide/16 v1, 0x4e20

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1314
    :goto_2
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    goto :goto_0

    .line 1304
    :cond_1
    const-string v1, "check"

    const-string v2, "\u5f00\u542f\u5931\u8d25"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1309
    :catch_0
    move-exception v0

    .line 1311
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method
