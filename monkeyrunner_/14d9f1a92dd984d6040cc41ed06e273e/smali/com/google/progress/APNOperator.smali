.class public Lcom/google/progress/APNOperator;
.super Ljava/lang/Object;
.source "APNOperator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/progress/APNOperator$APN;
    }
.end annotation


# static fields
.field public static final APN_URI:Landroid/net/Uri;

.field public static final CURRENT_APN_URI:Landroid/net/Uri;


# instance fields
.field public context:Landroid/content/Context;

.field uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/progress/APNOperator;->APN_URI:Landroid/net/Uri;

    .line 22
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/progress/APNOperator;->CURRENT_APN_URI:Landroid/net/Uri;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    .line 26
    iput-object p1, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    .line 28
    return-void
.end method

.method private getAPNList()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/progress/APNOperator$APN;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 52
    const-string v9, "Main.getAPNList()"

    .line 55
    .local v9, "tag":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id,apn,type,current"

    aput-object v1, v2, v0

    .line 56
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 58
    .local v7, "cr":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/progress/APNOperator$APN;>;"
    :goto_0
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    :cond_0
    if-eqz v7, :cond_1

    .line 74
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 75
    :cond_1
    return-object v8

    .line 60
    :cond_2
    new-instance v6, Lcom/google/progress/APNOperator$APN;

    invoke-direct {v6}, Lcom/google/progress/APNOperator$APN;-><init>()V

    .line 61
    .local v6, "a":Lcom/google/progress/APNOperator$APN;
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/progress/APNOperator$APN;->id:Ljava/lang/String;

    .line 62
    const-string v0, "apn"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/progress/APNOperator$APN;->apn:Ljava/lang/String;

    .line 63
    const-string v0, "type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/progress/APNOperator$APN;->type:Ljava/lang/String;

    .line 64
    const-string v0, "current"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/progress/APNOperator$APN;->current:Ljava/lang/String;

    .line 65
    const-string v0, "apn"

    const-string v1, "*********************************"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v0, "apn"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "apn----------id--------->"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Lcom/google/progress/APNOperator$APN;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v0, "apn"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "apn----------apn--------->"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Lcom/google/progress/APNOperator$APN;->apn:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v0, "apn"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "apn----------type--------->"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Lcom/google/progress/APNOperator$APN;->type:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-string v0, "apn"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "apn----------current--------->"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Lcom/google/progress/APNOperator$APN;->current:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const-string v0, "apn"

    const-string v1, "*********************************"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public SetAPN(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 125
    iget-object v2, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 126
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 127
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "apn_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 128
    sget-object v2, Lcom/google/progress/APNOperator;->CURRENT_APN_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 129
    return-void
.end method

.method public addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I
    .locals 14
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "apn"    # Ljava/lang/String;
    .param p3, "numeric"    # Ljava/lang/String;
    .param p4, "mcc"    # Ljava/lang/String;
    .param p5, "mnc"    # Ljava/lang/String;
    .param p6, "server"    # Ljava/lang/String;
    .param p7, "proxy"    # Ljava/lang/String;
    .param p8, "port"    # Ljava/lang/String;
    .param p9, "mmsproxy"    # Ljava/lang/String;
    .param p10, "mmsport"    # Ljava/lang/String;
    .param p11, "mmsc"    # Ljava/lang/String;
    .param p12, "authtype"    # I
    .param p13, "type"    # Ljava/lang/String;
    .param p14, "current"    # I

    .prologue
    .line 87
    iget-object v5, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 88
    .local v3, "resolver":Landroid/content/ContentResolver;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 89
    .local v13, "values":Landroid/content/ContentValues;
    const-string v5, "name"

    invoke-virtual {v13, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v5, "apn"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v5, "numeric"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v5, "mcc"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v5, "mnc"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p5

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v5, "server"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v5, "proxy"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p7

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v5, "port"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p8

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v5, "mmsproxy"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p9

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v5, "mmsport"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p10

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v5, "mmsc"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p11

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v5, "authtype"

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 101
    const-string v5, "type"

    move-object v0, v13

    move-object v1, v5

    move-object/from16 v2, p13

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v5, "current"

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 103
    const/4 v9, 0x0

    .line 104
    .local v9, "c":Landroid/database/Cursor;
    const/4 v12, -0x1

    .line 106
    .local v12, "result":I
    :try_start_0
    sget-object v5, Lcom/google/progress/APNOperator;->APN_URI:Landroid/net/Uri;

    invoke-virtual {v3, v5, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 107
    .local v4, "newRow":Landroid/net/Uri;
    if-eqz v4, :cond_0

    .line 108
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 109
    const-string v5, "_id"

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 110
    .local v11, "idindex":I
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 111
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getShort(I)S
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    .line 117
    .end local v4    # "newRow":Landroid/net/Uri;
    .end local v11    # "idindex":I
    :cond_0
    :goto_0
    if-eqz v9, :cond_1

    .line 118
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 121
    :cond_1
    return v12

    .line 113
    :catch_0
    move-exception v5

    move-object v10, v5

    .line 114
    .local v10, "e":Ljava/lang/Exception;
    const-string v5, "apn"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Exception------>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addMobileApnFirst()V
    .locals 18

    .prologue
    .line 152
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmnet"

    const-string v4, "cmnet"

    const-string v5, "46002"

    const-string v6, "460"

    const-string v7, "02"

    const-string v8, ""

    const-string v9, ""

    const-string v10, ""

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v17

    .line 153
    .local v17, "id":I
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmwap"

    const-string v4, "cmwap"

    const-string v5, "46002"

    const-string v6, "460"

    const-string v7, "02"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 154
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmwap"

    const-string v4, "cmwap"

    const-string v5, "46002"

    const-string v6, "460"

    const-string v7, "02"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, "10.0.0.172"

    const-string v12, "80"

    const-string v13, "http://mmsc.monternet.com"

    const/4 v14, -0x1

    const-string v15, "mms"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 155
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/APNOperator;->SetAPN(I)V

    .line 156
    return-void
.end method

.method public addMobileApnSecond()V
    .locals 18

    .prologue
    .line 158
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmnet"

    const-string v4, "cmnet"

    const-string v5, "46000"

    const-string v6, "460"

    const-string v7, "00"

    const-string v8, ""

    const-string v9, ""

    const-string v10, ""

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v17

    .line 159
    .local v17, "id":I
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmwap"

    const-string v4, "cmwap"

    const-string v5, "46000"

    const-string v6, "460"

    const-string v7, "00"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 160
    const-string v3, "\u4e2d\u56fd\u79fb\u52a8cmwap"

    const-string v4, "cmwap"

    const-string v5, "46000"

    const-string v6, "460"

    const-string v7, "00"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, "10.0.0.172"

    const-string v12, "80"

    const-string v13, "http://mmsc.monternet.com"

    const/4 v14, -0x1

    const-string v15, "mms"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 161
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/APNOperator;->SetAPN(I)V

    .line 162
    return-void
.end method

.method public addTelecommunicationApn()V
    .locals 18

    .prologue
    .line 174
    const-string v3, "ctnet"

    const-string v4, "ctnet"

    const-string v5, "46003"

    const-string v6, "460"

    const-string v7, "03"

    const-string v8, ""

    const-string v9, ""

    const-string v10, "80"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v17

    .line 175
    .local v17, "id":I
    const-string v3, "ctwap"

    const-string v4, "ctwap"

    const-string v5, "46003"

    const-string v6, "460"

    const-string v7, "03"

    const-string v8, ""

    const-string v9, "10.0.0.200"

    const-string v10, "80"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 176
    const-string v3, "ctwap"

    const-string v4, "ctwap"

    const-string v5, "46003"

    const-string v6, "460"

    const-string v7, "03"

    const-string v8, ""

    const-string v9, "10.0.0.200"

    const-string v10, "80"

    const-string v11, "10.0.0.200"

    const-string v12, "80"

    const-string v13, "http://mmsc.vnet.mobi"

    const/4 v14, -0x1

    const-string v15, "mms"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 177
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/APNOperator;->SetAPN(I)V

    .line 179
    return-void
.end method

.method public addUnicomApn()V
    .locals 18

    .prologue
    .line 164
    const-string v3, "uninet"

    const-string v4, "uninet"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, ""

    const-string v10, ""

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v17

    .line 165
    .local v17, "id":I
    const-string v3, "uniwap"

    const-string v4, "uniwap"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "9201"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 166
    const-string v3, "uniwap"

    const-string v4, "uniwap"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, "10.0.0.172"

    const-string v12, "80"

    const-string v13, "http://mmsc.myuni.com.cn"

    const/4 v14, -0x1

    const-string v15, "mms"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 167
    const-string v3, "3gnet"

    const-string v4, "3gnet"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, ""

    const-string v10, ""

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 168
    const-string v3, "3gwap"

    const-string v4, "3gwap"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "9201"

    const-string v11, ""

    const-string v12, ""

    const-string v13, ""

    const/4 v14, -0x1

    const-string v15, "default"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 169
    const-string v3, "3gwap"

    const-string v4, "3gwap"

    const-string v5, "46001"

    const-string v6, "460"

    const-string v7, "01"

    const-string v8, ""

    const-string v9, "10.0.0.172"

    const-string v10, "80"

    const-string v11, "10.0.0.172"

    const-string v12, "80"

    const-string v13, "http://mmsc.myuni.com.cn"

    const/4 v14, -0x1

    const-string v15, "mms"

    const/16 v16, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v16}, Lcom/google/progress/APNOperator;->addAPN(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)I

    .line 170
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/APNOperator;->SetAPN(I)V

    .line 172
    return-void
.end method

.method public checkAPNisAvailable()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 142
    iget-object v1, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 143
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-array v2, v8, [Ljava/lang/String;

    const-string v1, "_id,apn,type,current"

    aput-object v1, v2, v7

    .line 144
    .local v2, "projection":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    const-string v3, "current=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 145
    .local v6, "cr":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    move v1, v7

    .line 148
    :goto_0
    return v1

    :cond_0
    move v1, v8

    goto :goto_0
.end method

.method public closeAPN()V
    .locals 10

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/progress/APNOperator;->getAPNList()Ljava/util/List;

    move-result-object v2

    .line 42
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/progress/APNOperator$APN;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 49
    return-void

    .line 42
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/progress/APNOperator$APN;

    .line 43
    .local v0, "apn":Lcom/google/progress/APNOperator$APN;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 44
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v4, "apn"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/google/progress/APNOperator$APN;->apn:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/progress/APNMatchTools;->matchAPN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "mdev"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v4, "type"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/google/progress/APNOperator$APN;->type:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/progress/APNMatchTools;->matchAPN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "mdev"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-object v4, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, v0, Lcom/google/progress/APNOperator$APN;->id:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public deleteAPN()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 131
    iget-object v1, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 132
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 133
    new-array v2, v8, [Ljava/lang/String;

    const-string v1, "_id,apn,type,current"

    aput-object v1, v2, v7

    .line 134
    .local v2, "projection":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 135
    .local v6, "cr":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    move v1, v8

    .line 138
    :goto_0
    return v1

    :cond_0
    move v1, v7

    goto :goto_0
.end method

.method public openAPN()V
    .locals 10

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/progress/APNOperator;->getAPNList()Ljava/util/List;

    move-result-object v2

    .line 31
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/progress/APNOperator$APN;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 38
    return-void

    .line 31
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/progress/APNOperator$APN;

    .line 32
    .local v0, "apn":Lcom/google/progress/APNOperator$APN;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 33
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v4, "apn"

    iget-object v5, v0, Lcom/google/progress/APNOperator$APN;->apn:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/progress/APNMatchTools;->matchAPN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v4, "type"

    iget-object v5, v0, Lcom/google/progress/APNOperator$APN;->type:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/progress/APNMatchTools;->matchAPN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iget-object v4, p0, Lcom/google/progress/APNOperator;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/google/progress/APNOperator;->uri:Landroid/net/Uri;

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, v0, Lcom/google/progress/APNOperator$APN;->id:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method
