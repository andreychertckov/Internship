.class public Lcom/google/progress/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# instance fields
.field private context:Landroid/content/Context;

.field private fileWriter:Ljava/io/FileWriter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/progress/FileUtils;->context:Landroid/content/Context;

    .line 23
    return-void
.end method

.method public static createDir(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 158
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 161
    const-string v1, "***"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "make dir-->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 165
    :goto_0
    return-object v1

    .line 164
    :cond_0
    const-string v1, "***"

    const-string v2, "exist"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static delete(Ljava/lang/String;)Z
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 146
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static deleteDir(Ljava/io/File;)V
    .locals 1
    .param p0, "dirFile"    # Ljava/io/File;

    .prologue
    .line 172
    new-instance v0, Lcom/google/progress/FileUtils$1;

    invoke-direct {v0}, Lcom/google/progress/FileUtils$1;-><init>()V

    invoke-virtual {p0, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    .line 184
    return-void
.end method

.method public static isFileExists(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 152
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private readNumberInRaw(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f040000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 122
    .local v3, "is":Ljava/io/InputStream;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/16 v6, 0x12d0

    new-array v0, v6, [B

    .line 126
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .local v4, "len":I
    const/4 v6, -0x1

    if-ne v4, v6, :cond_0

    .line 136
    .end local v4    # "len":I
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 127
    .restart local v4    # "len":I
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-lt v2, v4, :cond_1

    .line 130
    :try_start_1
    new-instance v6, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v6, v0, v7, v4}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 132
    .end local v2    # "i":I
    .end local v4    # "len":I
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 133
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 134
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "err"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "i":I
    .restart local v4    # "len":I
    :cond_1
    :try_start_2
    aget-byte v6, v0, v2

    xor-int/lit8 v6, v6, 0x12

    int-to-byte v6, v6

    aput-byte v6, v0, v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getMemoryPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/data/data/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/progress/FileUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initFileWriter(Ljava/lang/String;)Ljava/io/FileWriter;
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/progress/FileUtils;->getMemoryPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 31
    :cond_0
    new-instance v1, Ljava/io/FileWriter;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    iput-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    .line 32
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    return-object v1
.end method

.method public readEncryptedFile(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 7
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 89
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/16 v5, 0x12d0

    new-array v0, v5, [B

    .line 93
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .local v3, "len":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 102
    .end local v3    # "len":I
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 94
    .restart local v3    # "len":I
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-lt v2, v3, :cond_1

    .line 97
    :try_start_1
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 99
    .end local v2    # "i":I
    .end local v3    # "len":I
    :catch_0
    move-exception v5

    move-object v1, v5

    .line 100
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 95
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "i":I
    .restart local v3    # "len":I
    :cond_1
    :try_start_2
    aget-byte v5, v0, v2

    xor-int/lit8 v5, v5, 0x12

    int-to-byte v5, v5

    aput-byte v5, v0, v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 94
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public readEncryptedFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 105
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 108
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 109
    .local v2, "fis":Ljava/io/FileInputStream;
    invoke-virtual {p0, v2}, Lcom/google/progress/FileUtils;->readEncryptedFile(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 114
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :goto_0
    return-object v3

    .line 110
    :catch_0
    move-exception v3

    move-object v0, v3

    .line 111
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 114
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public writeEncryptedFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 63
    .local v0, "bytes":[B
    array-length v6, v0

    .line 64
    .local v6, "len":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v6, :cond_0

    .line 67
    const/4 v3, 0x0

    .line 68
    .local v3, "fos":Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/progress/FileUtils;->getMemoryPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 78
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 83
    :goto_1
    const/4 v7, 0x1

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    return v7

    .line 65
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_0
    aget-byte v7, v0, v5

    xor-int/lit8 v7, v7, 0x12

    int-to-byte v7, v7

    aput-byte v7, v0, v5

    .line 64
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 73
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v7

    move-object v1, v7

    .line 74
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 78
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 75
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    const/4 v7, 0x0

    goto :goto_2

    .line 79
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 76
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 78
    :goto_5
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 82
    :goto_6
    throw v7

    .line 79
    :catch_2
    move-exception v1

    .line 80
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 79
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    .line 80
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 76
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 73
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v7

    move-object v1, v7

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method public writeTxtFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 42
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/progress/FileUtils;->initFileWriter(Ljava/lang/String;)Ljava/io/FileWriter;

    .line 43
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    invoke-virtual {v1, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :try_start_1
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 46
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 49
    :try_start_3
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 52
    :catch_1
    move-exception v1

    move-object v0, v1

    .line 53
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 47
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 49
    :try_start_4
    iget-object v2, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    if-eqz v2, :cond_1

    .line 50
    iget-object v2, p0, Lcom/google/progress/FileUtils;->fileWriter:Ljava/io/FileWriter;

    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 55
    :cond_1
    :goto_1
    throw v1

    .line 52
    :catch_2
    move-exception v2

    move-object v0, v2

    .line 53
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 52
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    move-object v0, v1

    .line 53
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
