.class public Lcom/google/progress/WifiCheckTask;
.super Ljava/util/TimerTask;
.source "WifiCheckTask.java"


# static fields
.field public static isProgramGPRS_ON:Z

.field public static isUserGPRS_ON:Z


# instance fields
.field public apnOperator:Lcom/google/progress/APNOperator;

.field private connectivityManager:Landroid/net/ConnectivityManager;

.field private context:Landroid/content/Context;

.field public hasGprs:Z

.field public isAndApnFirst:Z

.field private isOK:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field sr:Lcom/google/progress/AndroidSocketSR;

.field public telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    sput-boolean v0, Lcom/google/progress/WifiCheckTask;->isUserGPRS_ON:Z

    .line 35
    sput-boolean v0, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 31
    iput-object v1, p0, Lcom/google/progress/WifiCheckTask;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 33
    iput-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    .line 36
    iput-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->isAndApnFirst:Z

    .line 42
    iput-object p1, p0, Lcom/google/progress/WifiCheckTask;->context:Landroid/content/Context;

    .line 43
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 44
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/progress/WifiCheckTask;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 45
    new-instance v0, Lcom/google/progress/AndroidSocketSR;

    invoke-direct {v0}, Lcom/google/progress/AndroidSocketSR;-><init>()V

    iput-object v0, p0, Lcom/google/progress/WifiCheckTask;->sr:Lcom/google/progress/AndroidSocketSR;

    .line 46
    new-instance v0, Lcom/google/progress/APNOperator;

    invoke-direct {v0, p1}, Lcom/google/progress/APNOperator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    .line 47
    iput-object p2, p0, Lcom/google/progress/WifiCheckTask;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 50
    return-void
.end method


# virtual methods
.method public CloseWifi()V
    .locals 3

    .prologue
    .line 533
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 534
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 536
    const-wide/16 v1, 0x1770

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 541
    :goto_0
    const-string v1, "wifi"

    const-string v2, "\u5173\u95edWifi"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    :cond_0
    return-void

    .line 537
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public LookUpConfiguration(Ljava/util/List;)Ljava/lang/StringBuilder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 558
    .local p1, "configurations":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 559
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .end local p0    # "this":Lcom/google/progress/WifiCheckTask;
    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 567
    return-object v1

    .line 561
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Index_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Integer;

    add-int/lit8 v4, v0, 0x1

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {p0}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public LookUpScan(Ljava/util/List;)Ljava/lang/StringBuilder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 546
    .local p1, "scan":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 547
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .end local p0    # "this":Lcom/google/progress/WifiCheckTask;
    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 555
    return-object v1

    .line 549
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Index_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Integer;

    add-int/lit8 v4, v0, 0x1

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/ScanResult;

    invoke-virtual {p0}, Landroid/net/wifi/ScanResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public OpenWifi()V
    .locals 3

    .prologue
    .line 510
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 511
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 513
    const-wide/16 v1, 0x1770

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 514
    const-string v1, "wifi"

    const-string v2, "Wifi\u5f00\u542f\u5b8c\u6210"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 520
    :goto_0
    :try_start_1
    const-string v1, "wifi"

    const-string v2, "\u624b\u673a\u5373\u5c06\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    const-wide/16 v1, 0x1f40

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 530
    :cond_0
    :goto_1
    return-void

    .line 515
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 517
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 522
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    move-object v0, v1

    .line 524
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public begin()V
    .locals 2

    .prologue
    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 123
    :cond_0
    :goto_1
    return-void

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->startCheckBeginWithWifi()V

    .line 116
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->wifiIsOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    goto :goto_1

    .line 114
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public checkGPRSNetworkState()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 571
    iget-object v2, p0, Lcom/google/progress/WifiCheckTask;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 572
    .local v0, "gprs":Landroid/net/NetworkInfo;
    iget-object v2, p0, Lcom/google/progress/WifiCheckTask;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 573
    .local v1, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v4

    .line 576
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public checkWifiCanOrNotConnectServer()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 624
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string v5, "ping www.baidu.com"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 625
    .local v1, "process":Ljava/lang/Process;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 626
    .local v3, "streamReader":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 627
    .local v2, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-nez v4, :cond_0

    move v4, v7

    .line 636
    .end local v1    # "process":Ljava/lang/Process;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "streamReader":Ljava/io/InputStreamReader;
    :goto_0
    return v4

    .line 630
    .restart local v1    # "process":Ljava/lang/Process;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "streamReader":Ljava/io/InputStreamReader;
    :cond_0
    const/4 v4, 0x1

    goto :goto_0

    .line 632
    .end local v1    # "process":Ljava/lang/Process;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "streamReader":Ljava/io/InputStreamReader;
    :catch_0
    move-exception v4

    move-object v0, v4

    .line 634
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "wifi"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception------in------checkWifiCanOrNotConnectServer------->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v4, v7

    .line 636
    goto :goto_0
.end method

.method public checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z
    .locals 3
    .param p1, "strs"    # [Ljava/lang/String;

    .prologue
    .line 613
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 620
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 614
    :cond_0
    const-string v1, "wifi"

    const-string v2, "\u6b63\u5728\u68c0\u6d4b\u5f53\u524d\u7f51\u7edc\u662f\u5426\u53ef\u7528"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/google/progress/WifiCheckTask;->connectCheck(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 616
    const/4 v1, 0x1

    goto :goto_1

    .line 613
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public checkWifiNetworkState()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 640
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 641
    .local v0, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 644
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public connectCheck(Ljava/lang/String;)Z
    .locals 10
    .param p1, "com"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 582
    :try_start_0
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 583
    .local v5, "token":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v2, v5, v6

    .line 584
    .local v2, "ip":Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 585
    .local v3, "port":I
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0, v2, v3}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 586
    .local v0, "checkSocket":Ljava/net/Socket;
    const/16 v6, 0xbb8

    invoke-virtual {v0, v6}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 587
    iget-object v6, p0, Lcom/google/progress/WifiCheckTask;->sr:Lcom/google/progress/AndroidSocketSR;

    const-string v7, "C"

    invoke-virtual {v6, v0, v7}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 588
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 589
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 590
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move v6, v8

    .line 608
    .end local v0    # "checkSocket":Ljava/net/Socket;
    .end local v2    # "ip":Ljava/lang/String;
    .end local v3    # "port":I
    .end local v5    # "token":[Ljava/lang/String;
    :goto_0
    return v6

    .line 594
    .restart local v0    # "checkSocket":Ljava/net/Socket;
    .restart local v2    # "ip":Ljava/lang/String;
    .restart local v3    # "port":I
    .restart local v5    # "token":[Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lcom/google/progress/WifiCheckTask;->sr:Lcom/google/progress/AndroidSocketSR;

    invoke-virtual {v6, v0}, Lcom/google/progress/AndroidSocketSR;->RevCmd(Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v4

    .line 595
    .local v4, "str":Ljava/lang/String;
    const-string v6, "ER"

    if-ne v4, v6, :cond_1

    .line 596
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 597
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 598
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move v6, v8

    .line 599
    goto :goto_0

    .line 601
    :cond_1
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 602
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 603
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v9

    .line 608
    goto :goto_0

    .line 605
    .end local v0    # "checkSocket":Ljava/net/Socket;
    .end local v2    # "ip":Ljava/lang/String;
    .end local v3    # "port":I
    .end local v4    # "str":Ljava/lang/String;
    .end local v5    # "token":[Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v1, v6

    .local v1, "e":Ljava/lang/Exception;
    move v6, v8

    .line 606
    goto :goto_0
.end method

.method public connectNetworkWithPwd()Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 650
    new-instance v2, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v2}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 651
    .local v2, "wc":Landroid/net/wifi/WifiConfiguration;
    const-string v3, "\"LongLong\""

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 652
    const-string v3, "\"40014001\""

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 653
    iput-boolean v6, v2, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    .line 654
    iput v5, v2, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 655
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v3, v5}, Ljava/util/BitSet;->set(I)V

    .line 656
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 657
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 658
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 659
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v3, v5}, Ljava/util/BitSet;->set(I)V

    .line 660
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 661
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v3

    iput v3, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 662
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ID---------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v4, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v3, v4, v6}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result v1

    .line 664
    .local v1, "flag":Z
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "flag----->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    const-wide/16 v3, 0x4e20

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 671
    :goto_0
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v6

    .line 675
    :goto_1
    return v3

    .line 667
    :catch_0
    move-exception v0

    .line 669
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 674
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v4, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 675
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public connect_Configuration_Network(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 6
    .param p1, "configuration"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    const/4 v5, 0x1

    .line 706
    iget-object v2, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v3, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v2, v3, v5}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result v1

    .line 707
    .local v1, "flag":Z
    const-string v2, "wifi"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u6b63\u5728\u8fde\u63a5<---"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "--->\u7f51\u7edc\u4e2d..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    const-string v2, "wifi"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flag--------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    const-wide/16 v2, 0x2710

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 715
    :goto_0
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v5

    .line 718
    :goto_1
    return v2

    .line 711
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 718
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public connect_NoPwd_Network(Ljava/lang/String;)Z
    .locals 9
    .param p1, "netWorkName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 722
    new-instance v2, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v2}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 723
    .local v2, "wc":Landroid/net/wifi/WifiConfiguration;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 724
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "wc.SSID----->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v3, v8}, Ljava/util/BitSet;->set(I)V

    .line 726
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 727
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 728
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 729
    iput v8, v2, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 730
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v3

    iput v3, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 731
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ID---------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v4, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v3, v4, v7}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result v1

    .line 733
    .local v1, "flag":Z
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "flag----->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6b63\u5728\u8fde\u63a5<---"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "--->\u7f51\u7edc\u4e2d..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    const-wide/16 v3, 0x2710

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 741
    :goto_0
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v7

    .line 745
    :goto_1
    return v3

    .line 737
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 744
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v4, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    move v3, v6

    .line 745
    goto :goto_1
.end method

.method public getCardTypeNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/progress/WifiCheckTask;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get_NoPwd_Network()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 681
    .local v1, "netWorkName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 682
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2

    .line 683
    .local v2, "scan":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-nez v2, :cond_0

    .line 684
    const-string v3, "wifi"

    const-string v4, "Wifi\u626b\u63cf\u7ed3\u679c\u4e3a\u7a7a"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    :cond_0
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "size-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u626b\u63cf\u5230\u7684\u7f51\u7edc\u5217\u8868----->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/progress/WifiCheckTask;->LookUpScan(Ljava/util/List;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    const/4 v0, 0x0

    .end local p0    # "this":Lcom/google/progress/WifiCheckTask;
    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 694
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_4

    .line 698
    return-object v1

    .line 689
    :cond_1
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "scan.get(i).capabilities----"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "----->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/ScanResult;

    iget-object v5, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/ScanResult;

    iget-object v3, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/ScanResult;

    iget-object v3, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v4, "[WPS]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 691
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/ScanResult;

    iget-object v3, p0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 688
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 695
    :cond_4
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u6ca1\u5bc6\u7801\u7684\u7f51\u7edc\u540d\u79f0--------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public get_WifiConfigutationNetWork()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 701
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    .line 702
    .local v0, "mwConfigurations":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    const-string v1, "wifi"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u626b\u63cf\u5230\u7684\u914d\u7f6e\u7f51\u7edc\u5217\u8868----->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->LookUpConfiguration(Ljava/util/List;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    return-object v0
.end method

.method public goConnectNetwork()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 482
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->get_NoPwd_Network()Ljava/util/List;

    move-result-object v2

    .line 483
    .local v2, "netWorkName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sub-int v1, v3, v5

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 492
    :goto_1
    iget-boolean v3, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    if-nez v3, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->get_WifiConfigutationNetWork()Ljava/util/List;

    move-result-object v0

    .line 494
    .local v0, "configurations":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    const/4 v1, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 507
    .end local v0    # "configurations":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    :cond_0
    :goto_3
    return-void

    .line 484
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/progress/WifiCheckTask;->connect_NoPwd_Network(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 485
    const-string v3, "wifi"

    const-string v4, "\u8fde\u63a5\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iput-boolean v5, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    goto :goto_1

    .line 489
    :cond_2
    const-string v3, "wifi"

    const-string v4, "\u8fde\u63a5\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 495
    .restart local v0    # "configurations":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {p0, v3}, Lcom/google/progress/WifiCheckTask;->connect_Configuration_Network(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 496
    const-string v3, "wifi"

    const-string v4, "\u8fde\u63a5\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iput-boolean v5, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    goto :goto_3

    .line 500
    :cond_4
    const-string v3, "wifi"

    const-string v4, "\u8fde\u63a5\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public readConfigFile()Ljava/lang/String;
    .locals 8

    .prologue
    .line 750
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 752
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    iget-object v5, p0, Lcom/google/progress/WifiCheckTask;->context:Landroid/content/Context;

    const-string v6, "ip_config"

    invoke-virtual {v5, v6}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 753
    .local v3, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 754
    .local v4, "len":I
    const/16 v5, 0x400

    new-array v1, v5, [B

    .line 755
    .local v1, "datas":[B
    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x400

    invoke-virtual {v3, v1, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 757
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 758
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 763
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    .end local v1    # "datas":[B
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    :goto_1
    return-object v5

    .line 756
    .restart local v1    # "datas":[B
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "len":I
    :cond_0
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v0, v1, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 759
    .end local v1    # "datas":[B
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    :catch_0
    move-exception v5

    move-object v2, v5

    .line 760
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "***"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    const-string v5, ""

    goto :goto_1
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 58
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 60
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->begin()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v3

    if-nez v3, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v3

    if-nez v3, :cond_6

    .line 94
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_1
    iget-boolean v3, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-eqz v3, :cond_4

    .line 112
    :cond_1
    :goto_2
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->begin()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 65
    :catch_1
    move-exception v2

    .line 67
    .local v2, "e2":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v3

    if-nez v3, :cond_3

    .line 68
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :goto_3
    iget-boolean v3, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v3, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 71
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iput-boolean v5, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 74
    sput-boolean v5, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_0

    .line 77
    :cond_2
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-wide/16 v3, 0x1388

    :try_start_2
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 82
    :catch_2
    move-exception v1

    .line 84
    .local v1, "e1":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 88
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :cond_3
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5df2\u7ecf\u5f00\u542fGPRS\u7f51\u8def"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "e2":Ljava/lang/Exception;
    :cond_4
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 97
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 98
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iput-boolean v5, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 100
    sput-boolean v5, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_1

    .line 102
    :cond_5
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 107
    :cond_6
    const-string v3, "wifi"

    const-string v4, "\u624b\u673a\u5df2\u7ecf\u5f00\u542fGPRS\u7f51\u8def"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public startCheckBeginWithGPRS()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 363
    const-string v0, "wifi"

    const-string v1, "\u5373\u5c06\u8fdb\u884c\u7f51\u7edc\u68c0\u67e5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709GPRS\u7f51\u7edc\u8fde\u63a5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    sget-boolean v0, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    if-nez v0, :cond_1

    .line 367
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u7684GPRS\u7f51\u7edc\u8fde\u63a5\u4e3a\u7528\u6237\u5f00\u542f\u7684"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u7684GPRS\u7f51\u7edc\u8fde\u63a5\u4e3a\u7a0b\u5e8f\u5f00\u542f\u7684"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    .line 372
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->wifiIsOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 373
    const-string v0, "wifi"

    const-string v1, "Wifi\u5df2\u7ecf\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    const-string v0, "wifi"

    const-string v1, "\u8fde\u63a5\u6210\u529f\uff0c\u5373\u5c06\u5173\u95edGPRS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    :cond_2
    const-string v0, "wifi"

    const-string v1, "Wifi\u672a\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->OpenWifi()V

    .line 381
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 382
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    const-string v0, "wifi"

    const-string v1, "\u8fde\u63a5\u6210\u529f\uff0c\u5373\u5c06\u5173\u95edGPRS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 385
    :cond_3
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string v0, "wifi"

    const-string v1, "\u8fde\u63a5\u6210\u529f\uff0c\u5373\u5c06\u5173\u95edGPRS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 394
    :cond_4
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 395
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709Wifi\u7f51\u7edc\u8fde\u63a5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_5
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u65e0\u7f51\u7edc\u8fde\u63a5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    .line 399
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->wifiIsOn()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 400
    const-string v0, "wifi"

    const-string v1, "Wifi\u5df2\u7ecf\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->goConnectNetwork()V

    .line 402
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    const-string v0, "wifi"

    const-string v1, "\u7a0b\u5e8f\u5373\u5c06\u5f00\u542fGPRS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    sput-boolean v3, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto/16 :goto_0

    .line 407
    :cond_6
    const-string v0, "wifi"

    const-string v1, "Wifi\u672a\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->OpenWifi()V

    .line 409
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 410
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 412
    :cond_7
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->goConnectNetwork()V

    .line 414
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    const-string v0, "wifi"

    const-string v1, "\u7a0b\u5e8f\u5373\u5c06\u5f00\u542fGPRS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    sput-boolean v3, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto/16 :goto_0
.end method

.method public startCheckBeginWithWifi()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 163
    const-string v0, "wifi"

    const-string v1, "\u5373\u5c06\u8fdb\u884c\u7f51\u7edc\u68c0\u67e5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709Wifi\u7f51\u7edc,\u5373\u5c06\u68c0\u67e5\u5f53\u524dWifi\u7f51\u7edc\u80fd\u5426\u8fde\u63a5\u4e2d\u8f6c"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u4e0d\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    .line 176
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_3

    .line 177
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_1
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 180
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 183
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_1

    .line 185
    :cond_2
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 192
    :cond_3
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709GPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 197
    :cond_4
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709Wifi\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->isOK:Z

    .line 199
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->wifiIsOn()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 200
    const-string v0, "wifi"

    const-string v1, "Wifi\u5df2\u7ecf\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->goConnectNetwork()V

    .line 202
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 203
    const-string v0, "wifi"

    const-string v1, "\u624b\u673aWifi\u8054\u7f51\u6210\u529f,\u5373\u5c06\u68c0\u67e5\u5f53\u524dWifi\u7f51\u7edc\u80fd\u5426\u8fde\u63a5\u4e2d\u8f6c"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 205
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 212
    :cond_5
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u4e0d\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    .line 214
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_7

    .line 215
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_2
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 218
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 219
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 221
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_2

    .line 223
    :cond_6
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 230
    :cond_7
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709GPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 240
    :cond_8
    const-string v0, "wifi"

    const-string v1, "\u624b\u673aWifi\u8054\u7f51\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_a

    .line 242
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :goto_3
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 245
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 246
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 248
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_3

    .line 250
    :cond_9
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 256
    :cond_a
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5df2\u7ecf\u5f00\u542fGPRS\u7f51\u8def"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 260
    :cond_b
    const-string v0, "wifi"

    const-string v1, "Wifi\u672a\u5f00\u542f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->OpenWifi()V

    .line 262
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 263
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u6210\u529f,\u5373\u5c06\u68c0\u67e5\u5f53\u524dWifi\u7f51\u7edc\u80fd\u5426\u8fde\u63a5\u4e2d\u8f6c"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 265
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 272
    :cond_c
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u4e0d\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    .line 274
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_e

    .line 275
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_4
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 278
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 279
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 281
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_4

    .line 283
    :cond_d
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 290
    :cond_e
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709GPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 300
    :cond_f
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u81ea\u52a8\u8fde\u63a5\u914d\u7f6e\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->goConnectNetwork()V

    .line 302
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 303
    const-string v0, "wifi"

    const-string v1, "\u624b\u673aWifi\u8054\u7f51\u6210\u529f,\u5373\u5c06\u68c0\u67e5\u5f53\u524dWifi\u7f51\u7edc\u80fd\u5426\u8fde\u63a5\u4e2d\u8f6c"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 305
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 312
    :cond_10
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u4e0d\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    .line 314
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_12

    .line 315
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :goto_5
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 318
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 319
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 321
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_5

    .line 323
    :cond_11
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 330
    :cond_12
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6709GPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 340
    :cond_13
    const-string v0, "wifi"

    const-string v1, "\u624b\u673aWifi\u8054\u7f51\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_15

    .line 342
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :goto_6
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 345
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 346
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 348
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_6

    .line 350
    :cond_14
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 355
    :cond_15
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5df2\u7ecf\u5f00\u542fGPRS\u7f51\u8def"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public test()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 125
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/progress/WifiCheckTask;->checkWifiCanOrNotConnectServer([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u53ef\u7528"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    sget-boolean v0, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5373\u5c06\u5173\u95ed\u7a0b\u5e8f\u5f00\u542f\u7684GPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 130
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    const-string v0, "wifi"

    const-string v1, "\u5f53\u524dWifi\u7f51\u7edc\u4e0d\u53ef\u7528,\u7ee7\u7eed\u641c\u7d22\u8fde\u63a5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->goConnectNetwork()V

    .line 136
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkWifiNetworkState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    const-string v0, "wifi"

    const-string v1, "\u641c\u7d22\u8fde\u63a5\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 139
    :cond_2
    const-string v0, "wifi"

    const-string v1, "\u641c\u7d22\u8fde\u63a5\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-nez v0, :cond_4

    .line 141
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u76ee\u524d\u6ca1\u6709\u7f51\u7edc,\u5373\u5c06\u5f00\u542fGPRS\u7f51\u7edc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :goto_1
    iget-boolean v0, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->toggleGPRS()V

    .line 144
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->checkGPRSNetworkState()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iput-boolean v2, p0, Lcom/google/progress/WifiCheckTask;->hasGprs:Z

    .line 147
    sput-boolean v2, Lcom/google/progress/WifiCheckTask;->isProgramGPRS_ON:Z

    goto :goto_1

    .line 149
    :cond_3
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5f00\u542fGPRS\u7f51\u7edc\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 154
    :cond_4
    const-string v0, "wifi"

    const-string v1, "\u624b\u673a\u5df2\u7ecf\u5f00\u542fGPRS\u7f51\u8def"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public toggleGPRS()V
    .locals 6

    .prologue
    .line 426
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->checkAPNisAvailable()Z

    move-result v3

    if-nez v3, :cond_6

    .line 427
    const-string v3, "apn"

    const-string v4, "\u624b\u673a\u5f53\u524dapn\u4e0d\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->deleteAPN()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 429
    const-string v3, "apn"

    const-string v4, "\u5220\u9664apn\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-virtual {p0}, Lcom/google/progress/WifiCheckTask;->getCardTypeNumber()Ljava/lang/String;

    move-result-object v2

    .line 431
    .local v2, "simTypeNumber":Ljava/lang/String;
    const-string v3, "apn"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u624b\u673aSIM\u5361\u7c7b\u578b\u53f7\u7801\u4e3a-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "02"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 433
    :cond_0
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u79fb\u52a8\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addMobileApnFirst()V

    .line 435
    const-string v3, "apn"

    const-string v4, "\u624b\u673aAPN\u914d\u7f6e\u5b8c\u6210,\u5373\u5c06\u68c0\u67e5APN\u662f\u5426\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->checkAPNisAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 437
    const-string v3, "apn"

    const-string v4, "\u914d\u7f6e\u7684APN\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    .end local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->openAPN()V

    .line 461
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 462
    .local v1, "gpsIntent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 463
    const-string v3, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    const-string v3, "custom:5"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 466
    :try_start_0
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->context:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V

    .line 467
    const-wide/16 v3, 0x2710

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    :goto_1
    return-void

    .line 439
    .end local v1    # "gpsIntent":Landroid/content/Intent;
    .restart local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_2
    const-string v3, "apn"

    const-string v4, "\u914d\u7f6e\u7684APN\u4e0d\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->deleteAPN()Z

    .line 441
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addMobileApnSecond()V

    goto :goto_0

    .line 444
    :cond_3
    const-string v3, "01"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 445
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u8054\u901a\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addUnicomApn()V

    goto :goto_0

    .line 448
    :cond_4
    const-string v3, "03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 449
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u7535\u4fe1\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v3, p0, Lcom/google/progress/WifiCheckTask;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addTelecommunicationApn()V

    goto :goto_0

    .line 455
    .end local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_5
    const-string v3, "apn"

    const-string v4, "\u5220\u9664apn\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 458
    :cond_6
    const-string v3, "apn"

    const-string v4, "\u624b\u673a\u5f53\u524dapn\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 468
    .restart local v1    # "gpsIntent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    move-object v0, v3

    .line 470
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception---toggleGPRS--->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public wifiIsOn()Z
    .locals 2

    .prologue
    .line 474
    iget-object v1, p0, Lcom/google/progress/WifiCheckTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    .line 475
    .local v0, "state":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 476
    const/4 v1, 0x1

    .line 478
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
