.class public Lcom/google/progress/AndroidSocketSR;
.super Ljava/lang/Object;
.source "AndroidSocketSR.java"


# static fields
.field static isTure:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/google/progress/AndroidSocketSR;->isTure:I

    .line 16
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public RevCmd(Ljava/net/Socket;)Ljava/lang/String;
    .locals 7
    .param p1, "client"    # Ljava/net/Socket;

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 54
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 56
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 57
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 58
    .local v1, "in":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "str":Ljava/lang/String;
    const-string v4, "send_or_recv"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u7a0b\u5e8f\u8c03\u7528RevCmd\u63a5\u6536\u5230\u6570\u636e------->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v4, v3

    .line 80
    .end local v1    # "in":Ljava/io/BufferedReader;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "str":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 70
    :catch_0
    move-exception v4

    move-object v0, v4

    .line 72
    .local v0, "e":Ljava/net/SocketTimeoutException;
    const-string v4, ""

    goto :goto_0

    .line 74
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v4

    move-object v0, v4

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "url"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " \u8fd4\u56deER"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-string v4, "ER"

    goto :goto_0

    .line 80
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v4, "ER"

    goto :goto_0
.end method

.method public SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z
    .locals 8
    .param p1, "client"    # Ljava/net/Socket;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 21
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "str---->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v3}, Ljava/net/Socket;->sendUrgentData(I)V

    .line 25
    :goto_0
    sget v3, Lcom/google/progress/AndroidSocketSR;->isTure:I

    if-nez v3, :cond_0

    .line 26
    const/4 v3, 0x1

    sput v3, Lcom/google/progress/AndroidSocketSR;->isTure:I

    .line 27
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 29
    .local v0, "buf":[B
    const-string v3, "ta"

    const-string v4, "first"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 31
    .local v2, "os":Ljava/io/OutputStream;
    const-string v3, "ta"

    const-string v4, "second"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const/4 v3, 0x0

    array-length v4, v0

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 35
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 36
    const-wide/16 v3, 0x1388

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 37
    const-string v3, "send_or_recv"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u7a0b\u5e8f\u8c03\u7528SendCmd\u53d1\u9001\u6570\u636e--------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v3, 0x0

    sput v3, Lcom/google/progress/AndroidSocketSR;->isTure:I

    move v3, v7

    .line 45
    .end local v0    # "buf":[B
    .end local v2    # "os":Ljava/io/OutputStream;
    :goto_1
    return v3

    .line 25
    :cond_0
    const-wide/16 v3, 0x64

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41
    :catch_0
    move-exception v3

    move-object v1, v3

    .line 42
    .local v1, "ex":Ljava/lang/Exception;
    const-string v3, "ta"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ex--->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    sput v6, Lcom/google/progress/AndroidSocketSR;->isTure:I

    move v3, v6

    .line 45
    goto :goto_1
.end method
