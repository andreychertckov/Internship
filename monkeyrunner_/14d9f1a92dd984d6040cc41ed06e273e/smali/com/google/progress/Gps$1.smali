.class Lcom/google/progress/Gps$1;
.super Ljava/lang/Object;
.source "Gps.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/Gps;->initLocationListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/Gps;


# direct methods
.method constructor <init>(Lcom/google/progress/Gps;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/Gps$1;->this$0:Lcom/google/progress/Gps;

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/google/progress/Gps$1;)Lcom/google/progress/Gps;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/progress/Gps$1;->this$0:Lcom/google/progress/Gps;

    return-object v0
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 72
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/Gps$1$1;

    invoke-direct {v1, p0, p1}, Lcom/google/progress/Gps$1$1;-><init>(Lcom/google/progress/Gps$1;Landroid/location/Location;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 79
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 81
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 85
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 87
    return-void
.end method
