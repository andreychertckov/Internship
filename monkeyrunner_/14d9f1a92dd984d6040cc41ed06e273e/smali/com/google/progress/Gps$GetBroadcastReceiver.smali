.class public Lcom/google/progress/Gps$GetBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Gps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/progress/Gps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GetBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/progress/Gps;


# direct methods
.method public constructor <init>(Lcom/google/progress/Gps;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/progress/Gps$GetBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 192
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\u63a5\u6536\u5230\u5b9a\u4f4d\u5e7f\u64ad"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.progress.get"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "isOne"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    sget-boolean v0, Lcom/google/progress/Gps;->isGps:Z

    if-nez v0, :cond_0

    .line 196
    sput-boolean v2, Lcom/google/progress/Gps;->isGps:Z

    .line 197
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\u5f00\u59cbGPS\u5b9a\u4f4d"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/google/progress/Gps$GetBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-virtual {v0}, Lcom/google/progress/Gps;->getLocationInfo()V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const-string v0, "isOne"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    sget-boolean v0, Lcom/google/progress/Gps;->isGps:Z

    if-nez v0, :cond_0

    .line 202
    sput-boolean v2, Lcom/google/progress/Gps;->isGps:Z

    .line 203
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\u5f00\u59cbGPS\u5b9a\u4f4d"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/google/progress/Gps$GetBroadcastReceiver;->this$0:Lcom/google/progress/Gps;

    invoke-virtual {v0}, Lcom/google/progress/Gps;->getLocationInfo()V

    goto :goto_0
.end method
